# Brissmart Android

[![mobileBRIS logo](/MobileBris/app/src/main/res/mipmap-xxhdpi/app_ico.png)](http://git.brisyariah.co.id/valeputra/mobilebris-android)

Brissmart Android is developed for PT Bank BRI Syariah

## Installation

### Requirement
- AndroidStudio 2.x.x +
- JDK 8 +

### Init From Git
```sh
$ cd Brissmart
$ git init
$ git remote add origin http://git.brisyariah.co.id/echannel/Brissmart-mobile.git
$ git fetch
$ git pull -u origin master
```

License
----
ITG BRISyariah > echannel