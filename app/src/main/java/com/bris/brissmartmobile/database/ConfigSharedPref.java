package com.bris.brissmartmobile.database;

import android.content.Context;
import android.content.SharedPreferences;
import com.bris.brissmartmobile.model.LocationInfo;
import com.google.gson.Gson;


/**
 * Created by ryputra on 13/10/2017.
 */

public class ConfigSharedPref {
    public static final String MAIN_CONFIG = "mobileBrisPref";
    public static final String APP_VERSION = "app_version",
            QUOTES_VERSION = "qoutes_version",
            LAST_TRANSACTION = "last_trx",
            QIBLAT_DEGREE = "qiblat_degree",
            USER_LOCATION_INFO = "user_loc_info",
            NOTIFICATION = "notif_",
            USERINFO = "user_info_";

    public ConfigSharedPref() {}

    public static void setAppVersion(Context context, String version) {
        SharedPreferences.Editor editor = context.getSharedPreferences(
                MAIN_CONFIG, Context.MODE_PRIVATE).edit();
        editor.putString(APP_VERSION, version);
        editor.commit();
    }

    public static String getAppVersion(Context context) {
        SharedPreferences sharedPref = context.getSharedPreferences
                (MAIN_CONFIG, Context.MODE_PRIVATE);
        return sharedPref.getString(APP_VERSION, "");
    }

    public static void setUserName(Context context, String uname) {
        SharedPreferences.Editor editor = context.getSharedPreferences(
                MAIN_CONFIG, Context.MODE_PRIVATE).edit();
        editor.putString(USERINFO + "name", uname);
        editor.commit();
    }

    public static String getUserName(Context context) {
        SharedPreferences sharedPref = context.getSharedPreferences
                (MAIN_CONFIG, Context.MODE_PRIVATE);
        return sharedPref.getString(USERINFO + "name", "");
    }

    public static void setQuotesVersion(Context context, String version) {
        SharedPreferences.Editor editor = context.getSharedPreferences
                (MAIN_CONFIG, Context.MODE_PRIVATE).edit();
        editor.putString(QUOTES_VERSION, version);
        editor.commit();
    }

    public static String getQuotesVersion(Context context) {
        SharedPreferences sharedPref = context.getSharedPreferences
                (MAIN_CONFIG, Context.MODE_PRIVATE);
        return sharedPref.getString(QUOTES_VERSION, "");
    }

    public static void setLastTrx(Context context, String times) {
        SharedPreferences.Editor editor = context.getSharedPreferences
                (MAIN_CONFIG, Context.MODE_PRIVATE).edit();
        editor.putString(LAST_TRANSACTION, times);
        editor.commit();
    }

    public static String getLastTrx(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences
                (MAIN_CONFIG, Context.MODE_PRIVATE);
        return sharedPreferences.getString(LAST_TRANSACTION, "");
    }

    public static void setQiblatDegree(Context context, float degree) {
        SharedPreferences.Editor editor = context.getSharedPreferences
                (MAIN_CONFIG, Context.MODE_PRIVATE).edit();
        editor.putFloat(QIBLAT_DEGREE, degree);
        editor.commit();
    }

    public static float getQiblaDegree(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences
                (MAIN_CONFIG, Context.MODE_PRIVATE);
        return sharedPreferences.getFloat(QIBLAT_DEGREE, -1);
    }

    public static void setLocationConfig(Context context, LocationInfo locationConfig) {
        SharedPreferences.Editor editor = context.getSharedPreferences
                (MAIN_CONFIG, Context.MODE_PRIVATE).edit();
        Gson gson = new Gson();
        String json = gson.toJson(locationConfig);
        editor.putString(USER_LOCATION_INFO, json);
        editor.commit();
    }

    public static LocationInfo getLocationConfig(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences
                (MAIN_CONFIG, Context.MODE_PRIVATE);
        Gson gson = new Gson();
        String json = sharedPreferences.getString(USER_LOCATION_INFO, "");
        LocationInfo locationInfo = gson.fromJson(json, LocationInfo.class);
        return locationInfo;
    }

    public static void clearAll(Context context) {
        SharedPreferences pref = context.getSharedPreferences(MAIN_CONFIG, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.clear();
        editor.commit();
    }

    public static void disableNotif(Context context, String notifId) {
        SharedPreferences.Editor editor = context.getSharedPreferences
                (MAIN_CONFIG, Context.MODE_PRIVATE).edit();
        editor.putBoolean(NOTIFICATION + notifId, false);
        editor.commit();
    }

    public static boolean isNotifEnabled(Context context, String notifId) {
        SharedPreferences sharedPref = context.getSharedPreferences(MAIN_CONFIG, Context.MODE_PRIVATE);
        return sharedPref.getBoolean(NOTIFICATION + notifId, true);
    }

    public static void removeByKey(Context context, String key) {
        SharedPreferences preferences = context.getSharedPreferences(MAIN_CONFIG, 0);
        preferences.edit().remove(key).commit();
    }
}
