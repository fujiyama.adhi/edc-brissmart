package com.bris.brissmartmobile.fragment;

import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.bris.brissmartmobile.R;
import com.bris.brissmartmobile.listener.FavClickListener;
import com.bris.brissmartmobile.model.Favorite;
import com.bris.brissmartmobile.util.AppPreferences;
import com.bris.brissmartmobile.util.AppUtil;
import com.bris.brissmartmobile.util.AppUtilPrint;
import com.bris.brissmartmobile.util.NumberTextWatcherForThousand;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by valeputra on 6/22/17.
 */

public class FTransferRekeningBrisTunai extends Fragment implements FavClickListener, DialogInterface.OnKeyListener {

    private EditText et1, et2, et3;
    private TextInputLayout til1, til2, til3;
    private Button btn1, btnConfirm;
    private ImageButton btnFav;
    private List<Favorite> favDatas = new ArrayList<>();
    String param2, param3, rek_tujuan, amount;
    String pageId, tid;
    JSONObject json, resdata;
    String responseJSON, msgErr, rcode, name, cmsg;
    private Realm realm;
    private AlertDialog AlertDialog = null,
            confirmationDialog = null;
    private View view;

    private AppPreferences appPref;

    public FTransferRekeningBrisTunai() {}

    public View onCreateView(final LayoutInflater inflater, @Nullable final ViewGroup container, @Nullable Bundle savedInstanceState) {
        pageId = getArguments().getString("menu_id");

        view = inflater.inflate(R.layout.activity_transfer_bris_tunai, container, false);
        AppUtil.toolbarRegular(getContext(), view, pageId.replace("Tunai", ""));

        appPref = new AppPreferences(getContext());
        tid = appPref.getDeviceTid();
        Log.d("TID TRANSFER", tid);

        realm = Realm.getDefaultInstance();

        til1 = (TextInputLayout) view.findViewById(R.id.param1Til);
        et1 = (EditText) view.findViewById(R.id.param1Txt);

        til1.setVisibility(View.GONE);
        et1.setVisibility(View.GONE);

        til2 = (TextInputLayout) view.findViewById(R.id.param2Til);
        et2 = (EditText) view.findViewById(R.id.param2Txt);
        til2.setHint("Nomor Rekening BRI Syariah Tujuan");
        til3 = (TextInputLayout) view.findViewById(R.id.param3Til);
        et3 = (EditText) view.findViewById(R.id.param3Txt);
        et3.addTextChangedListener(new NumberTextWatcherForThousand(et3));
        btn1 = (Button) view.findViewById(R.id.btn_submit);
        btnFav = (ImageButton) view.findViewById(R.id.btn_daftar_fav);

        et2.setFocusable(false);
        et2.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                et2.setFocusableInTouchMode(true);

                return false;
            }
        });

        et3.setFocusable(false);
        et3.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                et3.setFocusableInTouchMode(true);

                return false;
            }
        });

        try {
            onActionButton();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return view;
    }

    private void onActionButton() {
        btnFav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    param2 = et2.getText().toString().trim();

                    if (param2.equalsIgnoreCase("")) {
                        getFavData();
                        AppUtil.displayDialogFavorit(getContext(), favDatas, FTransferRekeningBrisTunai.this, "FAVTRANS");
                        return;
                    }

                    AlertDialog dialogAlert;
                    AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                    builder.setCancelable(true);
                    builder.setMessage("Simpan data transaksi ini sebagai data favorit?");
                    builder.setPositiveButton("Simpan", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface alertdialog, int which) {
                            alertdialog.cancel();

                            if (AppUtil.checkFavorite("CASH", param2, "Rekening BRI Syariah", AppUtil.getDsn())) {
                                FragmentManager manager = getActivity().getSupportFragmentManager();
                                Fragment fragment;
                                FragmentTransaction transaction = manager.beginTransaction();
                                fragment = new FInsertUpdateDataFavorit();
                                Bundle bundle = new Bundle();
                                bundle.putString("title", pageId);
                                bundle.putString("jenispembayaran", "CASH");
                                bundle.putString("data1", "Rekening BRI Syariah");
                                bundle.putString("data3", param2);
                                bundle.putString("menutrx", "8000");
                                bundle.putString("submenutrx", "8002");
                                bundle.putString("namajenisfav", "Transfer Rekening BRI Syariah");

                                fragment.setArguments(bundle);
                                transaction.replace(R.id.fragment_payment_general, fragment);
                                transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
                                transaction.addToBackStack(null);
                                transaction.commit();

                            } else {
                                AppUtil.displayDialog(getContext(), "Data favorit Telah Ada");
                            }
                        }
                    });
                    builder.setNegativeButton("Pilih favorit", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface alertdialog, int which) {
                            alertdialog.cancel();
                            getFavData();
                            AppUtil.displayDialogFavorit(getContext(), favDatas, FTransferRekeningBrisTunai.this, "FAVTRANS");
                        }
                    });

                    dialogAlert = builder.create();
                    dialogAlert.show();
                } catch (Exception e) {
                    e.printStackTrace();
                    AppUtil.displayDialog(getContext(), "Maaf, Terjadi Kesalahan");
                }
            }
        });

        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(et2.getText().toString().length() == 0){
                    til2.setError(til2.getHint() + " Tidak Boleh Kosong!");
                    return;
                } else {
                    param2 = et2.getText().toString();
                }
                if(et3.getText().toString().length() == 0){
                    til3.setError(til3.getHint() + " Tidak Boleh Kosong!");
                    return;
                } else {
                    param3 = NumberTextWatcherForThousand.trimCommaOfString(et3.getText().toString().trim());
                }

                SoapActivity action = new SoapActivity();
                action.execute();

            }
        });
    }

    @Override
    public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
        if(keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount()==0){
            AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
            builder.setCancelable(false);
            builder.setMessage("Apakah anda yakin akan membatalkan proses " + pageId + "?");
            builder.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                    confirmationDialog.dismiss();
                }
            });
            builder.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                    confirmationDialog.show();
                }
            });
            if(AlertDialog == null || !AlertDialog.isShowing()) {
                AlertDialog = builder.create();
                AlertDialog.show();
            }
            return true;
        }
        return false;
    }

    @Override
    public void getFav(String label, String category) {

    }

    @Override
    public void onclikFav(String label, String kode_produk) {
        RealmResults<Favorite> fav = realm.where(Favorite.class)
                .equalTo("namafav", label)
                .equalTo("data3", kode_produk)
                .findAll();

        if (fav.size() > 0) {
            et2.setText(fav.get(0).getData3());
            AppUtil.closeListDialog();
        }
    }

    @Override
    public void deleteFav(String jenispembayaran, String menutrx, String submenutrx, String label) {

    }

    public void getFavData() {
        RealmResults<Favorite> fav = realm.where(Favorite.class)
                .equalTo("menutrx", "8000")
                .equalTo("submenutrx", "8002")
                .equalTo("jenispembayaran", "CASH")
                .findAll();

        RealmResults<Favorite> favorit = realm.where(Favorite.class).findAll();
        Log.d("Cek fav FTransRekCerdas", fav.toString());
        Log.d("Cek favorit", favorit.toString());

        if (fav.size() > 0) {
            favDatas.clear();
            favDatas.addAll(fav);
        } else {
            favDatas.clear();
        }
    }

    private class SoapActivity extends AsyncTask<Void, Void, Void> {


        @Override
        protected void onPreExecute() {

        }

        @Override
        protected Void doInBackground(Void... params) {
            convertir();
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            if (msgErr.equalsIgnoreCase("true")) {
                AppUtil.displayDialog(getContext(), responseJSON);
            }
        }

    }

    private void convertir() {

        try {
            getActivity().runOnUiThread(new Runnable() {
                public void run() {
                    AppUtil.initDialogProgress(getContext());
                    AppUtil.showDialog();
                }
            });

            JSONObject cmdparam = new JSONObject();
            cmdparam.put("accno", param2);
            cmdparam.put("amount", param3);
            String soapRes = AppUtil.soapReq(AppUtil.getDsn(), "depositBrisCash_verify", cmdparam);

            if (soapRes.equalsIgnoreCase("")) {
                AppUtil.hideDialog();
                msgErr = "true";
                responseJSON = "Gagal terhubung ke server";
            } else {
                json = new JSONObject(soapRes);
                resdata = json.getJSONObject("resdata");
                rcode = resdata.getString("rcode");
                msgErr = "false";

                if (rcode.equalsIgnoreCase("00")) {
                    String tcode = resdata.getString("tcode");
                    if (tcode.equalsIgnoreCase("00")) {
                        msgErr = "false";

                        new Thread() {
                            public void run() {
                                FTransferRekeningBrisTunai.this.getActivity().runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        AppUtil.hideDialog();
                                        try {
                                            LayoutInflater li = LayoutInflater.from(getContext());
                                            final View confirmDialog = li.inflate(R.layout.dialog_confirm_agen, null);
                                            btnConfirm = (Button) confirmDialog.findViewById(R.id.btn_submit);
                                            final TextView confirm = (TextView) confirmDialog.findViewById(R.id.txt_title_top);
                                            final EditText et4 = (EditText) confirmDialog.findViewById(R.id.txt_pin);

                                            confirm.setText("Anda akan melakukan Transfer ke BRI Syariah Rek " + param2 + " sebesar Rp. " + et3.getText().toString() + ". Jika benar masukkan PIN Anda");
                                            et4.addTextChangedListener(new TextWatcher() {
                                                @Override
                                                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                                                }

                                                @Override
                                                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                                                    if (charSequence.toString().trim().length() == 6) {
                                                        btnConfirm.setEnabled(true);
                                                        btnConfirm.setBackgroundResource(R.drawable.button_shape_blue);
                                                    } else {
                                                        btnConfirm.setEnabled(false);
                                                        btnConfirm.setBackgroundResource(R.drawable.button_shape_accent);
                                                    }
                                                }

                                                @Override
                                                public void afterTextChanged(Editable editable) {
                                                }
                                            });

                                            AlertDialog.Builder alert = new AlertDialog.Builder(getContext());
                                            alert.setView(confirmDialog);
                                            alert.setCancelable(false);
                                            confirmationDialog = alert.create();
                                            confirmationDialog.setOnKeyListener(FTransferRekeningBrisTunai.this);
                                            confirmationDialog.show();

                                            btnConfirm.setOnClickListener(new View.OnClickListener() {
                                                @Override
                                                public void onClick(View view) {

                                                    AppUtil.initDialogProgress(getContext());
                                                    AppUtil.showDialog();

                                                    Thread thread = new Thread(new Runnable() {

                                                        @Override
                                                        public void run() {
                                                            try {
                                                                JSONObject cmdparam = new JSONObject();
                                                                cmdparam.put("accno", param2);
                                                                cmdparam.put("amount", param3);
                                                                cmdparam.put("vpass", et4.getText().toString());
                                                                String soapRes = AppUtil.soapReq(AppUtil.getDsn(), "depositBrisCash_save", cmdparam);

                                                                if (soapRes.equalsIgnoreCase("")) {
                                                                    AppUtil.hideDialog();
                                                                    msgErr = "true";
                                                                    responseJSON = "Gagal terhubung ke server";
                                                                } else {
                                                                    json = new JSONObject(soapRes);
                                                                    resdata = json.getJSONObject("resdata");
                                                                    rcode = resdata.getString("rcode");
                                                                    cmsg = resdata.getString("cmsg");

                                                                    //khusus edc brissmart
                                                                    String date = AppUtil.getDateFormat();
//
                                                                    Bundle datas = new Bundle();
                                                                    datas.putString("menu_id", pageId.replace("Tunai", ""));
                                                                    datas.putString("tid", tid);
                                                                    datas.putString("mid", "4228267000001");
                                                                    datas.putString("type_card", "-");
                                                                    datas.putString("date", date);
                                                                    datas.putString("msg", cmsg);

                                                                    //khusus edc brissmart

                                                                    if (rcode.equalsIgnoreCase("00")) {
                                                                        AppUtil.hideDialog();
                                                                        getActivity().runOnUiThread(new Runnable() {
                                                                            public void run() {
                                                                                AppUtilPrint.printStruk(getContext(), datas);

                                                                                et2.getText().clear();
                                                                                et3.getText().clear();

                                                                                et2.setFocusable(false);
                                                                                et2.setOnTouchListener(new View.OnTouchListener() {
                                                                                    @Override
                                                                                    public boolean onTouch(View v, MotionEvent event) {

                                                                                        et2.setFocusableInTouchMode(true);

                                                                                        return false;
                                                                                    }
                                                                                });
                                                                                et3.setFocusable(false);
                                                                                et3.setOnTouchListener(new View.OnTouchListener() {
                                                                                    @Override
                                                                                    public boolean onTouch(View v, MotionEvent event) {

                                                                                        et3.setFocusableInTouchMode(true);

                                                                                        return false;
                                                                                    }
                                                                                });
                                                                                AppUtil.displayDialog(getContext(), cmsg);
                                                                                AppUtil.saveIntoInbox(cmsg, pageId, AppUtil.getDsn());
                                                                                confirmationDialog.dismiss();
                                                                            }
                                                                        });
                                                                    } else if (rcode.equalsIgnoreCase("13")) {
                                                                        AppUtil.hideDialog();
                                                                        getActivity().runOnUiThread(new Runnable() {
                                                                            public void run() {
                                                                                AppUtil.displayDialog(getContext(), cmsg);
                                                                                et4.getText().clear();
                                                                            }
                                                                        });
                                                                    } else {
                                                                        AppUtil.hideDialog();
                                                                        cmsg = resdata.getString("cmsg");
                                                                        getActivity().runOnUiThread(new Runnable() {
                                                                            public void run() {
                                                                                AppUtil.displayDialog(getContext(), cmsg);
                                                                                confirmationDialog.dismiss();
                                                                            }
                                                                        });
                                                                    }
                                                                }
                                                            } catch (Exception ex) {
                                                                msgErr = "true";
                                                                AppUtil.hideDialog();
                                                                Log.d("THREAD", ex.toString());
                                                                responseJSON = "Transaksi gagal";
                                                            }
                                                        }
                                                    });
                                                    thread.start();
                                                }
                                            });

                                        } catch (Exception e) {
                                            Log.d("THREAD 1", e.toString());
                                            msgErr = "true";
                                            AppUtil.hideDialog();
                                            responseJSON = "Transaksi gagal";
                                        }
                                    }
                                });
                            }
                        }.start();
                    } else {
                        AppUtil.hideDialog();
                        cmsg = resdata.getString("cmsg");
                        responseJSON = cmsg;
                        msgErr = "true";
                    }
                } else {
                    Log.d("else", "gagal");
                    AppUtil.hideDialog();
                    cmsg = resdata.getString("cmsg");
                    responseJSON = cmsg;
                    msgErr = "true";
                }
            }
        } catch (Exception ex) {
            msgErr = "true";
            AppUtil.hideDialog();
            Log.d("THREAD 2", ex.toString());
            responseJSON = "Transaksi gagal";
        }
    }
}

