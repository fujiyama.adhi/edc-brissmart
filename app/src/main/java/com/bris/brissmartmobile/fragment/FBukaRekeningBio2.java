package com.bris.brissmartmobile.fragment;

import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Spinner;

import com.bris.brissmartmobile.R;
import com.bris.brissmartmobile.util.AppUtil;
import com.bris.brissmartmobile.util.FormValidationUtil;
import com.bris.brissmartmobile.util.Menu;
import com.bris.brissmartmobile.util.NothingSelectedSpinnerAdapter;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.Arrays;


/**
 * Create by valeputra
 * Credits : akbaranjas
 */

public class FBukaRekeningBio2 extends Fragment {
    private View view;
    private EditText et_alamat, et_rt, et_rw, et_kode_pos, et_alamat_domisili, et_rt_domisili, et_rw_domisili, et_kode_pos_domisili;
    private TextInputLayout til_alamat, til_rt, til_rw, til_provinsi, til_kota, til_kecamatan, til_kelurahan, til_kode_pos, til_alamat_domisili, til_rt_domisili, til_rw_domisili, til_provinsi_domisili, til_kota_domisili, til_kecamatan_domisili, til_kelurahan_domisili, til_kode_pos_domisili;
    private AutoCompleteTextView act_provinsi, act_kota, act_kecamatan, act_kelurahan, act_provinsi_domisili, act_kota_domisili, act_kecamatan_domisili, act_kelurahan_domisili;
    private String idtype, idcard, name, jenis_kelamin, tempat_lahir, tgl_lahir, nama_ibu;
    private String alamat, rt, rw, propinsi, kota, kecamatan, kelurahan, jenis_penduduk,
            alamat_domisili, rt_domisili, rw_domisili, propinsi_domisili, kota_domisili, kecamatan_domisili, kelurahan_domisili, kodepos_domisili;
    private Spinner spinner_penduduk;
    private CheckBox cb_alamat_domisili;
    private Button btn1;

    private String jsonData, page_id;
    private JSONObject json;
    private JSONArray data;
    private String[] propinsi_id, propinsi_nama, propinsi_syiarnoprop,
            kota_id, kota_nama, kota_syiarnokota,
            kecamatan_id, kecamatan_nama, kecamatan_syiarnokecamatan,
            kelurahan_id, kelurahan_nama, kelurahan_syiarnokelurahan, kode_pos;
    private String[] propinsi_domisili_id, propinsi_domisili_nama, propinsi_domisili_syiarnoprop,
            kota_domisili_id, kota_domisili_nama, kota_domisili_syiarnokota,
            kecamatan_domisili_id, kecamatan_domisili_nama, kecamatan_domisili_syiarnokecamatan,
            kelurahan_domisili_id, kelurahan_domisili_nama, kelurahan_domisili_syiarnokelurahan, kode_pos_domisili;
    private String syiar_noprop, syiar_nokota, syiar_nokecamatan, syiar_nokelurahan, kodepos;
    private String syiar_noprop_domisili, syiar_nokota_domisili, syiar_nokecamatan_domisili, syiar_nokelurahan_domisili;

    public FBukaRekeningBio2() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.activity_buka_rekening_bio_2, container, false);

        idtype = getArguments().getString("idtype");
        idcard = getArguments().getString("idcard");
        name = getArguments().getString("name");
        jenis_kelamin = getArguments().getString("jenis_kelamin");
        tempat_lahir = getArguments().getString("tempat_lahir");
        tgl_lahir = getArguments().getString("tgl_lahir");
        nama_ibu = getArguments().getString("nama_ibu");
        page_id = getArguments().getString(Menu.MENU_ID);

        String[] arrayPenduduk = new String[]{
                "Penduduk", "Bukan Penduduk"
        };

        til_alamat = (TextInputLayout) view.findViewById(R.id.til_alamat);
        til_rt = (TextInputLayout) view.findViewById(R.id.til_rt);
        til_rw = (TextInputLayout) view.findViewById(R.id.til_rw);
        til_provinsi = (TextInputLayout) view.findViewById(R.id.til_provinsi);
        til_kota = (TextInputLayout) view.findViewById(R.id.til_kota);
        til_kecamatan = (TextInputLayout) view.findViewById(R.id.til_kecamatan);
        til_kelurahan = (TextInputLayout) view.findViewById(R.id.til_kelurahan);
        til_kode_pos = (TextInputLayout) view.findViewById(R.id.til_kode_pos);

        til_alamat_domisili = (TextInputLayout) view.findViewById(R.id.til_alamat_domisili);
        til_rt_domisili = (TextInputLayout) view.findViewById(R.id.til_rt_domisili);
        til_rw_domisili = (TextInputLayout) view.findViewById(R.id.til_rw_domisili);
        til_provinsi_domisili = (TextInputLayout) view.findViewById(R.id.til_provinsi_domisili);
        til_kota_domisili = (TextInputLayout) view.findViewById(R.id.til_kota_domisili);
        til_kecamatan_domisili = (TextInputLayout) view.findViewById(R.id.til_kecamatan_domisili);
        til_kelurahan_domisili = (TextInputLayout) view.findViewById(R.id.til_kelurahan_domisili);
        til_kode_pos_domisili = (TextInputLayout) view.findViewById(R.id.til_kode_pos_domisili);

        et_alamat = (EditText) view.findViewById(R.id.et_alamat);
        et_rt = (EditText) view.findViewById(R.id.et_rt);
        et_rw = (EditText) view.findViewById(R.id.et_rw);
        et_kode_pos = (EditText) view.findViewById(R.id.et_kode_pos);

        et_alamat_domisili = (EditText) view.findViewById(R.id.et_alamat_domisili);
        et_rt_domisili = (EditText) view.findViewById(R.id.et_rt_domisili);
        et_rw_domisili = (EditText) view.findViewById(R.id.et_rw_domisili);
        et_kode_pos_domisili = (EditText) view.findViewById(R.id.et_kode_pos_domisili);

        act_provinsi = (AutoCompleteTextView) view.findViewById(R.id.act_provinsi);
        act_kota = (AutoCompleteTextView) view.findViewById(R.id.act_kota);
        act_kecamatan = (AutoCompleteTextView) view.findViewById(R.id.act_kecamatan);
        act_kelurahan = (AutoCompleteTextView) view.findViewById(R.id.act_kelurahan);

        act_provinsi_domisili = (AutoCompleteTextView) view.findViewById(R.id.act_provinsi_domisili);
        act_kota_domisili = (AutoCompleteTextView) view.findViewById(R.id.act_kota_domisili);
        act_kecamatan_domisili = (AutoCompleteTextView) view.findViewById(R.id.act_kecamatan_domisili);
        act_kelurahan_domisili = (AutoCompleteTextView) view.findViewById(R.id.act_kelurahan_domisili);

//        jsonData = AppUtil.setAutoCompletePropinsi();
//        Log.d("Cek jsonData", jsonData);

        setAutoCompletePropinsi();

        spinner_penduduk = (Spinner) view.findViewById(R.id.spinner_penduduk);
        ArrayAdapter<String> adapterJenisKelamin = new ArrayAdapter<String>(getContext(),
                android.R.layout.simple_spinner_item, arrayPenduduk);
        adapterJenisKelamin.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_penduduk.setAdapter(new NothingSelectedSpinnerAdapter(
                adapterJenisKelamin,
                R.layout.spinner_penduduk_nothing_selected,
                getContext()
        ));

        cb_alamat_domisili = (CheckBox) view.findViewById(R.id.cb_alamat_domisili);

        btn1 = (Button) view.findViewById(R.id.btn_submit);

        try {
            onActionButton();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return view;
    }

    private void setAutoCompletePropinsi() {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    jsonData = AppUtil.setAutoCompletePropinsi();
                    Log.d("Cek jsonData", jsonData);
                    Log.d("data", jsonData);
                    json = new JSONObject(jsonData);
                    Log.d("json", json.toString());
                    data = json.getJSONArray("data");
                    Log.d("array", data.toString());

                    propinsi_id = new String[data.length()];
                    propinsi_nama = new String[data.length()];
                    propinsi_syiarnoprop = new String[data.length()];
                    for (int i = 0; i < data.length(); i++) {
                        JSONObject jo = data.getJSONObject(i);
                        propinsi_id[i] = jo.getString("id");
                        propinsi_nama[i] = jo.getString("nama");
                        propinsi_syiarnoprop[i] = jo.getString("syiar_no_prop");
                    }

                    act_provinsi.setThreshold(1);

                    getActivity().runOnUiThread(new Runnable() {
                        public void run() {
                            ArrayAdapter<String> propListAdapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_list_item_1,
                                    propinsi_nama);
                            propListAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            act_provinsi.setAdapter(propListAdapter);
                        }
                    });

                    act_provinsi.setOnTouchListener(new View.OnTouchListener() {
                        @Override
                        public boolean onTouch(View v, MotionEvent event) {
                            act_provinsi.setFocusableInTouchMode(true);
                            act_provinsi.showDropDown();
                            act_provinsi.requestFocus();
                            return false;
                        }
                    });

                    act_provinsi.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                        @Override
                        public void onFocusChange(View v, boolean hasFocus) {
                            String val = act_provinsi.getText().toString() + "";
                            Boolean code = Arrays.asList(propinsi_nama).contains(val);
                            if (!hasFocus)
                                if (!code) {
                                    til_provinsi.setErrorEnabled(true);
                                    til_provinsi.setError("Nama Propinsi tidak valid");
                                } else {
                                    til_provinsi.setErrorEnabled(false);
                                    til_provinsi.setError(null);
                                }
                        }
                    });

                    act_provinsi.addTextChangedListener((TextWatcher) getContext());

                    act_provinsi.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View arg1, int position, long id) {
                            String prop = act_provinsi.getText().toString();
                            int posisi = 0;
                            for (int i = 0; i < propinsi_nama.length; i++) {
                                if (propinsi_nama[i].equalsIgnoreCase(prop)) {
                                    Log.d("Test getPropinsi", String.valueOf(i));
                                    posisi = i;
                                }
                            }
                            String id_propinsi = propinsi_id[posisi];
                            setAutoCompleteKota(id_propinsi);

                            syiar_noprop = propinsi_syiarnoprop[posisi];
                        }
                    });
                } catch (Exception e) {
                    Log.d("Catch prop", e.toString());
                }
            }
        });
        thread.start();
    }

    private void setAutoCompleteKota(final String id_propinsi) {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    jsonData = AppUtil.setAutoCompleteKota(id_propinsi);
                    Log.d("data", jsonData);
                    json = new JSONObject(jsonData);
                    Log.d("json", json.toString());
                    data = json.getJSONArray("data");
                    Log.d("array", data.toString());

                    kota_id = new String[data.length()];
                    kota_nama = new String[data.length()];
                    kota_syiarnokota = new String[data.length()];
                    for (int i = 0; i < data.length(); i++) {
                        JSONObject jo = data.getJSONObject(i);
                        kota_id[i] = jo.getString("no_kab");
                        kota_nama[i] = jo.getString("nama");
                        kota_syiarnokota[i] = jo.getString("syiar_no_kab");
                        Log.d("Test kota", kota_nama.toString());
                    }

                    act_kota.setThreshold(1);

                    getActivity().runOnUiThread(new Runnable() {
                        public void run() {
                            ArrayAdapter<String> kotaListAdapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_list_item_1,
                                    kota_nama);
                            kotaListAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            act_kota.setAdapter(kotaListAdapter);
                        }
                    });

                    act_kota.setOnTouchListener(new View.OnTouchListener() {
                        @Override
                        public boolean onTouch(View v, MotionEvent event) {
                            act_kota.setFocusableInTouchMode(true);
                            act_kota.showDropDown();
                            act_kota.requestFocus();
                            return false;
                        }
                    });

                    act_kota.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                        @Override
                        public void onFocusChange(View v, boolean hasFocus) {
                            String val = act_kota.getText().toString() + "";
                            Boolean code = Arrays.asList(kota_nama).contains(val);
                            if (!hasFocus)
                                if (!code) {
                                    til_kota.setErrorEnabled(true);
                                    til_kota.setError("Nama Kota tidak valid");
                                } else {
                                    til_kota.setErrorEnabled(false);
                                    til_kota.setError(null);
                                }
                        }
                    });

                    act_kota.addTextChangedListener((TextWatcher) getContext());

                    act_kota.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            String kota = act_kota.getText().toString();
                            int posisi = 0;
                            for (int i = 0; i < kota_nama.length; i++) {
                                if (kota_nama[i].equalsIgnoreCase(kota)) {
                                    Log.d("Test getKota", String.valueOf(i));
                                    posisi = i;
                                }
                            }
                            String id_kota = kota_id[posisi];
                            setAutoCompleteKecamatan(id_propinsi, id_kota);

                            syiar_nokota = kota_syiarnokota[posisi];
                        }
                    });
                } catch (Exception e) {
                    Log.d("Catch kota", e.toString());
                }
            }
        });
        thread.start();
    }

    private void setAutoCompleteKecamatan(final String id_propinsi, final String id_kota) {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    jsonData = AppUtil.setAutoCompleteKecamatan(id_propinsi, id_kota);
                    Log.d("data", jsonData);
                    json = new JSONObject(jsonData);
                    Log.d("json", json.toString());
                    data = json.getJSONArray("data");
                    Log.d("array", data.toString());

                    kecamatan_id = new String[data.length()];
                    kecamatan_nama = new String[data.length()];
                    kecamatan_syiarnokecamatan = new String[data.length()];
                    for (int i = 0; i < data.length(); i++) {
                        JSONObject jo = data.getJSONObject(i);
                        kecamatan_id[i] = jo.getString("no_kec");
                        kecamatan_nama[i] = jo.getString("nama");
                        kecamatan_syiarnokecamatan[i] = jo.getString("syiar_no_kec");
                        Log.d("Test kecamatan", kecamatan_nama.toString());
                    }

                    act_kecamatan.setThreshold(1);

                    getActivity().runOnUiThread(new Runnable() {
                        public void run() {
                            ArrayAdapter<String> kecamatanListAdapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_list_item_1,
                                    kecamatan_nama);
                            kecamatanListAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            act_kecamatan.setAdapter(kecamatanListAdapter);
                        }
                    });

                    act_kecamatan.setOnTouchListener(new View.OnTouchListener() {
                        @Override
                        public boolean onTouch(View v, MotionEvent event) {
                            act_kecamatan.setFocusableInTouchMode(true);
                            act_kecamatan.showDropDown();
                            act_kecamatan.requestFocus();
                            return false;
                        }
                    });

                    act_kecamatan.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                        @Override
                        public void onFocusChange(View v, boolean hasFocus) {
                            String val = act_kecamatan.getText().toString() + "";
                            Boolean code = Arrays.asList(kecamatan_nama).contains(val);
                            if (!hasFocus)
                                if (!code) {
                                    til_kecamatan.setErrorEnabled(true);
                                    til_kecamatan.setError("Nama Kecamatan tidak valid");
                                } else {
                                    til_kecamatan.setErrorEnabled(false);
                                    til_kecamatan.setError(null);
                                }
                        }
                    });

                    act_kecamatan.addTextChangedListener((TextWatcher) getContext());

                    act_kecamatan.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            String kecamatan = act_kecamatan.getText().toString();
                            int posisi = 0;
                            for (int i = 0; i < kecamatan_nama.length; i++) {
                                if (kecamatan_nama[i].equalsIgnoreCase(kecamatan)) {
                                    Log.d("Test getKecamatan", String.valueOf(i));
                                    posisi = i;
                                }
                            }
                            String id_kecamatan = kecamatan_id[posisi];
                            setAutoCompleteKelurahan(id_propinsi, id_kota, id_kecamatan);

                            syiar_nokecamatan = kecamatan_syiarnokecamatan[posisi];
                        }
                    });
                } catch (Exception e) {
                    Log.d("Catch kecamatan", e.toString());
                }
            }
        });
        thread.start();
    }

    private void setAutoCompleteKelurahan(final String id_propinsi, final String id_kota, final String id_kecamatan) {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    jsonData = AppUtil.setAutoCompleteKelurahan(id_propinsi, id_kota, id_kecamatan);
                    Log.d("data", jsonData);
                    json = new JSONObject(jsonData);
                    Log.d("json", json.toString());
                    data = json.getJSONArray("data");
                    Log.d("array", data.toString());

                    kelurahan_id = new String[data.length()];
                    kelurahan_nama = new String[data.length()];
                    kelurahan_syiarnokelurahan = new String[data.length()];
                    kode_pos = new String[data.length()];
                    for (int i = 0; i < data.length(); i++) {
                        JSONObject jo = data.getJSONObject(i);
                        kelurahan_id[i] = jo.getString("id");
                        kelurahan_nama[i] = jo.getString("nama");
                        kelurahan_syiarnokelurahan[i] = jo.getString("syiar_no_kel");
                        kode_pos[i] = jo.getString("kode_pos");
                        Log.d("Test kelurahan", kelurahan_nama.toString());
                    }

                    act_kelurahan.setThreshold(1);

                    getActivity().runOnUiThread(new Runnable() {
                        public void run() {
                            ArrayAdapter<String> kelurahanListAdapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_list_item_1,
                                    kelurahan_nama);
                            kelurahanListAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            act_kelurahan.setAdapter(kelurahanListAdapter);
                        }
                    });

                    act_kelurahan.setOnTouchListener(new View.OnTouchListener() {
                        @Override
                        public boolean onTouch(View v, MotionEvent event) {
                            act_kelurahan.setFocusableInTouchMode(true);
                            act_kelurahan.showDropDown();
                            act_kelurahan.requestFocus();
                            return false;
                        }
                    });

                    act_kelurahan.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                        @Override
                        public void onFocusChange(View v, boolean hasFocus) {
                            String val = act_kelurahan.getText().toString() + "";
                            Boolean code = Arrays.asList(kelurahan_nama).contains(val);
                            if (!hasFocus)
                                if (!code) {
                                    til_kelurahan.setErrorEnabled(true);
                                    til_kelurahan.setError("Nama Kelurahan tidak valid");
                                } else {
                                    til_kelurahan.setErrorEnabled(false);
                                    til_kelurahan.setError(null);
                                }
                        }
                    });

                    act_kelurahan.addTextChangedListener((TextWatcher) getContext());

                    act_kelurahan.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            String kelurahan = act_kelurahan.getText().toString();
                            int posisi = 0;
                            for (int i = 0; i < kelurahan_nama.length; i++) {
                                if (kelurahan_nama[i].equalsIgnoreCase(kelurahan)) {
                                    Log.d("Test getKelurahan", String.valueOf(i));
                                    posisi = i;
                                }
                            }
                            kodepos = kode_pos[posisi];
                            syiar_nokelurahan = kelurahan_syiarnokelurahan[posisi];

                            et_kode_pos.setText(kodepos);
                        }
                    });
                } catch (Exception e) {
                    Log.d("Catch kelurahan", e.toString());
                }
            }
        });
        thread.start();
    }

    private void setAutoCompletePropinsiDomisili() {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    jsonData = AppUtil.setAutoCompletePropinsi();
                    Log.d("Cek jsonData", jsonData);
                    Log.d("data", jsonData);
                    json = new JSONObject(jsonData);
                    Log.d("json", json.toString());
                    data = json.getJSONArray("data");
                    Log.d("array", data.toString());

                    propinsi_domisili_id = new String[data.length()];
                    propinsi_domisili_nama = new String[data.length()];
                    propinsi_domisili_syiarnoprop = new String[data.length()];
                    for (int i = 0; i < data.length(); i++) {
                        JSONObject jo = data.getJSONObject(i);
                        propinsi_domisili_id[i] = jo.getString("id");
                        propinsi_domisili_nama[i] = jo.getString("nama");
                        propinsi_domisili_syiarnoprop[i] = jo.getString("syiar_no_prop");
                    }

                    act_provinsi_domisili.setThreshold(1);

                    getActivity().runOnUiThread(new Runnable() {
                        public void run() {
                            ArrayAdapter<String> propListAdapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_list_item_1,
                                    propinsi_domisili_nama);
                            propListAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            act_provinsi_domisili.setAdapter(propListAdapter);
                        }
                    });

                    act_provinsi_domisili.setOnTouchListener(new View.OnTouchListener() {
                        @Override
                        public boolean onTouch(View v, MotionEvent event) {
                            act_provinsi_domisili.setFocusableInTouchMode(true);
                            act_provinsi_domisili.showDropDown();
                            act_provinsi_domisili.requestFocus();
                            return false;
                        }
                    });

                    act_provinsi_domisili.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                        @Override
                        public void onFocusChange(View v, boolean hasFocus) {
                            String val = act_provinsi_domisili.getText().toString() + "";
                            Boolean code = Arrays.asList(propinsi_domisili_nama).contains(val);
                            if (!hasFocus)
                                if (!code) {
                                    til_provinsi_domisili.setErrorEnabled(true);
                                    til_provinsi_domisili.setError("Nama Propinsi tidak valid");
                                } else {
                                    til_provinsi_domisili.setErrorEnabled(false);
                                    til_provinsi_domisili.setError(null);
                                }
                        }
                    });

                    act_provinsi_domisili.addTextChangedListener((TextWatcher) getContext());

                    act_provinsi_domisili.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View arg1, int position, long id) {
                            String prop_domisili = act_provinsi_domisili.getText().toString();
                            int posisi = 0;
                            for (int i = 0; i < propinsi_domisili_nama.length; i++) {
                                if (propinsi_domisili_nama[i].equalsIgnoreCase(prop_domisili)) {
                                    Log.d("Test getPropinsi", String.valueOf(i));
                                    posisi = i;
                                }
                            }
                            String id_propinsi_domisili = propinsi_domisili_id[posisi];
                            setAutoCompleteKotaDomisili(id_propinsi_domisili);

                            syiar_noprop_domisili = propinsi_domisili_syiarnoprop[posisi];
                        }
                    });
                } catch (Exception e) {
                    Log.d("Catch prop", e.toString());
                }
            }
        });
        thread.start();
    }

    private void setAutoCompleteKotaDomisili(final String id_propinsi_domisili) {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    jsonData = AppUtil.setAutoCompleteKota(id_propinsi_domisili);
                    Log.d("data", jsonData);
                    json = new JSONObject(jsonData);
                    Log.d("json", json.toString());
                    data = json.getJSONArray("data");
                    Log.d("array", data.toString());

                    kota_domisili_id = new String[data.length()];
                    kota_domisili_nama = new String[data.length()];
                    kota_domisili_syiarnokota = new String[data.length()];
                    for (int i = 0; i < data.length(); i++) {
                        JSONObject jo = data.getJSONObject(i);
                        kota_domisili_id[i] = jo.getString("no_kab");
                        kota_domisili_nama[i] = jo.getString("nama");
                        kota_domisili_syiarnokota[i] = jo.getString("syiar_no_kab");
                        Log.d("Test kota", kota_domisili_nama.toString());
                    }

                    act_kota_domisili.setThreshold(1);

                    getActivity().runOnUiThread(new Runnable() {
                        public void run() {
                            ArrayAdapter<String> kotaListAdapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_list_item_1,
                                    kota_domisili_nama);
                            kotaListAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            act_kota_domisili.setAdapter(kotaListAdapter);
                        }
                    });

                    act_kota_domisili.setOnTouchListener(new View.OnTouchListener() {
                        @Override
                        public boolean onTouch(View v, MotionEvent event) {
                            act_kota_domisili.setFocusableInTouchMode(true);
                            act_kota_domisili.showDropDown();
                            act_kota_domisili.requestFocus();
                            return false;
                        }
                    });

                    act_kota_domisili.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                        @Override
                        public void onFocusChange(View v, boolean hasFocus) {
                            String val = act_kota_domisili.getText().toString() + "";
                            Boolean code = Arrays.asList(kota_domisili_nama).contains(val);
                            if (!hasFocus)
                                if (!code) {
                                    til_kota_domisili.setErrorEnabled(true);
                                    til_kota_domisili.setError("Nama Kota tidak valid");
                                } else {
                                    til_kota_domisili.setErrorEnabled(false);
                                    til_kota_domisili.setError(null);
                                }
                        }
                    });

                    act_kota_domisili.addTextChangedListener((TextWatcher) getContext());

                    act_kota_domisili.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            String kota_domisili = act_kota_domisili.getText().toString();
                            int posisi = 0;
                            for (int i = 0; i < kota_domisili_nama.length; i++) {
                                if (kota_domisili_nama[i].equalsIgnoreCase(kota_domisili)) {
                                    Log.d("Test getKota", String.valueOf(i));
                                    posisi = i;
                                }
                            }
                            String id_kota_domisili = kota_domisili_id[posisi];
                            setAutoCompleteKecamatanDomisili(id_propinsi_domisili, id_kota_domisili);

                            syiar_nokota_domisili = kota_domisili_syiarnokota[posisi];
                        }
                    });
                } catch (Exception e) {
                    Log.d("Catch kota", e.toString());
                }
            }
        });
        thread.start();
    }

    private void setAutoCompleteKecamatanDomisili(final String id_propinsi_domisili, final String id_kota_domisili) {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    jsonData = AppUtil.setAutoCompleteKecamatan(id_propinsi_domisili, id_kota_domisili);
                    Log.d("data", jsonData);
                    json = new JSONObject(jsonData);
                    Log.d("json", json.toString());
                    data = json.getJSONArray("data");
                    Log.d("array", data.toString());

                    kecamatan_domisili_id = new String[data.length()];
                    kecamatan_domisili_nama = new String[data.length()];
                    kecamatan_domisili_syiarnokecamatan = new String[data.length()];
                    for (int i = 0; i < data.length(); i++) {
                        JSONObject jo = data.getJSONObject(i);
                        kecamatan_domisili_id[i] = jo.getString("no_kec");
                        kecamatan_domisili_nama[i] = jo.getString("nama");
                        kecamatan_domisili_syiarnokecamatan[i] = jo.getString("syiar_no_kec");
                        Log.d("Test kecamatan", kecamatan_domisili_nama.toString());
                    }

                    act_kecamatan_domisili.setThreshold(1);

                    getActivity().runOnUiThread(new Runnable() {
                        public void run() {
                            ArrayAdapter<String> kecamatanListAdapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_list_item_1,
                                    kecamatan_domisili_nama);
                            kecamatanListAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            act_kecamatan_domisili.setAdapter(kecamatanListAdapter);
                        }
                    });

                    act_kecamatan_domisili.setOnTouchListener(new View.OnTouchListener() {
                        @Override
                        public boolean onTouch(View v, MotionEvent event) {
                            act_kecamatan_domisili.setFocusableInTouchMode(true);
                            act_kecamatan_domisili.showDropDown();
                            act_kecamatan_domisili.requestFocus();
                            return false;
                        }
                    });

                    act_kecamatan_domisili.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                        @Override
                        public void onFocusChange(View v, boolean hasFocus) {
                            String val = act_kecamatan_domisili.getText().toString() + "";
                            Boolean code = Arrays.asList(kecamatan_domisili_nama).contains(val);
                            if (!hasFocus)
                                if (!code) {
                                    til_kecamatan_domisili.setErrorEnabled(true);
                                    til_kecamatan_domisili.setError("Nama Kecamatan tidak valid");
                                } else {
                                    til_kecamatan_domisili.setErrorEnabled(false);
                                    til_kecamatan_domisili.setError(null);
                                }
                        }
                    });

                    act_kecamatan_domisili.addTextChangedListener((TextWatcher) getContext());

                    act_kecamatan_domisili.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            String kecamatan_domisili = act_kecamatan_domisili.getText().toString();
                            int posisi = 0;
                            for (int i = 0; i < kecamatan_domisili_nama.length; i++) {
                                if (kecamatan_domisili_nama[i].equalsIgnoreCase(kecamatan_domisili)) {
                                    Log.d("Test getKecamatan", String.valueOf(i));
                                    posisi = i;
                                }
                            }
                            String id_kecamatan_domisili = kecamatan_domisili_id[posisi];
                            setAutoCompleteKelurahanDomisili(id_propinsi_domisili, id_kota_domisili, id_kecamatan_domisili);

                            syiar_nokecamatan_domisili = kecamatan_domisili_syiarnokecamatan[posisi];
                        }
                    });
                } catch (Exception e) {
                    Log.d("Catch kecamatan", e.toString());
                }
            }
        });
        thread.start();
    }

    private void setAutoCompleteKelurahanDomisili(final String id_propinsi_domisili, final String id_kota_domisili, final String id_kecamatan_domisili) {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    jsonData = AppUtil.setAutoCompleteKelurahan(id_propinsi_domisili, id_kota_domisili, id_kecamatan_domisili);
                    Log.d("data", jsonData);
                    json = new JSONObject(jsonData);
                    Log.d("json", json.toString());
                    data = json.getJSONArray("data");
                    Log.d("array", data.toString());

                    kelurahan_domisili_id = new String[data.length()];
                    kelurahan_domisili_nama = new String[data.length()];
                    kelurahan_domisili_syiarnokelurahan = new String[data.length()];
                    kode_pos_domisili = new String[data.length()];
                    for (int i = 0; i < data.length(); i++) {
                        JSONObject jo = data.getJSONObject(i);
                        kelurahan_domisili_id[i] = jo.getString("id");
                        kelurahan_domisili_nama[i] = jo.getString("nama");
                        kelurahan_domisili_syiarnokelurahan[i] = jo.getString("syiar_no_kel");
                        kode_pos_domisili[i] = jo.getString("kode_pos");
                        Log.d("Test kelurahan", kelurahan_domisili_nama.toString());
                    }

                    act_kelurahan_domisili.setThreshold(1);

                    getActivity().runOnUiThread(new Runnable() {
                        public void run() {
                            ArrayAdapter<String> kelurahanListAdapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_list_item_1,
                                    kelurahan_domisili_nama);
                            kelurahanListAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            act_kelurahan_domisili.setAdapter(kelurahanListAdapter);
                        }
                    });

                    act_kelurahan_domisili.setOnTouchListener(new View.OnTouchListener() {
                        @Override
                        public boolean onTouch(View v, MotionEvent event) {
                            act_kelurahan_domisili.setFocusableInTouchMode(true);
                            act_kelurahan_domisili.showDropDown();
                            act_kelurahan_domisili.requestFocus();
                            return false;
                        }
                    });

                    act_kelurahan_domisili.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                        @Override
                        public void onFocusChange(View v, boolean hasFocus) {
                            String val = act_kelurahan_domisili.getText().toString() + "";
                            Boolean code = Arrays.asList(kelurahan_domisili_nama).contains(val);
                            if (!hasFocus)
                                if (!code) {
                                    til_kelurahan_domisili.setErrorEnabled(true);
                                    til_kelurahan_domisili.setError("Nama Kelurahan tidak valid");
                                } else {
                                    til_kelurahan_domisili.setErrorEnabled(false);
                                    til_kelurahan_domisili.setError(null);
                                }
                        }
                    });

                    act_kelurahan_domisili.addTextChangedListener((TextWatcher) getContext());

                    act_kelurahan_domisili.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            String kelurahan = act_kelurahan_domisili.getText().toString();
                            int posisi = 0;
                            for (int i = 0; i < kelurahan_domisili_nama.length; i++) {
                                if (kelurahan_domisili_nama[i].equalsIgnoreCase(kelurahan)) {
                                    Log.d("Test getKelurahan", String.valueOf(i));
                                    posisi = i;
                                }
                            }
                            kodepos_domisili = kode_pos_domisili[posisi];
                            syiar_nokelurahan_domisili = kelurahan_domisili_syiarnokelurahan[posisi];

                            et_kode_pos_domisili.setText(kodepos_domisili);
                        }
                    });
                } catch (Exception e) {
                    Log.d("Catch kelurahan", e.toString());
                }
            }
        });
        thread.start();
    }

    private void onActionButton() {
        cb_alamat_domisili.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (!isChecked) {
                    et_alamat_domisili.getText().clear();
                    et_rt_domisili.getText().clear();
                    et_rw_domisili.getText().clear();
                    et_kode_pos_domisili.getText().clear();

                    act_provinsi_domisili.getText().clear();
                    act_kota_domisili.getText().clear();
                    act_kecamatan_domisili.getText().clear();
                    act_kelurahan_domisili.getText().clear();
                    setAutoCompletePropinsiDomisili();
                } else {
                    et_alamat_domisili.setText(et_alamat.getText().toString().trim());
                    et_rt_domisili.setText(et_rt.getText().toString().trim());
                    et_rw_domisili.setText(et_rw.getText().toString().trim());
                    et_kode_pos_domisili.setText(et_kode_pos.getText().toString().trim());

                    act_provinsi_domisili.setText(act_provinsi.getText().toString().trim());
                    act_kota_domisili.setText(act_kota.getText().toString().trim());
                    act_kecamatan_domisili.setText(act_kecamatan.getText().toString().trim());
                    act_kelurahan_domisili.setText(act_kelurahan.getText().toString().trim());

                    String prop = act_provinsi_domisili.getText().toString();
                    String kota = act_kota_domisili.getText().toString();
                    String kecamatan = act_kecamatan_domisili.getText().toString();
                    String kelurahan = act_kelurahan_domisili.getText().toString();

                    int posisi_prop = 0, posisi_kota = 0, posisi_kecamatan = 0, posisi_kelurahan = 0;
                    for (int i = 0; i < propinsi_nama.length; i++) {
                        if (propinsi_nama[i].equalsIgnoreCase(prop)) {
                            Log.d("Test getPropinsi", String.valueOf(i));
                            posisi_prop = i;
                        }
                    }
                    syiar_noprop_domisili = propinsi_syiarnoprop[posisi_prop];

                    for (int i = 0; i < kota_nama.length; i++) {
                        if (kota_nama[i].equalsIgnoreCase(kota)) {
                            Log.d("Test getKota", String.valueOf(i));
                            posisi_kota = i;
                        }
                    }
                    syiar_nokota_domisili = kota_syiarnokota[posisi_kota];

                    for (int i = 0; i < kecamatan_nama.length; i++) {
                        if (kecamatan_nama[i].equalsIgnoreCase(kecamatan)) {
                            Log.d("Test getKecamatan", String.valueOf(i));
                            posisi_kecamatan = i;
                        }
                    }
                    syiar_nokecamatan_domisili = kecamatan_syiarnokecamatan[posisi_kecamatan];

                    for (int i = 0; i < kelurahan_nama.length; i++) {
                        if (kelurahan_nama[i].equalsIgnoreCase(kelurahan)) {
                            Log.d("Test getKelurahan", String.valueOf(i));
                            posisi_kelurahan = i;
                        }
                    }
                    syiar_nokelurahan_domisili = kelurahan_syiarnokelurahan[posisi_kelurahan];
                }
            }
        });

        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (spinner_penduduk.getSelectedItem() == null) {
                    FormValidationUtil.displayCustomDialog(getContext(),
                            getString(R.string.dialog_msg_jenis_penduduk),
                            getString(R.string.dialog_header_txt_alert));
                    return;
                } else {
                    if (spinner_penduduk.getSelectedItem().toString().equalsIgnoreCase("Penduduk")) {
                        jenis_penduduk = "R";
                    } else {
                        jenis_penduduk = "N";
                    }
                }

                if (et_alamat.getText().toString().length() == 0) {
                    til_alamat.setError("Alamat KTP Tidak Boleh Kosong!");
                    return;
                } else {
                    alamat = et_alamat.getText().toString();
                }

                if (et_rt.getText().toString().length() == 0) {
                    til_rt.setError("RT Tidak Boleh Kosong!");
                    return;
                } else {
                    rt = et_rt.getText().toString();
                }

                if (et_rw.getText().toString().length() == 0) {
                    til_rw.setError("RW Tidak Boleh Kosong!");
                    return;
                } else {
                    rw = et_rw.getText().toString();
                }

                if (act_provinsi.getText().toString().length() == 0) {
                    til_provinsi.setError("Provinsi Tidak Boleh Kosong!");
                    return;
                } else {
                    propinsi = act_provinsi.getText().toString();
                }

                if (act_kota.getText().toString().length() == 0) {
                    til_kota.setError("Kabupaten/Kota Tidak Boleh Kosong!");
                    return;
                } else {
                    kota = act_kota.getText().toString();
                }

                if (act_kecamatan.getText().toString().length() == 0) {
                    til_kecamatan.setError("Kecamatan Tidak Boleh Kosong!");
                    return;
                } else {
                    kecamatan = act_kecamatan.getText().toString();
                }

                if (act_kelurahan.getText().toString().length() == 0) {
                    til_kelurahan.setError("Kelurahan Tidak Boleh Kosong!");
                    return;
                } else {
                    kelurahan = act_kelurahan.getText().toString();
                }

                if (et_kode_pos.getText().toString().length() == 0) {
                    til_kode_pos.setError("Kode Pos Tidak Boleh Kosong!");
                    return;
                } else {
                    kodepos = et_kode_pos.getText().toString();
                }

                if (et_alamat_domisili.getText().toString().length() == 0) {
                    til_alamat_domisili.setError("Alamat Domisili Tidak Boleh Kosong!");
                    return;
                } else {
                    alamat_domisili = et_alamat_domisili.getText().toString();
                }

                if (et_rt_domisili.getText().toString().length() == 0) {
                    til_rt_domisili.setError("RT Domisili Tidak Boleh Kosong!");
                    return;
                } else {
                    rt_domisili = et_rt_domisili.getText().toString();
                }

                if (et_rw_domisili.getText().toString().length() == 0) {
                    til_rw_domisili.setError("RW Domisili Tidak Boleh Kosong!");
                    return;
                } else {
                    rw_domisili = et_rw_domisili.getText().toString();
                }

                if (act_provinsi_domisili.getText().toString().length() == 0) {
                    til_provinsi_domisili.setError("Provinsi Domisili Tidak Boleh Kosong!");
                    return;
                } else {
                    propinsi_domisili = act_provinsi_domisili.getText().toString();
                }

                if (act_kota_domisili.getText().toString().length() == 0) {
                    til_kota_domisili.setError("Kabupaten/Kota Domisili Tidak Boleh Kosong!");
                    return;
                } else {
                    kota_domisili = act_kota_domisili.getText().toString();
                }

                if (act_kecamatan_domisili.getText().toString().length() == 0) {
                    til_kecamatan_domisili.setError("Kecamatan Domisili Tidak Boleh Kosong!");
                    return;
                } else {
                    kecamatan_domisili = act_kecamatan_domisili.getText().toString();
                }

                if (act_kelurahan_domisili.getText().toString().length() == 0) {
                    til_kelurahan_domisili.setError("Kelurahan Domisili Tidak Boleh Kosong!");
                    return;
                } else {
                    kelurahan_domisili = act_kelurahan_domisili.getText().toString();
                }

                if (et_kode_pos_domisili.getText().toString().length() == 0) {
                    til_kode_pos_domisili.setError("Kode Pos Domisili Tidak Boleh Kosong!");
                    return;
                } else {
                    kodepos_domisili = et_kode_pos_domisili.getText().toString();
                }

                Fragment fr = new FBukaRekeningBio3();
                Bundle datas = new Bundle();
                datas.putString("idtype", idtype);
                datas.putString("idcard", idcard);
                datas.putString("name", name);
                datas.putString("jenis_kelamin", jenis_kelamin);
                datas.putString("tempat_lahir", tempat_lahir);
                datas.putString("tgl_lahir", tgl_lahir);
                datas.putString("nama_ibu", nama_ibu);
                datas.putString("alamat", alamat);
                datas.putString("rt", rt);
                datas.putString("rw", rw);
                datas.putString("propinsi", syiar_noprop);
                datas.putString("kota", syiar_nokota);
                datas.putString("kecamatan", syiar_nokecamatan);
                datas.putString("kelurahan", syiar_nokelurahan);
                datas.putString("kodepos", kodepos);
                datas.putString("alamat_domisili", alamat_domisili);
                datas.putString("rt_domisili", rt_domisili);
                datas.putString("rw_domisili", rw_domisili);
                datas.putString("propinsi_domisili", syiar_noprop_domisili);
                datas.putString("kota_domisili", syiar_nokota_domisili);
                datas.putString("kecamatan_domisili", syiar_nokecamatan_domisili);
                datas.putString("kelurahan_domisili", syiar_nokelurahan_domisili);
                datas.putString("kodepos_domisili", kodepos_domisili);
                datas.putString("jenis_penduduk", jenis_penduduk);
                datas.putString(Menu.MENU_ID, page_id);
                fr.setArguments(datas);
                openFragmentSliding(fr);

            }
        });
    }

    public void openFragmentSliding(Fragment fragment) {
        FragmentManager manager = getActivity().getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit)
                .replace(R.id.framelayout, fragment)
                .addToBackStack(null)
                .commit();
    }
}