package com.bris.brissmartmobile.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.bris.brissmartmobile.R;
import com.bris.brissmartmobile.adapter.MenuContentAdapter;
import com.bris.brissmartmobile.calculate.PrayerTimes;
import com.bris.brissmartmobile.util.AppPreferences;
import com.bris.brissmartmobile.model.LocationInfo;
import com.bris.brissmartmobile.util.LocationPermission;
import com.bris.brissmartmobile.listener.MenuClickListener;
import com.bris.brissmartmobile.islamic.ListJuzAmmaActivity;
import com.bris.brissmartmobile.islamic.MasjidNearmeActivity;
import com.bris.brissmartmobile.islamic.QiblaActivity;
import com.bris.brissmartmobile.islamic.QuotesActivity;
import com.bris.brissmartmobile.islamic.SholahTimesActivity;
import com.bris.brissmartmobile.util.Menu;
import com.bris.brissmartmobile.util.ListMenu;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by valeputra on 6/22/17.
 */

public class FragmentContentBsa extends Fragment implements MenuClickListener {
    private SimpleDateFormat format;
    private TextView locAddress, nextSholat, nextSholatTime, nextSholatCountdown;
    private CardView btnAllSholah;
    private LocationInfo locationInfo;
    private PrayerTimes prayerTimes;
    private Timer timer;
    private AppPreferences appPref;
    private Button btnActivatePraytime;
    private LinearLayout layoutPrayTimeDisabled;
    private TextView prayTimeQuotes;
    private ConstraintLayout layoutPrayTimeEnabled;
    private ListView lvContentMenus;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootview = inflater.inflate(R.layout.fragment_content, container, false);
        format = new SimpleDateFormat("hh:mm a", Locale.getDefault());
        locAddress = (TextView) rootview.findViewById(R.id.location_address);
        nextSholat = (TextView) rootview.findViewById(R.id.tv_next_sholat);
        nextSholatTime = (TextView) rootview.findViewById(R.id.tv_next_sholat_time);
        nextSholatCountdown = (TextView) rootview.findViewById(R.id.tv_next_sholat_count);
        btnAllSholah = (CardView) rootview.findViewById(R.id.btn_all_sholah_time);
        btnActivatePraytime = (Button) rootview.findViewById(R.id.btn_activate_praytime);
        prayTimeQuotes = (TextView) rootview.findViewById(R.id.tv_praytime_disable);
        layoutPrayTimeDisabled = (LinearLayout) rootview.findViewById(R.id.layout_praytime_disable);
        layoutPrayTimeEnabled = (ConstraintLayout) rootview.findViewById(R.id.layout_praytime_enable);
        lvContentMenus = (ListView) rootview.findViewById(R.id.lv_content);
        return rootview;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        appPref = new AppPreferences(getContext());
        initView();
    }

    private void initView() {
        buildContentMenus();
    }

    private void buildContentMenus() {
        List<ListMenu> allItems = listContentMenus();
        MenuContentAdapter customAdapter = new MenuContentAdapter(getContext(), allItems, this);
        lvContentMenus.setAdapter(customAdapter);
    }

    private List<ListMenu> listContentMenus() {
        List<ListMenu> menus = new ArrayList<>();
        Menu.content(getContext(), menus);
        return menus;
    }

    private void showLayoutPrayerTimesEnabled() {
        getNextSholah();
        layoutPrayTimeDisabled.setVisibility(View.GONE);
        layoutPrayTimeEnabled.setVisibility(View.VISIBLE);
        layoutPrayTimeEnabled.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("Cek onClick", "Mlebu");
                LocationPermission location = new LocationPermission(getContext());
                location.updateLocation(0);
            }
        });

        btnAllSholah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToSholahTimesActivity();
            }
        });
    }

    private void showLayoutPrayerTimesDisabled() {
        layoutPrayTimeDisabled.setVisibility(View.VISIBLE);
        prayTimeQuotes.setText(getString(R.string.prayTimeQuotes));
        btnActivatePraytime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToSholahTimesActivity();
            }
        });
    }

    private boolean isPrayerTimesEnabled(LocationInfo locationInfo) {
        if (locationInfo != null) {
            return true;
        }
        return false;
    }

    private void goToSholahTimesActivity() {
        Bundle mbundle = new Bundle();
        Intent i = new Intent(getContext(), SholahTimesActivity.class);
        mbundle.putString(Menu.MENU_ID, getContext().getString(R.string.menu_title_jadwal_sholat));
        i.putExtras(mbundle);
        getContext().startActivity(i);
    }

    private void getNextSholah() {
//        if (locationInfo == null) return;
        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        int timeZone = calendar.getTimeZone().getRawOffset()/(1000*60*60);
        int dst = calendar.getTimeZone().getDSTSavings();

        if (locationInfo.dls == 1){
            dst = 1;
        }

        // as applied in Indonesia
        PrayerTimes.Mazhab mazhab = PrayerTimes.Mazhab.PTC_MAZHAB_SHAFEI;
        PrayerTimes.Way way = PrayerTimes.Way.PTC_WAY_EGYPT;
        /*switch (locationInfo.way){
            case 0:
                way = PrayerTimes.Way.PTC_WAY_EGYPT;
                break;
            case 1:
                way = PrayerTimes.Way.PTC_WAY_KARACHI;
                break;
            case 2:
                way = PrayerTimes.Way.PTC_WAY_ISNA;
                break;
            case 3:
                way = PrayerTimes.Way.PTC_WAY_UMQURA;
                break;
            case 4:
                way = PrayerTimes.Way.PTC_WAY_MWL;
                break;
        }
        switch (locationInfo.mazhab){
            case 0:
                mazhab = PrayerTimes.Mazhab.PTC_MAZHAB_SHAFEI;
                break;
            case 1:
                mazhab = PrayerTimes.Mazhab.PTC_MAZHAB_HANAFI;
                break;
        }*/

        prayerTimes =  new PrayerTimes(day, month+1, year, locationInfo.latitude, locationInfo.longitude,
                timeZone, (locationInfo.dls>0), mazhab, way);

        updateViews();
    }

    private Date fajrDate , sunriseDate , duhrDate , asrDate , maghrebDate , ishaDate , midNightDate;
    private void updateViews() {
        Calendar mid = Calendar.getInstance();
        mid.set(Calendar.HOUR_OF_DAY , 0);
        mid.set(Calendar.MINUTE , 0);
        mid.set(Calendar.SECOND , 0);
        midNightDate = mid.getTime();

        Date [] dates = prayerTimes.get();

        fajrDate = dates[0];
        sunriseDate = dates[1];
        duhrDate = dates[2];
        asrDate = dates[3];
        maghrebDate = dates[4];
        ishaDate = dates[5];

//        Log.d("Loc Sholah Fajr", format.format(fajrDate));
//        Log.d("Loc Sholah sunriseDate", format.format(sunriseDate));
//        Log.d("Loc Sholah duhrDate", format.format(duhrDate));
//        Log.d("Loc Sholah asrDate", format.format(asrDate));
//        Log.d("Loc Sholah maghrebDate", format.format(maghrebDate));
//        Log.d("Loc Sholah ishaDate", format.format(ishaDate));

        checkActiveView();
    }

    private String nextPray = "";
    private Date nextDate, lastDate;
    private void checkActiveView() {
        if (fajrDate == null || sunriseDate == null || duhrDate == null || asrDate == null ||
                maghrebDate == null || ishaDate == null) return;

        Date current = Calendar.getInstance().getTime();

        if (current.after(ishaDate) && current.before(fajrDate)){
            nextPray = getString(R.string.txt_jadwal_sholat_subuh);
            lastDate = fajrDate;
            nextDate = sunriseDate;
        } else if (current.after(fajrDate) && current.before(duhrDate)){
            nextPray = getString(R.string.txt_jadwal_sholat_dzuhur);
            lastDate = sunriseDate;
            nextDate = duhrDate;
        } else if (current.after(duhrDate) && current.before(asrDate)){
            nextPray = getString(R.string.txt_jadwal_sholat_asr);
            lastDate = duhrDate;
            nextDate = asrDate;
        } else if (current.after(asrDate) && current.before(maghrebDate)){
            nextPray = getString(R.string.txt_jadwal_sholat_magrib);
            lastDate = asrDate;
            nextDate = maghrebDate;
        } else if (current.after(maghrebDate) && current.before(ishaDate)){
            nextPray = getString(R.string.txt_jadwal_sholat_isha);
            lastDate = maghrebDate;
            nextDate = ishaDate;
        } else {
            if (current.after(midNightDate) && current.before(fajrDate)){
                lastDate = getPrayerforPreviousDay().get()[5];
                nextDate = fajrDate;
            }else {

                lastDate = ishaDate;
                nextDate = getPrayerforNextDay().get()[0];
            }
        }

        if (! (locationInfo.locality.isEmpty() && locationInfo.city.isEmpty()) ) {
            String address = locationInfo.locality + ", " + locationInfo.city;
            if (address.length() > 45)
                address = (address.substring(0, 45)).trim() + "...";

            locAddress.setText(address);

        } else locAddress.setText("");

        nextSholatTime.setText(format.format(nextDate));
        nextSholat.setText(nextPray);
        updateCountDown(current);
    }

    Calendar endCal = Calendar.getInstance(),
            startCal = Calendar.getInstance(),
            currCal = Calendar.getInstance();

    private void updateCountDown(Date current) {
        endCal.setTime(nextDate);
        startCal.setTime(lastDate);
        currCal.setTime(current);
        timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                final long timeRemaining = endCal.getTimeInMillis() - Calendar.getInstance().getTimeInMillis();

                int seconds = (int) (timeRemaining / 1000) % 60;
                int minutes = (int) ((timeRemaining / (1000 * 60)) % 60);
                int hours = (int) ((timeRemaining / (1000 * 60 * 60)) % 24);
                int days = (int) (timeRemaining / (1000 * 60 * 60 * 24));
                boolean hasDays = days > 0;
                final String timeNow = String.format("%1$02d%4$s%2$02d%5$s%3$02d",
                        hasDays ? days : hours,
                        hasDays ? hours : minutes,
                        hasDays ? minutes : seconds,
                        hasDays ? ":" : ":",
                        hasDays ? ":" : ":");

                if (getActivity() != null) {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (timeRemaining <= 0) {
                                checkActiveView();
                                return;
                            }
                            nextSholatCountdown.setText("( " + timeNow + " )");
                        }
                    });
                }
            }
        } , 1000 , 1000);
    }

    private PrayerTimes getPrayerforPreviousDay() {
        if (locationInfo == null) return null;
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.DAY_OF_MONTH , calendar.get(Calendar.DAY_OF_MONTH) - 1);
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        int timeZone = calendar.getTimeZone().getRawOffset()/(1000*60*60);
        int dst = calendar.getTimeZone().getDSTSavings();
        if (locationInfo.dls == 1){
            dst = 1;
        }
        return new PrayerTimes(day, month+1, year, locationInfo.latitude, locationInfo.longitude, timeZone, !(dst>0),
                PrayerTimes.getDefaultMazhab(locationInfo.country_code), PrayerTimes.getDefaultWay(locationInfo.country_code));
    }


    private PrayerTimes getPrayerforNextDay() {
        if (locationInfo == null) return null;
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.DAY_OF_MONTH , calendar.get(Calendar.DAY_OF_MONTH) + 1);
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        int timeZone = calendar.getTimeZone().getRawOffset()/(1000*60*60);
        int dst = calendar.getTimeZone().getDSTSavings();
        if (locationInfo.dls == 1){
            dst = 1;
        }
        return new PrayerTimes(day, month+1, year, locationInfo.latitude, locationInfo.longitude, timeZone, !(dst>0),
                PrayerTimes.getDefaultMazhab(locationInfo.country_code), PrayerTimes.getDefaultWay(locationInfo.country_code));
    }

    @Override
    public void onResume() {
        super.onResume();
        if (isPrayerTimesEnabled(appPref.getLocationConfig())) {
            locationInfo = appPref.getLocationConfig();
            Log.d("Cek onResume","Mlebu");
            showLayoutPrayerTimesEnabled();
        } else {
            showLayoutPrayerTimesDisabled();
        }
    }

    @Override
    public void onMenuClick(String menu) {
        Bundle mbundle = new Bundle();
        Intent intent = null;
        if(menu.equalsIgnoreCase(getContext().getString(R.string.menu_title_motivasi_islami))) {
            intent = new Intent(getContext(), QuotesActivity.class);

        } else if(menu.equalsIgnoreCase(getContext().getString(R.string.menu_title_masjid_terdekat))) {
            intent = new Intent(getContext(), MasjidNearmeActivity.class);

        } else if (menu.equalsIgnoreCase(getContext().getString(R.string.menu_title_juz_amma))){
            intent = new Intent(getContext(), ListJuzAmmaActivity.class);

        } else if(menu.equalsIgnoreCase(getContext().getString(R.string.menu_title_arahkiblat))) {
            intent = new Intent(getContext(), QiblaActivity.class);
        }

        mbundle.putString(Menu.MENU_ID, menu);
        intent.putExtras(mbundle);
        getContext().startActivity(intent);
    }
}
