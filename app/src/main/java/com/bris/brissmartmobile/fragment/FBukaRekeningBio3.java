package com.bris.brissmartmobile.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.bris.brissmartmobile.R;
import com.bris.brissmartmobile.activity.ABukaRekening;
import com.bris.brissmartmobile.util.AppPreferences;
import com.bris.brissmartmobile.util.AppUtil;
import com.bris.brissmartmobile.util.AppUtilPrint;
import com.bris.brissmartmobile.util.FormValidationUtil;
import com.bris.brissmartmobile.util.Menu;
import com.bris.brissmartmobile.util.NothingSelectedSpinnerAdapter;
import com.bris.brissmartmobile.util.NumberTextWatcherForThousand;

import org.json.JSONObject;


/**
 * Create by valeputra
 * Credits : akbaranjas
 */

public class FBukaRekeningBio3 extends Fragment {
    private View view;
    private EditText et_no_hp, et_email, et_setoran_awal;
    private TextInputLayout til_no_hp, til_email, til_setoran_awal;
    private String no_hp, email, setoran_awal;
    private Spinner spinner_pekerjaan;
    private Button btn1;
    private String pekerjaan, kode_pekerjaan;
    private String idtype, idcard, name, jenis_kelamin, tempat_lahir, tgl_lahir, nama_ibu,
            alamat, rt, rw, propinsi, kota, kecamatan, kelurahan, kodepos, jenis_penduduk,
            alamat_domisili, rt_domisili, rw_domisili, propinsi_domisili, kota_domisili,
            kecamatan_domisili, kelurahan_domisili, kodepos_domisili;
    JSONObject json, resdata;
    String tid, page_id, responseJSON, msgErr, rcode, cmsg;

    private AppPreferences appPref;

    public FBukaRekeningBio3() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.activity_buka_rekening_bio_3, container, false);

        appPref = new AppPreferences(getContext());
        tid = appPref.getDeviceTid();
        Log.d("TID BUKA", tid);

        idtype = getArguments().getString("idtype");
        idcard = getArguments().getString("idcard");
        name = getArguments().getString("name");
        jenis_kelamin = getArguments().getString("jenis_kelamin");
        tempat_lahir = getArguments().getString("tempat_lahir");
        tgl_lahir = getArguments().getString("tgl_lahir");
        nama_ibu = getArguments().getString("nama_ibu");
        alamat = getArguments().getString("alamat");
        rt = getArguments().getString("rt");
        rw = getArguments().getString("rw");
        propinsi = getArguments().getString("propinsi");
        kota = getArguments().getString("kota");
        kecamatan = getArguments().getString("kecamatan");
        kelurahan = getArguments().getString("kelurahan");
        kodepos = getArguments().getString("kodepos");
        alamat_domisili = getArguments().getString("alamat_domisili");
        rt_domisili = getArguments().getString("rt_domisili");
        rw_domisili = getArguments().getString("rw_domisili");
        propinsi_domisili = getArguments().getString("propinsi_domisili");
        kota_domisili = getArguments().getString("kota_domisili");
        kecamatan_domisili = getArguments().getString("kecamatan_domisili");
        kelurahan_domisili = getArguments().getString("kelurahan_domisili");
        kodepos_domisili = getArguments().getString("kodepos_domisili");
        jenis_penduduk = getArguments().getString("jenis_penduduk");
        page_id = getArguments().getString(Menu.MENU_ID);

        String[] arrayPekerjaan = new String[] {
                            "PNS/BUMN", "TNI/POLRI", "Pegawai Swasta", "Profesional", "Wiraswasta",
                "Pelajar/Mahasiswa", "Pensiunan", "Ibu Rumah Tangga", "Petani/Nelayan/Peternak",
                "Pengacara/Bidang Hukum", "Tidak Bekerja"
                    };

        til_no_hp = (TextInputLayout) view.findViewById(R.id.til_no_hp);
        til_email = (TextInputLayout) view.findViewById(R.id.til_email);
        til_setoran_awal = (TextInputLayout) view.findViewById(R.id.til_setoran_awal);

        et_no_hp = (EditText) view.findViewById(R.id.et_no_hp);
        et_email = (EditText) view.findViewById(R.id.et_email);
        et_setoran_awal = (EditText) view.findViewById(R.id.et_setoran_awal);
        et_setoran_awal.addTextChangedListener(new NumberTextWatcherForThousand(et_setoran_awal));

        spinner_pekerjaan = (Spinner) view.findViewById(R.id.spinner_pekerjaan);
        ArrayAdapter<String> adapterPekerjaan = new ArrayAdapter<String>(getContext(),
                android.R.layout.simple_spinner_item, arrayPekerjaan);
        adapterPekerjaan.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_pekerjaan.setAdapter(new NothingSelectedSpinnerAdapter(
                adapterPekerjaan,
                R.layout.spinner_pekerjaan_nothing_selected,
                getContext()
        ));

        btn1 = (Button) view.findViewById(R.id.btn_submit);

        try {
            onActionButton();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return view;
    }

    private void onActionButton() {
        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (spinner_pekerjaan.getSelectedItem() == null) {
                    FormValidationUtil.displayCustomDialog(getContext(),
                            getString(R.string.dialog_msg_jenis_pekerjaan),
                            getString(R.string.dialog_header_txt_alert));
                    return;
                } else {
                    pekerjaan = spinner_pekerjaan.getSelectedItem().toString();
                    if (pekerjaan.equalsIgnoreCase("PNS/BUMN")) {
                        kode_pekerjaan = "30";
                    } else if (pekerjaan.equalsIgnoreCase("TNI/POLRI")) {
                        kode_pekerjaan = "31";
                    } else if (pekerjaan.equalsIgnoreCase("Pegawai Swasta")) {
                        kode_pekerjaan = "32";
                    } else if (pekerjaan.equalsIgnoreCase("Profesional")) {
                        kode_pekerjaan = "33";
                    } else if (pekerjaan.equalsIgnoreCase("Wiraswasta")) {
                        kode_pekerjaan = "34";
                    } else if (pekerjaan.equalsIgnoreCase("Pelajar/Mahasiswa")) {
                        kode_pekerjaan = "35";
                    } else if (pekerjaan.equalsIgnoreCase("Pensiunan")) {
                        kode_pekerjaan = "36";
                    } else if (pekerjaan.equalsIgnoreCase("Ibu Rumah Tangga")) {
                        kode_pekerjaan = "37";
                    } else if (pekerjaan.equalsIgnoreCase("Petani/Nelayan/Peternak")) {
                        kode_pekerjaan = "38";
                    } else if (pekerjaan.equalsIgnoreCase("Pengacara/Bidang Hukum")) {
                        kode_pekerjaan = "39";
                    } else if (pekerjaan.equalsIgnoreCase("Tidak Bekerja")) {
                        kode_pekerjaan = "40";
                    }
                }

                if (et_no_hp.getText().toString().length() == 0) {
                    til_no_hp.setError("Nomor Handphone Tidak Boleh Kosong!");
                    return;
                } else {
                    no_hp = et_no_hp.getText().toString();
                }

                if (et_setoran_awal.getText().toString().length() == 0) {
                    setoran_awal = "0";
                } else {
                    setoran_awal = NumberTextWatcherForThousand.trimCommaOfString(et_setoran_awal.getText().toString().trim());
                }

                if (et_email.getText().toString().length() == 0) {
                    email = "";
                } else {
                    email = et_email.getText().toString();
                }

                Log.d("Cek kode_pekerjaan", kode_pekerjaan);
                Log.d("Cek no_hp", no_hp);
                Log.d("Cek setoran_awal", setoran_awal);
                Log.d("Cek email", email);
                Log.d("Cek to run soap", "Ready");

                SoapActivity action = new SoapActivity();
                action.execute();

            }
        });
    }

    private class SoapActivity extends AsyncTask<Void, Void, Void> {


        @Override
        protected void onPreExecute() {

        }

        @Override
        protected Void doInBackground(Void... params) {
            convertir();
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            if (msgErr.equalsIgnoreCase("true")) {
                displayDialog(getContext(), responseJSON);
            }
        }

    }

    private void convertir() {
        try {
            getActivity().runOnUiThread(new Runnable() {
                public void run() {
                    AppUtil.initDialogProgress(getContext());
                    AppUtil.showDialog();
                }
            });

            JSONObject cmdparam = new JSONObject();
            cmdparam.put("name", name);
            cmdparam.put("idtype", idtype);
            cmdparam.put("idcard", idcard);
            cmdparam.put("parentidcard", "0");
            cmdparam.put("gender", jenis_kelamin);
            cmdparam.put("bplace", tempat_lahir);
            cmdparam.put("bdate", tgl_lahir);
            cmdparam.put("mothername", nama_ibu);
            cmdparam.put("addr", alamat);
            cmdparam.put("addrna", rt);
            cmdparam.put("addrca", rw);
            cmdparam.put("addrprov", propinsi);
            cmdparam.put("addrcity", kota);
            cmdparam.put("addrdis", kecamatan);
            cmdparam.put("addrvil", kelurahan);
            cmdparam.put("addrpostalcode", kodepos);
            cmdparam.put("resident", jenis_penduduk);
            cmdparam.put("addri", alamat_domisili);
            cmdparam.put("addrina", rt_domisili);
            cmdparam.put("addrica", rw_domisili);
            cmdparam.put("addriprov", propinsi_domisili);
            cmdparam.put("addricity", kota_domisili);
            cmdparam.put("addridis", kecamatan_domisili);
            cmdparam.put("addrivil", kelurahan_domisili);
            cmdparam.put("addripostalcode", kodepos_domisili);
            cmdparam.put("vdsn", no_hp);
            cmdparam.put("jobcode", kode_pekerjaan);
            cmdparam.put("email", email);
            cmdparam.put("deposit", setoran_awal);

            String soapRes = AppUtil.soapReq(AppUtil.getDsn(), "custid_save", cmdparam);

            if (soapRes.equalsIgnoreCase("")) {
                AppUtil.hideDialog();
                msgErr = "true";
                responseJSON = "Gagal terhubung ke server";
            } else {
                json = new JSONObject(soapRes);
                resdata = json.getJSONObject("resdata");
                Log.d("Cek resdata", resdata.toString());
                rcode = resdata.getString("rcode");
                msgErr = "false";

                if (rcode.equalsIgnoreCase("00")) {

                    AppUtil.hideDialog();
                    cmsg = resdata.getString("cmsg");
                    responseJSON = cmsg;
                    msgErr = "true";


                    //khusus edc brissmart
                    String date = AppUtil.getDateFormat();

                    Bundle datas = new Bundle();
                    datas.putString("menu_id", page_id);
                    datas.putString("tid", tid);
                    datas.putString("mid", "4228267000001");
                    datas.putString("type_card", "-");
                    datas.putString("date", date);
                    datas.putString("msg", cmsg);

                    getActivity().runOnUiThread(new Runnable() {
                        public void run() {
                            AppUtilPrint.printStruk(getContext(), datas);
                        }
                    });

                } else {
                    Log.d("else", "gagal");
                    AppUtil.hideDialog();
                    cmsg = resdata.getString("cmsg");
                    responseJSON = cmsg;
                    msgErr = "true";
                }
            }
        } catch (Exception ex) {
            AppUtil.hideDialog();
            Log.d("THREAD 2", ex.toString());
            responseJSON = "Transaksi Gagal";
            msgErr = "true";
        }
    }

    public void displayDialog(final Context context, String message) {
        LayoutInflater li = LayoutInflater.from(context);
        View msgDialog = li.inflate(R.layout.custom_dialog_message, null);
        Button btnOkMessage = (Button) msgDialog.findViewById(R.id.btn_close);
        TextView txt_msg_display = (TextView) msgDialog.findViewById(R.id.dialog_txt_msg_info);
        txt_msg_display.setText(message);
        AlertDialog.Builder alert = new AlertDialog.Builder(context);
        alert.setView(msgDialog);
        alert.setCancelable(false);
        final AlertDialog dialog = alert.create();

        btnOkMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                getActivity().finish();
            }
        });
        dialog.show();
    }

}