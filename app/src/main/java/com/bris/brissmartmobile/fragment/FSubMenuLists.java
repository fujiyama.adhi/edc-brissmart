package com.bris.brissmartmobile.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.bris.brissmartmobile.R;
import com.bris.brissmartmobile.adapter.ListViewAdapter;
import com.bris.brissmartmobile.util.ListViewMenu;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.annotation.Nullable;

/**
 * Created by valeputra on 6/22/17.
 */

public class FSubMenuLists extends Fragment {

    ListView lv_submenu;
    List<ListViewMenu> subMenus;
    private ListViewAdapter adapter;
    String pageTitle;
    private View view;

    public static final String[] menuContaintsMenu = new String[] {
            "Transfer Rekening BRI Syariah",
            "Transfer Rekening Bank Lain"
    };

    public FSubMenuLists() {}

    public View onCreateView(final LayoutInflater inflater, @Nullable final ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_submenulists, container, false);
        pageTitle = getArguments().getString("menu_id");
        Log.d("pageTitle SubMenuList", pageTitle);
        lv_submenu = (ListView) view.findViewById(R.id.lv_f_submenu_listview);
        subMenus = new ArrayList<>();

        Toolbar toolbar = (Toolbar) view.findViewById(R.id.tb_submenulist);
        toolbar.setNavigationIcon(R.mipmap.abc_ic_ab_back_mtrl_am_alpha);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().onBackPressed();
            }
        });
        TextView tvToolbar = (TextView) view.findViewById(R.id.tb_title_submenulist);
        tvToolbar.setText(pageTitle);

        // generate submenu
        switch (pageTitle) {

            case "Transfer":
//                subMenus.add(new ListViewMenu(R.drawable.ico_tunai, getString(R.string.submenu_title_setor_tabungan_cerdas), R.mipmap.ico_next));
                subMenus.add(new ListViewMenu(R.drawable.ico_transfer2, getString(R.string.submenu_title_rekening_cerdas), R.mipmap.ico_next));
                subMenus.add(new ListViewMenu(R.drawable.ico_transfer_green, getString(R.string.submenu_title_rekening_bris), R.mipmap.ico_next));
                subMenus.add(new ListViewMenu(R.drawable.ico_transfer_ex, getString(R.string.submenu_title_rekening_bank_lain), R.mipmap.ico_next));
                break;

            case "Transfer dari Rekening Tabungan Cerdas" :
                subMenus.add(new ListViewMenu(R.drawable.ico_tranfer_innew, getString(R.string.submenu_title_rekening_cerdas), R.mipmap.ico_next));
                subMenus.add(new ListViewMenu(R.drawable.ico_tranfer_innew, getString(R.string.submenu_title_rekening_bris), R.mipmap.ico_next));
                subMenus.add(new ListViewMenu(R.drawable.ico_tranfer_outnew, getString(R.string.submenu_title_rekening_bank_lain), R.mipmap.ico_next));
                break;

            case "PLN Postpaid" :

            case "Telepon" :

            case "Internet" :

            case "TV Berbayar" :

            case "Tiket KAI" :

            case "Tokopedia" :

            case "Institusi" :

            case "Pendidikan" :

            case "Asuransi" :
                subMenus.add(new ListViewMenu(R.drawable.ico_tunainew, getString(R.string.submenu_title_tunai), R.mipmap.ico_next));
                subMenus.add(new ListViewMenu(R.drawable.ico_tabungannew, getString(R.string.submenu_title_rekening_bsa), R.mipmap.ico_next));
//                subMenus.add(new ListViewMenu(R.drawable.ico_swipe_cardnew, getString(R.string.submenu_title_card_swipe), R.mipmap.ico_next));
                break;

            case "Token Listrik" :
                subMenus.add(new ListViewMenu(R.drawable.ico_tunainew, getString(R.string.submenu_title_tunai), R.mipmap.ico_next));
                subMenus.add(new ListViewMenu(R.drawable.ico_tabungannew, getString(R.string.submenu_title_rekening_bsa), R.mipmap.ico_next));
                break;

            case "Pulsa" :
                subMenus.add(new ListViewMenu(R.drawable.ico_tunainew, getString(R.string.submenu_title_tunai), R.mipmap.ico_next));
                subMenus.add(new ListViewMenu(R.drawable.ico_tabungannew, getString(R.string.submenu_title_rekening_bsa), R.mipmap.ico_next));
                break;

            case "Paket Data" :
                subMenus.add(new ListViewMenu(R.drawable.ico_tunainew, getString(R.string.submenu_title_tunai), R.mipmap.ico_next));
                subMenus.add(new ListViewMenu(R.drawable.ico_tabungannew, getString(R.string.submenu_title_rekening_bsa), R.mipmap.ico_next));
                break;

            case "Top Up GO-JEK" :
                subMenus.add(new ListViewMenu(R.drawable.ico_tunainew, getString(R.string.submenu_title_tunai), R.mipmap.ico_next));
                subMenus.add(new ListViewMenu(R.drawable.ico_tabungannew, getString(R.string.submenu_title_rekening_bsa), R.mipmap.ico_next));
                break;

            case "Cek Saldo" :

            case "Cek Mutasi" :
                subMenus.add(new ListViewMenu(R.drawable.ico_tunainew, "Tabungan Cerdas", R.mipmap.ico_next));
                break;

            case "Transfer Rekening BRI Syariah" :
                subMenus.add(new ListViewMenu(R.drawable.ico_tunainew, getString(R.string.submenu_title_tunai), R.mipmap.ico_next));
//                subMenus.add(new ListViewMenu(R.drawable.ico_swipe_cardnew, getString(R.string.submenu_title_card_swipe), R.mipmap.ico_next));
                break;

            case "Transfer Rekening Bank Lain" :
                subMenus.add(new ListViewMenu(R.drawable.ico_tunainew, getString(R.string.submenu_title_tunai), R.mipmap.ico_next));
//                subMenus.add(new ListViewMenu(R.drawable.ico_swipe_cardnew, getString(R.string.submenu_title_card_swipe), R.mipmap.ico_next));
                break;

            default: break;
        }

        // set adapter
        adapter = new ListViewAdapter(getContext(), subMenus);
        lv_submenu.setAdapter(adapter);
        lv_submenu.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                final TextView menu = (TextView) view.findViewById(R.id.tv_title_submenu);
                Bundle datas = new Bundle();
                if (pageTitle.equalsIgnoreCase("Transfer dari Rekening Tabungan Cerdas")) {
                    datas.putString("menu_id", menu.getText()+"");
                } else {
                    datas.putString("menu_id", pageTitle + " " + menu.getText()+"");
                }
                String menuId = menu.getText().toString();

                // to call/infalte rootView(activity)
                LayoutInflater inflater1 = LayoutInflater.from(getActivity());
                view = inflater1.inflate(R.layout.activity_alistview_constructor, container, false);

                // jika menu memiliki sub menu
                if(Arrays.asList(menuContaintsMenu).contains(pageTitle + " " + menu.getText()+"")) {
                    Fragment fr = new FSubMenuLists();
                    openFragmentSliding(fr, datas);
                } else {
                    // jika tidak meniliki submenu
                    menuThatNotContainMenu(menuId, datas);
                }
            }
        });

        return view;
    }

    private void menuThatNotContainMenu(String menuId, Bundle datas) {
        Log.d("DOR", menuId);
        Log.d("DOR", pageTitle);

        switch (menuId) {
            case "Tunai" :
                if (pageTitle.equalsIgnoreCase("PLN Postpaid")) {
                    goToFPembayaranTunai(datas);
                } else if (pageTitle.equalsIgnoreCase("Token Listrik") ||
                        pageTitle.equalsIgnoreCase("Pulsa") ||
                        pageTitle.equalsIgnoreCase("Paket Data") ||
                        pageTitle.equalsIgnoreCase("Top Up GO-JEK")){
                    goToFPembelianTunai(datas);
                } else if (pageTitle.equalsIgnoreCase("Cek Mutasi")) {
                    goToFCekMutasi(datas);
                } else if (pageTitle.equalsIgnoreCase("Transfer Rekening BRI Syariah")) {
                    goToFTransferRekeningBrisTunai(datas);
                } else if (pageTitle.equalsIgnoreCase("Transfer Rekening Bank Lain")) {
                    goToFTransferAntarBankTunai(datas);
                } else {
                    goToFPembayaranTunai(datas);
                }
                break;
            case "Kartu" :
                if (pageTitle.equalsIgnoreCase("Cek Saldo")) {
                    datas.putString("kartu_id", "Saldo");
                    goToFSwipeCard(datas);
                }
                else if (pageTitle.equalsIgnoreCase("Cek Mutasi")) {
                    datas.putString("kartu_id", "Mutasi");
                    goToFSwipeCard(datas);
                }
                else if (pageTitle.equalsIgnoreCase("Transfer Rekening BRI Syariah")) {
                    datas.putString("kartu_id", "Transfer");
                    goToFTransferRekeningBrisKartu(datas);
                }
                else if (pageTitle.equalsIgnoreCase("Transfer Rekening Bank Lain")) {
                    datas.putString("kartu_id", "Transfer");
                    goToFTransferAntarBankKartu(datas);
                }
                else if (pageTitle.equalsIgnoreCase("PLN Postpaid") ||
                        pageTitle.equalsIgnoreCase("Telepon") ||
                        pageTitle.equalsIgnoreCase("Internet") ||
                        pageTitle.equalsIgnoreCase("TV Berbayar") ||
                        pageTitle.equalsIgnoreCase("Tiket KAI") ||
                        pageTitle.equalsIgnoreCase("Tokopedia") ||
                        pageTitle.equalsIgnoreCase("Institusi") ||
                        pageTitle.equalsIgnoreCase("Pendidikan") ||
                        pageTitle.equalsIgnoreCase("Asuransi")) {
                    datas.putString("kartu_id", "Pembayaran");
                    goToFPembayaranKartu(datas);
                }

                break;
            case "Tabungan Cerdas" :
                if (pageTitle.equalsIgnoreCase("Cek Saldo")) {
                    goToFCekSaldo(datas);
                } else {
                    goToFSetorTunai(datas);
                }
                break;
            case "Rekening Tabungan Cerdas" :
                if (pageTitle.equalsIgnoreCase("Transfer")) {
                    goToFTransferRekeningCerdasTunai(datas);
                } else {
                    goToFTransferRekeningCerdasDebitBsa(datas);
                }
                break;
            case "Rekening BRI Syariah" :
                if (pageTitle.equalsIgnoreCase("Transfer")) {
                    goToFTransferRekeningBrisTunai(datas);
                } else {
                    goToFTransferRekeningBrisDebitBsa(datas);
                }
                break;
            case "Rekening Bank Lain" :
                if (pageTitle.equalsIgnoreCase("Transfer")) {
                    goToFTransferAntarBankTunai(datas);
                } else {
                    goToFTransferAntarBankDebitBsa(datas);
                }
                break;
            case "Debit Rekening Tabungan Cerdas" :
                if (pageTitle.equalsIgnoreCase("PLN Postpaid")) {
                    goToFPembayaranBSA(datas);
                } else if (pageTitle.equalsIgnoreCase("Token Listrik") ||
                        pageTitle.equalsIgnoreCase("Pulsa") ||
                        pageTitle.equalsIgnoreCase("Paket Data") ||
                        pageTitle.equalsIgnoreCase("Top Up GO-JEK")){
                    goToFPembelianBSA(datas);
                } else {
                    goToFPembayaranBSA(datas);
                }
                break;
            default:
                break;
        }
    }

    private void goToFSwipeCard(Bundle data) {
        Fragment fr = new FSwipeTrial();
        openFragmentSliding(fr, data);
    }
    private void goToFPembayaranKartu(Bundle datas) {
        FPembayaranKartu fr = new FPembayaranKartu();
        openFragmentSliding(fr, datas);
    }

    private void goToFCekSaldo(Bundle data) {
        Fragment fr = new FCekSaldo();
        openFragmentSliding(fr, data);
    }

    private void goToFCekMutasi(Bundle data) {
        Fragment fr = new FCekMutasi();
        openFragmentSliding(fr, data);
    }

    private void goToFSetorTunai(Bundle data) {
        Fragment fr = new FSetorTunai();
        openFragmentSliding(fr, data);
    }

    private void goToFTransferRekeningCerdasDebitBsa(Bundle data) {
        Fragment fr = new FTransferRekeningCerdasDebitBsa();
        openFragmentSliding(fr, data);
    }

    private void goToFTransferRekeningCerdasTunai(Bundle data) {
        Fragment fr = new FTransferRekeningCerdasTunai();
        openFragmentSliding(fr, data);
    }

    private void goToFTransferRekeningBrisDebitBsa(Bundle data) {
        Fragment fr = new FTransferRekeningBrisDebitBsa();
        openFragmentSliding(fr, data);
    }

    private void goToFTransferRekeningBrisTunai(Bundle data) {
        Fragment fr = new FTransferRekeningBrisTunai();
        openFragmentSliding(fr, data);
    }

    private void goToFTransferRekeningBrisKartu(Bundle data) {
        Fragment fr = new FTransferRekeningBrisKartu();
        openFragmentSliding(fr, data);
    }

    private void goToFTransferAntarBankTunai(Bundle datas) {
//        FTransferAntarBankTunai fr = new FTransferAntarBankTunai();
        Fragment fr = new FTransferAntarBankTunai();
        openFragmentSliding(fr, datas);
    }

    private void goToFTransferAntarBankKartu(Bundle datas) {
        Fragment fr = new FTransferAntarBankKartu();
        openFragmentSliding(fr, datas);
    }

    private void goToFTransferAntarBankDebitBsa(Bundle datas) {
        Fragment fr = new FTransferAntarBankDebitBsa();
        openFragmentSliding(fr, datas);
    }

    private void goToFPembayaranTunai(Bundle datas) {
        FPembayaranTunai fr = new FPembayaranTunai();
        openFragmentSliding(fr, datas);
    }

    private void goToFPembayaranBSA(Bundle datas) {
        FPembayaranBSA fr = new FPembayaranBSA();
        openFragmentSliding(fr, datas);
    }

    private void goToFPembelianTunai(Bundle datas) {
        FPembelianTunai fr = new FPembelianTunai();
        openFragmentSliding(fr, datas);
    }

    private void goToFPembelianBSA(Bundle datas) {
        FPembelianBSA fr = new FPembelianBSA();
        openFragmentSliding(fr, datas);
    }

    public void openFragmentSliding(Fragment fragment, Bundle datas) {
        FragmentManager manager = getActivity().getSupportFragmentManager();
        fragment.setArguments(datas); // insert bundling datas
        manager.beginTransaction()
                .replace(R.id.container_listview_root, fragment)
                .addToBackStack(null)
                .commit();
//        FragmentManager manager = getActivity().getSupportFragmentManager();
//        FragmentTransaction transaction = manager.beginTransaction();
//        fragment.setArguments(datas);
//        transaction.setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit)
//                .replace(R.id.fragment_submenulist, fragment)
//                .addToBackStack(null)
//                .commit();
    }
}

