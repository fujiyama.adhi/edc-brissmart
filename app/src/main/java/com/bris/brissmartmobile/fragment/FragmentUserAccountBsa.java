package com.bris.brissmartmobile.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bris.brissmartmobile.R;
import com.bris.brissmartmobile.activity.AGantiPasswordAgen;
import com.bris.brissmartmobile.activity.AGantiPasswordBsa;
import com.bris.brissmartmobile.model.UserBrissmart;
import com.bris.brissmartmobile.util.CustomImageViewRounded;
import com.bris.brissmartmobile.util.Menu;

import org.json.JSONObject;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by valeputra on 6/22/17.
 */

public class FragmentUserAccountBsa extends Fragment {
    private TextView tv_user, tv_nama, tv_user_hp, tv_user_password, tv_user_rekening, tv_user_status,
            tv_user_alamat, tv_user_bisnis;
    private Button btn_ganti_pasword;
    private CustomImageViewRounded ivUserAvatar;
    private ImageView img_user;
    private RealmResults<UserBrissmart> acc;
//    private AppPreferences appPref;
    private Realm realm;
    private View view;
    private ConstraintLayout layEregister, layHelp;

    private ProgressBar progressBarAvatar;

    private String status, dsn;

    SoapObject Request;
    SoapSerializationEnvelope soapEnvelope;
    HttpTransportSE transport;
    SoapPrimitive resultString;
    JSONObject json, resdata;
    String responseJSON, msgErr, response, rcode, message, name, phonenumber, acccore,
            agenstatus, address, business;

    public FragmentUserAccountBsa() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_user_account_bsa, container, false);

        tv_user = (TextView) view.findViewById(R.id.tv_user);
        tv_nama = (TextView) view.findViewById(R.id.tv_nama);
        tv_user_hp = (TextView) view.findViewById(R.id.tv_user_hp);
        tv_user_password = (TextView) view.findViewById(R.id.tv_user_password);
        btn_ganti_pasword = (Button) view.findViewById(R.id.ganti_password);

        ivUserAvatar = (CustomImageViewRounded) view.findViewById(R.id.iv_user_profile);
        img_user = (ImageView) view.findViewById(R.id.img_user);

        img_user.setImageResource(R.drawable.ico_user_circle);
//        progressBarAvatar = (ProgressBar) view.findViewById(R.id.progress_avatar);

        tv_user.setText(getStatus());
        tv_nama.setText(getUser());
        tv_user_hp.setText(getDsn());

        btn_ganti_pasword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle datas = new Bundle();
                Intent intent = new Intent(getContext(), AGantiPasswordBsa.class);
                datas.putString("status", "nondirect");
                intent.putExtras(datas);
                startActivity(intent);
            }
        });

        return view;
    }

    public String getStatus() {
        realm = Realm.getDefaultInstance();
        RealmResults<UserBrissmart> user = realm.where(UserBrissmart.class).findAll();
        status = (user.get(0).getStatus().isEmpty()) ? status.toUpperCase() : user.get(0).getStatus();
        return status;
    }

    public String getDsn() {
        realm = Realm.getDefaultInstance();
        RealmResults<UserBrissmart> user = realm.where(UserBrissmart.class).findAll();
        dsn = (user.get(0).getDsn().isEmpty()) ? dsn.toUpperCase() : user.get(0).getDsn();
        return dsn;
    }

    public String getUser() {
        realm = Realm.getDefaultInstance();
        RealmResults<UserBrissmart> user = realm.where(UserBrissmart.class).findAll();
        name = (user.get(0).getName().isEmpty() ) ? name.toUpperCase() : user.get(0).getName();
        return name;
    }

    @Override
    public void onResume() {
        super.onResume();
        setUserInfo();
//        setMultiAccStatus();
    }

    private void setUserInfo() {
//        if (!(mainActivity.userAvatar == null)) {
//            progressBarAvatar.setVisibility(View.GONE);
//            ivUserAvatar.setImageBitmap(ConverterBase64.decodeBase64(mainActivity.userAvatar));
//        }
//
//        if (! (mainActivity.userAvatar == null))
//            tvUsername.setText(mainActivity.userName);
//
//        tvPhoneNum.setText("+"+ mainActivity.userPhone);
//        tvLastTrx.setText(setLastTrx());
    }

//    private void goToEditUserProfile() {
//        Bundle userDatas = new Bundle();
//        Intent intentProfile = new Intent(getContext(), UserProfileActivity.class);
//        userDatas.putString("name", mainActivity.userName);
//        userDatas.putString("phone", mainActivity.userPhone);
//        intentProfile.putExtras(userDatas);
//        startActivity(intentProfile);
//    }

//    private String setLastTrx() {
//        return (appPref.getLastTrx() == "") ? "Belum Ada" : appPref.getLastTrx();
//    }
//
//    private ArrayList<ListMenu> getListAcc(RealmResults<Account> acc) {
//        ArrayList<ListMenu> items = new ArrayList<>();
//        for (int i=0 ; i<acc.size() ; i++) {
//            items.add(new ListMenu(0, acc.get(i).getAccount() + " (" + acc.get(i).getAlias() + ")", 0));
//        }
//        return items;
//    }
//
//    private void setMultiAccStatus() {
//        acc = realm.where(Account.class).findAll();
//
//        if (acc.size() > 1) {
//            setDefaultMultiAcc(acc);
//            statusMultiAcc = "Aktif";
//        } else {
//            ImageView iv = (ImageView) view.findViewById(R.id.ic_next_multiacc);
//            iv.setVisibility(View.GONE);
//            statusMultiAcc = "Belum Teregister";
//        }
//
//        tvMultiAccStatus.setText(statusMultiAcc);
//    }

//    private String getPhoneNum() {
//        RealmResults<User> user = realm.where(User.class).findAll();
//        phoneNumber = ( user.get(0).getMsisdn().isEmpty() ) ? phoneNumber.toUpperCase() : user.get(0).getMsisdn();
//        return phoneNumber;
//    }

//    private String getUsername() {
//        if (! (AppPreferences.getUserName(getContext()).isEmpty() || AppPreferences.getUserName(getContext()) == null) ) {
//            userName =  AppPreferences.getUserName(getContext()).toString();
//        }
//        return userName;
//    }

//    private void setDefaultMultiAcc(final RealmResults<Account> listAcc) {
//        final ArrayList<ListMenu> lvAcc = getListAcc(listAcc);
//        layMultiAcc.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                final BottomSheetDialog dialog = new BottomSheetDialog(getActivity());
//                View sheetView = getActivity().getLayoutInflater().inflate(R.layout.dialog_bottom_sheet_multiacc, null);
//                TextView tvResetAcc = (TextView) sheetView.findViewById(R.id.tv_reset_acc);
//                dialog.setContentView(sheetView);
//                BottomSheetBehavior bottomSheetBehavior = BottomSheetBehavior.from((View) sheetView.getParent());
//                bottomSheetBehavior.setPeekHeight(
//                        (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 200, getResources().getDisplayMetrics()));
//                final CustomBottomSheetDialogListView listView = (CustomBottomSheetDialogListView) dialog.findViewById(R.id.lv_bottom_sheet);
//
//                AccDialogAdapter adapter = new AccDialogAdapter(getActivity(), lvAcc);
//                listView.setAdapter(adapter);
//                listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//                    @Override
//                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                        final String str_acc = lvAcc.get(position).getTitle();
//                        final String acc_pos = parent.getAdapter().getItem(position).toString();
//                        final AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
//                        builder.setMessage("Atur '" + str_acc + "' sebagai default rekening ?")
//                                .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
//                                    public void onClickAsk(@SuppressWarnings("unused") final DialogInterface dialogConfirm, @SuppressWarnings("unused") final int id) {
//                                        appPref.setSettingUserPrefByType(Constants.Preferences.SETTINGS_KEY[0], acc_pos.trim());
//                                        dialogConfirm.dismiss();
//                                        AppUtil.showToastShort(getContext(), "Sukses");
//                                    }
//                                })
//                                .setNegativeButton("Batal", new DialogInterface.OnClickListener() {
//                                    public void onClickAsk(final DialogInterface dialogConfirm, @SuppressWarnings("unused") final int id) {
//                                        dialogConfirm.dismiss();
//                                    }
//                                });
//
//                        dialog.dismiss();
//                        final AlertDialog alert = builder.create();
//                        alert.show();
//                    }
//                });
//                dialog.show();
//
//                tvResetAcc.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClickAsk(View v) {
//                        final AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
//                        builder.setMessage("Anda akan melakukan Reset pengaturan default rekening ?")
//                                .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
//                                    public void onClickAsk(@SuppressWarnings("unused") final DialogInterface dialogConfirm, @SuppressWarnings("unused") final int id) {
//                                        appPref.removeByKey(Constants.Preferences.SETTING_USER_PREF + Constants.Preferences.SETTINGS_KEY[0]);
//                                        dialogConfirm.dismiss();
//                                        AppUtil.showToastShort(getContext(), "Sukses");
//                                    }
//                                })
//                                .setNegativeButton("Batal", new DialogInterface.OnClickListener() {
//                                    public void onClickAsk(final DialogInterface dialogConfirm, @SuppressWarnings("unused") final int id) {
//                                        dialogConfirm.dismiss();
//                                    }
//                                });
//
//                        dialog.dismiss();
//                        final AlertDialog alert = builder.create();
//                        alert.show();
//                    }
//                });
//                Bundle datas = new Bundle();
////                datas.putString(Menu.MENU_ID, childMenu);
//                Intent intent = new Intent(getContext(), ManageAccountActivity.class);
//                intent.putExtras(datas);
//                getContext().startActivity(intent);
//
//            }
//        });
//    }

    /****************************** ADAPTER **********************************/

//    private class AccDialogAdapter extends BaseAdapter {
//        private LayoutInflater layoutinflater;
//        private List<ListMenu> listStorage;
//        private Context dContext;
//        private AccDialogAdapter.ViewHolder listViewHolder;
//
//        public AccDialogAdapter(Context context, List<ListMenu> acc) {
//            this.dContext = context;
//            layoutinflater =(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//            listStorage = acc;
//        }
//
//        @Override
//        public int getCount() {
//            return listStorage.size();
//        }
//
//        @Override
//        public Object getItem(int position) {
//            return position;
//        }
//
//        @Override
//        public long getItemId(int position) {
//            return position;
//        }
//
//        @Override
//        public View getView(int position, View convertView, ViewGroup parent) {
//            if (convertView == null) {
//                listViewHolder = new ViewHolder();
//                convertView = layoutinflater.inflate(R.layout.lv_content_alistview_construct, parent, false);
//                listViewHolder.mIcon = (ImageView) convertView.findViewById(R.id.iv_icon_submenu);
//                listViewHolder.mTitle = (TextView) convertView.findViewById(R.id.tv_title_submenu);
//                listViewHolder.mIconNext = (ImageView) convertView.findViewById(R.id.iv_icon_next_submenu);
//
//                listViewHolder.mIcon.setVisibility(View.GONE);
//                listViewHolder.mIconNext.setVisibility(View.GONE);
//
//                convertView.setTag(listViewHolder);
//            } else {
//                listViewHolder = (AccDialogAdapter.ViewHolder) convertView.getTag();
//            }
//
//            listViewHolder.mIcon.setImageResource(listStorage.get(position).getIcon());
//            listViewHolder.mTitle.setText(listStorage.get(position).getTitle());
//            listViewHolder.mIconNext.setImageResource(listStorage.get(position).getIcon_next());
//
//            if (String.valueOf(position).equalsIgnoreCase(AppPreferences.getSettingUserPrefByType(dContext, Constants.Preferences.SETTINGS_KEY[0]))) {
//                listViewHolder.mTitle.setTextColor(ContextCompat.getColor(dContext, R.color.orange));
//                listViewHolder.mTitle.setTypeface(null, Typeface.BOLD);
//            } else {
//                listViewHolder.mTitle.setTextColor(ContextCompat.getColor(dContext, R.color.colorAccent));
//                listViewHolder.mTitle.setTypeface(null, Typeface.NORMAL);
//            }
//
//            return convertView;
//        }
//
//        private class ViewHolder {
//            ImageView mIcon;
//            TextView mTitle;
//            ImageView mIconNext;
//        }
//    }
}
