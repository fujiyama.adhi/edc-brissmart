package com.bris.brissmartmobile.fragment;

import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;

import com.bris.brissmartmobile.R;
import com.bris.brissmartmobile.listener.ConfirmTrxListener;
import com.bris.brissmartmobile.listener.FavClickListener;
import com.bris.brissmartmobile.model.Favorite;
import com.bris.brissmartmobile.util.AppPreferences;
import com.bris.brissmartmobile.util.AppUtil;
import com.bris.brissmartmobile.util.AppUtilPrint;
import com.bris.brissmartmobile.util.Menu;
import com.bris.brissmartmobile.util.NothingSelectedSpinnerAdapter;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by valeputra on 6/22/17.
 */

public class FPembayaranTunai extends Fragment implements ConfirmTrxListener, FavClickListener,
        TextWatcher, View.OnFocusChangeListener, View.OnTouchListener, DialogInterface.OnKeyListener {

    private Spinner spinner_jenis_tagihan;
    private AutoCompleteTextView autocomplete_nama_institusi;
    private EditText et1;
    private TextInputLayout til1, til2, tilNorek;
    private Button btn1, btnConfirm;
    private ImageButton btnFav;
    private String id_pelanggan, message, product, kode_transaksi;
    private String jenis_tagihan, jenis_pendidikan;
    private String pageId, tid;
    private String id, merchantid, swref, owner, su, sup, tariff, power, totOutstdBill,
            realTotOutstdBill, amount, periode, trx_name, reg_date, trx_amount, BIT61, bit11Biller, adminfee;
    private String data1, data4 = null, submenutrx, namajenisfav;
    private String[] listPLNPostpaid = null, listTelepon = null, listInternet = null,
            listTVBerbayar = null;
    private String bulantagihan, bulantagihan_proses;
    private JSONObject params, details;
    private ArrayAdapter<String> adapterProduct;
    private List<Favorite> favDatas = new ArrayList<>();
    private JSONObject json, resdata;
    private JSONArray institusi;
    private String[] institution;
    private String msgErr, rcode;
    private Realm realm;
    private AlertDialog AlertDialog = null,
            confirmationDialog = null;
    private View view;

    private AppPreferences appPref;

    public FPembayaranTunai() {
    }

    public View onCreateView(final LayoutInflater inflater, @Nullable final ViewGroup container, @Nullable Bundle savedInstanceState) {
        String produk = getArguments().getString(Menu.MENU_ID);

        if (produk.contains("Tunai")) {
            pageId = produk.substring(0, produk.indexOf(" Tunai"));
        }
        else {
            pageId = produk.substring(0, produk.indexOf(" Kartu"));
        }
        Log.d("Cek pageId", pageId);

        appPref = new AppPreferences(getContext());
        tid = appPref.getDeviceTid();

        listPLNPostpaid = getResources().getStringArray(R.array.product_bayar_electricity);
        listTelepon = getResources().getStringArray(R.array.product_bayar_telephone);
        listInternet = getResources().getStringArray(R.array.product_bayar_internet);
        listTVBerbayar = getResources().getStringArray(R.array.product_bayar_tv);

        if (pageId.equalsIgnoreCase("PLN Postpaid") || pageId.equalsIgnoreCase("Telepon") || pageId.equalsIgnoreCase("Internet") || pageId.equalsIgnoreCase("TV Berbayar")) {
            view = inflater.inflate(R.layout.activity_pembayaran_tagihan, container, false);

            spinner_jenis_tagihan = (Spinner) view.findViewById(R.id.spinner_jenis_tagihan);
            til1 = (TextInputLayout) view.findViewById(R.id.til_id_pelanggan);

            setSpinnerProduct(pageId);

            et1 = (EditText) view.findViewById(R.id.et_id_pelanggan);

        } else if (pageId.equalsIgnoreCase("Tiket KAI") || pageId.equalsIgnoreCase("Tokopedia")) {
            view = inflater.inflate(R.layout.activity_pembayaran_tiket, container, false);

            til1 = (TextInputLayout) view.findViewById(R.id.til_kode_bayar);
            et1 = (EditText) view.findViewById(R.id.et_kode_bayar);

        } else if (pageId.equalsIgnoreCase("Institusi") || pageId.equalsIgnoreCase("Asuransi")) {
            view = inflater.inflate(R.layout.activity_pembayaran_institusi, container, false);

            til1 = (TextInputLayout) view.findViewById(R.id.til_nomor_id);
            et1 = (EditText) view.findViewById(R.id.et_nomor_id);

            if (pageId.equalsIgnoreCase("Institusi")) {
                til1.setHint("Nomor ID");
            }
            if (pageId.equalsIgnoreCase("Asuransi")) {
                til1.setHint("Nomor Asuransi");
            }

            til2 = (TextInputLayout) view.findViewById(R.id.til_nama_institusi);
            autocomplete_nama_institusi = (AutoCompleteTextView) view.findViewById(R.id.act_nama_institusi);
            autocomplete_nama_institusi.setFocusable(false);
            Log.d("ID TEXT", pageId);
            setAutoCompleteText(pageId);

        } else if (pageId.equalsIgnoreCase("Pendidikan")) {
            view = inflater.inflate(R.layout.activity_pembayaran_pendidikan, container, false);

            spinner_jenis_tagihan = (Spinner) view.findViewById(R.id.spinner_jenis_pendidikan);
            setSpinnerProduct(pageId);

            til1 = (TextInputLayout) view.findViewById(R.id.til_nomor_siswa);
            et1 = (EditText) view.findViewById(R.id.et_nomor_siswa);

            til2 = (TextInputLayout) view.findViewById(R.id.til_nama_institusi);
            autocomplete_nama_institusi = (AutoCompleteTextView) view.findViewById(R.id.act_nama_institusi);
            autocomplete_nama_institusi.setFocusable(false);
            Log.d("ID TEXT", pageId);
            setAutoCompleteText(pageId);

        }

        tilNorek = (TextInputLayout) view.findViewById(R.id.til_nomor_bsa);
        tilNorek.setVisibility(View.GONE);

        btn1 = (Button) view.findViewById(R.id.btn_submit);
        btnFav = (ImageButton) view.findViewById(R.id.btn_daftar_fav);
        if (pageId.equalsIgnoreCase("Tiket KAI")) {
            btnFav.setVisibility(View.INVISIBLE);
        }

        et1.setFocusable(false);

        if (!(pageId.equalsIgnoreCase("Tiket KAI") || pageId.equalsIgnoreCase("Tokopedia"))) {
            et1.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    if (pageId.equalsIgnoreCase("Institusi") || pageId.equalsIgnoreCase("Asuransi") || pageId.equalsIgnoreCase("Pendidikan")) {
                        String val = autocomplete_nama_institusi.getText().toString() + "";
                        Boolean code = Arrays.asList(institution).contains(val);
                        if (!code) {
                            if (pageId.equalsIgnoreCase("Pendidikan")) {
                                if (spinner_jenis_tagihan.getSelectedItem() != null) {
                                    til2.setErrorEnabled(true);
                                    til2.setError("Kode Institusi Pendidikan tidak valid");
                                } else {
                                    et1.setFocusable(false);
                                }
                            } else {
                                til2.setErrorEnabled(true);
                                til2.setError("Kode Institusi Pendidikan tidak valid");
                            }
                        } else {
                            til2.setErrorEnabled(false);
                            til2.setError(null);

                            et1.setOnTouchListener(new View.OnTouchListener() {
                                @Override
                                public boolean onTouch(View v, MotionEvent event) {

                                    et1.setFocusableInTouchMode(true);

                                    return false;
                                }
                            });
                        }
                    }
                    return false;
                }
            });
        }
        if (pageId.equalsIgnoreCase("Tiket KAI") || pageId.equalsIgnoreCase("Tokopedia")) {
            et1.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {

                    et1.setFocusableInTouchMode(true);

                    return false;
                }
            });
        }

        AppUtil.toolbarRegular(getContext(), view, pageId);
        realm = Realm.getDefaultInstance();

        try {
            onActionButton();
        } catch (Exception e) {
            e.printStackTrace();
            AppUtil.displayDialog(getContext(), "Maaf, Terjadi Kesalahan");
        }

        return view;
    }

    private void setSpinnerProduct(String title) {
        if (title.equalsIgnoreCase("PLN Postpaid")) {
            adapterProduct = new ArrayAdapter<>(getContext(), R.layout.spinner_style,
                    getResources().getStringArray(R.array.product_bayar_electricity));
            adapterProduct.setDropDownViewResource(android.R.layout.simple_list_item_1);
            spinner_jenis_tagihan.setAdapter(new NothingSelectedSpinnerAdapter(
                    adapterProduct,
                    R.layout.spinner_product_nothing_selected,
                    getContext()
            ));
        } else if (title.equalsIgnoreCase("Telepon")) {
            adapterProduct = new ArrayAdapter<>(getContext(), R.layout.spinner_style,
                    getResources().getStringArray(R.array.product_bayar_telephone));
            adapterProduct.setDropDownViewResource(android.R.layout.simple_list_item_1);
            spinner_jenis_tagihan.setAdapter(new NothingSelectedSpinnerAdapter(
                    adapterProduct,
                    R.layout.spinner_product_nothing_selected,
                    getContext()
            ));
        } else if (title.equalsIgnoreCase("Internet")) {
            adapterProduct = new ArrayAdapter<>(getContext(), R.layout.spinner_style,
                    getResources().getStringArray(R.array.product_bayar_internet));
            adapterProduct.setDropDownViewResource(android.R.layout.simple_list_item_1);
            spinner_jenis_tagihan.setAdapter(new NothingSelectedSpinnerAdapter(
                    adapterProduct,
                    R.layout.spinner_product_nothing_selected,
                    getContext()
            ));
        } else if (title.equalsIgnoreCase("TV Berbayar")) {
            adapterProduct = new ArrayAdapter<>(getContext(), R.layout.spinner_style,
                    getResources().getStringArray(R.array.product_bayar_tv));
            adapterProduct.setDropDownViewResource(android.R.layout.simple_list_item_1);
            spinner_jenis_tagihan.setAdapter(new NothingSelectedSpinnerAdapter(
                    adapterProduct,
                    R.layout.spinner_product_nothing_selected,
                    getContext()
            ));
        } else if (title.equalsIgnoreCase("Pendidikan")) {
            adapterProduct = new ArrayAdapter<>(getContext(), R.layout.spinner_style,
                    getResources().getStringArray(R.array.product_bayar_pendidikan));
            adapterProduct.setDropDownViewResource(android.R.layout.simple_list_item_1);
            spinner_jenis_tagihan.setAdapter(new NothingSelectedSpinnerAdapter(
                    adapterProduct,
                    R.layout.spinner_product_nothing_selected,
                    getContext()
            ));
        }
        spinner_jenis_tagihan.setOnItemSelectedListener(new onItemListener());
    }

    private class onItemListener implements AdapterView.OnItemSelectedListener {
        @Override
        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
            if (pageId.equalsIgnoreCase("PLN Postpaid") || pageId.equalsIgnoreCase("Telepon") || pageId.equalsIgnoreCase("Internet") || pageId.equalsIgnoreCase("TV Berbayar")) {
                if (spinner_jenis_tagihan.getSelectedItem() != null) {
                    if (pageId.equalsIgnoreCase("Telepon")) {
                        til1.setHint("Nomor Telepon");
                    } else {
                        if (spinner_jenis_tagihan.getSelectedItem().toString().equalsIgnoreCase("PLN Non Tagihan Listrik")) {
                            til1.setHint("Nomor Registrasi");
                        } else {
                            til1.setHint("Nomor Pelanggan");
                        }
                    }
                    et1.setOnTouchListener(new View.OnTouchListener() {
                        @Override
                        public boolean onTouch(View v, MotionEvent event) {

                            et1.setFocusableInTouchMode(true);

                            return false;
                        }
                    });
                    et1.setFocusable(true);
                    et1.setFocusableInTouchMode(true);
                } else {
                    et1.setFocusable(false);
                    til1.setHint("Jenis Tagihan belum dipilih");
                }
            }
            if (pageId.equalsIgnoreCase("Pendidikan")) {
                if (spinner_jenis_tagihan.getSelectedItem() != null) {
                    if (spinner_jenis_tagihan.getSelectedItem().equals("Sekolah")) {
                        til1.setHint("Nomor Siswa");
                    } else {
                        til1.setHint("Nomor Mahasiswa");
                    }
                } else {
                    til1.setHint("Jenis Pendidikan belum dipilih");
                    til2.setHint("Jenis Pendidikan belum dipilih");
                }

                jenis_pendidikan = String.valueOf(spinner_jenis_tagihan.getSelectedItem());
                setAutoCompleteText(jenis_pendidikan);
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> adapterView) {
        }
    }

    private void setAutoCompleteText(final String pendidikan) {

        Log.d("Header", pendidikan);

        Thread thread = new Thread(new Runnable() {

            @Override
            public void run() {
                try {
                    String kode = "";
                    if (pendidikan.equalsIgnoreCase("Asuransi")) {
                        kode = "1";
                    } else if (pendidikan.equalsIgnoreCase("Institusi")) {
                        kode = "2";
                    } else if (pendidikan.equalsIgnoreCase("Sekolah")) {
                        kode = "3";
                    } else if (pendidikan.equalsIgnoreCase("Perguruan Tinggi")) {
                        kode = "4";
                    }
                    Log.d("kode", kode);

                    String jsonData = AppUtil.setAutoCompleteInstitusi(kode);
                    Log.d("data", jsonData);
                    json = new JSONObject(jsonData);
                    Log.d("json", json.toString());
                    institusi = json.getJSONArray("data");
                    Log.d("array", institusi.toString());

                    institution = new String[institusi.length()];
                    for (int i = 0; i < institusi.length(); i++) {
                        JSONObject jo = institusi.getJSONObject(i);
                        institution[i] = jo.getString("institusi_code") + " - " + jo.getString("institusi_name");
                    }
                    Log.d("institusi", institution.toString());

                    autocomplete_nama_institusi.setThreshold(1);

                    getActivity().runOnUiThread(new Runnable() {
                        public void run() {
                            ArrayAdapter<String> bankListAdapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_list_item_1,
                                    institution);
                            bankListAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            autocomplete_nama_institusi.setAdapter(bankListAdapter);
                        }
                    });

                    autocomplete_nama_institusi.setOnTouchListener(new View.OnTouchListener() {
                        @Override
                        public boolean onTouch(View v, MotionEvent event) {
                            autocomplete_nama_institusi.setFocusableInTouchMode(true);
                            autocomplete_nama_institusi.showDropDown();
                            autocomplete_nama_institusi.requestFocus();
                            return false;
                        }
                    });

                    autocomplete_nama_institusi.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                        @Override
                        public void onFocusChange(View v, boolean hasFocus) {
                            String val = autocomplete_nama_institusi.getText().toString() + "";
                            Boolean code = Arrays.asList(institution).contains(val);
                            if (!hasFocus)
                                if (!code) {
                                    til2.setErrorEnabled(true);
                                    til2.setError("Kode Institusi Pendidikan tidak valid");
                                } else {
                                    til2.setErrorEnabled(false);
                                    til2.setError(null);
                                }
                        }
                    });

                    autocomplete_nama_institusi.addTextChangedListener((TextWatcher) getContext());

                    if (pendidikan.equalsIgnoreCase("Asuransi")) {
                        til2.setHint("Pilih Asuransi");
                    } else if (pendidikan.equalsIgnoreCase("Institusi")) {
                        til2.setHint("Pilih Institusi");
                    } else if (pendidikan.equalsIgnoreCase("Sekolah")) {
                        til2.setHint("Pilih Sekolah");
                    } else if (pendidikan.equalsIgnoreCase("Perguruan Tinggi")) {
                        til2.setHint("Pilih Perguruan Tinggi");
                    }
                } catch (Exception ex) {
                    msgErr = "true";
                    Log.d("THREAD a", ex.toString());

                    getActivity().runOnUiThread(new Runnable() {
                        public void run() {
                            AppUtil.displayDialog(getContext(), "Maaf, Terjadi Kesalahan");
                        }
                    });

                }
            }
        });
        thread.start();
    }

    private void onActionButton() {
        btnFav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    if (pageId.equalsIgnoreCase("Institusi") || pageId.equalsIgnoreCase("Asuransi") || pageId.equalsIgnoreCase("Pendidikan") || pageId.equalsIgnoreCase("Tokopedia")) {
                        product = pageId;
                        if (pageId.equalsIgnoreCase("Pendidikan")) {
                            if (spinner_jenis_tagihan.getSelectedItem() == null) {
                                AppUtil.displayDialog(getContext(), "Pilih Produk dahulu untuk mendapatkan/simpan data favorit Anda");
                                return;
                            }
                        }
                    } else if (pageId.equalsIgnoreCase("PLN Postpaid") || pageId.equalsIgnoreCase("Telepon") || pageId.equalsIgnoreCase("Internet") || pageId.equalsIgnoreCase("TV Berbayar")) {
                        if (spinner_jenis_tagihan.getSelectedItem() == null) {
                            AppUtil.displayDialog(getContext(), "Pilih Produk dahulu untuk mendapatkan/simpan data favorit Anda");
                            return;
                        }
                        product = spinner_jenis_tagihan.getSelectedItem().toString();
                    }

                    if (pageId.equalsIgnoreCase("PLN Postpaid")) {
                        if (product.equalsIgnoreCase(listPLNPostpaid[0])) {
                            data1 = "TAGLIS";
                        } else {
                            data1 = "NONTAG";
                        }
                        data4 = product;
                        submenutrx = "7001";
                        namajenisfav = "Pembayaran PLN";
                    } else if (pageId.equalsIgnoreCase("Telepon")) {
                        if (product.equalsIgnoreCase(listTelepon[0]) || product.equalsIgnoreCase(listTelepon[1])) {
                            data1 = "TELKOM";
                        } else if (product.equalsIgnoreCase(listTelepon[2])) {
                            data1 = "HALO";
                        } else if (product.equalsIgnoreCase(listTelepon[3])) {
                            data1 = "XL";
                        } else if (product.equalsIgnoreCase(listTelepon[4])) {
                            data1 = "SMARTFREN";
                        }
                        data4 = product;
                        submenutrx = "7002";
                        namajenisfav = "Pembayaran Telepon";
                    } else if (pageId.equalsIgnoreCase("Internet")) {
                        if (product.equalsIgnoreCase(listInternet[0])) {
                            data1 = "TELKOM";
                        }
                        data4 = product;
                        submenutrx = "7003";
                        namajenisfav = "Pembayaran Internet";
                    } else if (pageId.equalsIgnoreCase("TV Berbayar")) {
                        if (product.equalsIgnoreCase(listTVBerbayar[0])) {
                            data1 = "TELKOM";
                        } else if (product.equalsIgnoreCase(listTVBerbayar[1])) {
                            data1 = "BIGTV";
                        } else if (product.equalsIgnoreCase(listTVBerbayar[2])) {
                            data1 = "INDOVISION";
                        }
                        data4 = product;
                        submenutrx = "7004";
                        namajenisfav = "Pembayaran TV";
                    } else if (pageId.equalsIgnoreCase("Tokopedia")) {
                        data1 = "TOKOPEDIA";
                        data4 = product;
                        submenutrx = "7009";
                        namajenisfav = "Pembayaran Tokopedia";
                    } else if (pageId.equalsIgnoreCase("Institusi")) {
                        submenutrx = "7006";
                        namajenisfav = "Pembayaran Institusi";
                    } else if (pageId.equalsIgnoreCase("Pendidikan")) {
                        if (jenis_pendidikan.equalsIgnoreCase("Sekolah")) {
                            data4 = "S";
                        } else if (jenis_pendidikan.equalsIgnoreCase("Perguruan Tinggi")) {
                            data4 = "P";
                        }
                        submenutrx = "7007";
                        namajenisfav = "Pembayaran Pendidikan";
                    } else if (pageId.equalsIgnoreCase("Asuransi")) {
                        submenutrx = "7008";
                        namajenisfav = "Pembayaran Asuransi";
                    }

                    final String id_product = et1.getText().toString().trim();

                    if (id_product.equalsIgnoreCase("")) {
                        if (pageId.equalsIgnoreCase("Institusi")) {
                            getFavData("7006");
                        } else if (pageId.equalsIgnoreCase("Pendidikan")) {
                            getFavData(data4);
                        } else if (pageId.equalsIgnoreCase("Asuransi")) {
                            getFavData("7008");
                        } else {
                            getFavData(product);
                        }
                        AppUtil.displayDialogFavorit(getContext(), favDatas, FPembayaranTunai.this, "BAYAR");
                        return;
                    }

                    AlertDialog dialogAlert;
                    AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                    builder.setCancelable(true);
                    builder.setMessage("Simpan data transaksi ini sebagai data favorit?");
                    builder.setPositiveButton("Simpan", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface alertdialog, int which) {
                            alertdialog.cancel();

                            if (AppUtil.checkFavorite("CASH", id_product, data1, AppUtil.getDsn())) {
                                android.support.v4.app.FragmentManager manager = getActivity().getSupportFragmentManager();

                                Fragment fragment;
                                android.support.v4.app.FragmentTransaction transaction = manager.beginTransaction();
                                fragment = new FInsertUpdateDataFavorit();
                                Bundle bundle = new Bundle();
                                bundle.putString("title", product);
                                bundle.putString("jenispembayaran", "CASH");
                                bundle.putString("menutrx", "7000");
                                bundle.putString("data1", data1);
                                bundle.putString("data3", id_product);
                                bundle.putString("data4", data4);
                                bundle.putString("submenutrx", submenutrx);
                                bundle.putString("namajenisfav", namajenisfav);
                                if (pageId.equalsIgnoreCase("Institusi") || pageId.equalsIgnoreCase("Pendidikan")
                                        || pageId.equalsIgnoreCase("Asuransi")) {
                                    bundle.putString("bank", autocomplete_nama_institusi.getText().toString());
                                }

                                fragment.setArguments(bundle);
                                transaction.replace(R.id.fragment_payment_general, fragment);
                                transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
                                transaction.addToBackStack(null);
                                transaction.commit();

                            } else {
                                AppUtil.displayDialog(getContext(), "Data favorit Telah Ada");
                            }
                        }
                    });
                    builder.setNegativeButton("Pilih favorit", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface alertdialog, int which) {
                            alertdialog.cancel();
                            if (pageId.equalsIgnoreCase("Institusi")) {
                                getFavData("7006");
                            } else if (pageId.equalsIgnoreCase("Pendidikan")) {
                                getFavData(data4);
                            } else if (pageId.equalsIgnoreCase("Asuransi")) {
                                getFavData("7008");
                            } else {
                                getFavData(product);
                            }
                            AppUtil.displayDialogFavorit(getContext(), favDatas, FPembayaranTunai.this, "BAYAR");
                        }
                    });

                    dialogAlert = builder.create();
                    dialogAlert.show();
                } catch (Exception e) {
                    e.printStackTrace();
                    AppUtil.displayDialog(getContext(), "Maaf, Terjadi Kesalahan");
                }
            }
        });

        et1.addTextChangedListener(this);

        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (!(pageId.equalsIgnoreCase("Tiket KAI") || pageId.equalsIgnoreCase("Tokopedia") || pageId.equalsIgnoreCase("Institusi") || pageId.equalsIgnoreCase("Asuransi"))) {
                    if (spinner_jenis_tagihan.getSelectedItem() == null) {
                        AppUtil.displayDialog(getContext(), "Jenis Tagihan Tidak Boleh Kosong!");
                        return;
                    } else {
                        product = spinner_jenis_tagihan.getSelectedItem().toString();
                    }
                }

                if (et1.getText().toString().length() == 0) {
                    til1.setError(til1.getHint() + " Tidak Boleh Kosong!");
                    return;
                } else {
                    id_pelanggan = et1.getText().toString();
                }

                SoapActivity action = new SoapActivity();
                action.execute();

            }
        });

    }

    @Override
    public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
            AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
            builder.setCancelable(false);
            builder.setMessage("Apakah anda yakin akan membatalkan proses Pembayaran " + pageId + "?");
            builder.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                    confirmationDialog.dismiss();
                }
            });
            builder.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                    confirmationDialog.show();
                }
            });
            if (AlertDialog == null || !AlertDialog.isShowing()) {
                AlertDialog = builder.create();
                AlertDialog.show();
            }
            return true;
        }
        return false;
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        if (et1.getText().toString().length() > 0) {
            til1.setError(null);
        }
    }

    @Override
    public void afterTextChanged(Editable s) {

    }

    @Override
    public void getFav(String label, String category) {

    }

    @Override
    public void onclikFav(String label, String kode_produk) {
        RealmResults<Favorite> fav = realm.where(Favorite.class)
                .equalTo("menutrx", "7000")
                .equalTo("jenispembayaran", "CASH")
                .equalTo("namafav", label)
                .equalTo("data1", kode_produk)
                .findAll();

        if (fav.size() > 0) {
            if (pageId.equalsIgnoreCase("Institusi") || pageId.equalsIgnoreCase("Pendidikan") || pageId.equalsIgnoreCase("Asuransi")) {
                String data1 = fav.get(0).getData1();
                String kodeinstitusi = data1.substring(0, data1.indexOf("|"));
                String namainstitusi = data1.substring(data1.indexOf("|") + 1, data1.length());
                autocomplete_nama_institusi.setText(kodeinstitusi + " - " + namainstitusi);
            }
            et1.setText(fav.get(0).getData3());
            AppUtil.closeListDialog();
        }
    }

    @Override
    public void deleteFav(String jenispembayaran, String menutrx, String submenutrx, String label) {

    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {

    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        return false;
    }

    @Override
    public void onConfirmClick(String pin) {

    }

    @Override
    public void onCancelClick() {

    }

    public void getFavData(String kode_produk) {
        if (pageId.equalsIgnoreCase("Institusi") || pageId.equalsIgnoreCase("Asuransi")) {
            RealmResults<Favorite> fav = realm.where(Favorite.class)
                    .equalTo("menutrx", "7000")
                    .equalTo("jenispembayaran", "CASH")
                    .equalTo("submenutrx", kode_produk)
                    .findAll();
            if (fav.size() > 0) {
                favDatas.clear();
                favDatas.addAll(fav);
            } else {
                favDatas.clear();
            }
        } else {
            RealmResults<Favorite> fav = realm.where(Favorite.class)
                    .equalTo("menutrx", "7000")
                    .equalTo("jenispembayaran", "CASH")
                    .equalTo("data4", kode_produk)
                    .findAll();

            Log.d("Cek fav", fav.toString());

            if (fav.size() > 0) {
                favDatas.clear();
                favDatas.addAll(fav);
            } else {
                favDatas.clear();
            }
        }
    }

    private class SoapActivity extends AsyncTask<Void, Void, Void> {


        @Override
        protected void onPreExecute() {

        }

        @Override
        protected Void doInBackground(Void... params) {
            convertir();
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            if (msgErr.equalsIgnoreCase("true")) {
                AppUtil.displayDialog(getContext(), message);
            }
        }

    }

    private void convertir() {

        try {
            String cmd;
            if (pageId.equalsIgnoreCase("Telepon")) {
                if (product.equalsIgnoreCase("Telepon Rumah") || product.equalsIgnoreCase("Telkom Flexi")) {
                    jenis_tagihan = "TELKOM";
                } else if (product.equalsIgnoreCase("Telkomsel Halo")) {
                    jenis_tagihan = "HALO";
                } else if (product.equalsIgnoreCase("XL")) {
                    jenis_tagihan = "XL";
                } else if (product.equalsIgnoreCase("Smartfren")) {
                    jenis_tagihan = "SMARTFREN";
                }
            } else if (pageId.equalsIgnoreCase("Internet")) {
                if (product.equalsIgnoreCase("Speedy")) {
                    jenis_tagihan = "TELKOM";
                }
            } else if (pageId.equalsIgnoreCase("TV Berbayar")) {
                if (product.equalsIgnoreCase("Transvision")) {
                    jenis_tagihan = "TELKOM";
                } else if (product.equalsIgnoreCase("BIG TV")) {
                    jenis_tagihan = "BIGTV";
                } else if (product.equalsIgnoreCase("Indovision")) {
                    jenis_tagihan = "INDOVISION";
                }
            } else if (pageId.equalsIgnoreCase("Tiket KAI")) {
                jenis_tagihan = "KAI";
            } else if (pageId.equalsIgnoreCase("Tokopedia")) {
                jenis_tagihan = "TOKOPEDIA";
            } else if (pageId.equalsIgnoreCase("Institusi") || pageId.equalsIgnoreCase("Pendidikan") || pageId.equalsIgnoreCase("Asuransi")) {
                jenis_tagihan = "SPP";
            }

            getActivity().runOnUiThread(new Runnable() {
                public void run() {
                    AppUtil.initDialogProgress(getContext());
                    AppUtil.showDialog();
                }
            });

            String dsn = AppUtil.getDsn();
            String phone_id = "62" + dsn.substring(1, dsn.length());

            JSONObject cmdparam = new JSONObject();
            if (pageId.equalsIgnoreCase("PLN Postpaid")) {
                if (product.equalsIgnoreCase("Tagihan PLN")) {
                    jenis_tagihan = "TAGLIS";
                } else {
                    jenis_tagihan = "NONTAG";
                }
                cmd = "postpaid_verify";
                cmdparam.put("id", id_pelanggan);
                cmdparam.put("merchantid", "6010");
                cmdparam.put("jenispln", jenis_tagihan);
            } else {
                cmd = "bayar_verify";
                cmdparam.put("message", AppUtil.API_MSG_PAYMENT + " " + jenis_tagihan + " " + id_pelanggan);
                cmdparam.put("msisdn", phone_id);
            }

            String soapRes = AppUtil.soapReq(AppUtil.getDsn(), cmd, cmdparam);

            if (soapRes.equalsIgnoreCase("")) {
                AppUtil.hideDialog();
                getActivity().runOnUiThread(new Runnable() {
                    public void run() {
                        AppUtil.displayDialog(getContext(), "Gagal terhubung ke server");
                    }
                });
            } else {
                json = new JSONObject(soapRes);
                resdata = json.getJSONObject("resdata");
                rcode = resdata.getString("rcode");
                msgErr = "false";

                if (!(pageId.equalsIgnoreCase("PLN Postpaid"))) {
                    message = resdata.getString("message");
                } else {
                    params = resdata.getJSONObject("params");
                    id = params.getString("id");
                    swref = params.getString("swref");
                    owner = params.getString("owner");
                    if (product.equalsIgnoreCase("Tagihan PLN")) {
                        merchantid = params.getString("merchantid");
                        su = params.getString("su");
                        sup = params.getString("sup");
                        tariff = params.getString("tariff");
                        power = params.getString("power");
                        totOutstdBill = params.getString("totOutstdBill");
                        realTotOutstdBill = params.getString("realTotOutstdBill");
                        amount = params.getString("amount");
                        periode = params.getString("period");
                        details = params.getJSONObject("details");
                        BIT61 = params.getString("BIT61");
                        bit11Biller = params.getString("bit11Biller");
                        adminfee = params.getString("adminfee");

                        int jmltag = Integer.valueOf(realTotOutstdBill);
                        int nom = Integer.parseInt(amount);
                        final String jml = NumberFormat.getNumberInstance(Locale.US).format(nom);

                        if (jmltag > 1) {
                            final String[] bulantag = new String[jmltag];
                            final String[] tahuntag = new String[jmltag];
                            final String[] periodetagihan = new String[jmltag];
                            String[] tagperiod = periode.split(",");

                            for (int i = 0; i < jmltag; i++) {
                                bulantag[i] = tagperiod[i].substring(0, 2);
                                tahuntag[i] = tagperiod[i].substring(3, tagperiod[i].length());

                                switch (bulantag[i]) {
                                    case "01":
                                        bulantag[i] = "JAN";
                                        break;
                                    case "02":
                                        bulantag[i] = "FEB";
                                        break;
                                    case "03":
                                        bulantag[i] = "MAR";
                                        break;
                                    case "04":
                                        bulantag[i] = "APR";
                                        break;
                                    case "05":
                                        bulantag[i] = "MEI";
                                        break;
                                    case "06":
                                        bulantag[i] = "JUN";
                                        break;
                                    case "07":
                                        bulantag[i] = "JUL";
                                        break;
                                    case "08":
                                        bulantag[i] = "AGU";
                                        break;
                                    case "09":
                                        bulantag[i] = "SEP";
                                        break;
                                    case "10":
                                        bulantag[i] = "OKT";
                                        break;
                                    case "11":
                                        bulantag[i] = "NOV";
                                        break;
                                    case "12":
                                        bulantag[i] = "DES";
                                        break;
                                    default:
                                        break;
                                }
                                periodetagihan[i] = bulantag[i] + tahuntag[i];
                            }

                            String bulan = Arrays.toString(periodetagihan);
                            bulantagihan = bulan.substring(1, bulan.length() - 1);

                        } else {

                            String bulan = periode.substring(0, 2);
                            String tahun = periode.substring(3, periode.length());

                            switch (bulan) {
                                case "01":
                                    bulan = "JAN";
                                    break;
                                case "02":
                                    bulan = "FEB";
                                    break;
                                case "03":
                                    bulan = "MAR";
                                    break;
                                case "04":
                                    bulan = "APR";
                                    break;
                                case "05":
                                    bulan = "MEI";
                                    break;
                                case "06":
                                    bulan = "JUN";
                                    break;
                                case "07":
                                    bulan = "JUL";
                                    break;
                                case "08":
                                    bulan = "AGU";
                                    break;
                                case "09":
                                    bulan = "SEP";
                                    break;
                                case "10":
                                    bulan = "OKT";
                                    break;
                                case "11":
                                    bulan = "NOV";
                                    break;
                                case "12":
                                    bulan = "DES";
                                    break;
                                default:
                                    break;
                            }

                            bulantagihan = bulan + tahun;
                        }

                        message = "Anda akan melakukan Pembayaran " + product + " No. Pelanggan" + id_pelanggan
                                + ", " + owner + ", " + tariff + "/ " + power + "VA, " + bulantagihan + ", Rp. " + jml
                                + ". Jika benar masukkan PIN Anda";

                    } else {

                        trx_name = params.getString("trx_name");
                        reg_date = params.getString("reg_date");
                        trx_amount = params.getString("trx_amount");

                        int nom = Integer.parseInt(trx_amount);
                        final String jml = NumberFormat.getNumberInstance(Locale.US).format(nom);

                        message = "Anda akan melakukan Pembayaran " + product + ", No. Registrasi" + id_pelanggan
                                + ", Tgl. Reg " + reg_date + ", " + trx_name + ", " + owner + ", Rp. " + jml
                                + ". Jika benar masukkan PIN Anda";
                    }
                }

                if (rcode.equalsIgnoreCase("00")) {
                    msgErr = "false";

                    new Thread() {
                        public void run() {
                            FPembayaranTunai.this.getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    AppUtil.hideDialog();
                                    try {
                                        LayoutInflater li = LayoutInflater.from(getContext());
                                        final View confirmDialog = li.inflate(R.layout.dialog_confirm_agen, null);
                                        btnConfirm = (Button) confirmDialog.findViewById(R.id.btn_submit);
                                        final TextView confirm = (TextView) confirmDialog.findViewById(R.id.txt_title_top);
                                        final EditText pin = (EditText) confirmDialog.findViewById(R.id.txt_pin);

                                        if (!(pageId.equalsIgnoreCase("PLN Postpaid"))) {
                                            String dialog = message.toString();
                                            confirm.setText(dialog.substring(0, dialog.indexOf("benar")) + " benar masukkan PIN Anda");
                                        } else {
                                            confirm.setText(message);
                                        }
                                        pin.addTextChangedListener(new TextWatcher() {
                                            @Override
                                            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                                            }

                                            @Override
                                            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                                                if (charSequence.toString().trim().length() == 6) {
                                                    btnConfirm.setEnabled(true);
                                                    btnConfirm.setBackgroundResource(R.drawable.button_shape_blue);
                                                } else {
                                                    btnConfirm.setEnabled(false);
                                                    btnConfirm.setBackgroundResource(R.drawable.button_shape_accent);
                                                }
                                            }

                                            @Override
                                            public void afterTextChanged(Editable editable) {
                                            }
                                        });

                                        AlertDialog.Builder alert = new AlertDialog.Builder(getContext());
                                        alert.setView(confirmDialog);
                                        alert.setCancelable(false);
                                        confirmationDialog = alert.create();
                                        confirmationDialog.setOnKeyListener(FPembayaranTunai.this);
                                        confirmationDialog.show();

                                        btnConfirm.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View view) {

                                                AppUtil.initDialogProgress(getContext());
                                                AppUtil.showDialog();

                                                Thread thread = new Thread(new Runnable() {

                                                    @Override
                                                    public void run() {

                                                        try {
                                                            String cmd;
                                                            String dsn = AppUtil.getDsn();
                                                            String phone_id = "62" + dsn.substring(1, dsn.length());

                                                            JSONObject cmdparam = new JSONObject();

                                                            if (pageId.equalsIgnoreCase("PLN Postpaid")) {
                                                                kode_transaksi = "7001";
                                                            } else if (pageId.equalsIgnoreCase("Telepon")) {
                                                                kode_transaksi = "7002";
                                                            } else if (pageId.equalsIgnoreCase("Internet")) {
                                                                kode_transaksi = "7003";
                                                            } else if (pageId.equalsIgnoreCase("TV Berbayar")) {
                                                                kode_transaksi = "7004";
                                                            } else if (pageId.equalsIgnoreCase("Tiket KAI")) {
                                                                kode_transaksi = "7005";
                                                            } else if (pageId.equalsIgnoreCase("Tokopedia")) {
                                                                kode_transaksi = "7009";
                                                            } else if (pageId.equalsIgnoreCase("Institusi") || pageId.equalsIgnoreCase("Pendidikan") || pageId.equalsIgnoreCase("Asuransi")) {
                                                                kode_transaksi = "7006 ";
                                                            }

                                                            if (pageId.equalsIgnoreCase("PLN Postpaid")) {
                                                                if (product.equalsIgnoreCase("Tagihan PLN")) {
                                                                    jenis_tagihan = "TAGLIS";
                                                                } else {
                                                                    jenis_tagihan = "NONTAG";
                                                                }

                                                                JSONObject params = new JSONObject();

                                                                params.put("id", id);
                                                                params.put("merchantid", merchantid);
                                                                params.put("swref", swref);
                                                                params.put("owner", owner);
                                                                params.put("su", su);
                                                                params.put("sup", sup);
                                                                params.put("tariff", tariff);
                                                                params.put("power", power);
                                                                params.put("totOutstdBill", totOutstdBill);
                                                                params.put("realTotOutstdBill", realTotOutstdBill);
                                                                params.put("amount", amount);
                                                                params.put("period", periode);
                                                                params.put("details", details);
                                                                params.put("BIT61", BIT61);
                                                                params.put("bit11Biller", bit11Biller);
                                                                params.put("src_accno", AppUtil.getAcc());
                                                                params.put("branch_id", "0001");
                                                                params.put("adminfee", adminfee);

                                                                cmd = "postpaid_save";
                                                                cmdparam.put("msgrequest", params);
                                                                cmdparam.put("jenispln", jenis_tagihan);
                                                                cmdparam.put("jenistransaksi", kode_transaksi);
                                                            } else {
                                                                cmd = "bayar_save";
                                                                cmdparam.put("message", "PIN " + pin.getText().toString());
                                                                cmdparam.put("msisdn", phone_id);
                                                                cmdparam.put("jenistransaksi", kode_transaksi);
                                                                cmdparam.put("idpelanggan", AppUtil.getDsn());
                                                            }

                                                            String soapRes = AppUtil.soapReq(AppUtil.getDsn(), cmd, cmdparam);

                                                            if (soapRes.equalsIgnoreCase("")) {
                                                                AppUtil.hideDialog();
                                                                getActivity().runOnUiThread(new Runnable() {
                                                                    public void run() {
                                                                        AppUtil.displayDialog(getContext(), "Gagal terhubung ke server");
                                                                    }
                                                                });
                                                            } else {
                                                                json = new JSONObject(soapRes);
                                                                resdata = json.getJSONObject("resdata");
                                                                rcode = resdata.getString("rcode");

                                                                if (pageId.equalsIgnoreCase("PLN Postpaid")) {
                                                                    Calendar calendar = Calendar.getInstance();
                                                                    SimpleDateFormat mdformat = new SimpleDateFormat("dd/MM/yy");
                                                                    SimpleDateFormat mtformat = new SimpleDateFormat("HH:mm");

                                                                    int nom = Integer.parseInt(amount);
                                                                    final String jml = NumberFormat.getNumberInstance(Locale.US).format(nom);
                                                                    int fee = Integer.parseInt(adminfee);
                                                                    final String admfee = NumberFormat.getNumberInstance(Locale.US).format(fee);

                                                                    if (product.equalsIgnoreCase("Tagihan PLN")) {

                                                                        int jmltag = Integer.valueOf(realTotOutstdBill);

                                                                        if (jmltag > 1) {
                                                                            final String[] bulantag = new String[jmltag];
                                                                            final String[] tahuntag = new String[jmltag];
                                                                            final String[] periodetagihan = new String[jmltag];
                                                                            String[] tagperiod = periode.split(",");

                                                                            for (int i = 0; i < jmltag; i++) {
                                                                                bulantag[i] = tagperiod[i].substring(0, 2);
                                                                                tahuntag[i] = tagperiod[i].substring(3, tagperiod[i].length());

                                                                                switch (bulantag[i]) {
                                                                                    case "01":
                                                                                        bulantag[i] = "JAN";
                                                                                        break;
                                                                                    case "02":
                                                                                        bulantag[i] = "FEB";
                                                                                        break;
                                                                                    case "03":
                                                                                        bulantag[i] = "MAR";
                                                                                        break;
                                                                                    case "04":
                                                                                        bulantag[i] = "APR";
                                                                                        break;
                                                                                    case "05":
                                                                                        bulantag[i] = "MEI";
                                                                                        break;
                                                                                    case "06":
                                                                                        bulantag[i] = "JUN";
                                                                                        break;
                                                                                    case "07":
                                                                                        bulantag[i] = "JUL";
                                                                                        break;
                                                                                    case "08":
                                                                                        bulantag[i] = "AGU";
                                                                                        break;
                                                                                    case "09":
                                                                                        bulantag[i] = "SEP";
                                                                                        break;
                                                                                    case "10":
                                                                                        bulantag[i] = "OKT";
                                                                                        break;
                                                                                    case "11":
                                                                                        bulantag[i] = "NOV";
                                                                                        break;
                                                                                    case "12":
                                                                                        bulantag[i] = "DES";
                                                                                        break;
                                                                                    default:
                                                                                        break;
                                                                                }
                                                                                periodetagihan[i] = bulantag[i] + tahuntag[i];
                                                                            }

                                                                            String bulan = Arrays.toString(periodetagihan);
                                                                            bulantagihan_proses = bulan.substring(1, bulan.length() - 1);

                                                                        } else {

                                                                            String bulan = periode.substring(0, 2);
                                                                            String tahun = periode.substring(3, periode.length());

                                                                            switch (bulan) {
                                                                                case "01":
                                                                                    bulan = "JAN";
                                                                                    break;
                                                                                case "02":
                                                                                    bulan = "FEB";
                                                                                    break;
                                                                                case "03":
                                                                                    bulan = "MAR";
                                                                                    break;
                                                                                case "04":
                                                                                    bulan = "APR";
                                                                                    break;
                                                                                case "05":
                                                                                    bulan = "MEI";
                                                                                    break;
                                                                                case "06":
                                                                                    bulan = "JUN";
                                                                                    break;
                                                                                case "07":
                                                                                    bulan = "JUL";
                                                                                    break;
                                                                                case "08":
                                                                                    bulan = "AGU";
                                                                                    break;
                                                                                case "09":
                                                                                    bulan = "SEP";
                                                                                    break;
                                                                                case "10":
                                                                                    bulan = "OKT";
                                                                                    break;
                                                                                case "11":
                                                                                    bulan = "NOV";
                                                                                    break;
                                                                                case "12":
                                                                                    bulan = "DES";
                                                                                    break;
                                                                                default:
                                                                                    break;
                                                                            }

                                                                            bulantagihan_proses = bulan + tahun;
                                                                        }

                                                                        message = mdformat.format(calendar.getTime()) + " " + mtformat.format(calendar.getTime()) +
                                                                                "\n" + swref + "\n" + id + "\n" + owner +
                                                                                "\n" + tariff + " / " + power + "VA \n" + bulantagihan_proses +
                                                                                "\n Rp. " + jml + "\n" + "Adm Rp. " + admfee + "\n" + "Sukses";

                                                                    } else {
                                                                        message = mdformat.format(calendar.getTime()) + " " + mtformat.format(calendar.getTime()) +
                                                                                "\n" + swref + "\n" + id + "\n Tgl. Reg " + reg_date + "\n" + trx_name +
                                                                                "\n" + owner + "\n Rp. " + jml + "\n" + "Adm Rp. " + admfee + "\n" + "Sukses";
                                                                    }
                                                                } else {
                                                                    message = resdata.getString("message");
                                                                }

                                                                if (rcode.equalsIgnoreCase("00")) {

                                                                    //khusus edc brissmart
                                                                    String date = AppUtil.getDateFormat();
//
                                                                    Bundle datas = new Bundle();
                                                                    datas.putString("menu_id", pageId);
                                                                    datas.putString("tid", tid);
                                                                    datas.putString("mid", "4228267000001");
                                                                    datas.putString("type_card", "-");
                                                                    datas.putString("date", date);
                                                                    datas.putString("msg", message);

                                                                    getActivity().runOnUiThread(new Runnable() {
                                                                        public void run() {

                                                                            AppUtilPrint.printStruk(getContext(), datas);

                                                                            AppUtil.hideDialog();
                                                                            et1.getText().clear();
                                                                            AppUtil.displayDialog(getContext(), message);
                                                                            AppUtil.saveIntoInbox(message, pageId, AppUtil.getDsn());
                                                                            confirmationDialog.dismiss();
                                                                        }
                                                                    });
                                                                } else {
                                                                    if (pageId.equalsIgnoreCase("PLN Postpaid")) {
                                                                        if (rcode.equalsIgnoreCase("13") || rcode.equalsIgnoreCase("55")) {
                                                                            message = "Kode PIN Salah atau Tidak Aktif";
                                                                            getActivity().runOnUiThread(new Runnable() {
                                                                                public void run() {
                                                                                    AppUtil.hideDialog();
                                                                                    AppUtil.displayDialog(getContext(), message);
                                                                                    pin.getText().clear();
                                                                                }
                                                                            });
                                                                        } else {
                                                                            if (rcode.equalsIgnoreCase("12")) {
                                                                                message = "Invalid Transaction";
                                                                            } else if (rcode.equalsIgnoreCase("03")) {
                                                                                message = "Transaksi E-Channel Tidak Terdaftar";
                                                                            } else if (rcode.equalsIgnoreCase("05")) {
                                                                                message = "Transaksi Core Banking gagal";
                                                                            } else if (rcode.equalsIgnoreCase("63")) {
                                                                                message = "Nilai Rupiah yang Anda Masukkan Salah";
                                                                            } else if (rcode.equalsIgnoreCase("14")) {
                                                                                message = til1.getHint() + " yang Anda Masukkan Salah, Mohon Teliti Kembali";
                                                                            } else if (rcode.equalsIgnoreCase("16")) {
                                                                                message = "Konsumen " + til1.getHint() + " " + id_pelanggan + " diblokir, Hubungi PLN";
                                                                            } else if (rcode.equalsIgnoreCase("21")) {
                                                                                message = "Tagihan Bulan Berjalan Belum Tersedia";
                                                                            } else if (rcode.equalsIgnoreCase("30")) {
                                                                                message = "Message Format Error";
                                                                            } else if (rcode.equalsIgnoreCase("43")) {
                                                                                message = "Pick Up, Stolen Card";
                                                                            } else if (rcode.equalsIgnoreCase("51")) {
                                                                                message = "Saldo Tidak Mencukupi";
                                                                            } else if (rcode.equalsIgnoreCase("52")) {
                                                                                message = "No Checking Account";
                                                                            } else if (rcode.equalsIgnoreCase("53")) {
                                                                                message = "No Savings Account";
                                                                            } else if (rcode.equalsIgnoreCase("54")) {
                                                                                message = "Expired Card / Target";
                                                                            } else if (rcode.equalsIgnoreCase("57")) {
                                                                                message = "Transaction Not Permitted To Cardholder";
                                                                            } else if (rcode.equalsIgnoreCase("75")) {
                                                                                message = "PIN Tries Exceeded";
                                                                            } else if (rcode.equalsIgnoreCase("76")) {
                                                                                message = "Invalid To Account";
                                                                            } else if (rcode.equalsIgnoreCase("77")) {
                                                                                message = "Invalid From account";
                                                                            } else if (rcode.equalsIgnoreCase("82")) {
                                                                                message = "Time Out at Issuer System";
                                                                            } else if (rcode.equalsIgnoreCase("88")) {
                                                                                message = "Tagihan Sudah Terbayar";
                                                                            } else if (rcode.equalsIgnoreCase("89")) {
                                                                                message = "Tagihan Bulan " + bulantagihan_proses + " Belum Tersedia";
                                                                            } else if (rcode.equalsIgnoreCase("91")) {
                                                                                message = "Issuer or Switch is Inoperative";
                                                                            } else if (rcode.equalsIgnoreCase("94")) {
                                                                                message = "Duplicate Transmission/Request/Reversal Message";
                                                                            } else if (rcode.equalsIgnoreCase("96")) {
                                                                                message = "System Malfunction / System Error";
                                                                            } else {
                                                                                message = "Transaksi Gagal";
                                                                            }

                                                                            getActivity().runOnUiThread(new Runnable() {
                                                                                public void run() {
                                                                                    AppUtil.hideDialog();
                                                                                    AppUtil.displayDialog(getContext(), message);
                                                                                    confirmationDialog.dismiss();
                                                                                }
                                                                            });
                                                                        }
                                                                    } else {
                                                                        if (rcode.equalsIgnoreCase("98")) {
                                                                            getActivity().runOnUiThread(new Runnable() {
                                                                                public void run() {
                                                                                    AppUtil.hideDialog();
                                                                                    AppUtil.displayDialog(getContext(), message);
                                                                                    pin.getText().clear();
                                                                                }
                                                                            });
                                                                        } else {
                                                                            getActivity().runOnUiThread(new Runnable() {
                                                                                public void run() {
                                                                                    AppUtil.hideDialog();
                                                                                    AppUtil.displayDialog(getContext(), message);
                                                                                    confirmationDialog.dismiss();
                                                                                }
                                                                            });
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        } catch (Exception ex) {
                                                            msgErr = "true";
                                                            AppUtil.hideDialog();
                                                            Log.d("THREAD", ex.toString());
                                                            message = "Transaksi tidak dapat diproses";
                                                        }
                                                    }
                                                });
                                                thread.start();
                                            }
                                        });

                                    } catch (Exception e) {
                                        AppUtil.hideDialog();
                                        msgErr = "true";
                                        message = "Transaksi tidak dapat diproses";
                                        Log.d("THREAD 1", e.toString());
                                    }
                                }
                            });
                        }
                    }.start();
                } else {
                    if (pageId.equalsIgnoreCase("PLN Postpaid")) {
                        if (rcode.equalsIgnoreCase("12")) {
                            message = "Invalid Transaction";
                        } else if (rcode.equalsIgnoreCase("03")) {
                            message = "Transaksi E-Channel Tidak Terdaftar";
                        } else if (rcode.equalsIgnoreCase("05")) {
                            message = "Transaksi Core Banking gagal";
                        } else if (rcode.equalsIgnoreCase("13")) {
                            message = "Kode PIN Salah atau Tidak Aktif";
                        } else if (rcode.equalsIgnoreCase("63")) {
                            message = "Nilai Rupiah yang Anda Masukkan Salah";
                        } else if (rcode.equalsIgnoreCase("14")) {
                            message = til1.getHint() + " yang Anda Masukkan Salah, Mohon Diteliti Kembali";
                        } else if (rcode.equalsIgnoreCase("16") || rcode.equalsIgnoreCase("77")) {
                            message = "Konsumen " + til1.getHint() + " " + id_pelanggan + " diblokir, Hubungi PLN";
                        } else if (rcode.equalsIgnoreCase("21")) {
                            message = "Tagihan Bulan Berjalan Belum Tersedia";
                        } else if (rcode.equalsIgnoreCase("51")) {
                            message = "Saldo Tidak Mencukupi";
                        } else if (rcode.equalsIgnoreCase("88")) {
                            message = "Tagihan Sudah Terbayar";
                        } else if (rcode.equalsIgnoreCase("89")) {
                            message = "Tagihan Bulan " + bulantagihan + " Belum Tersedia";
                        } else if (rcode.equalsIgnoreCase("27")) {
                            message = "Akun Nasabah Tabungan Cerdas Tidak Aktif";
                        } else if (rcode.equalsIgnoreCase("25")) {
                            message = "Akun Tidak Aktif";
                        } else {
                            message = "Transaksi Gagal";
                        }
                    }

                    getActivity().runOnUiThread(new Runnable() {
                        public void run() {
                            AppUtil.hideDialog();
                            AppUtil.displayDialog(getContext(), message);
                        }
                    });
                }
            }
        } catch (Exception ex) {
            msgErr = "true";
            AppUtil.hideDialog();
            Log.d("THREAD 2", ex.toString());
            message = "Transaksi tidak dapat diproses";
        }
    }
}

