package com.bris.brissmartmobile.fragment;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.bris.brissmartmobile.R;

public class FDialogInqCard extends DialogFragment {

    public FDialogInqCard() {

    }

    FDialogInqCard.InqClickListener inqClickListener;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.custom_dialog_message_card, null);

        String title = getArguments().getString("title");
        String message = getArguments().getString("message");

        Button btnCancelMessage = (Button) view.findViewById(R.id.btn_cancel);
        Button btnOkMessage = (Button) view.findViewById(R.id.btn_next);
        TextView txt_msg_display = (TextView) view.findViewById(R.id.dialog_txt_msg_info);
        TextView dialog_txt_header = (TextView) view.findViewById(R.id.dialog_txt_header);
        dialog_txt_header.setText(title);
        txt_msg_display.setText(message);

        btnOkMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                inqClickListener.dialogInqButton("oke");
                dismiss();
            }
        });

        btnCancelMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });

        return view;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            dialog = new Dialog(getContext(), android.R.style.Theme_Black_NoTitleBar);
        }
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        return dialog;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            if (context instanceof InqClickListener) {
                inqClickListener = (InqClickListener) context;
            }
            else {
                inqClickListener = (InqClickListener) getTargetFragment();
            }
        }
        catch (Exception e){
            // you should really do something here with this exception
        }
    }

    public interface InqClickListener {
        void dialogInqButton(String action);
    }

}
