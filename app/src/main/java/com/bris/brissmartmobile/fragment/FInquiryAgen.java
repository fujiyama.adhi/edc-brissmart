package com.bris.brissmartmobile.fragment;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bris.brissmartmobile.R;
import com.bris.brissmartmobile.activity.AInquiryAgen;
import com.bris.brissmartmobile.adapter.FInquiryAgenAdapter;
import com.bris.brissmartmobile.model.UserBrissmart;
import com.bris.brissmartmobile.util.AppUtil;
import com.bris.brissmartmobile.util.Menu;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by valeputra on 6/22/17.
 */

public class FInquiryAgen extends Fragment {

    RecyclerView rv_list_agen;
    LinearLayout ll_view_indicator;
    JSONObject json, resdata, cmdparamRes;
    JSONArray agent;
    String pageId;
    String count, dsn, syiar_noprop, syiar_nokota, dpage, dpageRes, dpageReq;
    String[] no, nama, alamat;
    private FInquiryAgenAdapter adapter;
    private View view;
    private ImageView imgPrev, imgNext;
    private TextView tv_indicator;

    String rcode, tsession, cmsg;

    public FInquiryAgen() {
    }

    public View onCreateView(final LayoutInflater inflater, @Nullable final ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.activity_inquiry_agen_list, container, false);
        rv_list_agen = (RecyclerView) view.findViewById(R.id.rv_list_agen);
        imgPrev = (ImageView) view.findViewById(R.id.img_prev);
        imgNext = (ImageView) view.findViewById(R.id.img_next);
        ll_view_indicator = (LinearLayout) view.findViewById(R.id.view_indicator);
        tv_indicator = (TextView) view.findViewById(R.id.tv_indicator);
        syiar_noprop = getArguments().getString("syiar_noprop");
        syiar_nokota = getArguments().getString("syiar_nokota");
        dpage = getArguments().getString("dpage");

        Bundle bundleDatas = getActivity().getIntent().getExtras();
        pageId = bundleDatas.getString(Menu.MENU_ID);
        customToolbar();

        try {
            json = new JSONObject(getArguments().getString("response"));
            resdata = json.getJSONObject("resdata");
            count = resdata.getString("count");
            agent = resdata.getJSONArray("agent");
            no = new String[agent.length()];
            nama = new String[agent.length()];
            alamat = new String[agent.length()];
            for (int i = 0; i < agent.length(); i++) {
                JSONObject jo = agent.getJSONObject(i);
                no[i] = (Integer.valueOf(dpage + "0") - 10 + i + 1) + "";
                nama[i] = jo.getString("iname");
                alamat[i] = jo.getString("iaddr");
                Log.d("Test StringArray", no[i]);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        tv_indicator.setText(dpage + " / " + count);
        if (dpage.equalsIgnoreCase("1")) {
            imgPrev.setVisibility(View.INVISIBLE);
        } else {
            imgPrev.setVisibility(View.VISIBLE);
            imgPrev.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dpageReq = String.valueOf(Integer.valueOf(dpage) - 1);

                    final Thread thread = new Thread(new Runnable() {

                        @Override
                        public void run() {
                            SoapActivity action = new SoapActivity();
                            action.execute();
                        }
                    });
                    thread.start();
                }
            });
        }

        if (!(Integer.valueOf(count) > 1) || dpage.equalsIgnoreCase(count)) {
            imgNext.setVisibility(View.INVISIBLE);
        } else {
            imgNext.setVisibility(View.VISIBLE);
            imgNext.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dpageReq = String.valueOf(Integer.valueOf(dpage) + 1);

                    final Thread thread = new Thread(new Runnable() {

                        @Override
                        public void run() {
                            SoapActivity action = new SoapActivity();
                            action.execute();
                        }
                    });
                    thread.start();
                }
            });
        }

        // set adapter
        adapter = new FInquiryAgenAdapter(no, nama, alamat, getContext());
        rv_list_agen.setAdapter(adapter);
        rv_list_agen.setLayoutManager(new LinearLayoutManager(getContext()));
        return view;
    }

    private class SoapActivity extends AsyncTask<Void, Void, Void> {


        @Override
        protected void onPreExecute() {

        }

        @Override
        protected Void doInBackground(Void... params) {
            convertir();
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
        }

    }

    private void convertir() {

        try {

            JSONObject cmdparam = new JSONObject();
            cmdparam.put("vdsn", AppUtil.getDsn());
            cmdparam.put("dprov", syiar_noprop);
            cmdparam.put("dcity", syiar_nokota);
            cmdparam.put("ddist", "");
            cmdparam.put("dpage", dpageReq);

            String soapRes = AppUtil.soapReq(AppUtil.getDsn(), "agent_search", cmdparam);

            if (soapRes.equalsIgnoreCase("")) {
                getActivity().runOnUiThread(new Runnable() {
                    public void run() {
                        AppUtil.displayDialog(getContext(), "Gagal terhubung ke server");
                    }
                });
            } else {
                json = new JSONObject(soapRes);
                resdata = json.getJSONObject("resdata");
                cmdparamRes = json.getJSONObject("cmdparam");
                dpageRes = cmdparamRes.getString("dpage");
                rcode = resdata.getString("rcode");
                count = resdata.getString("count");

                if (rcode.equalsIgnoreCase("00")) {
                    ll_view_indicator.setVisibility(View.VISIBLE);

                    Bundle datas = new Bundle();
                    datas.putString("response", String.valueOf(json));
                    datas.putString("syiar_noprop", syiar_noprop);
                    datas.putString("syiar_nokota", syiar_nokota);
                    datas.putString("dpage", dpageRes);

                    FInquiryAgen fSubMenu = new FInquiryAgen();
                    FragmentManager manager = getActivity().getSupportFragmentManager();
                    fSubMenu.setArguments(datas); // insert bundling datas
                    manager.beginTransaction()
                            .replace(R.id.fragment_inquiry_agen, fSubMenu)
                            .addToBackStack(null)
                            .commit();
                } else {
                    getActivity().runOnUiThread(new Runnable() {
                        public void run() {
                            AppUtil.displayDialog(getContext(), "Gagal terhubung ke server");
                        }
                    });
                }
            }
        } catch (Exception ex) {
            Log.d("THREAD a", ex.toString());
            getActivity().runOnUiThread(new Runnable() {
                public void run() {
                    AppUtil.displayDialog(getContext(), "Gagal terhubung ke server");
                }
            });
        }
    }

    private void customToolbar() {
        Toolbar toolbar = (Toolbar) view.findViewById(R.id.tb_inquiry_agen);
        TextView tvToolbar = (TextView) view.findViewById(R.id.tb_title_inquiry_agen);
        toolbar.setNavigationIcon(R.mipmap.abc_ic_ab_back_mtrl_am_alpha);
        tvToolbar.setText(pageId);
//        getActivity().setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle mbundle = new Bundle();mbundle.putString(Menu.MENU_ID, getContext().getString(R.string.menu_title_cari_agen_bsa));
                Intent iBack = new Intent(getContext(), AInquiryAgen.class);
                iBack.putExtras(mbundle);
                startActivity(iBack);
            }
        });
    }
}

