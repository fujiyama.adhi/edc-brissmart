package com.bris.brissmartmobile.fragment;

import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.bris.brissmartmobile.R;
import com.bris.brissmartmobile.listener.FavClickListener;
import com.bris.brissmartmobile.model.Favorite;
import com.bris.brissmartmobile.util.AppUtil;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by valeputra on 6/22/17.
 */

public class FCekSaldo extends Fragment implements FavClickListener, DialogInterface.OnKeyListener {

    private EditText et1;
    private TextInputLayout til1;
    private Button btn1, btnConfirm;
    private ImageButton btnFav;
    private List<Favorite> favDatas = new ArrayList<>();
    String param1;
    String pageId;
    JSONObject json, resdata;
    String responseJSON, msgErr, rcode, name, tsession, cmsg;
    private Realm realm;
    private AlertDialog AlertDialog = null,
            confirmationDialog = null;
    private View view;

    public FCekSaldo() {}

    public View onCreateView(final LayoutInflater inflater, @Nullable final ViewGroup container, @Nullable Bundle savedInstanceState) {
        pageId = getArguments().getString("menu_id");
        view = inflater.inflate(R.layout.fragment_cek_saldo, container, false);
        AppUtil.toolbarRegular(getContext(), view, pageId);

        til1 = (TextInputLayout) view.findViewById(R.id.til_param1Txt);
        et1 = (EditText) view.findViewById(R.id.param1Txt);
        btn1 = (Button) view.findViewById(R.id.btn_submit);
        btnFav = (ImageButton) view.findViewById(R.id.btn_daftar_fav);

        realm = Realm.getDefaultInstance();

        et1.setFocusable(false);
        et1.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                et1.setFocusableInTouchMode(true);

                return false;
            }
        });

        try {
            onActionButton();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return view;
    }

    private void onActionButton() {
        btnFav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    param1 = et1.getText().toString().trim();

                    if (param1.equalsIgnoreCase("")) {
                        getFavData();
                        AppUtil.displayDialogFavorit(getContext(), favDatas, FCekSaldo.this, "CEKSALDO");
                        return;
                    }

                    AlertDialog dialogAlert;
                    AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                    builder.setCancelable(true);
                    builder.setMessage("Simpan data transaksi ini sebagai data favorit?");
                    builder.setPositiveButton("Simpan", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface alertdialog, int which) {
                            alertdialog.cancel();

                            if (AppUtil.checkFavorite("CASH", param1, "Cek Saldo", AppUtil.getDsn())) {
                                FragmentManager manager = getActivity().getSupportFragmentManager();
                                Fragment fragment;
                                FragmentTransaction transaction = manager.beginTransaction();
                                fragment = new FInsertUpdateDataFavorit();
                                Bundle bundle = new Bundle();
                                bundle.putString("title", pageId);
                                bundle.putString("jenispembayaran", "CASH");
                                bundle.putString("data1", "Cek Saldo");
                                bundle.putString("data3", param1);
                                bundle.putString("menutrx", "1000");
                                bundle.putString("submenutrx", "1001");
                                bundle.putString("namajenisfav", "Cek Saldo");

                                fragment.setArguments(bundle);
                                transaction.replace(R.id.fragment_payment_general, fragment);
                                transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
                                transaction.addToBackStack(null);
                                transaction.commit();


                            } else {
                                AppUtil.displayDialog(getContext(), "Data favorit Telah Ada");
                            }
                        }
                    });
                    builder.setNegativeButton("Pilih favorit", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface alertdialog, int which) {
                            alertdialog.cancel();
                            getFavData();
                            AppUtil.displayDialogFavorit(getContext(), favDatas, FCekSaldo.this, "CEKSALDO");
                        }
                    });

                    dialogAlert = builder.create();
                    dialogAlert.show();
                } catch (Exception e) {
                    e.printStackTrace();
                    AppUtil.displayDialog(getContext(), "Maaf, Terjadi Kesalahan");
                }
            }
        });

        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (et1.getText().toString().length() == 0) {
                    til1.setError("Nomor Rekening Tidak Boleh Kosong!");
                    return;
                } else {
                    param1 = et1.getText().toString();
                }

                SoapActivity action = new SoapActivity();
                action.execute();

            }
        });
    }

    @Override
    public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
            AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
            builder.setCancelable(false);
            builder.setMessage("Apakah anda yakin akan membatalkan proses " + pageId + "?");
            builder.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                    confirmationDialog.dismiss();
                }
            });
            builder.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                    confirmationDialog.show();
                }
            });
            if (AlertDialog == null || !AlertDialog.isShowing()) {
                AlertDialog = builder.create();
                AlertDialog.show();
            }
            return true;
        }
        return false;
    }

    @Override
    public void getFav(String label, String category) {

    }

    @Override
    public void onclikFav(String label, String kode_produk) {
        RealmResults<Favorite> fav = realm.where(Favorite.class)
                .equalTo("menutrx", "1000")
                .equalTo("jenispembayaran", "CASH")
                .equalTo("namafav", label)
                .equalTo("data1", kode_produk)
                .findAll();

        if (fav.size() > 0) {
            et1.setText(fav.get(0).getData3());
            AppUtil.closeListDialog();
        }
    }

    @Override
    public void deleteFav(String jenispembayaran, String menutrx, String submenutrx, String label) {

    }

    public void getFavData() {
        RealmResults<Favorite> fav = realm.where(Favorite.class)
                .equalTo("menutrx", "1000")
                .equalTo("jenispembayaran", "CASH")
                .equalTo("data1", "Cek Saldo")
                .findAll();

        RealmResults<Favorite> favorit = realm.where(Favorite.class).findAll();
        Log.d("Cek fav FCekSaldo", fav.toString());
        Log.d("Cek favorit", favorit.toString());

        if (fav.size() > 0) {
            favDatas.clear();
            favDatas.addAll(fav);
        } else {
            favDatas.clear();
        }
    }

    private class SoapActivity extends AsyncTask<Void, Void, Void> {


        @Override
        protected void onPreExecute() {

        }

        @Override
        protected Void doInBackground(Void... params) {
            convertir();
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            if (msgErr.equalsIgnoreCase("true")) {
                AppUtil.displayDialog(getContext(), responseJSON);
            }
        }

    }

    private void convertir() {

        try {
            getActivity().runOnUiThread(new Runnable() {
                public void run() {
                    AppUtil.initDialogProgressSendingSMS(getContext());
                    AppUtil.showDialog();
                }
            });

            JSONObject cmdparam = new JSONObject();
            cmdparam.put("vdsn", param1);
            Log.d("Cek dsn", AppUtil.getDsn());
            String soapRes = AppUtil.soapReq(AppUtil.getDsn(), "balance_check_fagent_inq", cmdparam);

            if (soapRes.equalsIgnoreCase("")) {
                AppUtil.hideDialog();
                msgErr = "true";
                responseJSON = "Gagal terhubung ke server";
            } else {
                json = new JSONObject(soapRes);
                resdata = json.getJSONObject("resdata");
                rcode = resdata.getString("rcode");
                msgErr = "false";

                if (rcode.equalsIgnoreCase("00")) {
                    tsession = resdata.getString("tsession");
                    msgErr = "false";
                    new Thread() {
                        public void run() {
                            FCekSaldo.this.getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    AppUtil.hideDialog();
                                    try {
                                        LayoutInflater li = LayoutInflater.from(getContext());
                                        final View confirmDialog = li.inflate(R.layout.dialog_confirm_bsa, null);
                                        btnConfirm = (Button) confirmDialog.findViewById(R.id.btn_submit);
                                        final TextView confirm = (TextView) confirmDialog.findViewById(R.id.txt_title_top);
                                        final EditText et2 = (EditText) confirmDialog.findViewById(R.id.txt_pin);

                                        confirm.setText(param1 + " akan melakukan Pengecekan Saldo. Masukkan PIN yang dikirim melalui SMS");
                                        et2.addTextChangedListener(new TextWatcher() {
                                            @Override
                                            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                                            }

                                            @Override
                                            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                                                if (charSequence.toString().trim().length() == 7) {
                                                    btnConfirm.setEnabled(true);
                                                    btnConfirm.setBackgroundResource(R.drawable.button_shape_blue);
                                                } else {
                                                    btnConfirm.setEnabled(false);
                                                    btnConfirm.setBackgroundResource(R.drawable.button_shape_accent);
                                                }
                                            }

                                            @Override
                                            public void afterTextChanged(Editable editable) {
                                            }
                                        });

                                        AlertDialog.Builder alert = new AlertDialog.Builder(getContext());
                                        alert.setView(confirmDialog);
                                        alert.setCancelable(false);
                                        confirmationDialog = alert.create();
                                        confirmationDialog.setOnKeyListener(FCekSaldo.this);
                                        confirmationDialog.show();

                                        btnConfirm.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View view) {

                                                AppUtil.initDialogProgress(getContext());
                                                AppUtil.showDialog();

                                                Thread thread = new Thread(new Runnable() {

                                                    @Override
                                                    public void run() {
                                                        try {
                                                            JSONObject cmdparam = new JSONObject();

                                                            cmdparam.put("vdsn", param1);
                                                            cmdparam.put("tsession", tsession);
                                                            cmdparam.put("tpin", et2.getText().toString());
                                                            String soapRes = AppUtil.soapReq(AppUtil.getDsn(), "balance_check_fagent_pro", cmdparam);

                                                            if (soapRes.equalsIgnoreCase("")) {
                                                                AppUtil.hideDialog();
                                                                msgErr = "true";
                                                                responseJSON = "Gagal terhubung ke server";
                                                            } else {
                                                                json = new JSONObject(soapRes);
                                                                resdata = json.getJSONObject("resdata");
                                                                rcode = resdata.getString("rcode");
                                                                cmsg = resdata.getString("cmsg");

                                                                if (rcode.equalsIgnoreCase("00")) {
                                                                    AppUtil.hideDialog();
                                                                    getActivity().runOnUiThread(new Runnable() {
                                                                        public void run() {
                                                                            et1.getText().clear();
                                                                            AppUtil.displayDialog(getContext(), cmsg);
                                                                            AppUtil.saveIntoInbox(cmsg, pageId, AppUtil.getDsn());
                                                                            confirmationDialog.dismiss();
                                                                        }
                                                                    });
                                                                } else if (rcode.equalsIgnoreCase("13")) {
                                                                    AppUtil.hideDialog();
                                                                    getActivity().runOnUiThread(new Runnable() {
                                                                        public void run() {
                                                                            AppUtil.displayDialog(getContext(), cmsg);
                                                                            et2.getText().clear();
                                                                        }
                                                                    });
                                                                } else {
                                                                    AppUtil.hideDialog();
                                                                    getActivity().runOnUiThread(new Runnable() {
                                                                        public void run() {
                                                                            AppUtil.displayDialog(getContext(), cmsg);
                                                                            confirmationDialog.dismiss();
                                                                        }
                                                                    });
                                                                }
                                                            }
                                                        } catch (Exception ex) {
                                                            msgErr = "true";
                                                            AppUtil.hideDialog();
                                                            Log.d("THREAD", ex.toString());
                                                            responseJSON = "Transaksi gagal";
                                                        }
                                                    }
                                                });
                                                thread.start();
                                            }
                                        });

                                    } catch (Exception e) {
                                        Log.d("THREAD 1", e.toString());
                                        msgErr = "true";
                                        AppUtil.hideDialog();
                                        responseJSON = "Transaksi gagal";
                                    }
                                }
                            });
                        }
                    }.start();
                } else {
                    Log.d("else", "gagal");
                    AppUtil.hideDialog();
                    cmsg  = resdata.getString("cmsg");
                    responseJSON = cmsg;
                    msgErr = "true";
                }
            }
        } catch (Exception ex) {
            msgErr = "true";
            AppUtil.hideDialog();
            Log.d("THREAD 2", ex.toString());
            responseJSON = "Transaksi gagal";
        }
    }
}

