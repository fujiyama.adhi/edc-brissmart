package com.bris.brissmartmobile.fragment;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Point;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bris.brissmartmobile.adapter.HeadlineAdapter;
import com.bris.brissmartmobile.model.BankingPromoResponse;
import com.bris.brissmartmobile.promo.PromoActivity;
import com.bris.brissmartmobile.R;
//import com.bris.onlinebris.adapter.HeadlineAdapter;
import com.bris.brissmartmobile.activity.AInbox;
import com.bris.brissmartmobile.activity.AListViewConstruct;
import com.bris.brissmartmobile.adapter.MenuHomeBottomAdapterAgen;
import com.bris.brissmartmobile.adapter.MenuHomeTopAdapterAgen;
import com.bris.brissmartmobile.listener.MenuClickListener;
import com.bris.brissmartmobile.model.UserBrissmart;
import com.bris.brissmartmobile.util.ApiClientAdapter;
import com.bris.brissmartmobile.util.AppUtil;
import com.bris.brissmartmobile.util.ListMenu;
import com.bris.brissmartmobile.util.ListMenuBackground;
import com.bris.brissmartmobile.util.Menu;
import com.bris.brissmartmobile.util.ViewPagerWrapped;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import io.realm.Realm;
import io.realm.RealmResults;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by valeputra on 6/22/17.
 */

public class FragmentHomeAgen extends Fragment implements View.OnClickListener, MenuClickListener {
    private View view;
    private GridView gridViewTop, gridViewBottom;
    private CardView btnCallBris;
    private ViewPagerWrapped viewPagerHeadline;
    private TextView tvHeadlinePosition, cv_seemore;
    private CardView cv_promoPos;
    private int headlineCount = 0;
    private Realm realm;
    private String name, status;

    private ApiClientAdapter apiClientAdapter;
    private List<BankingPromoResponse.DataPromo> headlinePromo;
    private HeadlineAdapter headlineAdapter;

    private int gridTopNumberInRow = 4;
    private int gridBottomNumberInRow = 2;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        apiClientAdapter = new ApiClientAdapter(getContext());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_home, container, false);

        viewPagerHeadline  = (ViewPagerWrapped) view.findViewById(R.id.vp_headline);
        tvHeadlinePosition = (TextView) view.findViewById(R.id.tv_headline_promo_pos);

        cv_seemore = (TextView) view.findViewById(R.id.cardview_seemore);
        cv_promoPos = (CardView) view.findViewById(R.id.cardview_promo_pos);

        gridViewTop = (GridView)view.findViewById(R.id.gridview_top);
        gridViewBottom = (GridView) view.findViewById(R.id.gridview_bottom);

        btnCallBris = (CardView) view.findViewById(R.id.btn_call_bris);
        main();
        return view;
    }

    private void main() {
        buildHeadline();
        buildPromoHeadline();
        buildGridMenu();
        cv_seemore.setOnClickListener(this);
        btnCallBris.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_call_bris :
                AppUtil.displayDialogCall(getContext(), getString(R.string.alert_callbris));
                break;
            case  R.id.cardview_seemore:
                Intent i = new Intent(getContext(), PromoActivity.class);
                startActivity(i);
                break;
        }
    }

    private void buildHeadline() {
            TextView tvUsername = (TextView) view.findViewById(R.id.tv_username_agen);
            tvUsername.setText(getStatus() + " " + getUser());
    }

    private void buildPromoHeadline() {
        Call<BankingPromoResponse> call = apiClientAdapter.getApiInterface().getPromo("headline");
        call.enqueue(new Callback<BankingPromoResponse>() {
            @Override
            public void onResponse(Call<BankingPromoResponse> call, Response<BankingPromoResponse> response) {
                headlinePromo = response.body().getData();
                if (response.isSuccessful()) {
                    if (response.body().getData().size() > 0) {
                        initViewPagerHeadline(headlinePromo);
                        if (response.body().getData().size() > 1) {
                            headlineCount = headlinePromo.size();
                            tvHeadlinePosition.setText("1/" + headlineCount);
                            cv_seemore.setVisibility(View.VISIBLE);
                            cv_promoPos.setVisibility(View.VISIBLE);
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<BankingPromoResponse> call, Throwable t) {
                t.printStackTrace();
                Activity activity = getActivity();
                if(activity != null){
//                    AppUtil.displayDialog(getContext(), getResources().getString(R.string.txt_connection_failure));
                }

//                Toast.makeText(getContext(), getContext().getString(R.string.txt_connection_failure), Toast.LENGTH_SHORT);
            }
        });
    }

    private void initViewPagerHeadline(List<BankingPromoResponse.DataPromo> itemPromos) {
        LinearLayout ivHeadlinePlaceholder = (LinearLayout) view.findViewById(R.id.ivPlaceholder);
        ivHeadlinePlaceholder.setVisibility(View.INVISIBLE);
        headlinePromo = itemPromos;
        headlineAdapter = new HeadlineAdapter(getContext(), headlinePromo);
        viewPagerHeadline.setAdapter(headlineAdapter);
        viewPagerHeadline.setCurrentItem(headlineAdapter.FIRST_PAGE);
        AutoSwipeHeadline();
        updateIndicatorPos();
    }

    private void updateIndicatorPos() {
        viewPagerHeadline.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {}
            @Override
            public void onPageSelected(int position) {
                Log.d("pageFragment", "onPageSelected: "+position);
                tvHeadlinePosition.setText((position+1) + "/" + headlineCount);
            }
            @Override
            public void onPageScrollStateChanged(int state) {}
        });
    }

    public void AutoSwipeHeadline(){
        final Handler handler = new Handler();
        final Runnable Update = new Runnable() {
            public void run() {
                int currentPage = viewPagerHeadline.getCurrentItem();
                if (currentPage == headlineCount-1) {
                    currentPage = -1;
                }
                viewPagerHeadline.setCurrentItem(currentPage+1, true);
            }
        };

        Timer swipeTimer = new Timer();
        swipeTimer.schedule(new TimerTask() {

            @Override
            public void run() {
                handler.post(Update);
            }
        }, 10000, 30000);
    }

    private void buildGridMenu() {
        dynamicGridView();  // for gridview dinamicaly between screen resolution

        gridViewTop.setNumColumns(gridTopNumberInRow);
        gridViewBottom.setNumColumns(gridBottomNumberInRow);

        List<ListMenuBackground> listTopMenus = getListTopMenus();
        MenuHomeTopAdapterAgen adapterTop = new MenuHomeTopAdapterAgen(getActivity(), listTopMenus, FragmentHomeAgen.this);
        gridViewTop.setAdapter(adapterTop);

        List<ListMenu> listBottomMenus = getListBottomMenus();
        MenuHomeBottomAdapterAgen adapterBottom = new MenuHomeBottomAdapterAgen(getActivity(), listBottomMenus, FragmentHomeAgen.this);
        gridViewBottom.setAdapter(adapterBottom);

//        List<ListMenu> listTopMenu = gridMenus();
//        MenuHomeAgenBottomAdapter adapterBottom = new MenuHomeAgenBottomAdapter(getActivity(), listTopMenu);
//        gridViewTop.setAdapter(adapterBottom);
    }

    public void dynamicGridView() {
        Point size = new Point();
        getActivity().getWindowManager().getDefaultDisplay().getSize(size);
        int screenSize = size.x;
        Log.d("screen size", screenSize+"");
//        int numColomn = screenSize < 640 ? 2 : 3; // dynamic grid over screen res
        int numColomn = 3;
        gridViewTop.setNumColumns(numColomn);
    }

    private List<ListMenuBackground> getListTopMenus() {
        List<ListMenuBackground> menu = new ArrayList<>();
        Menu.homeTop(getContext(), menu);
        return menu;
    }

    private List<ListMenu> getListBottomMenus() {
        List<ListMenu> menu = new ArrayList<>();
        Menu.homeBottom(getContext(), menu);
        return menu;
    }

    public String getUser() {
        realm = Realm.getDefaultInstance();
        RealmResults<UserBrissmart> user = realm.where(UserBrissmart.class).findAll();
        name = (user.get(0).getName().isEmpty() ) ? name.toUpperCase() : user.get(0).getName();
        return name;
    }

    public String getStatus() {
        realm = Realm.getDefaultInstance();
        RealmResults<UserBrissmart> user = realm.where(UserBrissmart.class).findAll();
        status = (user.get(0).getStatus().isEmpty()) ? status.toUpperCase() : user.get(0).getStatus();
        return status;
    }

    @Override
    public void onMenuClick(String menu) {
        Bundle mbundle = new Bundle();
        Intent intent = null;

        if (menu.equalsIgnoreCase(getString(R.string.menu_title_rekening))) {
            intent = new Intent(getContext(), AListViewConstruct.class);
        } else if (menu.equalsIgnoreCase(getString(R.string.menu_title_transaksi))) {
            intent = new Intent(getContext(), AListViewConstruct.class);
        } else if (menu.equalsIgnoreCase(getContext().getString(R.string.menu_title_pembayaran))) {
            intent = new Intent(getContext(), AListViewConstruct.class);
        } else if(menu.equalsIgnoreCase(getContext().getString(R.string.menu_title_pembelian))) {
            intent = new Intent(getContext(), AListViewConstruct.class);
        } else if(menu.equalsIgnoreCase(getContext().getString(R.string.menu_title_donasi))) {
            intent = new Intent(getContext(), AListViewConstruct.class);
        } else if(menu.equalsIgnoreCase(getContext().getString(R.string.menu_title_favorit))) {
            intent = new Intent(getContext(), AListViewConstruct.class);
        } else if(menu.equalsIgnoreCase(getContext().getString(R.string.menu_title_inbox))) {
            intent = new Intent(getContext(), AInbox.class);
        } else if(menu.equalsIgnoreCase(getContext().getString(R.string.menu_title_promo))) {
            intent = new Intent(getContext(), PromoActivity.class);
        }

        mbundle.putString(Menu.MENU_ID, menu);
        intent.putExtras(mbundle);
        startActivity(intent);
    }
}