package com.bris.brissmartmobile.fragment;

import android.support.v7.app.AlertDialog;
import android.arch.lifecycle.ViewModelProviders;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;


import android.os.CountDownTimer;
import android.os.Looper;
import android.os.RemoteException;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.util.SparseArray;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.bris.brissmartmobile.MainViewModel;
import com.bris.brissmartmobile.HelperPin;
import com.bris.brissmartmobile.R;
import com.bris.brissmartmobile.app.BRISSMARTMobileRealmApp;
import com.bris.brissmartmobile.emv.EMVTLVParam;
import com.bris.brissmartmobile.util.AppUtil;
import com.vfi.smartpos.deviceservice.aidl.CheckCardListener;
import com.vfi.smartpos.deviceservice.aidl.EMVHandler;
import com.vfi.smartpos.deviceservice.aidl.IBeeper;
import com.vfi.smartpos.deviceservice.aidl.IEMV;
import com.vfi.smartpos.deviceservice.aidl.IPinpad;
import com.vfi.smartpos.deviceservice.aidl.PinInputListener;
import com.vfi.smartpos.deviceservice.constdefine.ConstCheckCardListener;
import com.vfi.smartpos.deviceservice.constdefine.ConstIPBOC;
import com.vfi.smartpos.deviceservice.constdefine.ConstIPinpad;
import com.vfi.smartpos.deviceservice.constdefine.ConstPBOCHandler;


import java.util.List;

public class FSwipeTrial extends Fragment implements DialogInterface.OnKeyListener {

    String msgErr, responseJSON;

    private MainViewModel mViewModel;
    private View view;
    Button btn_refresh;
    TextView tv_swipe_card;
//    private TextView log;
    public IEMV iemv;
    IBeeper iBeeper;
    EMVHandler emvHandler;
    IPinpad ipinpad;
    PinInputListener pinInputListener;
    SparseArray<String> tagOfF55 = null;
    public String applet, tsi, tvr, aid, cardName = "-", expiredDate ="";
    public String cdnumber;
    private boolean isChip = false;
    String pageId;

    private android.support.v7.app.AlertDialog AlertDialog = null;
    private AlertDialog confirmationDialog = null;

    public static FSwipeTrial newInstance() {
        return new FSwipeTrial();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        pageId = getArguments().getString("menu_id");
        view = inflater.inflate(R.layout.fragment_swipe_trial, container, false);
        AppUtil.toolbarRegular(getContext(), view, pageId);

        String id = getArguments().getString("kartu_id");
        Log.d("ID KARTU", id);



        btn_refresh = (Button) view.findViewById(R.id.btn_swipe_refresh);
        tv_swipe_card = (TextView) view.findViewById(R.id.tv_swipe_card);

//        log = (TextView) view.findViewById(R.id.log);
        iemv = BRISSMARTMobileRealmApp.getInstance().getiEMV();
        ipinpad = BRISSMARTMobileRealmApp.getInstance().getIpinpad();
        iBeeper = BRISSMARTMobileRealmApp.getInstance().getiBeeper();

        initializeEMV();
        initializePinInputListener();

        btn_refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isChip = false;
                Toast.makeText(getActivity().getBaseContext(), "Please insert or swipe card", Toast.LENGTH_LONG).show();
                tv_swipe_card.setText("Swipe atau Masukkan Kartu BRI Syariah");
                btn_refresh.setVisibility(View.INVISIBLE);
                startEmvProcess(0.0);
            }
        });

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(MainViewModel.class);

        isChip = false;
        startEmvProcess(0.0);
        // TODO: Use the ViewModel
    }

    /**
     *
     * @param amount
     * Step Number 1. to be executed, reading card
     */
    public void startEmvProcess(double amount){
        if (amount == 0){
            doSearchCard(TransType.T_BANLANCE, amount);
        } else {
            doSearchCard(TransType.T_PURCHASE, amount);
        }
    }

    enum TransType {
        T_BANLANCE, T_PURCHASE
    }

    public void doSearchCard(final TransType transType, final double amount){
        Bundle cardOption = new Bundle();
        cardOption.putBoolean( ConstIPBOC.checkCard.cardOption.KEY_Contactless_boolean, ConstIPBOC.checkCard.cardOption.VALUE_unsupported);
        cardOption.putBoolean( ConstIPBOC.checkCard.cardOption.KEY_SmartCard_boolean, ConstIPBOC.checkCard.cardOption.VALUE_supported);
        cardOption.putBoolean( ConstIPBOC.checkCard.cardOption.KEY_MagneticCard_boolean, ConstIPBOC.checkCard.cardOption.VALUE_supported);


        try {
            iemv.checkCard(cardOption, 5, new CheckCardListener.Stub() {
                        @Override
                        public void onCardSwiped(Bundle track) throws RemoteException {
                            Log.d( "TAG", "onCardSwiped ..." );
                            iemv.stopCheckCard();
                            iemv.abortEMV();

                            iBeeper.startBeep(200);

                            String pan = track.getString(ConstCheckCardListener.onCardSwiped.track.KEY_PAN_String);
                            String track1 = track.getString(ConstCheckCardListener.onCardSwiped.track.KEY_TRACK1_String);
                            if (track1 != null){
                                parseCardName(track1);
                            }
                            String track2 = track.getString(ConstCheckCardListener.onCardSwiped.track.KEY_TRACK2_String);
                            String track3 = track.getString(ConstCheckCardListener.onCardSwiped.track.KEY_TRACK3_String);
                            String serviceCode = track.getString(ConstCheckCardListener.onCardSwiped.track.KEY_SERVICE_CODE_String);
                            cdnumber = pan;
//                            log.append("Mag Card Swiped..");
//                            log.append("\n");
//                            log.append("Card Number:" + pan);
//                            log.append("\n");
//                            log.append("Track 2 Data:" + track2);
//                            log.append("\n");
//                            runOnUiThread(updateLog());
                            //incase magnetic card detected, just go to input pin
                            //and process it
                            doPinPad(true, 3);
                        }

                        @Override
                        public void onCardPowerUp() throws RemoteException {
                            iemv.stopCheckCard();
                            iemv.abortEMV();
                            iBeeper.startBeep(200);
//                            log.append("ICC Card Detected..");
//                            log.append("\n");
//                            log.append("Reading Card..");
//                            log.append("\n");
//                            runOnUiThread(updateLog());
                            isChip = true;
                            doEMV( ConstIPBOC.startEMV.intent.VALUE_cardType_smart_card , transType, (long) amount );
                        }

                        @Override
                        public void onCardActivate() throws RemoteException {
                            iemv.stopCheckCard();
                            iemv.abortEMV();
                            iBeeper.startBeep(200);
//                            log.append("ICC Contactless Card Detected..");
//                            log.append("\n");
//                            log.append("Reading Card..");
//                            runOnUiThread(updateLog());
                            isChip = true;
//                            doEMV( ConstIPBOC.startEMV.intent.VALUE_cardType_contactless, transType, (long) amount );
                        }

                        @Override
                        public void onTimeout() throws RemoteException {
//                            runOnUiThread(showToast("Timeout"));
//                            finish();
                            Log.e("Search Card", "Timeout");

                            getActivity().runOnUiThread(showButton("Timeout"));
                            iBeeper.startBeep(100);

                        }

                        @Override
                        public void onError(int error, String message) throws RemoteException {
                            System.out.println("error:" + error);
                            if (error == 3){
                                Looper.prepare();
                                new CountDownTimer(1000, 1000) {

                                    public void onTick(long millisUntilFinished) {
                                    }

                                    public void onFinish() {
//                                        runOnUiThread(showToast("Use Magnetic Card"));
                                        Log.e("Search Card", "Use Magnetic Card");
                                    }

                                }.start();
                                Looper.loop();
                            } else if (error == 100){
                                doSearchCard(transType, amount);
//                                runOnUiThread(showToast("Card not readable, please try again"));
                                Log.e("Search Card", "Card not readable, please try again");
                            }
                        }
                    }
            );
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    public Runnable showButton(String text){

        Runnable aRunnable = new Runnable(){
            public void run(){
                btn_refresh.setVisibility(View.VISIBLE);
                tv_swipe_card.setText("Waktu Habis\nSilahkan tekan tombol refresh");
                Toast.makeText(getActivity().getBaseContext(), text, Toast.LENGTH_SHORT).show();
            }
        };

        return aRunnable;

    }

    public void doPinPad(boolean isOnlinePin, int retryTimes) {
        Bundle param = new Bundle();
        Bundle globeParam = new Bundle();
        String panBlock = cdnumber;
        byte[] pinLimit = {0, 6};
        byte[] displayKey = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
        param.putByteArray("displayKeyValue", displayKey);
        param.putByteArray(ConstIPinpad.startPinInput.param.KEY_pinLimit_ByteArray, pinLimit);
        param.putInt(ConstIPinpad.startPinInput.param.KEY_timeout_int, 20);
        param.putBoolean(ConstIPinpad.startPinInput.param.KEY_isOnline_boolean, isOnlinePin);
        param.putString(ConstIPinpad.startPinInput.param.KEY_pan_String, panBlock);
        param.putInt(ConstIPinpad.startPinInput.param.KEY_desType_int, ConstIPinpad.startPinInput.param.Value_desType_3DES);
        param.putString( "promptString" , "Silakan Masukan PIN Anda") ;
        globeParam.putString(ConstIPinpad.startPinInput.globleParam.KEY_Display_BackSpace_String, "<");
        if (!isOnlinePin) {
            param.putString(ConstIPinpad.startPinInput.param.KEY_promptString_String, "OFFLINE PIN, retry times:" + retryTimes);
        }
        try {
            ipinpad.startPinInput(70, param, globeParam, pinInputListener);
        } catch (RemoteException e) {
            e.printStackTrace();
        }

    }

    private void parseCardName(String data){
        String txt = data.replace('^', '=');
        String[] list = txt.split("=");
        if (list.length > 1){
            cardName = HelperPin.cleanString(list[1]);
        }
        if (cardName.trim().length() == 0){
            cardName = "-";
        }
    }

    public void doEMV(int type, TransType transType, long amount ){
        Bundle emvIntent = new Bundle();
        emvIntent.putInt( ConstIPBOC.startEMV.intent.KEY_cardType_int, type );
        if( transType == TransType.T_PURCHASE ) {
            emvIntent.putLong(ConstIPBOC.startEMV.intent.KEY_authAmount_long, amount );
        }
        emvIntent.putBoolean(ConstIPBOC.startEMV.intent.KEY_isSupportQ_boolean, ConstIPBOC.startEMV.intent.VALUE_supported);
        emvIntent.putBoolean(ConstIPBOC.startEMV.intent.KEY_isSupportSM_boolean, ConstIPBOC.startEMV.intent.VALUE_supported);
        emvIntent.putBoolean(ConstIPBOC.startEMV.intent.KEY_isQPBOCForceOnline_boolean, ConstIPBOC.startEMV.intent.VALUE_forced );
        if( type == ConstIPBOC.startEMV.intent.VALUE_cardType_contactless ) {
            emvIntent.putByte( ConstIPBOC.startEMV.intent.KEY_transProcessCode_byte, (byte)0x00 );
        }
        emvIntent.putBoolean("isSupportPBOCFirst", false);
        try {
            iemv.startEMV(ConstIPBOC.startEMV.processType.full_process, emvIntent, emvHandler);
        } catch (RemoteException e) {
            System.out.println("Start EMV error:" + e.getMessage());
            e.printStackTrace();
        }
    }

    private void initializePinInputListener() {
        pinInputListener = new PinInputListener.Stub() {
            @Override
            public void onInput(int len, int key) throws RemoteException {
                System.out.println(">>len:" + len);
                System.out.println(">>key:" + key);
            }

            @Override
            public void onConfirm(byte[] data, boolean isNonePin) throws RemoteException {
//                log.append("Generate Pinblock Result: " + HelperPin.byte2HexStr(data));
//                log.append("\n");
//                runOnUiThread(updateLog());
                //only if icc card is inserted;

//                runOnUiThread(showToast(HelperPin.byte2HexStr(data)));

                Log.d("PIN", HelperPin.byte2HexStr(data));

                Log.d("Pinblock :", HelperPin.byte2HexStr(data));

                if (HelperPin.byte2HexStr(data).equalsIgnoreCase("098138BF9989C3D6")) {
//                    getActivity().runOnUiThread(showToast("Success"));

                    responseJSON = HelperPin.byte2HexStr(data);

                    SoapActivity action = new SoapActivity();
                    action.execute();
//                    getActivity().finish();
                } else {
                    getActivity().runOnUiThread(showButton("Pin Salah"));

                    getActivity().runOnUiThread(errorDialog());

                }

                if (isChip){
                    iemv.importPin(1, data);
                }
            }

            @Override
            public void onCancel() throws RemoteException {
                try {
                    iemv.abortEMV();
                } catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(int errorCode) throws RemoteException {
                try {
                    iemv.abortEMV();
                } catch (Exception e){
                    e.printStackTrace();
                }
            }
        };
    }

    /**
     * All transaction process is in here for icc card
     * Step Number 3. but initialize first before transaction
     */
    void initializeEMV() {

        emvHandler = new EMVHandler.Stub() {
            @Override
            public void onRequestAmount() throws RemoteException {
            }

            /**
             * Will go here if the card support multiple applet
             * Sub Number 0
             */
            @Override
            public void onSelectApplication(List<Bundle> appList) throws RemoteException {
                for (Bundle aidBundle : appList) {
                    String aidName = aidBundle.getString("aidName");
                    String aid = aidBundle.getString("aid");
                    String aidLabel = aidBundle.getString("aidLabel");
                    Log.i("DemoApp", "AID Name=" + aidName + " | AID Label=" + aidLabel + " | AID=" + aid);
                }
//                log.append("Multiple AID Detected..");
//                log.append("\n");
//                log.append("Select NSICCS");
//                log.append("\n");
//                runOnUiThread(updateLog());
                iemv.importAppSelection(1);
            }

            /**
             * Card Information after reading it
             * Sub Number 1
             */
            @Override
            public void onConfirmCardInfo(Bundle info) throws RemoteException {
                Log.d("DemoApp", "onConfirmCardInfo...");
//                log.append("Reading Card Information..");
//                log.append("\n");
//                runOnUiThread(updateLog());
                try {

                    byte[] data9F12 = iemv.getCardData("9F12");
                    if (data9F12 != null){
                        applet = HelperPin.convertHexToString(HelperPin.hexString(data9F12));
                    } else {
                        byte[] data50 = iemv.getCardData("50");
                        if (data50 != null){
                            applet = HelperPin.convertHexToString(HelperPin.hexString(data50));
                        }
                    }
                } catch (Exception e){
                    e.printStackTrace();
                }
                try {

                    byte[] data9B = iemv.getCardData("9B");

                    tsi = HelperPin.hexString(data9B);

                } catch (Exception e){
                    e.printStackTrace();
                }
                try {

                    byte[] data95 = iemv.getCardData("95");

                    tvr = HelperPin.hexString(data95);

                } catch (Exception e){
                    e.printStackTrace();
                }
                try {

                    byte[] data84 = iemv.getCardData("84");

                    aid = HelperPin.hexString(data84);

                } catch (Exception e){
                    e.printStackTrace();
                }
                try {

                    byte[] data5F20 = iemv.getCardData("5F20");
                    cardName = HelperPin.convertHexToString(HelperPin.hexString(data5F20)).replaceAll("  ", "");
                    if (cardName.trim().length() == 0){
                        cardName = "-";
                    }
                } catch (Exception e){
                    e.printStackTrace();
                }

                try {

                    byte[] data57 = iemv.getCardData("57");

                    String expired = HelperPin.hexString(data57);
                    expiredDate = getExpired(expired.replace("D", "="));

                } catch (Exception e){
                    e.printStackTrace();
                }

                //show the card information and confirm it
//                log.append("Applet Name :" + applet);
//                log.append("\n");
//                log.append("TSI :" +tsi);
//                log.append("\n");
//                log.append("TVR :" +tvr);
//                log.append("\n");
//                log.append("AID :" +aid);
//                log.append("\n");
//                log.append("Card Name :" +cardName);
//                log.append("\n");
//                log.append("Expired :" +expiredDate);
//                log.append("\n");
//                runOnUiThread(updateLog());
                //confirm it, go to next step
                cdnumber = info.getString(ConstPBOCHandler.onConfirmCardInfo.info.KEY_PAN_String);
                iemv.importCardConfirmResult(ConstIPBOC.importCardConfirmResult.pass.allowed);
            }

            /**
             * Doing pinpad, base on CVM on card
             * Sub Number 3
             */
            @Override
            public void onRequestInputPIN(boolean isOnlinePin, int retryTimes) throws RemoteException {
                doPinPad(isOnlinePin, retryTimes);
            }

            /**
             * Optional, by default is confirmed
             */
            @Override
            public void onConfirmCertInfo(String certType, String certInfo) throws RemoteException {
                iemv.importCertConfirmResult(ConstIPBOC.importCertConfirmResult.option.CONFIRM );
            }

            /**
             * Generate F55 for transaction, do online process here
             * Sub Number 4
             */
            @Override
            public void onRequestOnlineProcess(Bundle aaResult) throws RemoteException {
                tagOfF55 = new SparseArray<>();
                byte[] tlv;
                int[] tagList = {
                        0x82,
                        0x84,
                        0x95,
                        0x9a,
                        0x9c,
                        0x5f2a,
                        0x5f34,
                        0x9f02,
                        0x9F03,
                        0x9f10,
                        0x9f1a,
                        0x9f26,
                        0x9f27,
                        0x9f33,
                        0x9f34,
                        0x9f35,
                        0x9f36,
                        0x9f37
                };

                for (int tag : tagList) {
                    tlv = iemv.getCardData(Integer.toHexString(tag).toUpperCase());
                    if (null != tlv && tlv.length > 0) {
                        tagOfF55.put(tag, HelperPin.byte2HexStr(tlv));
                    } else {
                        Log.e("DemoApp", "getCardData:" + Integer.toHexString(tag) + ", fails");
                    }
                }

                StringBuffer buffer = new StringBuffer();
                if (tagOfF55 != null) {
                    for (int i = 0; i < tagOfF55.size(); i++) {
                        int tag = tagOfF55.keyAt(i);
                        String value = tagOfF55.valueAt(i);
                        if (value.length() > 0) {
                            byte[] tmp = appendF55(tag, value);
                            buffer.append(HelperPin.byte2HexStr(tmp));
                        }
                    }
                    tagOfF55 = null;

                }

                String f55 = buffer.toString();

//                log.append("F55 Generated");
//                log.append("\n");
//                log.append(f55);
//                log.append("\n");
//                runOnUiThread(updateLog());
            }

            /**
             * Validate online response from host to card
             * Sub Number 5
             * This is the sample
             * Bundle onlineResult = new Bundle();
             *             onlineResult.putBoolean( ConstIPBOC.inputOnlineResult.onlineResult.KEY_isOnline_boolean, true);
             *             onlineResult.putString(ConstIPBOC.inputOnlineResult.onlineResult.KEY_respCode_String, responseCode);
             *             onlineResult.putString( ConstIPBOC.inputOnlineResult.onlineResult.KEY_authCode_String, authCode);
             *             onlineResult.putString( ConstIPBOC.inputOnlineResult.onlineResult.KEY_field55_String, f55);
             *             iemv.importOnlineResult(onlineResult, new OnlineResultHandler.Stub() {
             *                 @Override
             *                 public void onProccessResult(int result, Bundle data) throws RemoteException {
             *                     emvHandler.onTransactionResult(result, data);
             *                 }
             *             });
             */


            /**
             * Final result after bypass online result to card
             * Sub Number 6
             */
            @Override
            public void onTransactionResult(int result, Bundle data) throws RemoteException {
                switch (result) {

                    case ConstPBOCHandler.onTransactionResult.result.AARESULT_TC:
//                        log.append("Transaction Completed");
//                        log.append("\n");
//                        runOnUiThread(updateLog());
                        break;
                    default:
//                        log.append("Transaction Failed");
//                        log.append("\n");
//                        runOnUiThread(updateLog());
                        break;
                }
            }
        };
    }

    private String getExpired(String trackData2){
        return trackData2 != null && !"".equals(trackData2) && trackData2.length() > 16 ? trackData2.substring(17, 21) : null;
    }

    public byte[] appendF55( int tag, String value ){
        EMVTLVParam emvtlvF55 = new EMVTLVParam();
        String tlv = emvtlvF55.append(tag, value);
        return HelperPin.hexStr2Byte(tlv);
    }

//    @Override
//    public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
//        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
//            AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
//            builder.setCancelable(false);
//            builder.setMessage("Apakah anda yakin akan membatalkan proses " + pageId + "?");
//            builder.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
//                @Override
//                public void onClick(DialogInterface dialog, int which) {
//                    dialog.cancel();
//                    confirmationDialog.dismiss();
//                }
//            });
//            builder.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
//                @Override
//                public void onClick(DialogInterface dialog, int which) {
//                    dialog.cancel();
//                    confirmationDialog.show();
//                }
//            });
//            if (AlertDialog == null || !AlertDialog.isShowing()) {
//                AlertDialog = builder.create();
//                AlertDialog.show();
//            }
//            return true;
//        }
//        return false;
//    }

    public Runnable showToast(final String message){

        Runnable aRunnable = new Runnable(){
            public void run(){
//                Toast.makeText(getActivity().getBaseContext(), message, Toast.LENGTH_LONG).show();
            }
        };

        return aRunnable;

    }

    @Override
    public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
            android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(getContext());
            builder.setCancelable(false);
            builder.setMessage("Apakah anda yakin akan membatalkan proses " + pageId + "?");
            builder.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                    confirmationDialog.dismiss();
                }
            });
            builder.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                    confirmationDialog.show();
                }
            });
            if (AlertDialog == null || !AlertDialog.isShowing()) {
                AlertDialog = builder.create();
                AlertDialog.show();
            }
            return true;
        }
        return false;
    }

    private class SoapActivity extends AsyncTask<Void, Void, Void> {


        @Override
        protected void onPreExecute() {

        }

        @Override
        protected Void doInBackground(Void... params) {
            convertir();
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            AppUtil.displayDialogCard(getContext(), responseJSON);
//            if (msgErr.equalsIgnoreCase("true")) {
//            }
//            AppUtil.displayDialog(getContext(), "");

        }

    }

    private void convertir() {

        try {
//            AppUtil.showDialog();

            msgErr = "false";


            if (!msgErr.equalsIgnoreCase("true")) {

                responseJSON = "Pin Block Anda :\n" + responseJSON;
//                AppUtil.displayDialogCard(getContext(), responseJSON);

            } else {

                responseJSON = "Gagal terhubung ke server";

            }


        } catch (Exception ex) {

            AppUtil.hideDialog();
            Log.d("THREAD 2", ex.toString());

        }

    }

    private Runnable errorDialog() {


        Runnable aRunnable = new Runnable(){
            public void run(){
                AppUtil.displayDialog(getContext(), "Pin Salah");
            }
        };

        return aRunnable;


    }
}
