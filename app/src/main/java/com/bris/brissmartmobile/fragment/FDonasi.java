package com.bris.brissmartmobile.fragment;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;

import com.bris.brissmartmobile.R;
import com.bris.brissmartmobile.listener.ConfirmTrxListener;
import com.bris.brissmartmobile.listener.FavClickListener;
import com.bris.brissmartmobile.model.Favorite;
import com.bris.brissmartmobile.util.AppPreferences;
import com.bris.brissmartmobile.util.AppUtil;
import com.bris.brissmartmobile.util.AppUtilPrint;
import com.bris.brissmartmobile.util.Menu;
import com.bris.brissmartmobile.util.NothingSelectedSpinnerAdapter;
import com.bris.brissmartmobile.util.NumberTextWatcherForThousand;

import org.json.JSONArray;
import org.json.JSONObject;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by valeputra on 6/22/17.
 */

public class FDonasi extends Fragment implements ConfirmTrxListener, FavClickListener,
        View.OnFocusChangeListener, View.OnTouchListener, DialogInterface.OnKeyListener {

    private Spinner spinner1;
    private TextView tvSpinner1;
    private EditText et1, et2, et3;
    private TextInputLayout til1, til2, til3;
    private Button btn1, btnConfirm;
    private ImageButton btnFav;
    String nominal_donasi, nama_donatur, nomor_hp, message, rekening, product, kode_transaksi;
    String tid, pageId, bstype;
    private ArrayAdapter<String> adapterProduct;
    JSONObject json, resdata;
    String msgErr, rcode, cmsg, tcode;
    private String data1, submenutrx, namajenisfav;
    private String[] listDonasiBaznas = null, listDonasi = null;
    private Realm realm;
    private List<Favorite> favDatas = new ArrayList<>();
    private AlertDialog AlertDialog = null,
            confirmationDialog = null;
    private View view;

    private AppPreferences appPref;

    public FDonasi() {
    }

    public View onCreateView(final LayoutInflater inflater, @Nullable final ViewGroup container, @Nullable Bundle savedInstanceState) {
        pageId = getArguments().getString(Menu.MENU_ID);
        Log.d("Cek pageId", pageId);

        listDonasiBaznas = getResources().getStringArray(R.array.product_donasi_baznas);
        listDonasi = getResources().getStringArray(R.array.product_donasi_nonbaznas);

        view = inflater.inflate(R.layout.activity_donasi, container, false);

        appPref = new AppPreferences(getContext());
        tid = appPref.getDeviceTid();
        Log.d("TID DONASI", tid);

        tvSpinner1 = (TextView) view.findViewById(R.id.tv_spinner1);
        tvSpinner1.setText("Jenis Donasi");
        spinner1 = (Spinner) view.findViewById(R.id.spinner1);

        setSpinnerProduct(pageId);

        et1 = (EditText) view.findViewById(R.id.et_nominal_donasi);
        til1 = (TextInputLayout) view.findViewById(R.id.til_nominal_donasi);
        et1.addTextChangedListener(new NumberTextWatcherForThousand(et1));

        et2 = (EditText) view.findViewById(R.id.et_nama_donatur);
        til2 = (TextInputLayout) view.findViewById(R.id.til_nama_donatur);

        et3 = (EditText) view.findViewById(R.id.et_nomor_hp);
        til3 = (TextInputLayout) view.findViewById(R.id.til_nomor_hp);

        btn1 = (Button) view.findViewById(R.id.btn_submit);
        btnFav = (ImageButton) view.findViewById(R.id.btn_daftar_fav);

        et1.setFocusable(false);
        et2.setFocusable(false);
        et3.setFocusable(false);

        btn1 = (Button) view.findViewById(R.id.btn_submit);
        btnFav = (ImageButton) view.findViewById(R.id.btn_daftar_fav);

        AppUtil.toolbarRegular(getContext(), view, pageId);
        realm = Realm.getDefaultInstance();

        try {
            onActionButton();
        } catch (Exception e) {
            e.printStackTrace();
            AppUtil.displayDialog(getContext(), "Maaf, Terjadi Kesalahan");
        }

        return view;
    }

    private void setSpinnerProduct(String title) {
        if (title.equalsIgnoreCase("Baznas")) {
            adapterProduct = new ArrayAdapter<>(getContext(), R.layout.spinner_style, getResources().getStringArray(R.array.product_donasi_baznas));
            adapterProduct.setDropDownViewResource(android.R.layout.simple_list_item_1);
            spinner1.setAdapter(new NothingSelectedSpinnerAdapter(
                    adapterProduct,
                    R.layout.spinner_donasi_nothing_selected,
                    getContext()
            ));
        } else {
            adapterProduct = new ArrayAdapter<>(getContext(), R.layout.spinner_style,
                    getResources().getStringArray(R.array.product_donasi_nonbaznas));
            adapterProduct.setDropDownViewResource(android.R.layout.simple_list_item_1);
            spinner1.setAdapter(new NothingSelectedSpinnerAdapter(
                    adapterProduct,
                    R.layout.spinner_donasi_nothing_selected,
                    getContext()
            ));
        }
        spinner1.setOnItemSelectedListener(new onItemListener());
    }

    private class onItemListener implements AdapterView.OnItemSelectedListener {
        @Override
        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
            if (spinner1.getSelectedItem() != null) {
                til1.setHint("Nominal Donasi");
                til2.setHint("Nama Donatur");
                til3.setHint("Nomor HP");

                et1.setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {

                        et1.setFocusableInTouchMode(true);

                        return false;
                    }
                });

                et2.setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {

                        et2.setFocusableInTouchMode(true);

                        return false;
                    }
                });

                et3.setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {

                        et3.setFocusableInTouchMode(true);

                        return false;
                    }
                });
            } else {
                til1.setHint(tvSpinner1.getText().toString() + " belum dipilih");
                til2.setHint(tvSpinner1.getText().toString() + " belum dipilih");
                til3.setHint(tvSpinner1.getText().toString() + " belum dipilih");
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> adapterView) {
        }
    }

    private void onActionButton() {
        btnFav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    product = spinner1.getSelectedItem().toString();
                    if (pageId.equalsIgnoreCase("Baznas")) {
                        submenutrx = "9001";
                        namajenisfav = "Donasi Baznas";
                        if (product.equalsIgnoreCase(listDonasiBaznas[0])) {
                            data1 = "01|Zakat";
                        } else if (product.equalsIgnoreCase(listDonasiBaznas[1])) {
                            data1 = "02|Infaq";
                        } else if (product.equalsIgnoreCase(listDonasiBaznas[2])) {
                            data1 = "03|Shodaqoh";
                        } else if (product.equalsIgnoreCase(listDonasiBaznas[3])) {
                            data1 = "04|Qurban";
                        }
                    } else {
                        if (product.equalsIgnoreCase(listDonasi[0])) {
                            data1 = "01|Zakat";
                        } else if (product.equalsIgnoreCase(listDonasi[1])) {
                            data1 = "02|Infaq";
                        } else if (product.equalsIgnoreCase(listDonasi[2])) {
                            data1 = "03|Shodaqoh";
                        }

                        if (pageId.equalsIgnoreCase("Dompet Dhuafa")) {
                            submenutrx = "9002";
                            namajenisfav = "Donasi Dompet Dhuafa";
                        } else if (pageId.equalsIgnoreCase("Griya Yatim")) {
                            submenutrx = "9003";
                            namajenisfav = "Donasi Griya Yatim";
                        } else if (pageId.equalsIgnoreCase("ZIS BRI Syariah")) {
                            submenutrx = "9004";
                            namajenisfav = "Donasi ZIS BRISyariah";
                        } else if (pageId.equalsIgnoreCase("Bazis DKI Jakarta")) {
                            submenutrx = "9005";
                            namajenisfav = "Donasi Bazis DKI Jakarta";
                        } else if (pageId.equalsIgnoreCase("Yayasan Dompet Sosial Madani Bali")) {
                            submenutrx = "9006";
                            namajenisfav = "Donasi Yayasan Dompet Sosial Madani Bali";
                        } else if (pageId.equalsIgnoreCase("Yayasan Baitul Maal Hidayatullah")) {
                            submenutrx = "9007";
                            namajenisfav = "Donasi Yayasan Baitul Maal Hidayatullah";
                        } else if (pageId.equalsIgnoreCase("LazisNU Jatim")) {
                            submenutrx = "9008";
                            namajenisfav = "Donasi LazisNU Jatim";
                        }
                    }

                    nama_donatur = et2.getText().toString();
                    nomor_hp = et3.getText().toString();

                    if (nomor_hp.equalsIgnoreCase("")) {
                        getFavData(submenutrx);
                        AppUtil.displayDialogFavorit(getContext(), favDatas, FDonasi.this, "BAYAR");
                        return;
                    }

                    AlertDialog dialogAlert;
                    AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                    builder.setCancelable(true);
                    builder.setMessage("Simpan data transaksi ini sebagai data favorit?");
                    builder.setPositiveButton("Simpan", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface alertdialog, int which) {
                            alertdialog.cancel();

                            if (AppUtil.checkFavorite("CASH", nomor_hp, data1, AppUtil.getDsn())) {
                                android.support.v4.app.FragmentManager manager = getActivity().getSupportFragmentManager();

                                Fragment fragment;
                                FragmentTransaction transaction = manager.beginTransaction();
                                fragment = new FInsertUpdateDataFavorit();
                                Bundle bundle = new Bundle();
                                bundle.putString("title", product);
                                bundle.putString("jenispembayaran", "CASH");
                                bundle.putString("menutrx", "9000");
                                bundle.putString("data1", data1);
                                bundle.putString("data3", nomor_hp);
                                bundle.putString("data4", nama_donatur);
                                bundle.putString("submenutrx", submenutrx);
                                bundle.putString("namajenisfav", namajenisfav);

                                fragment.setArguments(bundle);
                                transaction.replace(R.id.fragment_payment_general, fragment);
                                transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
                                transaction.addToBackStack(null);
                                transaction.commit();

                            } else {
                                AppUtil.displayDialog(getContext(), "Data favorit Telah Ada");
                            }
                        }
                    });
                    builder.setNegativeButton("Pilih favorit", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface alertdialog, int which) {
                            alertdialog.cancel();
                            getFavData(submenutrx);
                            AppUtil.displayDialogFavorit(getContext(), favDatas, FDonasi.this, "BAYAR");
                        }
                    });

                    dialogAlert = builder.create();
                    dialogAlert.show();
                } catch (Exception e) {
                    e.printStackTrace();
                    AppUtil.displayDialog(getContext(), "Maaf, Terjadi Kesalahan");
                }
            }
        });

        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (spinner1.getSelectedItem() == null) {
                    AppUtil.displayDialog(getContext(), tvSpinner1.getText().toString() + " Tidak Boleh Kosong!");
                    return;
                } else {
                    product = spinner1.getSelectedItem().toString();
                }

                if (et1.getText().toString().length() == 0) {
                    til1.setError("Nominal Donasi Tidak Boleh Kosong!");
                    return;
                } else {
                    nominal_donasi = NumberTextWatcherForThousand.trimCommaOfString(et1.getText().toString().trim());
                }

                if (et2.getText().toString().length() == 0) {
                    til2.setError("Nama Donatur Tidak Boleh Kosong!");
                    return;
                } else {
                    nama_donatur = et2.getText().toString();
                }

                if (et3.getText().toString().length() == 0) {
                    til3.setError("Nomor HP Tidak Boleh Kosong!");
                    return;
                } else {
                    nomor_hp = et3.getText().toString();
                }

                SoapActivity action = new SoapActivity();
                action.execute();

            }
        });

    }

    @Override
    public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
            AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
            builder.setCancelable(false);
            builder.setMessage("Apakah anda yakin akan membatalkan proses Donasi " + pageId + "?");
            builder.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                    confirmationDialog.dismiss();
                }
            });
            builder.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                    confirmationDialog.show();
                }
            });
            if (AlertDialog == null || !AlertDialog.isShowing()) {
                AlertDialog = builder.create();
                AlertDialog.show();
            }
            return true;
        }
        return false;
    }

    @Override
    public void getFav(String label, String category) {

    }

    @Override
    public void onclikFav(String label, String kode_produk) {
        RealmResults<Favorite> fav = realm.where(Favorite.class)
                .equalTo("menutrx", "9000")
                .equalTo("jenispembayaran", "CASH")
                .equalTo("submenutrx", submenutrx)
                .equalTo("data1", kode_produk)
                .equalTo("namafav", label)

                .findAll();

        if (fav.size() > 0) {
            et2.setText(fav.get(0).getData4());
            et3.setText(fav.get(0).getData3());
            AppUtil.closeListDialog();
        }
    }

    @Override
    public void deleteFav(String jenispembayaran, String menutrx, String submenutrx, String label) {

    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {

    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        return false;
    }

    @Override
    public void onConfirmClick(String pin) {

    }

    @Override
    public void onCancelClick() {

    }

    public void getFavData(String kode_produk) {
        RealmResults<Favorite> fav = realm.where(Favorite.class)
                .equalTo("menutrx", "9000")
                .equalTo("jenispembayaran", "CASH")
                .equalTo("submenutrx", kode_produk)
                .equalTo("data1", data1)
                .findAll();

        Log.d("Cek fav", fav.toString());

        if (fav.size() > 0) {
            favDatas.clear();
            favDatas.addAll(fav);
        } else {
            favDatas.clear();
        }
    }

    private class SoapActivity extends AsyncTask<Void, Void, Void> {


        @Override
        protected void onPreExecute() {

        }

        @Override
        protected Void doInBackground(Void... params) {
            convertir();
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            if (msgErr.equalsIgnoreCase("true")) {
                AppUtil.displayDialog(getContext(), message);
            }
        }

    }

    private void convertir() {

        try {

            if (pageId.equalsIgnoreCase("Baznas")) {
                rekening = "1000783214";
                if (product.equalsIgnoreCase("Zakat")) {
                    bstype = "01";
                } else if (product.equalsIgnoreCase("Infaq")) {
                    bstype = "02";
                } else if (product.equalsIgnoreCase("Shodaqoh")) {
                    bstype = "03";
                } else if (product.equalsIgnoreCase("Qurban")) {
                    bstype = "04";
                }
            } else if (pageId.equalsIgnoreCase("Dompet Dhuafa")) {
                rekening = "1000782919";
            } else if (pageId.equalsIgnoreCase("Griya Yatim")) {
                rekening = "6530999999";
            } else if (pageId.equalsIgnoreCase("ZIS BRI Syariah")) {
                rekening = "1003987716";
            } else if (pageId.equalsIgnoreCase("Bazis DKI Jakarta")) {
                rekening = "1021861649";
            } else if (pageId.equalsIgnoreCase("Yayasan Dompet Sosial Madani Bali")) {
                rekening = "1011557577";
            } else if (pageId.equalsIgnoreCase("Yayasan Baitul Maal Hidayatullah")) {
                rekening = "1004088267";
            } else if (pageId.equalsIgnoreCase("LazisNU Jatim")) {
                rekening = "1032098203";
            }

            getActivity().runOnUiThread(new Runnable() {
                public void run() {
                    AppUtil.initDialogProgress(getContext());
                    AppUtil.showDialog();
                }
            });

            String dsn = AppUtil.getDsn();
            String phone_id = "62" + dsn.substring(1, dsn.length());

            String cmd;
            JSONObject cmdparam = new JSONObject();
            if (pageId.equalsIgnoreCase("Baznas")) {
                cmd = "basnas_verify";
                cmdparam.put("vdsn", nomor_hp);
                cmdparam.put("cust_name", nama_donatur);
                cmdparam.put("deposit", nominal_donasi);
                cmdparam.put("bstype", bstype);
            } else {
                cmd = "donasi_verify";
                cmdparam.put("message", AppUtil.API_MSG_TRF + " " + rekening + " " + nominal_donasi + " " + product);
                cmdparam.put("msisdn", phone_id);
            }

            String soapRes = AppUtil.soapReq(AppUtil.getDsn(), cmd, cmdparam);

            if (soapRes.equalsIgnoreCase("")) {
                AppUtil.hideDialog();
                getActivity().runOnUiThread(new Runnable() {
                    public void run() {
                        AppUtil.displayDialog(getContext(), "Gagal terhubung ke server");
                    }
                });
            } else {
                json = new JSONObject(soapRes);
                resdata = json.getJSONObject("resdata");
                rcode = resdata.getString("rcode");
                msgErr = "false";

                if (pageId.equalsIgnoreCase("Baznas")) {
                    tcode = resdata.getString("tcode");
                    cmsg = resdata.getString("cmsg");
                } else {
                    message = resdata.getString("message");
                }

                if (rcode.equalsIgnoreCase("00")) {
                    msgErr = "false";
                    new Thread() {
                        public void run() {
                            FDonasi.this.getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    AppUtil.hideDialog();
                                    try {
                                        LayoutInflater li = LayoutInflater.from(getContext());
                                        final View confirmDialog = li.inflate(R.layout.dialog_confirm_agen, null);
                                        btnConfirm = (Button) confirmDialog.findViewById(R.id.btn_submit);
                                        final TextView confirm = (TextView) confirmDialog.findViewById(R.id.txt_title_top);
                                        final EditText pin = (EditText) confirmDialog.findViewById(R.id.txt_pin);

                                        confirm.setText("Donasi " + product + " " + pageId + " atas nama " + nama_donatur + ", No. HP " + nomor_hp + " dengan nominal Rp. " + et1.getText().toString() + ". Jika benar masukkan password Anda");

                                        pin.addTextChangedListener(new TextWatcher() {
                                            @Override
                                            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                                            }

                                            @Override
                                            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                                                if (charSequence.toString().trim().length() == 6) {
                                                    btnConfirm.setEnabled(true);
                                                    btnConfirm.setBackgroundResource(R.drawable.button_shape_blue);
                                                } else {
                                                    btnConfirm.setEnabled(false);
                                                    btnConfirm.setBackgroundResource(R.drawable.button_shape_accent);
                                                }
                                            }

                                            @Override
                                            public void afterTextChanged(Editable editable) {
                                            }
                                        });

                                        AlertDialog.Builder alert = new AlertDialog.Builder(getContext());
                                        alert.setView(confirmDialog);
                                        alert.setCancelable(false);
                                        confirmationDialog = alert.create();
                                        confirmationDialog.setOnKeyListener(FDonasi.this);
                                        confirmationDialog.show();

                                        btnConfirm.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View view) {

                                                AppUtil.initDialogProgress(getContext());
                                                AppUtil.showDialog();

                                                Thread thread = new Thread(new Runnable() {

                                                    @Override
                                                    public void run() {

                                                        try {

                                                            if (pageId.equalsIgnoreCase("Baznas")) {
                                                                kode_transaksi = "9001";
                                                            } else if (pageId.equalsIgnoreCase("Dompet Dhuafa")) {
                                                                kode_transaksi = "9002";
                                                            } else if (pageId.equalsIgnoreCase("Griya Yatim")) {
                                                                kode_transaksi = "9003";
                                                            } else if (pageId.equalsIgnoreCase("ZIS BRI Syariah")) {
                                                                kode_transaksi = "9004";
                                                            } else if (pageId.equalsIgnoreCase("Bazis DKI Jakarta")) {
                                                                kode_transaksi = "9005";
                                                            } else if (pageId.equalsIgnoreCase("Yayasan Dompet Sosial Madani Bali")) {
                                                                kode_transaksi = "9006";
                                                            } else if (pageId.equalsIgnoreCase("Yayasan Baitul Maal Hidayatullah")) {
                                                                kode_transaksi = "9007";
                                                            } else if (pageId.equalsIgnoreCase("LazisNU Jatim")) {
                                                                kode_transaksi = "9008";
                                                            }


                                                            String dsn = AppUtil.getDsn();
                                                            String phone_id = "62" + dsn.substring(1, dsn.length());

                                                            String cmd;
                                                            JSONObject cmdparam = new JSONObject();

                                                            if (pageId.equalsIgnoreCase("Baznas")) {
                                                                cmd = "basnas_process";
                                                                cmdparam.put("vdsn", nomor_hp);
                                                                cmdparam.put("cust_name", nama_donatur);
                                                                cmdparam.put("deposit", nominal_donasi);
                                                                cmdparam.put("bstype", bstype);
                                                                cmdparam.put("vpass", pin.getText().toString());
                                                            } else {
                                                                cmd = "donasi_save";
                                                                cmdparam.put("message", "PIN " + pin.getText().toString());
                                                                cmdparam.put("msisdn", phone_id);
                                                                cmdparam.put("jenistransaksi", kode_transaksi);
                                                                cmdparam.put("idpelanggan", "Donasi " + product + " ke " + pageId + " atas Nama " + nama_donatur + " No HP " + nomor_hp + " sebesar Rp. " + nominal_donasi);
                                                                cmdparam.put("vdsn", nomor_hp);
                                                            }

                                                            String soapRes = AppUtil.soapReq(AppUtil.getDsn(), cmd, cmdparam);

                                                            if (soapRes.equalsIgnoreCase("")) {
                                                                AppUtil.hideDialog();
                                                                getActivity().runOnUiThread(new Runnable() {
                                                                    public void run() {
                                                                        AppUtil.displayDialog(getContext(), "Gagal terhubung ke server");
                                                                    }
                                                                });
                                                            } else {
                                                                json = new JSONObject(soapRes);
                                                                resdata = json.getJSONObject("resdata");
                                                                rcode = resdata.getString("rcode");
                                                                if (pageId.equalsIgnoreCase("Baznas")) {
                                                                    cmsg = resdata.getString("cmsg");
                                                                } else {
                                                                    message = resdata.getString("message");
                                                                    cmsg = message;
                                                                }

                                                                if (rcode.equalsIgnoreCase("00")) {

                                                                    //khusus edc brissmart
                                                                    String date = AppUtil.getDateFormat();
//
                                                                    Bundle datas = new Bundle();
                                                                    datas.putString("menu_id", pageId);
                                                                    datas.putString("tid", tid);
                                                                    datas.putString("mid", "4228267000001");
                                                                    datas.putString("type_card", "-");
                                                                    datas.putString("date", date);
                                                                    datas.putString("msg", cmsg);

                                                                    getActivity().runOnUiThread(new Runnable() {
                                                                        public void run() {

                                                                            AppUtilPrint.printStruk(getContext(), datas);

                                                                            AppUtil.hideDialog();
                                                                            et1.getText().clear();
                                                                            AppUtil.displayDialog(getContext(), cmsg);
                                                                            AppUtil.saveIntoInbox(cmsg, pageId, AppUtil.getDsn());
                                                                            confirmationDialog.dismiss();
                                                                        }
                                                                    });
                                                                } else if (rcode.equalsIgnoreCase("98")) {
                                                                    getActivity().runOnUiThread(new Runnable() {
                                                                        public void run() {
                                                                            AppUtil.hideDialog();
                                                                            AppUtil.displayDialog(getContext(), cmsg);
                                                                            pin.getText().clear();

                                                                        }
                                                                    });
                                                                } else {
                                                                    getActivity().runOnUiThread(new Runnable() {
                                                                        public void run() {
                                                                            AppUtil.hideDialog();
                                                                            AppUtil.displayDialog(getContext(), cmsg);
                                                                            confirmationDialog.dismiss();
                                                                        }
                                                                    });
                                                                }
                                                            }
                                                        } catch (Exception ex) {
                                                            msgErr = "true";
                                                            AppUtil.hideDialog();
                                                            Log.d("THREAD", ex.toString());
                                                            message = "Transaksi tidak dapat diproses";
                                                        }
                                                    }
                                                });
                                                thread.start();
                                            }
                                        });

                                    } catch (Exception e) {
                                        AppUtil.hideDialog();
                                        msgErr = "true";
                                        message = "Transaksi tidak dapat diproses";
                                        Log.d("THREAD 1", e.toString());
                                    }
                                }
                            });
                        }
                    }.start();
                } else {
                    Log.d("else", "gagal");
                    if (pageId.equalsIgnoreCase("Baznas")) {
                        message = resdata.getString("cmsg");
                    } else {
                        message = resdata.getString("message");
                    }
                    getActivity().runOnUiThread(new Runnable() {
                        public void run() {
                            AppUtil.hideDialog();
                            AppUtil.displayDialog(getContext(), message);
                        }
                    });
                }
            }
        } catch (Exception ex) {
            msgErr = "true";
            AppUtil.hideDialog();
            Log.d("THREAD 2", ex.toString());
            message = "Transaksi tidak dapat diproses";
        }
    }
}

