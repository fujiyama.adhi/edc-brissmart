package com.bris.brissmartmobile.fragment;

import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Looper;
import android.os.RemoteException;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.util.SparseArray;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bris.brissmartmobile.HelperPin;
import com.bris.brissmartmobile.PrinterFonts;
import com.bris.brissmartmobile.R;
import com.bris.brissmartmobile.app.BRISSMARTMobileRealmApp;
import com.bris.brissmartmobile.emv.EMVTLVParam;
import com.bris.brissmartmobile.listener.FavClickListener;
import com.bris.brissmartmobile.model.Favorite;
import com.bris.brissmartmobile.util.AppUtil;
import com.bris.brissmartmobile.util.AppUtilPrint;
import com.bris.brissmartmobile.util.NumberTextWatcherForThousand;
import com.vfi.smartpos.deviceservice.aidl.CheckCardListener;
import com.vfi.smartpos.deviceservice.aidl.EMVHandler;
import com.vfi.smartpos.deviceservice.aidl.IBeeper;
import com.vfi.smartpos.deviceservice.aidl.IEMV;
import com.vfi.smartpos.deviceservice.aidl.IPinpad;
import com.vfi.smartpos.deviceservice.aidl.PinInputListener;
import com.vfi.smartpos.deviceservice.constdefine.ConstCheckCardListener;
import com.vfi.smartpos.deviceservice.constdefine.ConstIPBOC;
import com.vfi.smartpos.deviceservice.constdefine.ConstIPinpad;
import com.vfi.smartpos.deviceservice.constdefine.ConstPBOCHandler;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;

public class FTransferRekeningBrisKartu extends Fragment implements FavClickListener, DialogInterface.OnKeyListener, FDialogInqCard.InqClickListener {

    private int amount, fee;
    private EditText et1, et2, et3;
    private TextInputLayout til1, til2, til3;
    private Button btn1, btnConfirm;
    private ImageButton btnFav;
    private ImageView iv_swipe_card;
    private TextView tv_swipe_card;
    private boolean isChip = false;
    private List<Favorite> favDatas = new ArrayList<>();
    String param2, param3, rek_tujuan;
    String pageId;
    JSONObject json, resdata;
    String responseJSON, msgErr = "false", rcode, name, cmsg;
    private Realm realm;
    private android.support.v7.app.AlertDialog AlertDialog = null,
            confirmationDialog = null;
    private View view;
    public IEMV iemv;
    IBeeper iBeeper;
    EMVHandler emvHandler;
    IPinpad ipinpad;
    private String applet, tsi, tvr, aid,cardName = "-", expiredDate ="", cdnumber;
    SparseArray<String> tagOfF55 = null;
    PinInputListener pinInputListener;


    public FTransferRekeningBrisKartu() {}

    public View onCreateView(final LayoutInflater inflater, @Nullable final ViewGroup container, @Nullable Bundle savedInstanceState) {
        pageId = getArguments().getString("menu_id");

        view = inflater.inflate(R.layout.activity_transfer_bris_kartu, container, false);
        AppUtil.toolbarRegular(getContext(), view, pageId.replace("Kartu", ""));

        realm = Realm.getDefaultInstance();

        til1 = (TextInputLayout) view.findViewById(R.id.param1Til);
        et1 = (EditText) view.findViewById(R.id.param1Txt);

        til1.setVisibility(View.GONE);
        et1.setVisibility(View.GONE);

        til2 = (TextInputLayout) view.findViewById(R.id.param2Til);
        et2 = (EditText) view.findViewById(R.id.param2Txt);
        til2.setHint("Nomor Rekening BRI Syariah Tujuan");
        til3 = (TextInputLayout) view.findViewById(R.id.param3Til);
        et3 = (EditText) view.findViewById(R.id.param3Txt);
        et3.addTextChangedListener(new NumberTextWatcherForThousand(et3));
        btn1 = (Button) view.findViewById(R.id.btn_submit);
        btnFav = (ImageButton) view.findViewById(R.id.btn_daftar_fav);
        tv_swipe_card = (TextView) view.findViewById(R.id.tv_swipe_card2);
        iv_swipe_card = (ImageView) view.findViewById(R.id.iv_swipe_card);

        et2.setFocusable(false);
        et2.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                et2.setFocusableInTouchMode(true);

                return false;
            }
        });

        et3.setFocusable(false);
        et3.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                et3.setFocusableInTouchMode(true);

                return false;
            }
        });

        iemv = BRISSMARTMobileRealmApp.getInstance().getiEMV();
        ipinpad = BRISSMARTMobileRealmApp.getInstance().getIpinpad();
        iBeeper = BRISSMARTMobileRealmApp.getInstance().getiBeeper();

        initializeEMV();
        initializePinInputListener();

        try {
            onActionButton();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return view;
    }

    private void onActionButton() {
        btnFav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    param2 = et2.getText().toString().trim();

                    if (param2.equalsIgnoreCase("")) {
                        getFavData();
                        AppUtil.displayDialogFavorit(getContext(), favDatas, FTransferRekeningBrisKartu.this, "FAVTRANS");
                        return;
                    }

                    AlertDialog dialogAlert;
                    AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                    builder.setCancelable(true);
                    builder.setMessage("Simpan data transaksi ini sebagai data favorit?");
                    builder.setPositiveButton("Simpan", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface alertdialog, int which) {
                            alertdialog.cancel();

                            if (AppUtil.checkFavorite("CASH", param2, "Rekening BRI Syariah", AppUtil.getDsn())) {
                                FragmentManager manager = getActivity().getSupportFragmentManager();
                                Fragment fragment;
                                FragmentTransaction transaction = manager.beginTransaction();
                                fragment = new FInsertUpdateDataFavorit();
                                Bundle bundle = new Bundle();
                                bundle.putString("title", pageId);
                                bundle.putString("jenispembayaran", "CASH");
                                bundle.putString("data1", "Rekening BRI Syariah");
                                bundle.putString("data3", param2);
                                bundle.putString("menutrx", "8000");
                                bundle.putString("submenutrx", "8002");
                                bundle.putString("namajenisfav", "Transfer Rekening BRI Syariah");

                                fragment.setArguments(bundle);
                                transaction.replace(R.id.fragment_payment_general, fragment);
                                transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
                                transaction.addToBackStack(null);
                                transaction.commit();

                            } else {
                                AppUtil.displayDialog(getContext(), "Data favorit Telah Ada");
                            }
                        }
                    });
                    builder.setNegativeButton("Pilih favorit", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface alertdialog, int which) {
                            alertdialog.cancel();
                            getFavData();
                            AppUtil.displayDialogFavorit(getContext(), favDatas, FTransferRekeningBrisKartu.this, "FAVTRANS");
                        }
                    });

                    dialogAlert = builder.create();
                    dialogAlert.show();
                } catch (Exception e) {
                    e.printStackTrace();
                    AppUtil.displayDialog(getContext(), "Maaf, Terjadi Kesalahan");
                }
            }
        });

        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(et2.getText().toString().length() == 0){
                    til2.setError(til2.getHint() + " Tidak Boleh Kosong!");
                    return;
                } else {
                    param2 = et2.getText().toString();
                }
                if(et3.getText().toString().length() == 0){
                    til3.setError(til3.getHint() + " Tidak Boleh Kosong!");
                    return;
                } else {
                    param3 = NumberTextWatcherForThousand.trimCommaOfString(et3.getText().toString().trim());
                }

                rek_tujuan = et2.getText().toString();
                amount = Integer.parseInt(param3);
                isChip = false;
                startEmvProcess(0.0);

                Toast.makeText(getActivity().getBaseContext(), "Please insert or swipe card", Toast.LENGTH_LONG).show();
                tv_swipe_card.setVisibility(View.VISIBLE);
                iv_swipe_card.setVisibility(View.VISIBLE);
                tv_swipe_card.setText("Swipe atau Masukkan Kartu BRI Syariah");
                btn1.setVisibility(View.INVISIBLE);
                btn1.setText("Refresh");
                hideKeyboard();

//                SoapActivity action = new SoapActivity();
//                action.execute();

            }
        });
    }

    private void hideKeyboard() {

        InputMethodManager mgr = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        mgr.hideSoftInputFromWindow(et1.getWindowToken(), 0);

    }

    @Override
    public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
        if(keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount()==0){
            AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
            builder.setCancelable(false);
            builder.setMessage("Apakah anda yakin akan membatalkan proses " + pageId + "?");
            builder.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                    confirmationDialog.dismiss();
                }
            });
            builder.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                    confirmationDialog.show();
                }
            });
            if(AlertDialog == null || !AlertDialog.isShowing()) {
                AlertDialog = builder.create();
                AlertDialog.show();
            }
            return true;
        }
        return false;
    }

    @Override
    public void getFav(String label, String category) {

    }

    @Override
    public void onclikFav(String label, String kode_produk) {
        RealmResults<Favorite> fav = realm.where(Favorite.class)
                .equalTo("namafav", label)
                .equalTo("data3", kode_produk)
                .findAll();

        if (fav.size() > 0) {
            et2.setText(fav.get(0).getData3());
            AppUtil.closeListDialog();
        }
    }

    @Override
    public void deleteFav(String jenispembayaran, String menutrx, String submenutrx, String label) {

    }

    public void getFavData() {
        RealmResults<Favorite> fav = realm.where(Favorite.class)
                .equalTo("menutrx", "8000")
                .equalTo("submenutrx", "8002")
                .equalTo("jenispembayaran", "CASH")
                .findAll();

        RealmResults<Favorite> favorit = realm.where(Favorite.class).findAll();
        Log.d("Cek fav FTransRekCerdas", fav.toString());
        Log.d("Cek favorit", favorit.toString());

        if (fav.size() > 0) {
            favDatas.clear();
            favDatas.addAll(fav);
        } else {
            favDatas.clear();
        }
    }

    @Override
    public void dialogInqButton(String action) {
        showToast(action);
        doPinPad(true, 3);
    }

    private class SoapActivity extends AsyncTask<Void, Void, Void> {


        @Override
        protected void onPreExecute() {

        }

        @Override
        protected Void doInBackground(Void... params) {
            convertir2();
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
//            if (msgErr.equalsIgnoreCase("true")) {
//                AppUtil.displayDialog(getContext(), responseJSON);
//            }

            if (msgErr.equalsIgnoreCase("true")) {
                Log.e("MSG", "ERROR");
            } else {
                Log.d("PRINT", "callPrintStruk");
                callPrint(responseJSON);
            }
        }

    }

    private void convertir2() {

        try {
            if (!msgErr.equalsIgnoreCase("true")) {

                if (isChip) {
                    responseJSON = "CHIP";
                    Log.d("SOAP","PIN BENAR");
                } else {
                    responseJSON = "CARD";
                    Log.d("SOAP","PIN BENAR");
                }

            } else {

                responseJSON = "Gagal terhubung ke server";

            }


        } catch (Exception ex) {

            AppUtil.hideDialog();
            Log.d("THREAD 2", ex.toString());

        }

    }

    private void convertir() {

        try {
            getActivity().runOnUiThread(new Runnable() {
                public void run() {
                    AppUtil.initDialogProgress(getContext());
                    AppUtil.showDialog();
                }
            });

            JSONObject cmdparam = new JSONObject();
            cmdparam.put("accno", param2);
            cmdparam.put("amount", param3);
            String soapRes = AppUtil.soapReq(AppUtil.getDsn(), "depositBrisCash_verify", cmdparam);

            if (soapRes.equalsIgnoreCase("")) {
                AppUtil.hideDialog();
                msgErr = "true";
                responseJSON = "Gagal terhubung ke server";
            } else {
                json = new JSONObject(soapRes);
                resdata = json.getJSONObject("resdata");
                rcode = resdata.getString("rcode");
                msgErr = "false";

                if (rcode.equalsIgnoreCase("00")) {
                    String tcode = resdata.getString("tcode");
                    if (tcode.equalsIgnoreCase("00")) {
                        msgErr = "false";

                        new Thread() {
                            public void run() {
                                FTransferRekeningBrisKartu.this.getActivity().runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        AppUtil.hideDialog();
                                        try {
                                            LayoutInflater li = LayoutInflater.from(getContext());
                                            final View confirmDialog = li.inflate(R.layout.dialog_confirm_agen, null);
                                            btnConfirm = (Button) confirmDialog.findViewById(R.id.btn_submit);
                                            final TextView confirm = (TextView) confirmDialog.findViewById(R.id.txt_title_top);
                                            final EditText et4 = (EditText) confirmDialog.findViewById(R.id.txt_pin);

                                            confirm.setText("Anda akan melakukan Transfer ke BRI Syariah Rek " + param2 + " sebesar Rp. " + et3.getText().toString() + ". Jika benar masukkan PIN Anda");
                                            et4.addTextChangedListener(new TextWatcher() {
                                                @Override
                                                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                                                }

                                                @Override
                                                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                                                    if (charSequence.toString().trim().length() == 6) {
                                                        btnConfirm.setEnabled(true);
                                                        btnConfirm.setBackgroundResource(R.drawable.button_shape_blue);
                                                    } else {
                                                        btnConfirm.setEnabled(false);
                                                        btnConfirm.setBackgroundResource(R.drawable.button_shape_accent);
                                                    }
                                                }

                                                @Override
                                                public void afterTextChanged(Editable editable) {
                                                }
                                            });

                                            AlertDialog.Builder alert = new AlertDialog.Builder(getContext());
                                            alert.setView(confirmDialog);
                                            alert.setCancelable(false);
                                            confirmationDialog = alert.create();
                                            confirmationDialog.setOnKeyListener(FTransferRekeningBrisKartu.this);
                                            confirmationDialog.show();

                                            btnConfirm.setOnClickListener(new View.OnClickListener() {
                                                @Override
                                                public void onClick(View view) {

                                                    AppUtil.initDialogProgress(getContext());
                                                    AppUtil.showDialog();

                                                    Thread thread = new Thread(new Runnable() {

                                                        @Override
                                                        public void run() {
                                                            try {
                                                                JSONObject cmdparam = new JSONObject();
                                                                cmdparam.put("accno", param2);
                                                                cmdparam.put("amount", param3);
                                                                cmdparam.put("vpass", et4.getText().toString());
                                                                String soapRes = AppUtil.soapReq(AppUtil.getDsn(), "depositBrisCash_save", cmdparam);

                                                                if (soapRes.equalsIgnoreCase("")) {
                                                                    AppUtil.hideDialog();
                                                                    msgErr = "true";
                                                                    responseJSON = "Gagal terhubung ke server";
                                                                } else {
                                                                    json = new JSONObject(soapRes);
                                                                    resdata = json.getJSONObject("resdata");
                                                                    rcode = resdata.getString("rcode");
                                                                    cmsg = resdata.getString("cmsg");

                                                                    if (rcode.equalsIgnoreCase("00")) {
                                                                        AppUtil.hideDialog();
                                                                        getActivity().runOnUiThread(new Runnable() {
                                                                            public void run() {
                                                                                et2.getText().clear();
                                                                                et3.getText().clear();

                                                                                et2.setFocusable(false);
                                                                                et2.setOnTouchListener(new View.OnTouchListener() {
                                                                                    @Override
                                                                                    public boolean onTouch(View v, MotionEvent event) {

                                                                                        et2.setFocusableInTouchMode(true);

                                                                                        return false;
                                                                                    }
                                                                                });
                                                                                et3.setFocusable(false);
                                                                                et3.setOnTouchListener(new View.OnTouchListener() {
                                                                                    @Override
                                                                                    public boolean onTouch(View v, MotionEvent event) {

                                                                                        et3.setFocusableInTouchMode(true);

                                                                                        return false;
                                                                                    }
                                                                                });
                                                                                AppUtil.displayDialog(getContext(), cmsg);
                                                                                AppUtil.saveIntoInbox(cmsg, pageId, AppUtil.getDsn());
                                                                                confirmationDialog.dismiss();
                                                                            }
                                                                        });
                                                                    } else if (rcode.equalsIgnoreCase("13")) {
                                                                        AppUtil.hideDialog();
                                                                        getActivity().runOnUiThread(new Runnable() {
                                                                            public void run() {
                                                                                AppUtil.displayDialog(getContext(), cmsg);
                                                                                et4.getText().clear();
                                                                            }
                                                                        });
                                                                    } else {
                                                                        AppUtil.hideDialog();
                                                                        cmsg = resdata.getString("cmsg");
                                                                        getActivity().runOnUiThread(new Runnable() {
                                                                            public void run() {
                                                                                AppUtil.displayDialog(getContext(), cmsg);
                                                                                confirmationDialog.dismiss();
                                                                            }
                                                                        });
                                                                    }
                                                                }
                                                            } catch (Exception ex) {
                                                                msgErr = "true";
                                                                AppUtil.hideDialog();
                                                                Log.d("THREAD", ex.toString());
                                                                responseJSON = "Transaksi gagal";
                                                            }
                                                        }
                                                    });
                                                    thread.start();
                                                }
                                            });

                                        } catch (Exception e) {
                                            Log.d("THREAD 1", e.toString());
                                            msgErr = "true";
                                            AppUtil.hideDialog();
                                            responseJSON = "Transaksi gagal";
                                        }
                                    }
                                });
                            }
                        }.start();
                    } else {
                        AppUtil.hideDialog();
                        cmsg = resdata.getString("cmsg");
                        responseJSON = cmsg;
                        msgErr = "true";
                    }
                } else {
                    Log.d("else", "gagal");
                    AppUtil.hideDialog();
                    cmsg = resdata.getString("cmsg");
                    responseJSON = cmsg;
                    msgErr = "true";
                }
            }
        } catch (Exception ex) {
            msgErr = "true";
            AppUtil.hideDialog();
            Log.d("THREAD 2", ex.toString());
            responseJSON = "Transaksi gagal";
        }
    }

    /**
     *
     * @param amount
     * Step Number 1. to be executed, reading card
     */
    public void startEmvProcess(double amount){
        if (amount == 0){
            doSearchCard(TransType.T_BANLANCE, amount);
        } else {
            doSearchCard(TransType.T_PURCHASE, amount);
        }
    }

    enum TransType {
        T_BANLANCE, T_PURCHASE
    }

    public void doSearchCard(final TransType transType, final double amount){
        Bundle cardOption = new Bundle();
        cardOption.putBoolean( ConstIPBOC.checkCard.cardOption.KEY_Contactless_boolean, ConstIPBOC.checkCard.cardOption.VALUE_unsupported);
        cardOption.putBoolean( ConstIPBOC.checkCard.cardOption.KEY_SmartCard_boolean, ConstIPBOC.checkCard.cardOption.VALUE_supported);
        cardOption.putBoolean( ConstIPBOC.checkCard.cardOption.KEY_MagneticCard_boolean, ConstIPBOC.checkCard.cardOption.VALUE_supported);


        try {
            iemv.checkCard(cardOption, 5, new CheckCardListener.Stub() {
                        @Override
                        public void onCardSwiped(Bundle track) throws RemoteException {
                            Log.d( "TAG", "onCardSwiped ..." );
                            iemv.stopCheckCard();
                            iemv.abortEMV();

                            iBeeper.startBeep(200);

                            String pan = track.getString(ConstCheckCardListener.onCardSwiped.track.KEY_PAN_String);
                            String track1 = track.getString(ConstCheckCardListener.onCardSwiped.track.KEY_TRACK1_String);
                            if (track1 != null){
                                parseCardName(track1);
                            }
                            String track2 = track.getString(ConstCheckCardListener.onCardSwiped.track.KEY_TRACK2_String);
                            String track3 = track.getString(ConstCheckCardListener.onCardSwiped.track.KEY_TRACK3_String);
                            String serviceCode = track.getString(ConstCheckCardListener.onCardSwiped.track.KEY_SERVICE_CODE_String);
                            cdnumber = pan;
                            expiredDate = track2.substring(17, 21);
//                            log.append("Mag Card Swiped..");
//                            log.append("\n");
//                            log.append("Card Number:" + pan);
//                            log.append("\n");
//                            log.append("Track 2 Data:" + track2);
//                            log.append("\n");
//                            runOnUiThread(updateLog());
                            //incase magnetic card detected, just go to input pin
                            //and process it

                            getActivity().runOnUiThread(new Runnable() {
                                public void run() {
                                    AppUtil.initDialogProgress(getContext());
                                }
                            });

                            getActivity().runOnUiThread(showDialogThreat(pageId ,generateTextInq()));
                            getActivity().runOnUiThread(showButton(" "));
//                            doPinPad(true, 3);
                        }

                        @Override
                        public void onCardPowerUp() throws RemoteException {
                            iemv.stopCheckCard();
                            iemv.abortEMV();
                            iBeeper.startBeep(200);
//                            log.append("ICC Card Detected..");
//                            log.append("\n");
//                            log.append("Reading Card..");
//                            log.append("\n");
//                            runOnUiThread(updateLog());
                            isChip = true;

                            getActivity().runOnUiThread(new Runnable() {
                                public void run() {
                                    AppUtil.initDialogProgress(getContext());
                                }
                            });

                            doEMV( ConstIPBOC.startEMV.intent.VALUE_cardType_smart_card , transType, (long) amount );
                        }

                        @Override
                        public void onCardActivate() throws RemoteException {
                            iemv.stopCheckCard();
                            iemv.abortEMV();
                            iBeeper.startBeep(200);
//                            log.append("ICC Contactless Card Detected..");
//                            log.append("\n");
//                            log.append("Reading Card..");
//                            runOnUiThread(updateLog());
                            isChip = true;
//                            doEMV( ConstIPBOC.startEMV.intent.VALUE_cardType_contactless, transType, (long) amount );
                        }

                        @Override
                        public void onTimeout() throws RemoteException {
//                            runOnUiThread(showToast("Timeout"));
//                            finish();
                            Log.e("Search Card", "Timeout");
                            AppUtil.hideDialog();
                            getActivity().runOnUiThread(showButton("Timeout"));
                            iBeeper.startBeep(100);

                        }

                        @Override
                        public void onError(int error, String message) throws RemoteException {
                            System.out.println("error:" + error);
                            if (error == 3){
                                Looper.prepare();
                                new CountDownTimer(1000, 1000) {

                                    public void onTick(long millisUntilFinished) {
                                    }

                                    public void onFinish() {
//                                        runOnUiThread(showToast("Use Magnetic Card"));
                                        AppUtil.hideDialog();
                                        getActivity().runOnUiThread(showButton("Error " + ""+error));
                                        try {
                                            iBeeper.startBeep(100);
                                        } catch (RemoteException e) {
                                            e.printStackTrace();
                                        }
                                        Log.e("Search Card", "Use Magnetic Card");

                                    }

                                }.start();
                                Looper.loop();
                            } else if (error == 100){
                                doSearchCard(transType, amount);
//                                runOnUiThread(showToast("Card not readable, please try again"));
                                Log.e("Search Card", "Card not readable, please try again");
                            }
                        }
                    }
            );
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    public void doEMV(int type, TransType transType, long amount ){
        getActivity().runOnUiThread(showButton(" "));
        Bundle emvIntent = new Bundle();
        emvIntent.putInt( ConstIPBOC.startEMV.intent.KEY_cardType_int, type );
        if( transType == TransType.T_PURCHASE ) {
            emvIntent.putLong(ConstIPBOC.startEMV.intent.KEY_authAmount_long, amount );
        }
        emvIntent.putBoolean(ConstIPBOC.startEMV.intent.KEY_isSupportQ_boolean, ConstIPBOC.startEMV.intent.VALUE_supported);
        emvIntent.putBoolean(ConstIPBOC.startEMV.intent.KEY_isSupportSM_boolean, ConstIPBOC.startEMV.intent.VALUE_supported);
        emvIntent.putBoolean(ConstIPBOC.startEMV.intent.KEY_isQPBOCForceOnline_boolean, ConstIPBOC.startEMV.intent.VALUE_forced );
        if( type == ConstIPBOC.startEMV.intent.VALUE_cardType_contactless ) {
            emvIntent.putByte( ConstIPBOC.startEMV.intent.KEY_transProcessCode_byte, (byte)0x00 );
        }
        emvIntent.putBoolean("isSupportPBOCFirst", false);
        try {
            iemv.startEMV(ConstIPBOC.startEMV.processType.full_process, emvIntent, emvHandler);
        } catch (RemoteException e) {
            System.out.println("Start EMV error:" + e.getMessage());
            e.printStackTrace();
        }
    }

    void initializeEMV() {

        emvHandler = new EMVHandler.Stub() {
            @Override
            public void onRequestAmount() throws RemoteException {
            }

            /**
             * Will go here if the card support multiple applet
             * Sub Number 0
             */
            @Override
            public void onSelectApplication(List<Bundle> appList) throws RemoteException {
                for (Bundle aidBundle : appList) {
                    String aidName = aidBundle.getString("aidName");
                    String aid = aidBundle.getString("aid");
                    String aidLabel = aidBundle.getString("aidLabel");
                    Log.i("DemoApp", "AID Name=" + aidName + " | AID Label=" + aidLabel + " | AID=" + aid);
                }
//                log.append("Multiple AID Detected..");
//                log.append("\n");
//                log.append("Select NSICCS");
//                log.append("\n");
//                runOnUiThread(updateLog());
                iemv.importAppSelection(1);
            }

            /**
             * Card Information after reading it
             * Sub Number 1
             */
            @Override
            public void onConfirmCardInfo(Bundle info) throws RemoteException {
                Log.d("DemoApp", "onConfirmCardInfo...");
//                log.append("Reading Card Information..");
//                log.append("\n");
//                runOnUiThread(updateLog());
                try {

                    byte[] data9F12 = iemv.getCardData("9F12");
                    if (data9F12 != null){
                        applet = HelperPin.convertHexToString(HelperPin.hexString(data9F12));
                    } else {
                        byte[] data50 = iemv.getCardData("50");
                        if (data50 != null){
                            applet = HelperPin.convertHexToString(HelperPin.hexString(data50));
                        }
                    }
                } catch (Exception e){
                    e.printStackTrace();
                }
                try {

                    byte[] data9B = iemv.getCardData("9B");

                    tsi = HelperPin.hexString(data9B);

                } catch (Exception e){
                    e.printStackTrace();
                }
                try {

                    byte[] data95 = iemv.getCardData("95");

                    tvr = HelperPin.hexString(data95);

                } catch (Exception e){
                    e.printStackTrace();
                }
                try {

                    byte[] data84 = iemv.getCardData("84");

                    aid = HelperPin.hexString(data84);

                } catch (Exception e){
                    e.printStackTrace();
                }
                try {

                    byte[] data5F20 = iemv.getCardData("5F20");
                    cardName = HelperPin.convertHexToString(HelperPin.hexString(data5F20)).replaceAll("  ", "");
                    if (cardName.trim().length() == 0){
                        cardName = "-";
                    }
                } catch (Exception e){
                    e.printStackTrace();
                }

                try {

                    byte[] data57 = iemv.getCardData("57");

                    String expired = HelperPin.hexString(data57);
                    expiredDate = getExpired(expired.replace("D", "="));

                } catch (Exception e){
                    e.printStackTrace();
                }

                //show the card information and confirm it
//                log.append("Applet Name :" + applet);
//                log.append("\n");
//                log.append("TSI :" +tsi);
//                log.append("\n");
//                log.append("TVR :" +tvr);
//                log.append("\n");
//                log.append("AID :" +aid);
//                log.append("\n");
//                log.append("Card Name :" +cardName);
//                log.append("\n");
//                log.append("Expired :" +expiredDate);
//                log.append("\n");
//                runOnUiThread(updateLog());
                //confirm it, go to next step
                cdnumber = info.getString(ConstPBOCHandler.onConfirmCardInfo.info.KEY_PAN_String);
                iemv.importCardConfirmResult(ConstIPBOC.importCardConfirmResult.pass.allowed);
            }

            /**
             * Doing pinpad, base on CVM on card
             * Sub Number 3
             */
            @Override
            public void onRequestInputPIN(boolean isOnlinePin, int retryTimes) throws RemoteException {
                AppUtil.hideDialog();
                getActivity().runOnUiThread(showDialogThreat(pageId, generateTextInq()));

//                doPinPad(isOnlinePin, retryTimes);
            }

            /**
             * Optional, by default is confirmed
             */
            @Override
            public void onConfirmCertInfo(String certType, String certInfo) throws RemoteException {
                iemv.importCertConfirmResult(ConstIPBOC.importCertConfirmResult.option.CONFIRM );
            }

            /**
             * Generate F55 for transaction, do online process here
             * Sub Number 4
             */
            @Override
            public void onRequestOnlineProcess(Bundle aaResult) throws RemoteException {
                tagOfF55 = new SparseArray<>();
                byte[] tlv;
                int[] tagList = {
                        0x82,
                        0x84,
                        0x95,
                        0x9a,
                        0x9c,
                        0x5f2a,
                        0x5f34,
                        0x9f02,
                        0x9F03,
                        0x9f10,
                        0x9f1a,
                        0x9f26,
                        0x9f27,
                        0x9f33,
                        0x9f34,
                        0x9f35,
                        0x9f36,
                        0x9f37
                };

                for (int tag : tagList) {
                    tlv = iemv.getCardData(Integer.toHexString(tag).toUpperCase());
                    if (null != tlv && tlv.length > 0) {
                        tagOfF55.put(tag, HelperPin.byte2HexStr(tlv));
                    } else {
                        Log.e("DemoApp", "getCardData:" + Integer.toHexString(tag) + ", fails");
                    }
                }

                StringBuffer buffer = new StringBuffer();
                if (tagOfF55 != null) {
                    for (int i = 0; i < tagOfF55.size(); i++) {
                        int tag = tagOfF55.keyAt(i);
                        String value = tagOfF55.valueAt(i);
                        if (value.length() > 0) {
                            byte[] tmp = appendF55(tag, value);
                            buffer.append(HelperPin.byte2HexStr(tmp));
                        }
                    }
                    tagOfF55 = null;

                }

                String f55 = buffer.toString();

//                log.append("F55 Generated");
//                log.append("\n");
//                log.append(f55);
//                log.append("\n");
//                runOnUiThread(updateLog());
            }

            /**
             * Validate online response from host to card
             * Sub Number 5
             * This is the sample
             * Bundle onlineResult = new Bundle();
             *             onlineResult.putBoolean( ConstIPBOC.inputOnlineResult.onlineResult.KEY_isOnline_boolean, true);
             *             onlineResult.putString(ConstIPBOC.inputOnlineResult.onlineResult.KEY_respCode_String, responseCode);
             *             onlineResult.putString( ConstIPBOC.inputOnlineResult.onlineResult.KEY_authCode_String, authCode);
             *             onlineResult.putString( ConstIPBOC.inputOnlineResult.onlineResult.KEY_field55_String, f55);
             *             iemv.importOnlineResult(onlineResult, new OnlineResultHandler.Stub() {
             *                 @Override
             *                 public void onProccessResult(int result, Bundle data) throws RemoteException {
             *                     emvHandler.onTransactionResult(result, data);
             *                 }
             *             });
             */


            /**
             * Final result after bypass online result to card
             * Sub Number 6
             */
            @Override
            public void onTransactionResult(int result, Bundle data) throws RemoteException {
                switch (result) {

                    case ConstPBOCHandler.onTransactionResult.result.AARESULT_TC:
//                        log.append("Transaction Completed");
//                        log.append("\n");
//                        runOnUiThread(updateLog());
                        break;
                    default:
//                        log.append("Transaction Failed");
//                        log.append("\n");
//                        runOnUiThread(updateLog());
                        break;
                }
            }
        };
    }

    private void parseCardName(String data){
        String txt = data.replace('^', '=');
        String[] list = txt.split("=");
        if (list.length > 1){
            cardName = HelperPin.cleanString(list[1]);
        }
        if (cardName.trim().length() == 0){
            cardName = "-";
        }
    }

    private String getExpired(String trackData2){
        return trackData2 != null && !"".equals(trackData2) && trackData2.length() > 16 ? trackData2.substring(17, 21) : null;
    }

    public byte[] appendF55( int tag, String value ){
        EMVTLVParam emvtlvF55 = new EMVTLVParam();
        String tlv = emvtlvF55.append(tag, value);
        return HelperPin.hexStr2Byte(tlv);
    }

    public Runnable showDialogThreat(final String title, final String message){

        Runnable aRunnable = new Runnable(){
            public void run(){
                AppUtil.hideDialog();
                Bundle datas = new Bundle();
                datas.putString("title", title);
                datas.putString("message", message);
//                displayDialogInfoCard(getContext(), message, "card");

                FDialogInqCard dialogFragment = new FDialogInqCard();
                dialogFragment.setArguments(datas);
                dialogFragment.setTargetFragment(FTransferRekeningBrisKartu.this, 0);
                dialogFragment.show(getFragmentManager(), "dialog");
            }
        };

        return aRunnable;

    }

    public String generateTextInq() {
        String text;
        String scdnumber = cdnumber.substring(0, 6) + "******" + cdnumber.substring(12, 16);
        String sexpiredDate = expiredDate.substring(2,4) + "/" + expiredDate.substring(0,2);

        text = "ALL CARD" + "\n\n" +
                "Card Name : " + cardName + "\n" +
                "Card Number : " + scdnumber + "\n" +
                "Exp date : " + sexpiredDate;

        return text;
    }

    public Runnable showButton(String text){

        Runnable aRunnable = new Runnable(){
            public void run(){
                btn1.setVisibility(View.VISIBLE);
                tv_swipe_card.setText("Waktu Habis\nSilahkan tekan tombol refresh");

                if (!text.equalsIgnoreCase(" ")) {
                    Toast.makeText(getActivity().getBaseContext(), text, Toast.LENGTH_SHORT).show();
                }
            }
        };

        return aRunnable;

    }

    public Runnable showToast(final String message){

        Runnable aRunnable = new Runnable(){
            public void run(){
                Toast.makeText(getActivity().getBaseContext(), message, Toast.LENGTH_LONG).show();
            }
        };

        return aRunnable;

    }

    public void doPinPad(boolean isOnlinePin, int retryTimes) {
        Bundle param = new Bundle();
        Bundle globeParam = new Bundle();
        String panBlock = cdnumber;
        byte[] pinLimit = {0, 6};
        byte[] displayKey = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
        param.putByteArray("displayKeyValue", displayKey);
        param.putByteArray(ConstIPinpad.startPinInput.param.KEY_pinLimit_ByteArray, pinLimit);
        param.putInt(ConstIPinpad.startPinInput.param.KEY_timeout_int, 20);
        param.putBoolean(ConstIPinpad.startPinInput.param.KEY_isOnline_boolean, isOnlinePin);
        param.putString(ConstIPinpad.startPinInput.param.KEY_pan_String, panBlock);
        param.putInt(ConstIPinpad.startPinInput.param.KEY_desType_int, ConstIPinpad.startPinInput.param.Value_desType_3DES);
        param.putString( "promptString" , "Silakan Masukan PIN Anda") ;
        globeParam.putString(ConstIPinpad.startPinInput.globleParam.KEY_Display_BackSpace_String, "<");
        if (!isOnlinePin) {
            param.putString(ConstIPinpad.startPinInput.param.KEY_promptString_String, "OFFLINE PIN, retry times:" + retryTimes);
        }
        try {
            ipinpad.startPinInput(70, param, globeParam, pinInputListener);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    private void initializePinInputListener() {
        pinInputListener = new PinInputListener.Stub() {
            @Override
            public void onInput(int len, int key) throws RemoteException {
                System.out.println(">>len:" + len);
                System.out.println(">>key:" + key);
            }

            @Override
            public void onConfirm(byte[] data, boolean isNonePin) throws RemoteException {
//                log.append("Generate Pinblock Result: " + HelperPin.byte2HexStr(data));
//                log.append("\n");
//                runOnUiThread(updateLog());
                //only if icc card is inserted;

//                runOnUiThread(showToast(HelperPin.byte2HexStr(data)));
                AppUtil.hideDialog();
                Log.d("PIN", HelperPin.byte2HexStr(data));

                Log.d("Pinblock :", HelperPin.byte2HexStr(data));

                if (HelperPin.byte2HexStr(data).equalsIgnoreCase("585A53A7A866EBA8")) {
//                    getActivity().runOnUiThread(showToast("Success"));

                    responseJSON = HelperPin.byte2HexStr(data);

                    SoapActivity action = new SoapActivity();
                    action.execute();
//                    getActivity().finish();
                } else {
                    getActivity().runOnUiThread(showButton("Pin Salah"));
                    getActivity().runOnUiThread(errorDialog());

                }

                if (isChip){
                    iemv.importPin(1, data);
                }
            }

            @Override
            public void onCancel() throws RemoteException {
                try {
                    AppUtil.hideDialog();
                    getActivity().runOnUiThread(showButton(" "));
                    iemv.abortEMV();
                } catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(int errorCode) throws RemoteException {
                try {
                    AppUtil.hideDialog();
                    getActivity().runOnUiThread(showButton("Timeout"));
                    iemv.abortEMV();
                } catch (Exception e){
                    e.printStackTrace();
                }
            }
        };
    }

    private Runnable errorDialog() {


        Runnable aRunnable = new Runnable(){
            public void run(){
                AppUtil.displayDialog(getContext(), "Pin Salah");
            }
        };

        return aRunnable;


    }

    public void callPrint(String typeCard) {
//        amount = 50000;
        fee = 5000;


        String date = new SimpleDateFormat("dd/MM/yyyy").format(Calendar.getInstance().getTime());
        String time = new SimpleDateFormat("HH:mm:ss").format(Calendar.getInstance().getTime());

        Bundle datas = new Bundle();
        datas.putString("type_trx", "TRANSFER");
        datas.putString("prod_id", "BRIS");
        datas.putString("tid", "08267001");
        datas.putString("mid", "4228267000001");
        datas.putString("type_card", typeCard);
        datas.putString("card_no", cdnumber);
        datas.putString("date", date);
        datas.putString("time", time);
        datas.putString("acc_des", rek_tujuan);
        datas.putString("nama_pelanggan", "ADI PRASTYO");
        datas.putString("amount", String.valueOf(amount));
        datas.putString("fee", String.valueOf(fee));
        datas.putString("total", String.valueOf(amount+fee));

        AppUtilPrint.printStruk(getContext(), datas);
    }
}