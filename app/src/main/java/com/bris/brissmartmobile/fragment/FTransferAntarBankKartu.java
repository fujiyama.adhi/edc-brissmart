package com.bris.brissmartmobile.fragment;

import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.bris.brissmartmobile.R;
import com.bris.brissmartmobile.listener.FavClickListener;
import com.bris.brissmartmobile.model.Favorite;
import com.bris.brissmartmobile.util.AppUtil;
import com.bris.brissmartmobile.util.Menu;
import com.bris.brissmartmobile.util.NumberTextWatcherForThousand;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;

public class FTransferAntarBankKartu extends Fragment implements FavClickListener, DialogInterface.OnKeyListener {

    JSONObject json, resdata;
    String pageId, message;
    private View view;
    private EditText etRekTujuan, etNominal;
    private TextInputLayout tilNamaBank, tilRekTujuan, tilNominal, tilNorekBsa;
    private AutoCompleteTextView actvNamaBank;
    private ImageButton btnFav;
    private Button btn1, btnConfirm;

    String rcode, kodebank, rektujuan, nominal;
    JSONArray banks;
    String idBankLain, nameBankLain, networkPriorityBankLain;

    private Realm realm;

    private String[] bank;
    private String[] id_bank, name, networkPriority;

    private List<Favorite> favDatas = new ArrayList<>();

    private android.support.v7.app.AlertDialog AlertDialog = null,
            confirmationDialog = null;

    public FTransferAntarBankKartu() {
    }

    public View onCreateView(final LayoutInflater inflater, @Nullable final ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.activity_transfer_antar_bank_kartu, container, false);

        pageId = getArguments().getString(Menu.MENU_ID);
//        AppUtil.toolbarRegular(getContext(), view, produk);
        AppUtil.toolbarRegular(getContext(), view, pageId.replace("Kartu", ""));

        tilNorekBsa = (TextInputLayout) view.findViewById(R.id.til_rekening_asal);
        tilNorekBsa.setVisibility(View.GONE);

        tilRekTujuan = (TextInputLayout) view.findViewById(R.id.til_rekening_tujuan);
        etRekTujuan = (EditText) view.findViewById(R.id.et_rekening_tujuan);
        etRekTujuan.setFocusable(false);

        tilNominal = (TextInputLayout) view.findViewById(R.id.til_nominal_transfer);
        etNominal = (EditText) view.findViewById(R.id.et_nominal_transfer);
        etNominal.addTextChangedListener(new NumberTextWatcherForThousand(etNominal));
        etNominal.setFocusable(false);

        btn1 = (Button) view.findViewById(R.id.btn_submit);
        btnFav = (ImageButton) view.findViewById(R.id.btn_daftar_fav);

        tilNamaBank = (TextInputLayout) view.findViewById(R.id.til_nama_bank);
        actvNamaBank = (AutoCompleteTextView) view.findViewById(R.id.act_nama_bank);
        actvNamaBank.setFocusable(false);

        setAutoCompleteText();

        etRekTujuan.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                etRekTujuan.setFocusableInTouchMode(true);

                return false;
            }
        });

        etNominal.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                etNominal.setFocusableInTouchMode(true);

                return false;
            }
        });

        realm = Realm.getDefaultInstance();

        try {
            onActionButton();
        } catch (Exception e) {
            e.printStackTrace();
            Log.d("Error", e.toString());
            AppUtil.displayDialog(getContext(), "Maaf, Terjadi Kesalahan");
        }

        return view;
    }

    private void setAutoCompleteText() {
        final Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {

                    JSONObject cmdparam = new JSONObject();
                    String soapRes = AppUtil.soapReq(AppUtil.getDsn(), "getbanklist", cmdparam);

                    if (soapRes.equalsIgnoreCase("")) {
                        AppUtil.hideDialog();
                        getActivity().runOnUiThread(new Runnable() {
                            public void run() {
                                AppUtil.displayDialog(getContext(), "Gagal terhubung ke server");
                            }
                        });
                    } else {
                        json = new JSONObject(soapRes);
                        resdata = json.getJSONObject("resdata");

                        banks = resdata.getJSONArray("list");
                        Log.d("List bank", banks.toString());

                        bank = new String[banks.length()];
                        id_bank = new String[banks.length()];
                        name = new String[banks.length()];
                        networkPriority = new String[banks.length()];
                        for (int i = 0; i < banks.length(); i++) {
                            JSONObject jo = banks.getJSONObject(i);
                            bank[i] = jo.getString("Id") + " - " + jo.getString("Name");
                            id_bank[i] = jo.getString("Id");
                            name[i] = jo.getString("Name");
                            networkPriority[i] = jo.getString("NetworkPriority");
                        }
                        Log.d("bank", bank[2].toString());

                        actvNamaBank.setThreshold(1);

                        getActivity().runOnUiThread(new Runnable() {
                            public void run() {
                                if (banks.equals("")) {
                                    AppUtil.displayDialog(getContext(), "Transaksi tidak dapat dilakukan");
                                } else {
                                    ArrayAdapter<String> bankListAdapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_list_item_1,
                                            bank);
                                    bankListAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                    actvNamaBank.setAdapter(bankListAdapter);
                                }
                            }
                        });


                        actvNamaBank.setOnTouchListener(new View.OnTouchListener() {
                            @Override
                            public boolean onTouch(View v, MotionEvent event) {
                                actvNamaBank.setFocusableInTouchMode(true);
                                actvNamaBank.showDropDown();
                                actvNamaBank.requestFocus();
                                return false;
                            }
                        });

                        actvNamaBank.addTextChangedListener((TextWatcher) getContext());

                        actvNamaBank.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> parent, View arg1, int position, long id) {
                                String bankLain = actvNamaBank.getText().toString();
                                int posisi = 0;
                                for (int i = 0; i < bank.length; i++) {
                                    if (bank[i].equalsIgnoreCase(bankLain)) {
                                        posisi = i;
                                    }
                                }
                                idBankLain = id_bank[posisi];
                                nameBankLain = name[posisi];
                                networkPriorityBankLain = networkPriority[posisi];
                            }
                        });

                        actvNamaBank.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                            @Override
                            public void onFocusChange(View v, boolean hasFocus) {
                                String val = actvNamaBank.getText().toString() + "";
                                Boolean code = Arrays.asList(bank).contains(val);
                                if (!hasFocus)
                                    if (!code) {
                                        tilNamaBank.setErrorEnabled(true);
                                        tilNamaBank.setError("Kode Bank tidak valid");
                                    } else {
                                        tilNamaBank.setErrorEnabled(false);
                                        tilNamaBank.setError(null);
                                    }
                            }
                        });
                        actvNamaBank.addTextChangedListener((TextWatcher) getContext());
                    }
                } catch (Exception ex) {
                    Log.d("THREAD a", ex.toString());
//                    AppUtil.displayDialog(getContext(), "Maaf, Terjadi Kesalahan");
//                    getActivity().runOnUiThread(new Runnable() {
//                        @Override
//                        public void run() {
//                            AppUtil.displayDialog(getContext(), "Transaksi tidak dapat dilakukan");
//                        }
//                    });
                }
            }
        });
        thread.start();
    }

    private void onActionButton() {
        btnFav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    rektujuan = etRekTujuan.getText().toString().trim();
                    String bankLain = actvNamaBank.getText().toString();
                    int posisi = 0;
                    for (int i = 0; i < bank.length; i++) {
                        if (bank[i].equalsIgnoreCase(bankLain)) {
                            posisi = i;
                        }
                    }
                    idBankLain = id_bank[posisi];
                    nameBankLain = name[posisi];
                    networkPriorityBankLain = networkPriority[posisi];
                    final String kode_bank = idBankLain + "|" + nameBankLain + "|" + networkPriorityBankLain;

                    if (rektujuan.equalsIgnoreCase("")) {
                        getFavData();
                        AppUtil.displayDialogFavorit(getContext(), favDatas, FTransferAntarBankKartu.this, "FAVTRANSA");
                        return;
                    }

                    AlertDialog dialogAlert;
                    AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                    builder.setCancelable(true);
                    builder.setMessage("Simpan data transaksi ini sebagai data favorit?");
                    builder.setPositiveButton("Simpan", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface alertdialog, int which) {
                            alertdialog.cancel();

                            if (AppUtil.checkFavorite("CASH", rektujuan, kode_bank, AppUtil.getDsn())) {
                                FragmentManager manager = getActivity().getSupportFragmentManager();
                                Fragment fragment;
                                FragmentTransaction transaction = manager.beginTransaction();
                                fragment = new FInsertUpdateDataFavorit();
                                Bundle bundle = new Bundle();
                                bundle.putString("title", getString(R.string.txt_title_menu_transfer_antar_bank));
                                bundle.putString("jenispembayaran", "CASH");
                                bundle.putString("bank", actvNamaBank.getText().toString());
                                bundle.putString("data3", rektujuan);
                                bundle.putString("menutrx", "8000");
                                bundle.putString("submenutrx", "8009");
                                bundle.putString("namajenisfav", "Transfer Bank Lain");

                                fragment.setArguments(bundle);
                                transaction.replace(R.id.fragment_payment_general, fragment);
                                transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
                                transaction.addToBackStack(null);
                                transaction.commit();


                            } else {
                                AppUtil.displayDialog(getContext(), "Data favorit Telah Ada");
                            }
                        }
                    });
                    builder.setNegativeButton("Pilih favorit", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface alertdialog, int which) {
                            alertdialog.cancel();
                            getFavData();
                            AppUtil.displayDialogFavorit(getContext(), favDatas, FTransferAntarBankKartu.this, "FAVTRANSA");
                        }
                    });

                    dialogAlert = builder.create();
                    dialogAlert.show();
                } catch (Exception e) {
                    e.printStackTrace();
                    AppUtil.displayDialog(getContext(), "Maaf, Terjadi Kesalahan");
                }
            }
        });

        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                kodebank = actvNamaBank.getText().toString().trim();
                Boolean isValid = Arrays.asList(bank).contains(kodebank);

                rektujuan = etRekTujuan.getText().toString();
                nominal = NumberTextWatcherForThousand.trimCommaOfString(etNominal.getText().toString().trim());

                if (!isValid) {
                    tilNamaBank.setError("Kode Bank Tidak Valid!");
                    return;
                } else {
                    int index = kodebank.indexOf(" ");
                    kodebank = kodebank.substring(0, index);
                }
                if (etRekTujuan.getText().toString().trim().length() == 0) {
                    tilRekTujuan.setError("Nomor Rekening Tujuan Tidak Boleh Kosong!");
                    return;
                }
                if (etNominal.getText().toString().length() == 0) {
                    tilNominal.setError(tilNominal.getHint() + " Tidak Boleh Kosong!");
                    return;
                }

                SoapActivity action = new SoapActivity();
                action.execute();
            }
        });

    }

    @Override
    public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
            AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
            builder.setCancelable(false);
            builder.setMessage("Apakah anda yakin akan membatalkan proses Pembayaran " + pageId + "?");
            builder.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                    confirmationDialog.dismiss();
                }
            });
            builder.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                    confirmationDialog.show();
                }
            });
            if (AlertDialog == null || !AlertDialog.isShowing()) {
                AlertDialog = builder.create();
                AlertDialog.show();
            }
            return true;
        }
        return false;
    }

    @Override
    public void getFav(String label, String category) {

    }

    @Override
    public void onclikFav(String label, String kode_produk) {
        RealmResults<Favorite> fav = realm.where(Favorite.class)
                .equalTo("namafav", label)
                .equalTo("data1", kode_produk)
                .findAll();
        Log.d("Cek fav", fav.toString());

        if (fav.size() > 0) {
            String data1 = fav.get(0).getData1();
            String kodebank = data1.substring(0, data1.indexOf("|"));
            String namaprioritybank = data1.substring(data1.indexOf("|") + 1, data1.length());
            Log.d("Cek namaprioritybank", namaprioritybank);
            String namabank = namaprioritybank.substring(0, namaprioritybank.indexOf("|"));
            actvNamaBank.setText(kodebank + " - " + namabank);
            etRekTujuan.setText(fav.get(0).getData3());
            AppUtil.closeListDialog();
        }
    }

    @Override
    public void deleteFav(String jenispembayaran, String menutrx, String submenutrx, String label) {

    }

    public void getFavData() {
        favDatas.clear();
        Realm mRealm = Realm.getDefaultInstance();
        favDatas = mRealm.copyFromRealm(
                mRealm.where(Favorite.class)
                        .equalTo("menutrx", "8000")
                        .equalTo("submenutrx", "8009")
                        .equalTo("jenispembayaran", "CASH")
                        .findAll());
        Log.d("Cek getFavData", favDatas.toString());
        mRealm.close();
    }

    private class SoapActivity extends AsyncTask<Void, Void, Void> {


        @Override
        protected void onPreExecute() {

        }

        @Override
        protected Void doInBackground(Void... params) {
            convertir();
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
//            if (msgErr.equalsIgnoreCase("true")) {
//                AppUtil.displayDialog(getContext(), message);
//            }
        }

    }

    private void convertir() {

        try {
            getActivity().runOnUiThread(new Runnable() {
                public void run() {
                    AppUtil.initDialogProgress(getContext());
                    AppUtil.showDialog();
                }
            });

            String dsn = AppUtil.getDsn();
            String phone_id = "62" + dsn.substring(1, dsn.length());

            JSONObject cmdparam = new JSONObject();
            cmdparam.put("message", AppUtil.API_MSG_TRFA + " " + kodebank + " " + rektujuan + " " + nominal);
            cmdparam.put("msisdn", phone_id);
            String soapRes = AppUtil.soapReq(AppUtil.getDsn(), "trfbanklain_verify", cmdparam);

            if (soapRes.equalsIgnoreCase("")) {
                AppUtil.hideDialog();
                getActivity().runOnUiThread(new Runnable() {
                    public void run() {
                        AppUtil.displayDialog(getContext(), "Gagal terhubung ke server");
                    }
                });
            } else {
                json = new JSONObject(soapRes);
                resdata = json.getJSONObject("resdata");
                rcode = resdata.getString("rcode");
                message = resdata.getString("message");

                if (rcode.equalsIgnoreCase("00")) {
                    new Thread() {
                        public void run() {
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    AppUtil.hideDialog();
                                    try {
                                        LayoutInflater li = LayoutInflater.from(getContext());
                                        final View confirmDialog = li.inflate(R.layout.dialog_confirm_agen, null);
                                        btnConfirm = (Button) confirmDialog.findViewById(R.id.btn_submit);
                                        final TextView confirm = (TextView) confirmDialog.findViewById(R.id.txt_title_top);
                                        final EditText pin = (EditText) confirmDialog.findViewById(R.id.txt_pin);

                                        String dialog = message.toString();
                                        confirm.setText(dialog.substring(0, dialog.indexOf("benar")) + " benar masukkan PIN Anda");
                                        pin.addTextChangedListener(new TextWatcher() {
                                            @Override
                                            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                                            }

                                            @Override
                                            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                                                if (charSequence.toString().trim().length() == 6) {
                                                    btnConfirm.setEnabled(true);
                                                    btnConfirm.setBackgroundResource(R.drawable.button_shape_blue);
                                                } else {
                                                    btnConfirm.setEnabled(false);
                                                    btnConfirm.setBackgroundResource(R.drawable.button_shape_accent);
                                                }
                                            }

                                            @Override
                                            public void afterTextChanged(Editable editable) {
                                            }
                                        });

                                        AlertDialog.Builder alert = new AlertDialog.Builder(getContext());
                                        alert.setView(confirmDialog);
                                        alert.setCancelable(false);
                                        confirmationDialog = alert.create();
                                        confirmationDialog.setOnKeyListener(FTransferAntarBankKartu.this);
                                        confirmationDialog.show();

                                        btnConfirm.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View view) {

                                                AppUtil.initDialogProgress(getContext());
                                                AppUtil.showDialog();

                                                Thread thread = new Thread(new Runnable() {

                                                    @Override
                                                    public void run() {
                                                        try {
                                                            String dsn = AppUtil.getDsn();
                                                            String phone_id = "62" + dsn.substring(1, dsn.length());

                                                            JSONObject cmdparam = new JSONObject();
                                                            cmdparam.put("message", "PIN " + pin.getText().toString());
                                                            cmdparam.put("msisdn", phone_id);
                                                            cmdparam.put("jenistransaksi", "8009");
                                                            cmdparam.put("idpelanggan", kodebank + " " + rektujuan + " " + nominal);
                                                            String soapRes = AppUtil.soapReq(AppUtil.getDsn(), "trfbanklain_save", cmdparam);

                                                            if (soapRes.equalsIgnoreCase("")) {
                                                                AppUtil.hideDialog();
                                                                getActivity().runOnUiThread(new Runnable() {
                                                                    public void run() {
                                                                        AppUtil.displayDialog(getContext(), "Gagal terhubung ke server");
                                                                    }
                                                                });
                                                            } else {
                                                                json = new JSONObject(soapRes);
                                                                resdata = json.getJSONObject("resdata");
                                                                rcode = resdata.getString("rcode");
                                                                message = resdata.getString("message");

                                                                if (rcode.equalsIgnoreCase("00")) {
                                                                    AppUtil.hideDialog();
                                                                    getActivity().runOnUiThread(new Runnable() {
                                                                        public void run() {
                                                                            actvNamaBank.getText().clear();
                                                                            etRekTujuan.getText().clear();
                                                                            etNominal.getText().clear();
                                                                            actvNamaBank.setFocusable(false);
                                                                            actvNamaBank.setOnTouchListener(new View.OnTouchListener() {
                                                                                @Override
                                                                                public boolean onTouch(View v, MotionEvent event) {
                                                                                    actvNamaBank.setFocusableInTouchMode(true);
                                                                                    return false;
                                                                                }
                                                                            });
                                                                            etRekTujuan.setOnTouchListener(new View.OnTouchListener() {
                                                                                @Override
                                                                                public boolean onTouch(View v, MotionEvent event) {
                                                                                    etRekTujuan.setFocusableInTouchMode(true);
                                                                                    return false;
                                                                                }
                                                                            });
                                                                            etNominal.setOnTouchListener(new View.OnTouchListener() {
                                                                                @Override
                                                                                public boolean onTouch(View v, MotionEvent event) {
                                                                                    etNominal.setFocusableInTouchMode(true);
                                                                                    return false;
                                                                                }
                                                                            });
                                                                            AppUtil.displayDialog(getContext(), message);
                                                                            AppUtil.saveIntoInbox(message, pageId, AppUtil.getDsn());
                                                                            confirmationDialog.dismiss();
                                                                        }
                                                                    });
                                                                } else if (rcode.equalsIgnoreCase("98")) {
                                                                    AppUtil.hideDialog();
                                                                    getActivity().runOnUiThread(new Runnable() {
                                                                        public void run() {
                                                                            AppUtil.displayDialog(getContext(), message);
                                                                            pin.getText().clear();

                                                                        }
                                                                    });
                                                                } else {
                                                                    AppUtil.hideDialog();
                                                                    getActivity().runOnUiThread(new Runnable() {
                                                                        public void run() {
                                                                            AppUtil.displayDialog(getContext(), message);
                                                                            confirmationDialog.dismiss();
                                                                        }
                                                                    });
                                                                }
                                                            }
                                                        } catch (Exception ex) {
                                                            AppUtil.hideDialog();
                                                            Log.d("THREAD", ex.toString());
                                                            getActivity().runOnUiThread(new Runnable() {
                                                                public void run() {
                                                                    AppUtil.displayDialog(getContext(), message);
                                                                }
                                                            });
                                                        }
                                                    }
                                                });
                                                thread.start();
                                            }
                                        });
                                    } catch (Exception e) {
                                        AppUtil.hideDialog();
                                        Log.d("THREAD 1", e.toString());
                                        getActivity().runOnUiThread(new Runnable() {
                                            public void run() {
                                                AppUtil.displayDialog(getContext(), message);
                                            }
                                        });
                                    }
                                }
                            });
                        }
                    }.start();
                } else {
                    AppUtil.hideDialog();
                    Log.d("else", "gagal");
                    getActivity().runOnUiThread(new Runnable() {
                        public void run() {
                            AppUtil.displayDialog(getContext(), message);
                        }
                    });
                }
            }
        } catch (Exception ex) {
            AppUtil.hideDialog();
            Log.d("THREAD a", ex.toString());
            getActivity().runOnUiThread(new Runnable() {
                public void run() {
                    AppUtil.displayDialog(getContext(), "Transaksi tidak dapat dilakukan");
                }
            });
        }
    }
}