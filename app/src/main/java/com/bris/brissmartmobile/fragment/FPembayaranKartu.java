package com.bris.brissmartmobile.fragment;

import android.content.Context;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.arch.lifecycle.ViewModelProviders;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;


import android.os.CountDownTimer;
import android.os.Looper;
import android.os.RemoteException;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.util.SparseArray;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bris.brissmartmobile.MainViewModel;
import com.bris.brissmartmobile.HelperPin;
import com.bris.brissmartmobile.PrinterFonts;
import com.bris.brissmartmobile.R;
import com.bris.brissmartmobile.app.BRISSMARTMobileRealmApp;
import com.bris.brissmartmobile.emv.EMVTLVParam;
import com.bris.brissmartmobile.listener.ConfirmTrxListener;
import com.bris.brissmartmobile.listener.FavClickListener;
import com.bris.brissmartmobile.model.Favorite;
import com.bris.brissmartmobile.util.AppUtil;
import com.bris.brissmartmobile.util.AppUtilPrint;
import com.bris.brissmartmobile.util.Menu;
import com.bris.brissmartmobile.util.NothingSelectedSpinnerAdapter;
import com.vfi.smartpos.deviceservice.aidl.CheckCardListener;
import com.vfi.smartpos.deviceservice.aidl.EMVHandler;
import com.vfi.smartpos.deviceservice.aidl.IBeeper;
import com.vfi.smartpos.deviceservice.aidl.IDeviceService;
import com.vfi.smartpos.deviceservice.aidl.IEMV;
import com.vfi.smartpos.deviceservice.aidl.IPinpad;
import com.vfi.smartpos.deviceservice.aidl.PinInputListener;
import com.vfi.smartpos.deviceservice.constdefine.ConstCheckCardListener;
import com.vfi.smartpos.deviceservice.constdefine.ConstIPBOC;
import com.vfi.smartpos.deviceservice.constdefine.ConstIPinpad;
import com.vfi.smartpos.deviceservice.constdefine.ConstPBOCHandler;


import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;

public class FPembayaranKartu extends Fragment implements ConfirmTrxListener, FavClickListener, FDialogInqCard.InqClickListener,
        TextWatcher, View.OnFocusChangeListener, View.OnTouchListener, DialogInterface.OnKeyListener {

    String msgErr = "false", responseJSON;
    private String jenis_tagihan, jenis_pendidikan, id_pelanggan, message, product, kode_transaksi;
    private String data1, data4 = null, submenutrx, namajenisfav;
    private int amount, fee;
    private MainViewModel mViewModel;
    private View view;
    private Button btn_refresh;
    private ImageButton btnFav;
    private ImageView iv_swipe_card;
    private TextView tv_swipe_card;
    private Spinner spinner_jenis_tagihan;
    private TextInputLayout til1, til2, tilNorek;
    private EditText et1;
    private JSONObject json, resdata;
    private JSONArray institusi;
    private String[] institution;
    private AutoCompleteTextView autocomplete_nama_institusi;
    private ArrayAdapter<String> adapterProduct;
    private List<Favorite> favDatas = new ArrayList<>();
    private Realm realm;
    //    private TextView log;
    public IEMV iemv;
    IBeeper iBeeper;
    IDeviceService idevice;
    EMVHandler emvHandler;
    IPinpad ipinpad;
    PinInputListener pinInputListener;
    SparseArray<String> tagOfF55 = null;
    private String applet, tsi, tvr, aid, cardName = "-", expiredDate ="";
    private String cdnumber;
    private boolean isChip = false;
    String pageId;


    private String[] listPLNPostpaid = null, listTelepon = null, listInternet = null,
            listTVBerbayar = null;

    private android.support.v7.app.AlertDialog AlertDialog = null;
    private AlertDialog confirmationDialog = null;

    public FPembayaranKartu() {
    }

    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        String produk = getArguments().getString(Menu.MENU_ID);

        if (produk.contains("Tunai")) {
            pageId = produk.substring(0, produk.indexOf(" Tunai"));
        }
        else {
            pageId = produk.substring(0, produk.indexOf(" Kartu"));
        }
        Log.d("Cek pageId", produk);

        listPLNPostpaid = getResources().getStringArray(R.array.product_bayar_electricity);
        listTelepon = getResources().getStringArray(R.array.product_bayar_telephone);
        listInternet = getResources().getStringArray(R.array.product_bayar_internet);
        listTVBerbayar = getResources().getStringArray(R.array.product_bayar_tv);

        if (pageId.equalsIgnoreCase("PLN Postpaid") ||
                pageId.equalsIgnoreCase("Telepon") ||
                pageId.equalsIgnoreCase("Internet") ||
                pageId.equalsIgnoreCase("TV Berbayar")) {
            view = inflater.inflate(R.layout.activity_pembayaran_tagihan_kartu, container, false);

            spinner_jenis_tagihan = (Spinner) view.findViewById(R.id.spinner_jenis_tagihan_kartu);
            til1 = (TextInputLayout) view.findViewById(R.id.til_id_pelanggan);
            btn_refresh = (Button) view.findViewById(R.id.btn_swipe_refresh2);
            tv_swipe_card = (TextView) view.findViewById(R.id.tv_swipe_card2);
            iv_swipe_card = (ImageView) view.findViewById(R.id.iv_swipe_card);

            setSpinnerProduct(pageId);

            et1 = (EditText) view.findViewById(R.id.et_id_pelanggan);
        }
        else if (pageId.equalsIgnoreCase("Tiket KAI") ||
                pageId.equalsIgnoreCase("Tokopedia")) {
            view = inflater.inflate(R.layout.activity_pembayaran_tiket_kartu, container, false);

            til1 = (TextInputLayout) view.findViewById(R.id.til_kode_bayar);
            et1 = (EditText) view.findViewById(R.id.et_kode_bayar);
            btn_refresh = (Button) view.findViewById(R.id.btn_swipe_refresh2);
            tv_swipe_card = (TextView) view.findViewById(R.id.tv_swipe_card2);
            iv_swipe_card = (ImageView) view.findViewById(R.id.iv_swipe_card);
        }
        else if (pageId.equalsIgnoreCase("Institusi") ||
                pageId.equalsIgnoreCase("Asuransi")) {
            view = inflater.inflate(R.layout.activity_pembayaran_institusi_kartu, container, false);

            til1 = (TextInputLayout) view.findViewById(R.id.til_nomor_id);
            et1 = (EditText) view.findViewById(R.id.et_nomor_id);
            btn_refresh = (Button) view.findViewById(R.id.btn_swipe_refresh2);
            tv_swipe_card = (TextView) view.findViewById(R.id.tv_swipe_card2);
            iv_swipe_card = (ImageView) view.findViewById(R.id.iv_swipe_card);


            if (pageId.equalsIgnoreCase("Institusi")) {
                til1.setHint("Nomor ID");
            }
            if (pageId.equalsIgnoreCase("Asuransi")) {
                til1.setHint("Nomor Asuransi");
            }

            til2 = (TextInputLayout) view.findViewById(R.id.til_nama_institusi);
            autocomplete_nama_institusi = (AutoCompleteTextView) view.findViewById(R.id.act_nama_institusi);
            autocomplete_nama_institusi.setFocusable(false);
            Log.d("ID TEXT", pageId);
            setAutoCompleteText(pageId);
        }
        else if (pageId.equalsIgnoreCase("Pendidikan")) {
            view = inflater.inflate(R.layout.activity_pembayaran_pendidikan_kartu, container, false);

            spinner_jenis_tagihan = (Spinner) view.findViewById(R.id.spinner_jenis_pendidikan);
            setSpinnerProduct(pageId);

            til1 = (TextInputLayout) view.findViewById(R.id.til_nomor_siswa);
            et1 = (EditText) view.findViewById(R.id.et_nomor_siswa);
            btn_refresh = (Button) view.findViewById(R.id.btn_swipe_refresh2);
            tv_swipe_card = (TextView) view.findViewById(R.id.tv_swipe_card2);
            iv_swipe_card = (ImageView) view.findViewById(R.id.iv_swipe_card);

            til2 = (TextInputLayout) view.findViewById(R.id.til_nama_institusi);
            autocomplete_nama_institusi = (AutoCompleteTextView) view.findViewById(R.id.act_nama_institusi);
            autocomplete_nama_institusi.setFocusable(false);
            Log.d("ID TEXT", pageId);
            setAutoCompleteText(pageId);
        }

        tilNorek = (TextInputLayout) view.findViewById(R.id.til_nomor_bsa);
        tilNorek.setVisibility(View.GONE);
        btnFav = (ImageButton) view.findViewById(R.id.btn_daftar_fav);
        if (pageId.equalsIgnoreCase("Tiket KAI")) {
            btnFav.setVisibility(View.INVISIBLE);
        }

        et1.setFocusable(false);

        if (!(pageId.equalsIgnoreCase("Tiket KAI") ||
                pageId.equalsIgnoreCase("Tokopedia"))) {
            et1.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    if (pageId.equalsIgnoreCase("Institusi") ||
                            pageId.equalsIgnoreCase("Asuransi") ||
                            pageId.equalsIgnoreCase("Pendidikan")) {
                        String val = autocomplete_nama_institusi.getText().toString() + "";
                        Boolean code = Arrays.asList(institution).contains(val);
                        if (!code) {
                            if (pageId.equalsIgnoreCase("Pendidikan")) {
                                if (spinner_jenis_tagihan.getSelectedItem() != null) {
                                    til2.setErrorEnabled(true);
                                    til2.setError("Kode Institusi Pendidikan tidak valid");
                                } else {
                                    et1.setFocusable(false);
                                }
                            } else {
                                til2.setErrorEnabled(true);
                                til2.setError("Kode Institusi Pendidikan tidak valid");
                            }
                        } else {
                            til2.setErrorEnabled(false);
                            til2.setError(null);

                            et1.setOnTouchListener(new View.OnTouchListener() {
                                @Override
                                public boolean onTouch(View v, MotionEvent event) {

                                    et1.setFocusableInTouchMode(true);

                                    return false;
                                }
                            });
                        }
                    }
                    return false;
                }
            });
        }
        if (pageId.equalsIgnoreCase("Tiket KAI") ||
                pageId.equalsIgnoreCase("Tokopedia")) {
            et1.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {

                    et1.setFocusableInTouchMode(true);

                    return false;
                }
            });
        }

        AppUtil.toolbarRegular(getContext(), view, pageId);
        realm = Realm.getDefaultInstance();

        String id = getArguments().getString("kartu_id");
        Log.d("ID KARTU", id);


//        log = (TextView) view.findViewById(R.id.log);

        iemv = BRISSMARTMobileRealmApp.getInstance().getiEMV();
        ipinpad = BRISSMARTMobileRealmApp.getInstance().getIpinpad();
        iBeeper = BRISSMARTMobileRealmApp.getInstance().getiBeeper();
        PrinterFonts.initialize(getActivity().getAssets());

        initializeEMV();
        initializePinInputListener();


        btn_refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!(pageId.equalsIgnoreCase("Tiket KAI") ||
                        pageId.equalsIgnoreCase("Tokopedia") ||
                        pageId.equalsIgnoreCase("Institusi") ||
                        pageId.equalsIgnoreCase("Asuransi"))) {
                    if (spinner_jenis_tagihan.getSelectedItem() == null) {
                        AppUtil.displayDialog(getContext(), "Jenis Tagihan Tidak Boleh Kosong!");
                        return;
                    } else {
                        product = spinner_jenis_tagihan.getSelectedItem().toString();
                    }
                }

                if (et1.getText().toString().length() == 0){
                    til1.setError(til1.getHint() + " Tidak Boleh Kosong!");
                    return;
                } else {
                    id_pelanggan = et1.getText().toString();
                    isChip = false;

                }

                isChip = false;
                startEmvProcess(0.0);

                Toast.makeText(getActivity().getBaseContext(), "Please insert or swipe card", Toast.LENGTH_LONG).show();
                tv_swipe_card.setVisibility(View.VISIBLE);
                iv_swipe_card.setVisibility(View.VISIBLE);
                tv_swipe_card.setText("Swipe atau Masukkan Kartu BRI Syariah");
                btn_refresh.setVisibility(View.INVISIBLE);
                btn_refresh.setText("Refresh");
                hideKeyboard();
            }
        });

        try {
            onActionButton();
        }
        catch (Exception e) {
            e.printStackTrace();
            AppUtil.displayDialog(getContext(), "Maaf, Terjadi Kesalahan");
        }

        return view;
    }

    private void hideKeyboard() {

        InputMethodManager mgr = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        mgr.hideSoftInputFromWindow(et1.getWindowToken(), 0);

    }

    private void setSpinnerProduct(String title) {
        if (title.equalsIgnoreCase("PLN Postpaid")) {
            adapterProduct = new ArrayAdapter<>(getContext(), R.layout.spinner_style,
                    getResources().getStringArray(R.array.product_bayar_electricity));
            adapterProduct.setDropDownViewResource(android.R.layout.simple_list_item_1);
            spinner_jenis_tagihan.setAdapter(new NothingSelectedSpinnerAdapter(
                    adapterProduct,
                    R.layout.spinner_product_nothing_selected,
                    getContext()
            ));
        }
        else if (title.equalsIgnoreCase("Telepon")) {
            adapterProduct = new ArrayAdapter<>(getContext(), R.layout.spinner_style,
                    getResources().getStringArray(R.array.product_bayar_telephone));
            adapterProduct.setDropDownViewResource(android.R.layout.simple_list_item_1);
            spinner_jenis_tagihan.setAdapter(new NothingSelectedSpinnerAdapter(
                    adapterProduct,
                    R.layout.spinner_product_nothing_selected,
                    getContext()
            ));
        }
        else if (title.equalsIgnoreCase("Internet")) {
            adapterProduct = new ArrayAdapter<>(getContext(), R.layout.spinner_style,
                    getResources().getStringArray(R.array.product_bayar_internet));
            adapterProduct.setDropDownViewResource(android.R.layout.simple_list_item_1);
            spinner_jenis_tagihan.setAdapter(new NothingSelectedSpinnerAdapter(
                    adapterProduct,
                    R.layout.spinner_product_nothing_selected,
                    getContext()
            ));
        }
        else if (title.equalsIgnoreCase("TV Berbayar")) {
            adapterProduct = new ArrayAdapter<>(getContext(), R.layout.spinner_style,
                    getResources().getStringArray(R.array.product_bayar_tv));
            adapterProduct.setDropDownViewResource(android.R.layout.simple_list_item_1);
            spinner_jenis_tagihan.setAdapter(new NothingSelectedSpinnerAdapter(
                    adapterProduct,
                    R.layout.spinner_product_nothing_selected,
                    getContext()
            ));
        }
        else if (title.equalsIgnoreCase("Pendidikan")) {
            adapterProduct = new ArrayAdapter<>(getContext(), R.layout.spinner_style,
                    getResources().getStringArray(R.array.product_bayar_pendidikan));
            adapterProduct.setDropDownViewResource(android.R.layout.simple_list_item_1);
            spinner_jenis_tagihan.setAdapter(new NothingSelectedSpinnerAdapter(
                    adapterProduct,
                    R.layout.spinner_product_nothing_selected,
                    getContext()
            ));
        }
        spinner_jenis_tagihan.setOnItemSelectedListener(new onItemListener());
    }

    @Override
    public void dialogInqButton(String action) {
        doPinPad(true, 3);
    }

    private class onItemListener implements AdapterView.OnItemSelectedListener {
        @Override
        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
            if (pageId.equalsIgnoreCase("PLN Postpaid") || pageId.equalsIgnoreCase("Telepon") || pageId.equalsIgnoreCase("Internet") || pageId.equalsIgnoreCase("TV Berbayar")) {
                if (spinner_jenis_tagihan.getSelectedItem() != null) {
                    if (pageId.equalsIgnoreCase("Telepon")) {
                        til1.setHint("Nomor Telepon");
                    } else {
                        if (spinner_jenis_tagihan.getSelectedItem().toString().equalsIgnoreCase("PLN Non Tagihan Listrik")) {
                            til1.setHint("Nomor Registrasi");
                        } else {
                            til1.setHint("Nomor Pelanggan");
                        }
                    }
                    et1.setOnTouchListener(new View.OnTouchListener() {
                        @Override
                        public boolean onTouch(View v, MotionEvent event) {

                            et1.setFocusableInTouchMode(true);

                            return false;
                        }
                    });
                    et1.setFocusable(true);
                    et1.setFocusableInTouchMode(true);
                } else {
                    et1.setFocusable(false);
                    til1.setHint("Jenis Tagihan belum dipilih");
                }
            }
            if (pageId.equalsIgnoreCase("Pendidikan")) {
                if (spinner_jenis_tagihan.getSelectedItem() != null) {
                    if (spinner_jenis_tagihan.getSelectedItem().equals("Sekolah")) {
                        til1.setHint("Nomor Siswa");
                    } else {
                        til1.setHint("Nomor Mahasiswa");
                    }
                } else {
                    til1.setHint("Jenis Pendidikan belum dipilih");
                    til2.setHint("Jenis Pendidikan belum dipilih");
                }

                jenis_pendidikan = String.valueOf(spinner_jenis_tagihan.getSelectedItem());
                setAutoCompleteText(jenis_pendidikan);
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> adapterView) {
        }
    }

    private void setAutoCompleteText(final String pendidikan) {

        Log.d("Header", pendidikan);

        Thread thread = new Thread(new Runnable() {

            @Override
            public void run() {
                try {
                    String kode = "";
                    if (pendidikan.equalsIgnoreCase("Asuransi")) {
                        kode = "1";
                    } else if (pendidikan.equalsIgnoreCase("Institusi")) {
                        kode = "2";
                    } else if (pendidikan.equalsIgnoreCase("Sekolah")) {
                        kode = "3";
                    } else if (pendidikan.equalsIgnoreCase("Perguruan Tinggi")) {
                        kode = "4";
                    }

                    String jsonData = AppUtil.setAutoCompleteInstitusi(kode);
                    Log.d("data", jsonData);
                    json = new JSONObject(jsonData);
                    Log.d("json", json.toString());
                    institusi = json.getJSONArray("data");
                    Log.d("array", institusi.toString());

                    institution = new String[institusi.length()];
                    for (int i = 0; i < institusi.length(); i++) {
                        JSONObject jo = institusi.getJSONObject(i);
                        institution[i] = jo.getString("institusi_code") + " - " + jo.getString("institusi_name");
                    }
                    Log.d("institusi", institution.toString());

                    autocomplete_nama_institusi.setThreshold(1);

                    getActivity().runOnUiThread(new Runnable() {
                        public void run() {
                            ArrayAdapter<String> bankListAdapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_list_item_1,
                                    institution);
                            bankListAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            autocomplete_nama_institusi.setAdapter(bankListAdapter);
                        }
                    });

                    autocomplete_nama_institusi.setOnTouchListener(new View.OnTouchListener() {
                        @Override
                        public boolean onTouch(View v, MotionEvent event) {
                            autocomplete_nama_institusi.setFocusableInTouchMode(true);
                            autocomplete_nama_institusi.showDropDown();
                            autocomplete_nama_institusi.requestFocus();
                            return false;
                        }
                    });

                    autocomplete_nama_institusi.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                        @Override
                        public void onFocusChange(View v, boolean hasFocus) {
                            String val = autocomplete_nama_institusi.getText().toString() + "";
                            Boolean code = Arrays.asList(institution).contains(val);
                            if (!hasFocus)
                                if (!code) {
                                    til2.setErrorEnabled(true);
                                    til2.setError("Kode Institusi Pendidikan tidak valid");
                                } else {
                                    til2.setErrorEnabled(false);
                                    til2.setError(null);
                                }
                        }
                    });

                    autocomplete_nama_institusi.addTextChangedListener((TextWatcher) getContext());

                    if (pendidikan.equalsIgnoreCase("Asuransi")) {
                        til2.setHint("Pilih Asuransi");
                    } else if (pendidikan.equalsIgnoreCase("Institusi")) {
                        til2.setHint("Pilih Institusi");
                    } else if (pendidikan.equalsIgnoreCase("Sekolah")) {
                        til2.setHint("Pilih Sekolah");
                    } else if (pendidikan.equalsIgnoreCase("Perguruan Tinggi")) {
                        til2.setHint("Pilih Perguruan Tinggi");
                    }
                } catch (Exception ex) {
                    msgErr = "true";
                    Log.d("THREAD a", ex.toString());
                    AppUtil.displayDialog(getContext(), "Maaf, Terjadi Kesalahan");
                }
            }
        });
        thread.start();
    }

    private void onActionButton(){

        btnFav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    if (pageId.equalsIgnoreCase("Institusi") || pageId.equalsIgnoreCase("Asuransi") || pageId.equalsIgnoreCase("Pendidikan") || pageId.equalsIgnoreCase("Tokopedia")) {
                        product = pageId;
                        if (pageId.equalsIgnoreCase("Pendidikan")) {
                            if (spinner_jenis_tagihan.getSelectedItem() == null) {
                                AppUtil.displayDialog(getContext(), "Pilih Produk dahulu untuk mendapatkan/simpan data favorit Anda");
                                return;
                            }
                        }
                    } else if (pageId.equalsIgnoreCase("PLN Postpaid") || pageId.equalsIgnoreCase("Telepon") || pageId.equalsIgnoreCase("Internet") || pageId.equalsIgnoreCase("TV Berbayar")) {
                        if (spinner_jenis_tagihan.getSelectedItem() == null) {
                            AppUtil.displayDialog(getContext(), "Pilih Produk dahulu untuk mendapatkan/simpan data favorit Anda");
                            return;
                        }
                        product = spinner_jenis_tagihan.getSelectedItem().toString();
                    }

                    if (pageId.equalsIgnoreCase("PLN Postpaid")) {
                        if (product.equalsIgnoreCase(listPLNPostpaid[0])) {
                            data1 = "TAGLIS";
                        } else {
                            data1 = "NONTAG";
                        }
                        data4 = product;
                        submenutrx = "7001";
                        namajenisfav = "Pembayaran PLN";
                    } else if (pageId.equalsIgnoreCase("Telepon")) {
                        if (product.equalsIgnoreCase(listTelepon[0]) || product.equalsIgnoreCase(listTelepon[1])) {
                            data1 = "TELKOM";
                        } else if (product.equalsIgnoreCase(listTelepon[2])) {
                            data1 = "HALO";
                        } else if (product.equalsIgnoreCase(listTelepon[3])) {
                            data1 = "XL";
                        } else if (product.equalsIgnoreCase(listTelepon[4])) {
                            data1 = "SMARTFREN";
                        }
                        data4 = product;
                        submenutrx = "7002";
                        namajenisfav = "Pembayaran Telepon";
                    } else if (pageId.equalsIgnoreCase("Internet")) {
                        if (product.equalsIgnoreCase(listInternet[0])) {
                            data1 = "TELKOM";
                        }
                        data4 = product;
                        submenutrx = "7003";
                        namajenisfav = "Pembayaran Internet";
                    } else if (pageId.equalsIgnoreCase("TV Berbayar")) {
                        if (product.equalsIgnoreCase(listTVBerbayar[0])) {
                            data1 = "TELKOM";
                        } else if (product.equalsIgnoreCase(listTVBerbayar[1])) {
                            data1 = "BIGTV";
                        } else if (product.equalsIgnoreCase(listTVBerbayar[2])) {
                            data1 = "INDOVISION";
                        }
                        data4 = product;
                        submenutrx = "7004";
                        namajenisfav = "Pembayaran TV";
                    } else if (pageId.equalsIgnoreCase("Tokopedia")) {
                        data1 = "TOKOPEDIA";
                        data4 = product;
                        submenutrx = "7009";
                        namajenisfav = "Pembayaran Tokopedia";
                    } else if (pageId.equalsIgnoreCase("Institusi")) {
                        submenutrx = "7006";
                        namajenisfav = "Pembayaran Institusi";
                    } else if (pageId.equalsIgnoreCase("Pendidikan")) {
                        if (jenis_pendidikan.equalsIgnoreCase("Sekolah")) {
                            data4 = "S";
                        } else if (jenis_pendidikan.equalsIgnoreCase("Perguruan Tinggi")) {
                            data4 = "P";
                        }
                        submenutrx = "7007";
                        namajenisfav = "Pembayaran Pendidikan";
                    } else if (pageId.equalsIgnoreCase("Asuransi")) {
                        submenutrx = "7008";
                        namajenisfav = "Pembayaran Asuransi";
                    }

                    final String id_product = et1.getText().toString().trim();

                    if (id_product.equalsIgnoreCase("")) {
                        if (pageId.equalsIgnoreCase("Institusi")) {
                            getFavData("7006");
                        } else if (pageId.equalsIgnoreCase("Pendidikan")) {
                            getFavData(data4);
                        } else if (pageId.equalsIgnoreCase("Asuransi")) {
                            getFavData("7008");
                        } else {
                            getFavData(product);
                        }
                        AppUtil.displayDialogFavorit(getContext(), favDatas, FPembayaranKartu.this, "BAYAR");
                        return;
                    }

                    AlertDialog dialogAlert;
                    AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                    builder.setCancelable(true);
                    builder.setMessage("Simpan data transaksi ini sebagai data favorit?");
                    builder.setPositiveButton("Simpan", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface alertdialog, int which) {
                            alertdialog.cancel();

                            if (AppUtil.checkFavorite("CASH", id_product, data1, AppUtil.getDsn())) {
                                android.support.v4.app.FragmentManager manager = getActivity().getSupportFragmentManager();

                                Fragment fragment;
                                android.support.v4.app.FragmentTransaction transaction = manager.beginTransaction();
                                fragment = new FInsertUpdateDataFavorit();
                                Bundle bundle = new Bundle();
                                bundle.putString("title", product);
                                bundle.putString("jenispembayaran", "CASH");
                                bundle.putString("menutrx", "7000");
                                bundle.putString("data1", data1);
                                bundle.putString("data3", id_product);
                                bundle.putString("data4", data4);
                                bundle.putString("submenutrx", submenutrx);
                                bundle.putString("namajenisfav", namajenisfav);
                                if (pageId.equalsIgnoreCase("Institusi") || pageId.equalsIgnoreCase("Pendidikan")
                                        || pageId.equalsIgnoreCase("Asuransi")) {
                                    bundle.putString("bank", autocomplete_nama_institusi.getText().toString());
                                }

                                fragment.setArguments(bundle);
                                transaction.replace(R.id.fragment_payment_card, fragment);
                                transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
                                transaction.addToBackStack(null);
                                transaction.commit();

                            } else {
                                AppUtil.displayDialog(getContext(), "Data favorit Telah Ada");
                            }
                        }
                    });
                    builder.setNegativeButton("Pilih favorit", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface alertdialog, int which) {
                            alertdialog.cancel();
                            if (pageId.equalsIgnoreCase("Institusi")) {
                                getFavData("7006");
                            } else if (pageId.equalsIgnoreCase("Pendidikan")) {
                                getFavData(data4);
                            } else if (pageId.equalsIgnoreCase("Asuransi")) {
                                getFavData("7008");
                            } else {
                                getFavData(product);
                            }
                            AppUtil.displayDialogFavorit(getContext(), favDatas, FPembayaranKartu.this, "BAYAR");
                        }
                    });

                    dialogAlert = builder.create();
                    dialogAlert.show();
                } catch (Exception e) {
                    e.printStackTrace();
                    AppUtil.displayDialog(getContext(), "Maaf, Terjadi Kesalahan");
                }
            }
        });
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(MainViewModel.class);
//        InitializeFontFiles();
//        Toast.makeText(getActivity().getApplicationContext(), "onActivityCreated", Toast.LENGTH_SHORT).show();

//        isChip = false;
//        startEmvProcess(0.0);
        // TODO: Use the ViewModel
    }

    /**
     *
     * @param amount
     * Step Number 1. to be executed, reading card
     */
    public void startEmvProcess(double amount){
        if (amount == 0){
            doSearchCard(TransType.T_BANLANCE, amount);
        } else {
            doSearchCard(TransType.T_PURCHASE, amount);
        }
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {

    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {

    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        return false;
    }

    @Override
    public void onConfirmClick(String pin) {

    }

    @Override
    public void onCancelClick() {

    }

    @Override
    public void getFav(String label, String category) {

    }

    @Override
    public void onclikFav(String label, String kode_produk) {
        RealmResults<Favorite> fav = realm.where(Favorite.class)
                .equalTo("menutrx", "7000")
                .equalTo("jenispembayaran", "CASH")
                .equalTo("namafav", label)
                .equalTo("data1", kode_produk)
                .findAll();

        if (fav.size() > 0) {
            if (pageId.equalsIgnoreCase("Institusi") || pageId.equalsIgnoreCase("Pendidikan") || pageId.equalsIgnoreCase("Asuransi")) {
                String data1 = fav.get(0).getData1();
                String kodeinstitusi = data1.substring(0, data1.indexOf("|"));
                String namainstitusi = data1.substring(data1.indexOf("|") + 1, data1.length());
                autocomplete_nama_institusi.setText(kodeinstitusi + " - " + namainstitusi);
            }
            et1.setText(fav.get(0).getData3());
            AppUtil.closeListDialog();
        }
    }

    @Override
    public void deleteFav(String jenispembayaran, String menutrx, String submenutrx, String label) {

    }

    public void getFavData(String kode_produk) {
        if (pageId.equalsIgnoreCase("Institusi") || pageId.equalsIgnoreCase("Asuransi")) {
            RealmResults<Favorite> fav = realm.where(Favorite.class)
                    .equalTo("menutrx", "7000")
                    .equalTo("jenispembayaran", "CASH")
                    .equalTo("submenutrx", kode_produk)
                    .findAll();
            if (fav.size() > 0) {
                favDatas.clear();
                favDatas.addAll(fav);
            } else {
                favDatas.clear();
            }
        } else {
            RealmResults<Favorite> fav = realm.where(Favorite.class)
                    .equalTo("menutrx", "7000")
                    .equalTo("jenispembayaran", "CASH")
                    .equalTo("data4", kode_produk)
                    .findAll();

            Log.d("Cek fav", fav.toString());

            if (fav.size() > 0) {
                favDatas.clear();
                favDatas.addAll(fav);
            } else {
                favDatas.clear();
            }
        }
    }

    enum TransType {
        T_BANLANCE, T_PURCHASE
    }

    public void doSearchCard(final TransType transType, final double amount){
        Bundle cardOption = new Bundle();
        cardOption.putBoolean( ConstIPBOC.checkCard.cardOption.KEY_Contactless_boolean, ConstIPBOC.checkCard.cardOption.VALUE_unsupported);
        cardOption.putBoolean( ConstIPBOC.checkCard.cardOption.KEY_SmartCard_boolean, ConstIPBOC.checkCard.cardOption.VALUE_supported);
        cardOption.putBoolean( ConstIPBOC.checkCard.cardOption.KEY_MagneticCard_boolean, ConstIPBOC.checkCard.cardOption.VALUE_supported);


        try {
            iemv.checkCard(cardOption, 5, new CheckCardListener.Stub() {
                        @Override
                        public void onCardSwiped(Bundle track) throws RemoteException {
                            Log.d( "TAG", "onCardSwiped ..." );
                            iemv.stopCheckCard();
                            iemv.abortEMV();

                            iBeeper.startBeep(200);

                            String pan = track.getString(ConstCheckCardListener.onCardSwiped.track.KEY_PAN_String);
                            String track1 = track.getString(ConstCheckCardListener.onCardSwiped.track.KEY_TRACK1_String);
                            if (track1 != null){
                                parseCardName(track1);
                            }
                            String track2 = track.getString(ConstCheckCardListener.onCardSwiped.track.KEY_TRACK2_String);
                            String track3 = track.getString(ConstCheckCardListener.onCardSwiped.track.KEY_TRACK3_String);
                            String serviceCode = track.getString(ConstCheckCardListener.onCardSwiped.track.KEY_SERVICE_CODE_String);
                            cdnumber = pan;
                            expiredDate = track2.substring(17, 21);
//                            log.append("Mag Card Swiped..");
//                            log.append("\n");
//                            log.append("Card Number:" + pan);
//                            log.append("\n");
//                            log.append("Track 2 Data:" + track2);
//                            log.append("\n");
//                            runOnUiThread(updateLog());
                            //incase magnetic card detected, just go to input pin
                            //and process it

                            getActivity().runOnUiThread(new Runnable() {
                                public void run() {
                                    AppUtil.initDialogProgress(getContext());
                                }
                            });

                            getActivity().runOnUiThread(showDialogThreat(pageId ,generateTextInq()));
                            getActivity().runOnUiThread(showButton(" "));
//                            doPinPad(true, 3);
                        }

                        @Override
                        public void onCardPowerUp() throws RemoteException {
                            iemv.stopCheckCard();
                            iemv.abortEMV();
                            iBeeper.startBeep(200);
//                            log.append("ICC Card Detected..");
//                            log.append("\n");
//                            log.append("Reading Card..");
//                            log.append("\n");
//                            runOnUiThread(updateLog());
                            isChip = true;

                            getActivity().runOnUiThread(new Runnable() {
                                public void run() {
                                    AppUtil.initDialogProgress(getContext());
                                }
                            });

                            doEMV( ConstIPBOC.startEMV.intent.VALUE_cardType_smart_card , transType, (long) amount );
                        }

                        @Override
                        public void onCardActivate() throws RemoteException {
                            iemv.stopCheckCard();
                            iemv.abortEMV();
                            iBeeper.startBeep(200);
//                            log.append("ICC Contactless Card Detected..");
//                            log.append("\n");
//                            log.append("Reading Card..");
//                            runOnUiThread(updateLog());
                            isChip = true;
//                            doEMV( ConstIPBOC.startEMV.intent.VALUE_cardType_contactless, transType, (long) amount );
                        }

                        @Override
                        public void onTimeout() throws RemoteException {
//                            runOnUiThread(showToast("Timeout"));
//                            finish();
                            Log.e("Search Card", "Timeout");
                            AppUtil.hideDialog();
                            getActivity().runOnUiThread(showButton("Timeout"));
                            iBeeper.startBeep(100);

                        }

                        @Override
                        public void onError(int error, String message) throws RemoteException {
                            System.out.println("error:" + error);
                            if (error == 3){
                                Looper.prepare();
                                new CountDownTimer(1000, 1000) {

                                    public void onTick(long millisUntilFinished) {
                                    }

                                    public void onFinish() {
//                                        runOnUiThread(showToast("Use Magnetic Card"));
                                        AppUtil.hideDialog();
                                        getActivity().runOnUiThread(showButton("Error " + ""+error));
                                        try {
                                            iBeeper.startBeep(100);
                                        } catch (RemoteException e) {
                                            e.printStackTrace();
                                        }
                                        Log.e("Search Card", "Use Magnetic Card");

                                    }

                                }.start();
                                Looper.loop();
                            } else if (error == 100){
                                doSearchCard(transType, amount);
//                                runOnUiThread(showToast("Card not readable, please try again"));
                                Log.e("Search Card", "Card not readable, please try again");
                            }
                        }
                    }
            );
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    public Runnable showButton(String text){

        Runnable aRunnable = new Runnable(){
            public void run(){
                btn_refresh.setVisibility(View.VISIBLE);
                tv_swipe_card.setText("Waktu Habis\nSilahkan tekan tombol refresh");

                if (!text.equalsIgnoreCase(" ")) {
                    Toast.makeText(getActivity().getBaseContext(), text, Toast.LENGTH_SHORT).show();
                }
            }
        };

        return aRunnable;

    }

    public void doPinPad(boolean isOnlinePin, int retryTimes) {
        Bundle param = new Bundle();
        Bundle globeParam = new Bundle();
        String panBlock = cdnumber;
        byte[] pinLimit = {0, 6};
        byte[] displayKey = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
        param.putByteArray("displayKeyValue", displayKey);
        param.putByteArray(ConstIPinpad.startPinInput.param.KEY_pinLimit_ByteArray, pinLimit);
        param.putInt(ConstIPinpad.startPinInput.param.KEY_timeout_int, 20);
        param.putBoolean(ConstIPinpad.startPinInput.param.KEY_isOnline_boolean, isOnlinePin);
        param.putString(ConstIPinpad.startPinInput.param.KEY_pan_String, panBlock);
        param.putInt(ConstIPinpad.startPinInput.param.KEY_desType_int, ConstIPinpad.startPinInput.param.Value_desType_3DES);
        param.putString( "promptString" , "Silakan Masukan PIN Anda") ;
        globeParam.putString(ConstIPinpad.startPinInput.globleParam.KEY_Display_BackSpace_String, "<");
        if (!isOnlinePin) {
            param.putString(ConstIPinpad.startPinInput.param.KEY_promptString_String, "OFFLINE PIN, retry times:" + retryTimes);
        }
        try {
            ipinpad.startPinInput(70, param, globeParam, pinInputListener);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    private void parseCardName(String data){
        String txt = data.replace('^', '=');
        String[] list = txt.split("=");
        if (list.length > 1){
            cardName = HelperPin.cleanString(list[1]);
        }
        if (cardName.trim().length() == 0){
            cardName = "-";
        }
    }

    public void doEMV(int type, TransType transType, long amount ){
        getActivity().runOnUiThread(showButton(" "));
        Bundle emvIntent = new Bundle();
        emvIntent.putInt( ConstIPBOC.startEMV.intent.KEY_cardType_int, type );
        if( transType == TransType.T_PURCHASE ) {
            emvIntent.putLong(ConstIPBOC.startEMV.intent.KEY_authAmount_long, amount );
        }
        emvIntent.putBoolean(ConstIPBOC.startEMV.intent.KEY_isSupportQ_boolean, ConstIPBOC.startEMV.intent.VALUE_supported);
        emvIntent.putBoolean(ConstIPBOC.startEMV.intent.KEY_isSupportSM_boolean, ConstIPBOC.startEMV.intent.VALUE_supported);
        emvIntent.putBoolean(ConstIPBOC.startEMV.intent.KEY_isQPBOCForceOnline_boolean, ConstIPBOC.startEMV.intent.VALUE_forced );
        if( type == ConstIPBOC.startEMV.intent.VALUE_cardType_contactless ) {
            emvIntent.putByte( ConstIPBOC.startEMV.intent.KEY_transProcessCode_byte, (byte)0x00 );
        }
        emvIntent.putBoolean("isSupportPBOCFirst", false);
        try {
            iemv.startEMV(ConstIPBOC.startEMV.processType.full_process, emvIntent, emvHandler);
        } catch (RemoteException e) {
            System.out.println("Start EMV error:" + e.getMessage());
            e.printStackTrace();
        }
    }

    private void initializePinInputListener() {
        pinInputListener = new PinInputListener.Stub() {
            @Override
            public void onInput(int len, int key) throws RemoteException {
                System.out.println(">>len:" + len);
                System.out.println(">>key:" + key);
            }

            @Override
            public void onConfirm(byte[] data, boolean isNonePin) throws RemoteException {
//                log.append("Generate Pinblock Result: " + HelperPin.byte2HexStr(data));
//                log.append("\n");
//                runOnUiThread(updateLog());
                //only if icc card is inserted;

//                runOnUiThread(showToast(HelperPin.byte2HexStr(data)));
                AppUtil.hideDialog();
                Log.d("PIN", HelperPin.byte2HexStr(data));

                Log.d("Pinblock :", HelperPin.byte2HexStr(data));

                if (HelperPin.byte2HexStr(data).equalsIgnoreCase("585A53A7A866EBA8")) {
//                    getActivity().runOnUiThread(showToast("Success"));

                    responseJSON = HelperPin.byte2HexStr(data);

                    SoapActivity action = new SoapActivity();
                    action.execute();
//                    getActivity().finish();
                } else {
                    getActivity().runOnUiThread(showButton("Pin Salah"));
                    getActivity().runOnUiThread(errorDialog());

                }

                if (isChip){
                    iemv.importPin(1, data);
                }
            }

            @Override
            public void onCancel() throws RemoteException {
                try {
                    AppUtil.hideDialog();
                    getActivity().runOnUiThread(showButton(" "));
                    iemv.abortEMV();
                } catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(int errorCode) throws RemoteException {
                try {
                    AppUtil.hideDialog();
                    getActivity().runOnUiThread(showButton("Timeout"));
                    iemv.abortEMV();
                } catch (Exception e){
                    e.printStackTrace();
                }
            }
        };
    }

    public String generateTextInq() {
        String text;
        String scdnumber = cdnumber.substring(0, 6) + "******" + cdnumber.substring(12, 16);
        String sexpiredDate = expiredDate.substring(2,4) + "/" + expiredDate.substring(0,2);

        text = "ALL CARD" + "\n\n" +
                "Card Name : " + cardName + "\n" +
                "Card Number : " + scdnumber + "\n" +
                "Exp date : " + sexpiredDate;

        return text;
    }

    /**
     * All transaction process is in here for icc card
     * Step Number 3. but initialize first before transaction
     */
    void initializeEMV() {

        emvHandler = new EMVHandler.Stub() {
            @Override
            public void onRequestAmount() throws RemoteException {
            }

            /**
             * Will go here if the card support multiple applet
             * Sub Number 0
             */
            @Override
            public void onSelectApplication(List<Bundle> appList) throws RemoteException {
                for (Bundle aidBundle : appList) {
                    String aidName = aidBundle.getString("aidName");
                    String aid = aidBundle.getString("aid");
                    String aidLabel = aidBundle.getString("aidLabel");
                    Log.i("DemoApp", "AID Name=" + aidName + " | AID Label=" + aidLabel + " | AID=" + aid);
                }
//                log.append("Multiple AID Detected..");
//                log.append("\n");
//                log.append("Select NSICCS");
//                log.append("\n");
//                runOnUiThread(updateLog());
                iemv.importAppSelection(1);
            }

            /**
             * Card Information after reading it
             * Sub Number 1
             */
            @Override
            public void onConfirmCardInfo(Bundle info) throws RemoteException {
                Log.d("DemoApp", "onConfirmCardInfo...");
//                log.append("Reading Card Information..");
//                log.append("\n");
//                runOnUiThread(updateLog());
                try {

                    byte[] data9F12 = iemv.getCardData("9F12");
                    if (data9F12 != null){
                        applet = HelperPin.convertHexToString(HelperPin.hexString(data9F12));
                    } else {
                        byte[] data50 = iemv.getCardData("50");
                        if (data50 != null){
                            applet = HelperPin.convertHexToString(HelperPin.hexString(data50));
                        }
                    }
                } catch (Exception e){
                    e.printStackTrace();
                }
                try {

                    byte[] data9B = iemv.getCardData("9B");

                    tsi = HelperPin.hexString(data9B);

                } catch (Exception e){
                    e.printStackTrace();
                }
                try {

                    byte[] data95 = iemv.getCardData("95");

                    tvr = HelperPin.hexString(data95);

                } catch (Exception e){
                    e.printStackTrace();
                }
                try {

                    byte[] data84 = iemv.getCardData("84");

                    aid = HelperPin.hexString(data84);

                } catch (Exception e){
                    e.printStackTrace();
                }
                try {

                    byte[] data5F20 = iemv.getCardData("5F20");
                    cardName = HelperPin.convertHexToString(HelperPin.hexString(data5F20)).replaceAll("  ", "");
                    if (cardName.trim().length() == 0){
                        cardName = "-";
                    }
                } catch (Exception e){
                    e.printStackTrace();
                }

                try {

                    byte[] data57 = iemv.getCardData("57");

                    String expired = HelperPin.hexString(data57);
                    expiredDate = getExpired(expired.replace("D", "="));

                } catch (Exception e){
                    e.printStackTrace();
                }

                //show the card information and confirm it
//                log.append("Applet Name :" + applet);
//                log.append("\n");
//                log.append("TSI :" +tsi);
//                log.append("\n");
//                log.append("TVR :" +tvr);
//                log.append("\n");
//                log.append("AID :" +aid);
//                log.append("\n");
//                log.append("Card Name :" +cardName);
//                log.append("\n");
//                log.append("Expired :" +expiredDate);
//                log.append("\n");
//                runOnUiThread(updateLog());
                //confirm it, go to next step
                cdnumber = info.getString(ConstPBOCHandler.onConfirmCardInfo.info.KEY_PAN_String);
                iemv.importCardConfirmResult(ConstIPBOC.importCardConfirmResult.pass.allowed);
            }

            /**
             * Doing pinpad, base on CVM on card
             * Sub Number 3
             */
            @Override
            public void onRequestInputPIN(boolean isOnlinePin, int retryTimes) throws RemoteException {
                AppUtil.hideDialog();
                getActivity().runOnUiThread(showDialogThreat(pageId, generateTextInq()));

//                doPinPad(isOnlinePin, retryTimes);
            }

            /**
             * Optional, by default is confirmed
             */
            @Override
            public void onConfirmCertInfo(String certType, String certInfo) throws RemoteException {
                iemv.importCertConfirmResult(ConstIPBOC.importCertConfirmResult.option.CONFIRM );
            }

            /**
             * Generate F55 for transaction, do online process here
             * Sub Number 4
             */
            @Override
            public void onRequestOnlineProcess(Bundle aaResult) throws RemoteException {
                tagOfF55 = new SparseArray<>();
                byte[] tlv;
                int[] tagList = {
                        0x82,
                        0x84,
                        0x95,
                        0x9a,
                        0x9c,
                        0x5f2a,
                        0x5f34,
                        0x9f02,
                        0x9F03,
                        0x9f10,
                        0x9f1a,
                        0x9f26,
                        0x9f27,
                        0x9f33,
                        0x9f34,
                        0x9f35,
                        0x9f36,
                        0x9f37
                };

                for (int tag : tagList) {
                    tlv = iemv.getCardData(Integer.toHexString(tag).toUpperCase());
                    if (null != tlv && tlv.length > 0) {
                        tagOfF55.put(tag, HelperPin.byte2HexStr(tlv));
                    } else {
                        Log.e("DemoApp", "getCardData:" + Integer.toHexString(tag) + ", fails");
                    }
                }

                StringBuffer buffer = new StringBuffer();
                if (tagOfF55 != null) {
                    for (int i = 0; i < tagOfF55.size(); i++) {
                        int tag = tagOfF55.keyAt(i);
                        String value = tagOfF55.valueAt(i);
                        if (value.length() > 0) {
                            byte[] tmp = appendF55(tag, value);
                            buffer.append(HelperPin.byte2HexStr(tmp));
                        }
                    }
                    tagOfF55 = null;

                }

                String f55 = buffer.toString();

//                log.append("F55 Generated");
//                log.append("\n");
//                log.append(f55);
//                log.append("\n");
//                runOnUiThread(updateLog());
            }

            /**
             * Validate online response from host to card
             * Sub Number 5
             * This is the sample
             * Bundle onlineResult = new Bundle();
             *             onlineResult.putBoolean( ConstIPBOC.inputOnlineResult.onlineResult.KEY_isOnline_boolean, true);
             *             onlineResult.putString(ConstIPBOC.inputOnlineResult.onlineResult.KEY_respCode_String, responseCode);
             *             onlineResult.putString( ConstIPBOC.inputOnlineResult.onlineResult.KEY_authCode_String, authCode);
             *             onlineResult.putString( ConstIPBOC.inputOnlineResult.onlineResult.KEY_field55_String, f55);
             *             iemv.importOnlineResult(onlineResult, new OnlineResultHandler.Stub() {
             *                 @Override
             *                 public void onProccessResult(int result, Bundle data) throws RemoteException {
             *                     emvHandler.onTransactionResult(result, data);
             *                 }
             *             });
             */


            /**
             * Final result after bypass online result to card
             * Sub Number 6
             */
            @Override
            public void onTransactionResult(int result, Bundle data) throws RemoteException {
                switch (result) {

                    case ConstPBOCHandler.onTransactionResult.result.AARESULT_TC:
//                        log.append("Transaction Completed");
//                        log.append("\n");
//                        runOnUiThread(updateLog());
                        break;
                    default:
//                        log.append("Transaction Failed");
//                        log.append("\n");
//                        runOnUiThread(updateLog());
                        break;
                }
            }
        };
    }

    private String getExpired(String trackData2){
        return trackData2 != null && !"".equals(trackData2) && trackData2.length() > 16 ? trackData2.substring(17, 21) : null;
    }

    public byte[] appendF55( int tag, String value ){
        EMVTLVParam emvtlvF55 = new EMVTLVParam();
        String tlv = emvtlvF55.append(tag, value);
        return HelperPin.hexStr2Byte(tlv);
    }

//    @Override
//    public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
//        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
//            AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
//            builder.setCancelable(false);
//            builder.setMessage("Apakah anda yakin akan membatalkan proses " + pageId + "?");
//            builder.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
//                @Override
//                public void onClick(DialogInterface dialog, int which) {
//                    dialog.cancel();
//                    confirmationDialog.dismiss();
//                }
//            });
//            builder.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
//                @Override
//                public void onClick(DialogInterface dialog, int which) {
//                    dialog.cancel();
//                    confirmationDialog.show();
//                }
//            });
//            if (AlertDialog == null || !AlertDialog.isShowing()) {
//                AlertDialog = builder.create();
//                AlertDialog.show();
//            }
//            return true;
//        }
//        return false;
//    }

    public Runnable showToast(final String message){

        Runnable aRunnable = new Runnable(){
            public void run(){
                Toast.makeText(getActivity().getBaseContext(), message, Toast.LENGTH_LONG).show();
            }
        };

        return aRunnable;

    }

    public Runnable showDialogThreat(final String title, final String message){

        Runnable aRunnable = new Runnable(){
            public void run(){
                AppUtil.hideDialog();
                Bundle datas = new Bundle();
                datas.putString("title", title);
                datas.putString("message", message);
//                displayDialogInfoCard(getContext(), message, "card");

                FDialogInqCard dialogFragment = new FDialogInqCard();
                dialogFragment.setArguments(datas);
                dialogFragment.setTargetFragment(FPembayaranKartu.this, 0);
                dialogFragment.show(getFragmentManager(), "dialog");
            }
        };

        return aRunnable;

    }

    @Override
    public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
            android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(getContext());
            builder.setCancelable(false);
            builder.setMessage("Apakah anda yakin akan membatalkan proses " + pageId + "?");
            builder.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                    confirmationDialog.dismiss();
                }
            });
            builder.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                    confirmationDialog.show();
                }
            });
            if (AlertDialog == null || !AlertDialog.isShowing()) {
                AlertDialog = builder.create();
                AlertDialog.show();
            }
            return true;
        }
        return false;
    }

    private class SoapActivity extends AsyncTask<Void, Void, Void> {


        @Override
        protected void onPreExecute() {

        }

        @Override
        protected Void doInBackground(Void... params) {
            convertir();
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            if (msgErr.equalsIgnoreCase("true")) {
                Log.e("MSG", "ERROR");
            } else {
                Log.d("PRINT", "callPrintStruk");
                callPrint(responseJSON);
            }
        }

    }

    private void convertir() {

        try {
            if (!msgErr.equalsIgnoreCase("true")) {

                if (isChip) {
                    responseJSON = "CHIP";
                    Log.d("SOAP","PIN BENAR");
                } else {
                    responseJSON = "CARD";
                    Log.d("SOAP","PIN BENAR");
                }

            } else {

                responseJSON = "Gagal terhubung ke server";

            }


        } catch (Exception ex) {

            AppUtil.hideDialog();
            Log.d("THREAD 2", ex.toString());

        }

    }

    private Runnable errorDialog() {


        Runnable aRunnable = new Runnable(){
            public void run(){
                AppUtil.displayDialog(getContext(), "Pin Salah");
            }
        };

        return aRunnable;


    }

    private void  showInq() {
        displayDialogInfoCard(getContext(), "Hasil Inquiry Product", "inq");
    }

    public void displayDialogInfoCard(final Context context, String message, String type) {
        LayoutInflater li = LayoutInflater.from(context);
        View msgDialog = li.inflate(R.layout.custom_dialog_message_card, null);
        Button btnCancelMessage = (Button) msgDialog.findViewById(R.id.btn_cancel);
        Button btnOkMessage = (Button) msgDialog.findViewById(R.id.btn_next);
        TextView txt_msg_display = (TextView) msgDialog.findViewById(R.id.dialog_txt_msg_info);
        txt_msg_display.setText(message);
        AlertDialog.Builder alert = new AlertDialog.Builder(context);
        alert.setView(msgDialog);
        alert.setCancelable(false);
        final AlertDialog dialog = alert.create();

        btnOkMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (type.equalsIgnoreCase("card")){
                    showInq();
                } else {
                    doPinPad(true, 3);
                }
                dialog.dismiss();
            }
        });

        btnCancelMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    public void callPrint(String typeCard) {
        amount = 50000;
        fee = 5000;


        String date = new SimpleDateFormat("dd/MM/yyyy").format(Calendar.getInstance().getTime());
        String time = new SimpleDateFormat("HH:mm:ss").format(Calendar.getInstance().getTime());

        Bundle datas = new Bundle();
//        datas.putString("type_trx", "TRANSFER");
//        datas.putString("prod_id", "BRIS");
        datas.putString("type_trx", getArguments().getString("kartu_id"));
        datas.putString("prod_id", pageId);
        datas.putString("tid", "08267001");
        datas.putString("mid", "4228267000001");
        datas.putString("type_card", typeCard);
        datas.putString("card_no", cdnumber);
        datas.putString("date", date);
        datas.putString("time", time);
        datas.putString("id_pelanggan", id_pelanggan);
        datas.putString("nama_pelanggan", "ADI PRASTYO");
        datas.putString("amount", Integer.toString(amount));
        datas.putString("fee", Integer.toString(fee));
        datas.putString("total", Integer.toString(amount+fee));
        AppUtilPrint.printStruk(getContext(), datas);
    }
}
