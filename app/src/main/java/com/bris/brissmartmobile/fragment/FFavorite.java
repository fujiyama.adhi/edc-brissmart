package com.bris.brissmartmobile.fragment;


import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.bris.brissmartmobile.R;
import com.bris.brissmartmobile.adapter.FFavoriteAdapter;
import com.bris.brissmartmobile.listener.FavClickListener;
import com.bris.brissmartmobile.model.Favorite;
import com.bris.brissmartmobile.util.AppUtil;
import com.bris.brissmartmobile.util.Menu;
import com.bris.brissmartmobile.util.NothingSelectedSpinnerAdapter;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;

public class FFavorite extends Fragment implements FavClickListener {
    private Spinner spinnerjenispembayaran, spinnertrans;
    private ImageButton btnTambah;
    private Realm realm;
    private FFavoriteAdapter favAdapter;
    private ProgressDialog mProgressDialog = null;
    private RecyclerView rvFav;
    private RelativeLayout rlFavEmpty;
    private List<Favorite> dataFav = new ArrayList<>();
    private String pageTitle, menutrx = null, submenutrx = null, namajenisfav = null;
    private String data1 = "", data4 = "";
    private String[] arrJenisPembayaran = null, arrSpinnerCateg = null;
    private View view;
    private String jenispembayaran;
    private String[] listPembayaran = null;
    private String[] listPembelian = null;
    private String[] listDonasi = null;

    public FFavorite() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_favorit_tunai_debitbsa, container, false);

        if (getArguments().containsKey("jenispembayaran")) {
            jenispembayaran = getArguments().getString("jenispembayaran");
        }

        spinnertrans = (Spinner) view.findViewById(R.id.spinner_fav_trans);
        spinnerjenispembayaran = (Spinner) view.findViewById(R.id.spinner_fav_trans_jenis_pembayaran);

        rlFavEmpty = (RelativeLayout) view.findViewById(R.id.relative_empty);
        rlFavEmpty.setVisibility(View.INVISIBLE);

        btnTambah = (ImageButton) view.findViewById(R.id.btn_add_fav);
        btnTambah.setVisibility(View.INVISIBLE);
        rvFav = (RecyclerView) view.findViewById(R.id.rv_fav_trans);
        realm = Realm.getDefaultInstance();

        listPembayaran = getResources().getStringArray(R.array.product_pembayaran_all);
        listPembelian = getResources().getStringArray(R.array.product_pembelian_all);
        listDonasi = getResources().getStringArray(R.array.product_donasi_all);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Bundle args = getArguments();
        if (args != null) {
            menutrx = getArguments().getString("menutrx");
            pageTitle = getArguments().getString(Menu.MENU_ID);

            if (pageTitle.equalsIgnoreCase("Rekening Tabungan Cerdas Favorit") || pageTitle.equalsIgnoreCase("Rekening Tabungan Cerdas")
                    || pageTitle.equalsIgnoreCase("Rekening BRI Syariah")) {
                spinnertrans.setVisibility(View.GONE);
                spinnerjenispembayaran.setVisibility(View.GONE);
            }

            if (pageTitle.equalsIgnoreCase("Donasi Favorit")) {
                spinnerjenispembayaran.setVisibility(View.GONE);
            }
        }
        customToolbar(view);
        main();
    }

    private void main() {

        ArrayAdapter<String> adapter = null;
        if (pageTitle.equalsIgnoreCase("Rekening Tabungan Cerdas Favorit")) {
            submenutrx = "1001";
            data1 = "Cek Saldo";
            namajenisfav = "Nomor Rekening Tabungan Cerdas Favorit";
            getDataFavorit(menutrx, jenispembayaran, data1);
            btnTambah.setVisibility(View.VISIBLE);
        } else if (pageTitle.equalsIgnoreCase("Rekening Tabungan Cerdas")) {
            jenispembayaran = "CASH";
            submenutrx = "8001";
            data1 = "Rekening Tabungan Cerdas";
            namajenisfav = "Transfer Rekening Tabungan Cerdas";
            getDataFavorit(menutrx, jenispembayaran, submenutrx);
        } else if (pageTitle.equalsIgnoreCase("Rekening BRI Syariah")) {
            jenispembayaran = "CASH";
            submenutrx = "8002";
            data1 = "Rekening BRI Syariah";
            namajenisfav = "Transfer Rekening BRI Syariah";
            getDataFavorit(menutrx, jenispembayaran, submenutrx);
        } else {
            switch (pageTitle) {
                case "Transfer Favorit":
                    arrSpinnerCateg = getResources().getStringArray(R.array.product_transfer_all);
                    adapter = new ArrayAdapter<>(getContext(), R.layout.spinner_style_fav, arrSpinnerCateg);
                    break;
                case "Pembayaran Favorit":
                    arrSpinnerCateg = getResources().getStringArray(R.array.product_pembayaran_all);
                    adapter = new ArrayAdapter<>(getContext(), R.layout.spinner_style_fav, arrSpinnerCateg);
                    break;
                case "Pembelian Favorit":
                    arrSpinnerCateg = getResources().getStringArray(R.array.product_pembelian_all);
                    adapter = new ArrayAdapter<>(getContext(), R.layout.spinner_style_fav, arrSpinnerCateg);
                    break;
                case "Donasi Favorit":
                    arrSpinnerCateg = getResources().getStringArray(R.array.product_donasi_all);
                    adapter = new ArrayAdapter<>(getContext(), R.layout.spinner_style_fav, arrSpinnerCateg);
                    break;
                default:
                    break;
            }

            adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
            spinnertrans.setAdapter(new NothingSelectedSpinnerAdapter(
                    adapter,
                    R.layout.spinner_favorit_nothing_selected,
                    getContext()
            ));
        }

        //set up recycleview
        favAdapter = new FFavoriteAdapter(dataFav, getContext(), FFavorite.this, "");
        rvFav.setHasFixedSize(true);
        rvFav.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvFav.setAdapter(favAdapter);
        favAdapter.notifyDataSetChanged();

        onAction();
    }


    private void onAction() {
        spinnertrans.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (spinnertrans.getSelectedItem() != null) {
                    spinnerjenispembayaran.setEnabled(true);
                    ArrayAdapter<String> adapterJenisPembayaran = null;
                    arrJenisPembayaran = getResources().getStringArray(R.array.jenis_pembayaran);
                    adapterJenisPembayaran = new ArrayAdapter<>(getContext(), R.layout.spinner_style_fav, arrJenisPembayaran);
                    adapterJenisPembayaran.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
                    spinnerjenispembayaran.setAdapter(new NothingSelectedSpinnerAdapter(
                            adapterJenisPembayaran,
                            R.layout.spinner_favorit_jenis_pembayaran_nothing_selected,
                            getContext()
                    ));

                    String produk = spinnertrans.getSelectedItem().toString().trim();

                    if (pageTitle.equalsIgnoreCase("Transfer Favorit")) {
                        if (produk.equalsIgnoreCase("Transfer Rekening Tabungan Cerdas")) {
//                            jenispembayaran = "DEBIT";
                            submenutrx = "8001";
                            data1 = "Rekening Tabungan Cerdas";
                            namajenisfav = "Transfer Rekening Tabungan Cerdas";
//                            spinnerjenispembayaran.setVisibility(View.GONE);
//                            getDataFavorit(menutrx, jenispembayaran, submenutrx);
//                            favAdapter.notifyDataSetChanged();
                            spinnerjenispembayaran.setVisibility(View.VISIBLE);
                            btnTambah.setVisibility(View.INVISIBLE);
//                            btnTambah.setVisibility(View.VISIBLE);
                        } else if (produk.equalsIgnoreCase("Transfer Rekening BRI Syariah")) {
//                            jenispembayaran = "DEBIT";
                            submenutrx = "8002";
                            data1 = "Rekening BRI Syariah";
                            namajenisfav = "Transfer Rekening BRI Syariah";
//                            spinnerjenispembayaran.setVisibility(View.GONE);
//                            getDataFavorit(menutrx, jenispembayaran, submenutrx);
//                            favAdapter.notifyDataSetChanged();
                            spinnerjenispembayaran.setVisibility(View.VISIBLE);
                            btnTambah.setVisibility(View.INVISIBLE);
//                            btnTambah.setVisibility(View.VISIBLE);
                        } else {
                            submenutrx = "8009";
                            namajenisfav = "Transfer Bank Lain";
                            spinnerjenispembayaran.setVisibility(View.VISIBLE);
                            btnTambah.setVisibility(View.INVISIBLE);
                        }
                    } else if (pageTitle.equalsIgnoreCase("Pembayaran Favorit")) {
                        if (produk.equalsIgnoreCase(listPembayaran[0]) || produk.equalsIgnoreCase(listPembayaran[1])) {
                            submenutrx = "7001";
                            namajenisfav = "Pembayaran PLN";
                            if (produk.equalsIgnoreCase(listPembayaran[0])) {
                                data1 = "TAGLIS";
                            } else if (produk.equalsIgnoreCase(listPembayaran[1])) {
                                data1 = "NONTAG";
                            }
                            data4 = produk;
                        } else if (produk.equalsIgnoreCase(listPembayaran[2]) || produk.equalsIgnoreCase(listPembayaran[3])
                                || produk.equalsIgnoreCase(listPembayaran[4]) || produk.equalsIgnoreCase(listPembayaran[5])
                                || produk.equalsIgnoreCase(listPembayaran[6])) {
                            submenutrx = "7002";
                            namajenisfav = "Pembayaran Telepon";
                            if (produk.equalsIgnoreCase(listPembayaran[2]) || produk.equalsIgnoreCase(listPembayaran[3])) {
                                data1 = "TELKOM";
                            } else if (produk.equalsIgnoreCase(listPembayaran[4])) {
                                data1 = "HALO";
                            } else if (produk.equalsIgnoreCase(listPembayaran[5])) {
                                data1 = "XL";
                            } else if (produk.equalsIgnoreCase(listPembayaran[6])) {
                                data1 = "SMARTFREN";
                            }
                            data4 = produk;
                        } else if (produk.equalsIgnoreCase(listPembayaran[7])) {
                            submenutrx = "7003";
                            namajenisfav = "Pembayaran Internet";
                            data1 = "TELKOM";
                            data4 = produk;
                        } else if (produk.equalsIgnoreCase(listPembayaran[8]) || produk.equalsIgnoreCase(listPembayaran[9])
                                || produk.equalsIgnoreCase(listPembayaran[10])) {
                            submenutrx = "7004";
                            namajenisfav = "Pembayaran TV";
                            if (produk.equalsIgnoreCase(listPembayaran[8])) {
                                data1 = "TELKOM";
                            } else if (produk.equalsIgnoreCase(listPembayaran[9])) {
                                data1 = "BIGTV";
                            } else if (produk.equalsIgnoreCase(listPembayaran[10])) {
                                data1 = "INDOVISION";
                            }
                            data4 = produk;
                        } else if (produk.equalsIgnoreCase(listPembayaran[11])) {
                            submenutrx = "7009";
                            namajenisfav = "Pembayaran Tokopedia";
                            data1 = "TOKOPEDIA";
                            data4 = produk;
                        } else if (produk.equalsIgnoreCase(listPembayaran[12])) {
                            submenutrx = "7006";
                            namajenisfav = "Pembayaran Institusi";
                        } else if (produk.equalsIgnoreCase(listPembayaran[13])) {
                            data4 = "S";
                            submenutrx = "7007";
                            namajenisfav = "Pembayaran Pendidikan";
                        } else if (produk.equalsIgnoreCase(listPembayaran[14])) {
                            data4 = "P";
                            submenutrx = "7007";
                            namajenisfav = "Pembayaran Pendidikan";
                        } else if (produk.equalsIgnoreCase(listPembayaran[15])) {
                            submenutrx = "7008";
                            namajenisfav = "Pembayaran Asuransi";
                        }
                    } else if (pageTitle.equalsIgnoreCase("Pembelian Favorit")) {
                        if (produk.equalsIgnoreCase(listPembelian[0])) {
                            submenutrx = "6001";
                            namajenisfav = "Pembelian Token Listrik PLN";
                            data1 = "Token Listrik";
                        } else if (produk.equalsIgnoreCase(listPembelian[1]) || produk.equalsIgnoreCase(listPembelian[2])
                                || produk.equalsIgnoreCase(listPembelian[3]) || produk.equalsIgnoreCase(listPembelian[4])
                                || produk.equalsIgnoreCase(listPembelian[5]) || produk.equalsIgnoreCase(listPembelian[6])
                                || produk.equalsIgnoreCase(listPembelian[7])) {
                            submenutrx = "6002";
                            namajenisfav = "Pembelian Pulsa";
                            if (produk.equalsIgnoreCase(listPembelian[1])) {
                                data1 = "SIMPATI";
                            } else if (produk.equalsIgnoreCase(listPembelian[2])) {
                                data1 = "XL";
                            } else if (produk.equalsIgnoreCase(listPembelian[3])) {
                                data1 = "ESIA";
                            } else if (produk.equalsIgnoreCase(listPembelian[4])) {
                                data1 = "SMARTFREN";
                            } else if (produk.equalsIgnoreCase(listPembelian[5]) || produk.equalsIgnoreCase(listPembelian[7])) {
                                data1 = "INDOSAT";
                            } else if (produk.equalsIgnoreCase(listPembelian[6])) {
                                data1 = "STARONE";
                            }
                        } else if (produk.equalsIgnoreCase(listPembelian[8])) {
                            submenutrx = "6003";
                            namajenisfav = "Pembelian Paket Data";
                            data1 = "TSELDATA";
                        } else if (produk.equalsIgnoreCase(listPembelian[9]) || produk.equalsIgnoreCase(listPembelian[10])) {
                            submenutrx = "6004";
                            namajenisfav = "Pembelian Saldo Gopay";
                            if (produk.equalsIgnoreCase(listPembelian[9])) {
                                data1 = "GOPAY";
                            } else if (produk.equalsIgnoreCase(listPembelian[10])) {
                                data1 = "GOPAYDRIVER";
                            }
                        }
                        data4 = produk;
                    } else if (pageTitle.equalsIgnoreCase("Donasi Favorit")) {
                        jenispembayaran = "CASH";
                        if (produk.equalsIgnoreCase(listDonasi[0]) || produk.equalsIgnoreCase(listDonasi[1])
                                || produk.equalsIgnoreCase(listDonasi[2]) || produk.equalsIgnoreCase(listDonasi[3])) {
                            submenutrx = "9001";
                            namajenisfav = "Donasi Baznas";
                            if (produk.equalsIgnoreCase(listDonasi[0])) {
                                data1 = "01|Zakat";
                            } else if (produk.equalsIgnoreCase(listDonasi[1])) {
                                data1 = "02|Infaq";
                            } else if (produk.equalsIgnoreCase(listDonasi[2])) {
                                data1 = "03|Shodaqoh";
                            } else if (produk.equalsIgnoreCase(listDonasi[3])) {
                                data1 = "04|Qurban";
                            }
                        } else if (produk.equalsIgnoreCase(listDonasi[4]) || produk.equalsIgnoreCase(listDonasi[5])
                                || produk.equalsIgnoreCase(listDonasi[6])) {
                            submenutrx = "9002";
                            namajenisfav = "Donasi Dompet Dhuafa";
                            if (produk.equalsIgnoreCase(listDonasi[4])) {
                                data1 = "01|Zakat";
                            } else if (produk.equalsIgnoreCase(listDonasi[5])) {
                                data1 = "02|Infaq";
                            } else if (produk.equalsIgnoreCase(listDonasi[6])) {
                                data1 = "03|Shodaqoh";
                            }
                        } else if (produk.equalsIgnoreCase(listDonasi[7]) || produk.equalsIgnoreCase(listDonasi[8])
                                || produk.equalsIgnoreCase(listDonasi[9])) {
                            submenutrx = "9003";
                            namajenisfav = "Donasi Griya Yatim";
                            if (produk.equalsIgnoreCase(listDonasi[7])) {
                                data1 = "01|Zakat";
                            } else if (produk.equalsIgnoreCase(listDonasi[8])) {
                                data1 = "02|Infaq";
                            } else if (produk.equalsIgnoreCase(listDonasi[9])) {
                                data1 = "03|Shodaqoh";
                            }
                        } else if (produk.equalsIgnoreCase(listDonasi[10]) || produk.equalsIgnoreCase(listDonasi[11])
                                || produk.equalsIgnoreCase(listDonasi[12])) {
                            submenutrx = "9004";
                            namajenisfav = "Donasi ZIS BRISyariah";
                            if (produk.equalsIgnoreCase(listDonasi[10])) {
                                data1 = "01|Zakat";
                            } else if (produk.equalsIgnoreCase(listDonasi[11])) {
                                data1 = "02|Infaq";
                            } else if (produk.equalsIgnoreCase(listDonasi[12])) {
                                data1 = "03|Shodaqoh";
                            }
                        } else if (produk.equalsIgnoreCase(listDonasi[13]) || produk.equalsIgnoreCase(listDonasi[14])
                                || produk.equalsIgnoreCase(listDonasi[15])) {
                            submenutrx = "9005";
                            namajenisfav = "Donasi Bazis DKI Jakarta";
                            if (produk.equalsIgnoreCase(listDonasi[13])) {
                                data1 = "01|Zakat";
                            } else if (produk.equalsIgnoreCase(listDonasi[14])) {
                                data1 = "02|Infaq";
                            } else if (produk.equalsIgnoreCase(listDonasi[15])) {
                                data1 = "03|Shodaqoh";
                            }
                        } else if (produk.equalsIgnoreCase(listDonasi[16]) || produk.equalsIgnoreCase(listDonasi[17])
                                || produk.equalsIgnoreCase(listDonasi[18])) {
                            submenutrx = "9006";
                            namajenisfav = "Donasi Yayasan Dompet Sosial Madani Bali";
                            if (produk.equalsIgnoreCase(listDonasi[16])) {
                                data1 = "01|Zakat";
                            } else if (produk.equalsIgnoreCase(listDonasi[17])) {
                                data1 = "02|Infaq";
                            } else if (produk.equalsIgnoreCase(listDonasi[18])) {
                                data1 = "03|Shodaqoh";
                            }
                        } else if (produk.equalsIgnoreCase(listDonasi[19]) || produk.equalsIgnoreCase(listDonasi[20])
                                || produk.equalsIgnoreCase(listDonasi[21])) {
                            submenutrx = "9007";
                            namajenisfav = "Donasi Yayasan Baitul Maal Hidayatullah";
                            if (produk.equalsIgnoreCase(listDonasi[19])) {
                                data1 = "01|Zakat";
                            } else if (produk.equalsIgnoreCase(listDonasi[20])) {
                                data1 = "02|Infaq";
                            } else if (produk.equalsIgnoreCase(listDonasi[21])) {
                                data1 = "03|Shodaqoh";
                            }
                        } else if (produk.equalsIgnoreCase(listDonasi[22]) || produk.equalsIgnoreCase(listDonasi[23])
                                || produk.equalsIgnoreCase(listDonasi[24])) {
                            submenutrx = "9008";
                            namajenisfav = "Donasi LazisNU Jatim";
                            if (produk.equalsIgnoreCase(listDonasi[22])) {
                                data1 = "01|Zakat";
                            } else if (produk.equalsIgnoreCase(listDonasi[23])) {
                                data1 = "02|Infaq";
                            } else if (produk.equalsIgnoreCase(listDonasi[24])) {
                                data1 = "03|Shodaqoh";
                            }
                        }
                        getDataFavorit(menutrx, jenispembayaran, data1);
                        favAdapter.notifyDataSetChanged();
                    }

                } else {
                    spinnerjenispembayaran.setEnabled(false);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });

        spinnerjenispembayaran.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (spinnerjenispembayaran.getSelectedItem() != null) {
                    btnTambah.setVisibility(View.VISIBLE);
                    if (spinnerjenispembayaran.getSelectedItem().toString().trim().equalsIgnoreCase("Tunai")) {
                        jenispembayaran = "CASH";
                    } else {
                        jenispembayaran = "DEBIT";
                    }

                    if (pageTitle.equalsIgnoreCase("Transfer Favorit") || submenutrx.equalsIgnoreCase("7006") || submenutrx.equalsIgnoreCase("7008")) {
                        getDataFavorit(menutrx, jenispembayaran, submenutrx);
                    } else if (submenutrx.equalsIgnoreCase("7007")) {
                        getDataFavorit(menutrx, jenispembayaran, data4);
                    } else {
                        getDataFavorit(menutrx, jenispembayaran, data1);
                    }
                }
                favAdapter.notifyDataSetChanged();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        btnTambah.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                final Bundle bundle = new Bundle();
                FragmentManager manager = getFragmentManager();

                manager.addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
                    @Override
                    public void onBackStackChanged() {
                        switch (menutrx) {
                            case "1000":
                                getDataFavorit(menutrx, getArguments().getString("jenispembayaran"), "Cek Saldo");
                                break;
                            case "8000":
                                if (pageTitle.equalsIgnoreCase("Rekening Tabungan Cerdas") || pageTitle.equalsIgnoreCase("Rekening BRI Syariah")) {
                                    getDataFavorit(menutrx, "CASH", submenutrx);
                                } else if (spinnertrans.getSelectedItem().toString().trim().equalsIgnoreCase("Transfer Rekening Tabungan Cerdas")) {
                                    getDataFavorit(menutrx, "DEBIT", submenutrx);
                                } else if (spinnertrans.getSelectedItem().toString().trim().equalsIgnoreCase("Transfer Rekening BRI Syariah")) {
                                    getDataFavorit(menutrx, "DEBIT", submenutrx);
                                } else {
                                    getDataFavorit(menutrx, jenispembayaran, submenutrx);
                                }
                                break;
                            case "7000":
                                if (submenutrx.equalsIgnoreCase("7006") || submenutrx.equalsIgnoreCase("7008")) {
                                    getDataFavorit(menutrx, jenispembayaran, submenutrx);
                                } else if (submenutrx.equalsIgnoreCase("7007")) {
                                    getDataFavorit(menutrx, jenispembayaran, data4);
                                } else {
                                    getDataFavorit(menutrx, jenispembayaran, data1);
                                }
                                break;
                            case "6000":
                                getDataFavorit(menutrx, jenispembayaran, data1);
                                break;
                            case "9000":
                                getDataFavorit(menutrx, jenispembayaran, data1);
                                break;
                            default:
                                break;
                        }
                        favAdapter.notifyDataSetChanged();
                    }
                });

                Fragment fragment;
                FragmentTransaction transaction = manager.beginTransaction();
                fragment = new FInsertUpdateDataFavorit();

                if (pageTitle.equalsIgnoreCase("Rekening Tabungan Cerdas Favorit")) {
                    bundle.putString("title", "Nomor Rekening Tabungan Cerdas");
                } else if (pageTitle.equalsIgnoreCase("Rekening Tabungan Cerdas")) {
                    bundle.putString("title", "Transfer Nomor Rekening Tabungan Cerdas");
                } else if (pageTitle.equalsIgnoreCase("Rekening BRI Syariah")) {
                    bundle.putString("title", "Transfer Rekening BRI Syariah");
                } else {
                    bundle.putString("title", spinnertrans.getSelectedItem().toString().trim());
                }
                bundle.putString("jenispembayaran", jenispembayaran);
                bundle.putString("data1", data1);
                bundle.putString("data4", data4);
                bundle.putString("menutrx", menutrx);
                bundle.putString(Menu.MENU_ID, pageTitle);
                bundle.putString("submenutrx", submenutrx);
                bundle.putString("namajenisfav", namajenisfav);
//                if (submenutrx.equalsIgnoreCase("7007")) {
//                    bundle.putString("data4", data4);
//                }
//                if (pageTitle.equalsIgnoreCase("Transfer Favorit")) {
//                    bundle.putString("submenutrx", submenutrx);
//                }

                fragment.setArguments(bundle);
                transaction.replace(R.id.fragment_fav_trans, fragment);
                transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });
    }


    public void getDataFavorit(String menutrx, String jenisPembayaran, String data1) {
        RealmResults<Favorite> fav;
        if (pageTitle.equalsIgnoreCase("Rekening Tabungan Cerdas") || pageTitle.equalsIgnoreCase("Rekening BRI Syariah")
                || pageTitle.equalsIgnoreCase("Transfer Favorit") || submenutrx.equalsIgnoreCase("7006")
                || submenutrx.equalsIgnoreCase("7008")) {
            fav = realm.where(Favorite.class)
                    .equalTo("menutrx", menutrx)
                    .equalTo("submenutrx", data1)
                    .equalTo("jenispembayaran", jenisPembayaran)
                    .findAll();
        } else if (submenutrx.equalsIgnoreCase("7007")) {
            fav = realm.where(Favorite.class)
                    .equalTo("menutrx", menutrx)
                    .equalTo("data4", data1)
                    .equalTo("jenispembayaran", jenisPembayaran)
                    .findAll();
        } else if (menutrx.equalsIgnoreCase("9000")) {
            fav = realm.where(Favorite.class)
                    .equalTo("menutrx", menutrx)
                    .equalTo("jenispembayaran", jenisPembayaran)
                    .equalTo("submenutrx", submenutrx)
                    .equalTo("data1", data1)
                    .findAll();
        } else {
            fav = realm.where(Favorite.class)
                    .equalTo("menutrx", menutrx)
                    .equalTo("jenispembayaran", jenisPembayaran)
                    .equalTo("data1", data1)
                    .findAll();
        }

        if (fav.size() > 0) {
            rlFavEmpty.setVisibility(View.INVISIBLE);
            dataFav.clear();
            dataFav.addAll(fav);
        } else {
            rlFavEmpty.setVisibility(View.VISIBLE);
            dataFav.clear();
        }

    }

    @Override
    public void getFav(final String label, final String category) {
    }

    @Override
    public void onclikFav(String namafav, final String data1) {
        FragmentManager manager = getFragmentManager();
        Fragment fragment;

        Log.d("TEST onclickfav", "namafav :" + namafav + ", data1" + data1);

        FragmentTransaction transaction = manager.beginTransaction();
        fragment = new FInsertUpdateDataFavorit();
        final Bundle bundle = new Bundle();
        bundle.putString("namafav", namafav);
        bundle.putString("menutrx", menutrx);
        bundle.putString("data1", data1);
        bundle.putString("jenispembayaran", jenispembayaran);
        bundle.putString("submenutrx", submenutrx);
        if (submenutrx.equalsIgnoreCase("7007")) {
            bundle.putString("data4", data4);
        }
        if (pageTitle.equalsIgnoreCase("Rekening Tabungan Cerdas Favorit")) {
            bundle.putString("title", "Rekening Tabungan Cerdas");
        } else if (pageTitle.equalsIgnoreCase("Rekening Tabungan Cerdas")) {
            bundle.putString("title", "Transfer Rekening Tabungan Cerdas");
        } else if (pageTitle.equalsIgnoreCase("Rekening BRI Syariah")) {
            bundle.putString("title", "Transfer Rekening BRI Syariah");
        } else {
            bundle.putString("title", spinnertrans.getSelectedItem().toString().trim());
        }
//        if (pageTitle.equalsIgnoreCase("Transfer Favorit")) {
//            bundle.putString("submenutrx", submenutrx);
//        }
        fragment.setArguments(bundle);
        transaction.replace(R.id.fragment_fav_trans, fragment);
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        transaction.addToBackStack(null);
        transaction.commit();

        manager.addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
            @Override
            public void onBackStackChanged() {
                Log.d("Cek onBack", "Mlebu");
                switch (menutrx) {
                    case "1000":
                        getDataFavorit(menutrx, getArguments().getString("jenispembayaran"), "Cek Saldo");
                        break;
                    case "8000":
                        if (pageTitle.equalsIgnoreCase("Rekening Tabungan Cerdas") || pageTitle.equalsIgnoreCase("Rekening BRI Syariah")) {
                            getDataFavorit(menutrx, "CASH", submenutrx);
                        } else if (spinnertrans.getSelectedItem().toString().trim().equalsIgnoreCase("Transfer Rekening Tabungan Cerdas")) {
                            getDataFavorit(menutrx, "DEBIT", submenutrx);
                        } else if (spinnertrans.getSelectedItem().toString().trim().equalsIgnoreCase("Transfer Rekening BRI Syariah")) {
                            getDataFavorit(menutrx, "DEBIT", submenutrx);
                        } else {
                            getDataFavorit(menutrx, jenispembayaran, submenutrx);
                        }
                        break;
                    case "7000":
                        if (submenutrx.equalsIgnoreCase("7006") || submenutrx.equalsIgnoreCase("7008")) {
                            getDataFavorit(menutrx, jenispembayaran, submenutrx);
                        } else if (submenutrx.equalsIgnoreCase("7007")) {
                            getDataFavorit(menutrx, jenispembayaran, data4);
                        } else {
                            getDataFavorit(menutrx, jenispembayaran, data1);
                        }
                        break;
                    case "6000":
                        getDataFavorit(menutrx, jenispembayaran, data1);
                        break;
                    case "9000":
                        getDataFavorit(menutrx, jenispembayaran, data1);
                    default:
                        break;
                }
                favAdapter.notifyDataSetChanged();
            }
        });
    }

    @Override
    public void deleteFav(final String jenispembayaran, final String menutrx, final String submenutrx, final String namafav) {

        AlertDialog dialogAlert = null;
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setCancelable(false);
        builder.setMessage("Anda akan menghapus list '" + namafav + "' ?");
        builder.setPositiveButton("Hapus", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface alertdialog, int which) {
                alertdialog.cancel();

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        deleteFavTrans(jenispembayaran, menutrx, submenutrx, namafav);
                    }
                });

                if (pageTitle.equalsIgnoreCase("Rekening Tabungan Cerdas Favorit")) {
                    getDataFavorit(menutrx, jenispembayaran, data1);
                } else if (pageTitle.equalsIgnoreCase("Transfer Favorit") ||
                        pageTitle.equalsIgnoreCase("Rekening Tabungan Cerdas") || pageTitle.equalsIgnoreCase("Rekening BRI Syariah")) {
                    getDataFavorit(menutrx, jenispembayaran, submenutrx);
                } else if (pageTitle.equalsIgnoreCase("Pembayaran Favorit")) {
                    if (submenutrx.equalsIgnoreCase("7006") || submenutrx.equalsIgnoreCase("7008")) {
                        getDataFavorit(menutrx, jenispembayaran, submenutrx);
                    } else if (submenutrx.equalsIgnoreCase("7007")) {
                        getDataFavorit(menutrx, jenispembayaran, data4);
                    } else {
                        getDataFavorit(menutrx, jenispembayaran, data1);
                    }
                } else if (pageTitle.equalsIgnoreCase("Donasi Favorit")) {
                    getDataFavorit(menutrx, "CASH", data1);
                } else {
                    getDataFavorit(menutrx, jenispembayaran, spinnertrans.getSelectedItem().toString().trim());
                }

                favAdapter.notifyDataSetChanged();

                mProgressDialog = new ProgressDialog(getContext());
                mProgressDialog.setIndeterminate(true);
                mProgressDialog.setCancelable(false);
                mProgressDialog.setMessage(getString(R.string.dialog_onprogress_txt_sinkronisasi));

            }
        });

        builder.setNegativeButton("Batal", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface alertdialog, int which) {
                alertdialog.cancel();
            }
        });

        if (dialogAlert == null || !dialogAlert.isShowing()) {
            dialogAlert = builder.create();
            dialogAlert.show();
        }
    }

    private void deleteFavTrans(final String jenispembayaran, final String menutrx, final String submenutrx, final String namafav) {
        final String id_fav;
        RealmResults<Favorite> fav = realm.where(Favorite.class)
                .equalTo("jenispembayaran", jenispembayaran)
                .equalTo("menutrx", menutrx)
                .equalTo("submenutrx", submenutrx)
                .equalTo("namafav", namafav)
                .findAll();

        id_fav = fav.get(0).getIdfav();

        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {

                boolean delFav = AppUtil.deleteFavorite(id_fav);

                if (!delFav) {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            AppUtil.displayDialog(getContext(), "Proses sinkronisasi data gagal");
                        }
                    });
                }

            }
        });
        thread.start();

        Log.d("Cek Update fav", fav.toString());

        if (fav.size() > 0) {
            realm.beginTransaction();
            fav.deleteAllFromRealm();
            realm.commitTransaction();
        }
    }

    public void showDialog() {

        if (mProgressDialog != null && !mProgressDialog.isShowing())
            mProgressDialog.show();
    }

    public void hideDialog() {

        if (mProgressDialog != null && mProgressDialog.isShowing())
            mProgressDialog.dismiss();
    }

    private void customToolbar(View view) {
        Toolbar toolbar = (Toolbar) view.findViewById(R.id.tb_favorit);
        TextView tv_toolbar = (TextView) view.findViewById(R.id.tv_tb_favorit);
        tv_toolbar.setText(pageTitle);
        toolbar.setNavigationIcon(R.mipmap.abc_ic_ab_back_mtrl_am_alpha);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().onBackPressed();
            }
        });
    }
}
