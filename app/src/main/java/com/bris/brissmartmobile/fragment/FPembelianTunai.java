package com.bris.brissmartmobile.fragment;

import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;

import com.bris.brissmartmobile.R;
import com.bris.brissmartmobile.listener.ConfirmTrxListener;
import com.bris.brissmartmobile.listener.FavClickListener;
import com.bris.brissmartmobile.model.Favorite;
import com.bris.brissmartmobile.util.AppPreferences;
import com.bris.brissmartmobile.util.AppUtil;
import com.bris.brissmartmobile.util.AppUtilPrint;
import com.bris.brissmartmobile.util.Menu;
import com.bris.brissmartmobile.util.NothingSelectedSpinnerAdapter;
import com.bris.brissmartmobile.util.NumberTextWatcherForThousand;

import org.json.JSONObject;

import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by valeputra on 6/22/17.
 */

public class FPembelianTunai extends Fragment implements ConfirmTrxListener, FavClickListener,
        TextWatcher, View.OnFocusChangeListener, View.OnTouchListener, DialogInterface.OnKeyListener {

    private Spinner spinner1, spinner2;
    private TextView tvSpinner1, tvSpinner2;
    private EditText et1, et2;
    private TextInputLayout til1, til2, tilNorek;
    private Button btn1, btnConfirm;
    private ImageButton btnFav;
    private String id_pelanggan, nominal_gopay, nominal, paket_type, message, product, kode_transaksi;
    private String jenis_pembelian;
    private String tid, pageId;
    private ArrayAdapter<String> adapterProduct;
    private List<Favorite> favDatas = new ArrayList<>();
    private JSONObject json, resdata;
    private String id, idtype, jenisid, merchantid, branch_id, bit11Biller, bit42Biller, bit60Biller, bit61Biller, meter_id,
            subscr_id, swref, owner, tariff, power, bit62Biller, nut, bit127Biller, adminfee,
            purchasetype;
    private String data1 = null, data4 = null, submenutrx, namajenisfav;
    private String[] listPulsa = null, listPaketData = null, listGopay = null;
    private JSONObject params, lon;
    private String msgErr, rcode;
    private Realm realm;
    private AlertDialog AlertDialog = null,
            confirmationDialog = null;
    private View view;

    private AppPreferences appPref;

    public FPembelianTunai() {
    }

    public View onCreateView(final LayoutInflater inflater, @Nullable final ViewGroup container, @Nullable Bundle savedInstanceState) {
        String produk = getArguments().getString(Menu.MENU_ID);
        pageId = produk.substring(0, produk.indexOf(" Tunai"));
        Log.d("Cek pageId", pageId);

        appPref = new AppPreferences(getContext());
        tid = appPref.getDeviceTid();
        Log.d("TID PEMBELIAN", tid);

        listPulsa = getResources().getStringArray(R.array.product_pembelian_pulsa);
        listPaketData = getResources().getStringArray(R.array.product_pembelian_paket_internet);
        listGopay = getResources().getStringArray(R.array.product_pembelian_gopay);

        if (pageId.equalsIgnoreCase("Token Listrik")) {
            view = inflater.inflate(R.layout.activity_pembelian_listrik, container, false);

            tvSpinner1 = (TextView) view.findViewById(R.id.tv_spinner1);
            tvSpinner1.setText("Nominal");

            spinner1 = (Spinner) view.findViewById(R.id.spinner1);
            setSpinnerProduct(pageId);

            et1 = (EditText) view.findViewById(R.id.et_id_pelanggan);
            til1 = (TextInputLayout) view.findViewById(R.id.til_id_pelanggan);

        } else if (pageId.equalsIgnoreCase("Pulsa") || pageId.equalsIgnoreCase("Paket Data")) {
            view = inflater.inflate(R.layout.activity_pembelian_pulsa, container, false);

            tvSpinner1 = (TextView) view.findViewById(R.id.tv_spinner1);
            tvSpinner2 = (TextView) view.findViewById(R.id.tv_spinner2);

            if (pageId.equalsIgnoreCase("Pulsa")) {
                tvSpinner1.setText("Jenis Pulsa");
            } else if (pageId.equalsIgnoreCase("Paket Data")) {
                tvSpinner1.setText("Jenis Paket Data");
            }

            tvSpinner2.setText("Nominal Pulsa");

            spinner1 = (Spinner) view.findViewById(R.id.spinner1);
            spinner2 = (Spinner) view.findViewById(R.id.spinner2);

            setSpinnerProduct(pageId);

            et1 = (EditText) view.findViewById(R.id.et_id_pelanggan);
            til1 = (TextInputLayout) view.findViewById(R.id.til_id_pelanggan);

        } else if (pageId.equalsIgnoreCase("Top Up GO-JEK")) {
            view = inflater.inflate(R.layout.activity_pembelian_gopay, container, false);

            tvSpinner1 = (TextView) view.findViewById(R.id.tv_spinner1);
            tvSpinner1.setText("Jenis GoPay");

            spinner1 = (Spinner) view.findViewById(R.id.spinner1);

            setSpinnerProduct(pageId);

            et1 = (EditText) view.findViewById(R.id.et_id_pelanggan);
            til1 = (TextInputLayout) view.findViewById(R.id.til_id_pelanggan);

            et2 = (EditText) view.findViewById(R.id.et_nominal);
            til2 = (TextInputLayout) view.findViewById(R.id.til_nominal);

            et2.setFocusable(false);

        }

        tilNorek = (TextInputLayout) view.findViewById(R.id.til_nomor_bsa);
        tilNorek.setVisibility(View.GONE);

        btn1 = (Button) view.findViewById(R.id.btn_submit);
        btnFav = (ImageButton) view.findViewById(R.id.btn_daftar_fav);

        et1.setFocusable(false);

        AppUtil.toolbarRegular(getContext(), view, pageId);
        realm = Realm.getDefaultInstance();

        try {
            onActionButton();
        } catch (Exception e) {
            e.printStackTrace();
            AppUtil.displayDialog(getContext(), "Maaf, Terjadi Kesalahan");
        }

        return view;
    }

    private void setSpinnerProduct(String title) {
        if (title.equalsIgnoreCase("Token Listrik")) {
            adapterProduct = new ArrayAdapter<>(getContext(), R.layout.spinner_style, getResources().getStringArray(R.array.amount_pembelian_token_listrik));
            adapterProduct.setDropDownViewResource(android.R.layout.simple_list_item_1);
            spinner1.setAdapter(new NothingSelectedSpinnerAdapter(
                    adapterProduct,
                    R.layout.spinner_nominal_nothing_selected,
                    getContext()
            ));
        } else if (title.equalsIgnoreCase("Pulsa")) {
            adapterProduct = new ArrayAdapter<>(getContext(), R.layout.spinner_style,
                    getResources().getStringArray(R.array.product_pembelian_pulsa));
            adapterProduct.setDropDownViewResource(android.R.layout.simple_list_item_1);
            spinner1.setAdapter(new NothingSelectedSpinnerAdapter(
                    adapterProduct,
                    R.layout.spinner_operator_nothing_selected,
                    getContext()
            ));
            spinner2.setAdapter(new NothingSelectedSpinnerAdapter(
                    adapterProduct,
                    R.layout.spinner_nominal_nothing_selected,
                    getContext()
            ));
        } else if (title.equalsIgnoreCase("Pulsa Telkomsel")) {
            adapterProduct = new ArrayAdapter<>(getContext(), R.layout.spinner_style,
                    getResources().getStringArray(R.array.amount_pembelian_pulsa_telkomsel));
            adapterProduct.setDropDownViewResource(android.R.layout.simple_list_item_1);
            spinner2.setAdapter(new NothingSelectedSpinnerAdapter(
                    adapterProduct,
                    R.layout.spinner_nominal_nothing_selected,
                    getContext()
            ));
        } else if (title.equalsIgnoreCase("Pulsa XL")) {
            adapterProduct = new ArrayAdapter<>(getContext(), R.layout.spinner_style,
                    getResources().getStringArray(R.array.amount_pembelian_pulsa_xl));
            adapterProduct.setDropDownViewResource(android.R.layout.simple_list_item_1);
            spinner2.setAdapter(new NothingSelectedSpinnerAdapter(
                    adapterProduct,
                    R.layout.spinner_nominal_nothing_selected,
                    getContext()
            ));
        } else if (title.equalsIgnoreCase("Pulsa Smartfren")) {
            adapterProduct = new ArrayAdapter<>(getContext(), R.layout.spinner_style,
                    getResources().getStringArray(R.array.amount_pembelian_pulsa_smartfren));
            adapterProduct.setDropDownViewResource(android.R.layout.simple_list_item_1);
            spinner2.setAdapter(new NothingSelectedSpinnerAdapter(
                    adapterProduct,
                    R.layout.spinner_nominal_nothing_selected,
                    getContext()
            ));
        } else if (title.equalsIgnoreCase("Pulsa Indosat Ooredoo")) {
            adapterProduct = new ArrayAdapter<>(getContext(), R.layout.spinner_style,
                    getResources().getStringArray(R.array.amount_pembelian_pulsa_indosat));
            adapterProduct.setDropDownViewResource(android.R.layout.simple_list_item_1);
            spinner2.setAdapter(new NothingSelectedSpinnerAdapter(
                    adapterProduct,
                    R.layout.spinner_nominal_nothing_selected,
                    getContext()
            ));
        } else if (title.equalsIgnoreCase("Paket Data")) {
            adapterProduct = new ArrayAdapter<>(getContext(), R.layout.spinner_style,
                    getResources().getStringArray(R.array.product_pembelian_paket_internet));
            adapterProduct.setDropDownViewResource(android.R.layout.simple_list_item_1);
            spinner1.setAdapter(new NothingSelectedSpinnerAdapter(
                    adapterProduct,
                    R.layout.spinner_product_nothing_selected,
                    getContext()
            ));
            spinner2.setAdapter(new NothingSelectedSpinnerAdapter(
                    adapterProduct,
                    R.layout.spinner_nominal_nothing_selected,
                    getContext()
            ));
        } else if (title.equalsIgnoreCase("Paket Telkomsel")) {
            adapterProduct = new ArrayAdapter<>(getContext(), R.layout.spinner_style,
                    getResources().getStringArray(R.array.amount_pembelian_paketdata_telkomsel));
            adapterProduct.setDropDownViewResource(android.R.layout.simple_list_item_1);
            spinner2.setAdapter(new NothingSelectedSpinnerAdapter(
                    adapterProduct,
                    R.layout.spinner_nominal_nothing_selected,
                    getContext()
            ));
        } else if (title.equalsIgnoreCase("Paket Indosat")) {
            adapterProduct = new ArrayAdapter<>(getContext(), R.layout.spinner_style,
                    getResources().getStringArray(R.array.amount_pembelian_paketdata_indosat));
            adapterProduct.setDropDownViewResource(android.R.layout.simple_list_item_1);
            spinner2.setAdapter(new NothingSelectedSpinnerAdapter(
                    adapterProduct,
                    R.layout.spinner_nominal_nothing_selected,
                    getContext()
            ));
        } else if (title.equalsIgnoreCase("Top Up GO-JEK")) {
            adapterProduct = new ArrayAdapter<>(getContext(), R.layout.spinner_style,
                    getResources().getStringArray(R.array.product_pembelian_gopay));
            adapterProduct.setDropDownViewResource(android.R.layout.simple_list_item_1);
            spinner1.setAdapter(new NothingSelectedSpinnerAdapter(
                    adapterProduct,
                    R.layout.spinner_product_nothing_selected,
                    getContext()
            ));
        }
        spinner1.setOnItemSelectedListener(new onItemListener());
    }

    private class onItemListener implements AdapterView.OnItemSelectedListener {
        @Override
        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
            if (spinner1.getSelectedItem() != null) {
                if (pageId.equalsIgnoreCase("Token Listrik")) {
                    til1.setHint("Nomor Meter/Nomor Pelanggan");
                } else if (pageId.equalsIgnoreCase("Top Up GO-JEK")) {
                    til1.setHint("Nomor Handphone");
                    til2.setHint("Nominal");

                    et2.setOnTouchListener(new View.OnTouchListener() {
                        @Override
                        public boolean onTouch(View v, MotionEvent event) {

                            et2.setFocusableInTouchMode(true);

                            return false;
                        }
                    });
                    et2.addTextChangedListener(new NumberTextWatcherForThousand(et2));

                } else {
                    til1.setHint("Nomor Handphone");
                }

                et1.setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {

                        et1.setFocusableInTouchMode(true);

                        return false;
                    }
                });

                if (pageId.equalsIgnoreCase("Pulsa") || pageId.equalsIgnoreCase("Paket Data")) {
                    spinner2.setEnabled(true);
                    setSpinnerProduct(spinner1.getSelectedItem().toString());
                }
            } else {
                if (pageId.equalsIgnoreCase("Pulsa") || pageId.equalsIgnoreCase("Paket Data")) {
                    spinner2.setEnabled(false);
                }
                til1.setHint(tvSpinner1.getText().toString() + " belum dipilih");
                if (pageId.equalsIgnoreCase("Top Up GO-JEK")) {
                    til2.setHint(tvSpinner1.getText().toString() + " belum dipilih");
                }
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> adapterView) {
        }
    }

    private void onActionButton() {
        btnFav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    if (!pageId.equalsIgnoreCase("Token Listrik")) {
                        if (spinner1.getSelectedItem() == null) {
                            AppUtil.displayDialog(getContext(), "Pilih Produk dahulu untuk mendapatkan/simpan data favorit Anda");
                            return;
                        }
                        product = spinner1.getSelectedItem().toString();
                    } else {
                        product = pageId;
                    }

                    if (pageId.equalsIgnoreCase("Token Listrik")) {
                        data1 = product;
                        data4 = product;
                        submenutrx = "6001";
                        namajenisfav = "Pembelian Token Listrik PLN";
                    } else if (pageId.equalsIgnoreCase("Pulsa")) {
                        if (product.equalsIgnoreCase(listPulsa[0])) {
                            data1 = "SIMPATI";
                        } else if (product.equalsIgnoreCase(listPulsa[1])) {
                            data1 = "XL";
                        } else if (product.equalsIgnoreCase(listPulsa[2])) {
                            data1 = "SMARTFREN";
                        } else if (product.equalsIgnoreCase(listPulsa[3])) {
                            data1 = "INDOSAT";
                        }
                        data4 = product;
                        submenutrx = "6002";
                        namajenisfav = "Pembelian Pulsa";
                    } else if (pageId.equalsIgnoreCase("Paket Data")) {
                        if (product.equalsIgnoreCase(listPaketData[0])) {
                            data1 = "TSELDATA";
                        } else if (product.equalsIgnoreCase(listPaketData[1])) {
                            data1 = "INDOSATDATA";
                        }
                        data4 = product;
                        submenutrx = "6003";
                        namajenisfav = "Pembelian Paket Data";
                    } else if (pageId.equalsIgnoreCase("Top Up GO-JEK")) {
                        if (product.equalsIgnoreCase(listGopay[0])) {
                            data1 = "GOPAY";
                        } else if (product.equalsIgnoreCase(listGopay[1])) {
                            data1 = "GOPAYDRIVER";
                        }
                        data4 = product;
                        submenutrx = "6004";
                        namajenisfav = "Pembelian Saldo Gopay";
                    }

                    final String id_product = et1.getText().toString().trim();

                    if (id_product.equalsIgnoreCase("")) {
                        getFavData(data4);
                        AppUtil.displayDialogFavorit(getContext(), favDatas, FPembelianTunai.this, "BAYAR");
                        return;
                    }

                    AlertDialog dialogAlert;
                    AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                    builder.setCancelable(true);
                    builder.setMessage("Simpan data transaksi ini sebagai data favorit?");
                    builder.setPositiveButton("Simpan", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface alertdialog, int which) {
                            alertdialog.cancel();

                            if (AppUtil.checkFavorite("CASH", id_product, data1, AppUtil.getDsn())) {
                                android.support.v4.app.FragmentManager manager = getActivity().getSupportFragmentManager();
                                Fragment fragment;
                                FragmentTransaction transaction = manager.beginTransaction();
                                fragment = new FInsertUpdateDataFavorit();
                                Bundle bundle = new Bundle();
                                bundle.putString("title", product);
                                bundle.putString("jenispembayaran", "CASH");
                                bundle.putString("menutrx", "6000");
                                bundle.putString("data1", data1);
                                bundle.putString("data3", id_product);
                                bundle.putString("data4", data4);
                                bundle.putString("submenutrx", submenutrx);
                                bundle.putString("namajenisfav", namajenisfav);

                                fragment.setArguments(bundle);
                                transaction.replace(R.id.fragment_payment_general, fragment);
                                transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
                                transaction.addToBackStack(null);
                                transaction.commit();

                            } else {
                                AppUtil.displayDialog(getContext(), "Data favorit Telah Ada");
                            }
                        }
                    });
                    builder.setNegativeButton("Pilih favorit", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface alertdialog, int which) {
                            alertdialog.cancel();
                            getFavData(data4);
                            AppUtil.displayDialogFavorit(getContext(), favDatas, FPembelianTunai.this, "BAYAR");
                        }
                    });

                    dialogAlert = builder.create();
                    dialogAlert.show();
                } catch (Exception e) {
                    e.printStackTrace();
                    AppUtil.displayDialog(getContext(), "Maaf, Terjadi Kesalahan");
                }
            }
        });

        et1.addTextChangedListener(this);

        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (!(pageId.equalsIgnoreCase("Token Listrik"))) {

                    if (spinner1.getSelectedItem() == null) {
                        AppUtil.displayDialog(getContext(), tvSpinner1.getText().toString() + " Tidak Boleh Kosong!");
                        return;
                    } else {
                        product = spinner1.getSelectedItem().toString();
                    }

                    if (pageId.equalsIgnoreCase("Pulsa") || pageId.equalsIgnoreCase("Paket Data")) {

                        if (spinner2.getSelectedItem() == null) {
                            AppUtil.displayDialog(getContext(), "Nominal Tidak Boleh Kosong!");
                            return;
                        } else {
                            if (pageId.equalsIgnoreCase("Paket Data")) {
                                if (product.equalsIgnoreCase(listPaketData[1])) {
                                    String paket = spinner2.getSelectedItem().toString();
                                    String nominal_paket = paket.substring(paket.indexOf("(") + 1, paket.indexOf(")"));
                                    nominal = NumberTextWatcherForThousand.trimCommaOfString(nominal_paket);

                                    if (paket.contains("Freedom")) {
                                        paket_type = "freedom";
                                    }
                                    else if (paket.contains("Extra Kuota")) {
                                        paket_type = "extra";
                                    }
                                    else if (paket.contains("Paket Internet")) {
                                        paket_type = "unlimited";
                                    }
                                } else {
                                    nominal = NumberTextWatcherForThousand.trimCommaOfString(spinner2.getSelectedItem().toString());
                                }
                            } else {
                                nominal = NumberTextWatcherForThousand.trimCommaOfString(spinner2.getSelectedItem().toString());
                            }
                        }
                    }
                } else {
                    if (spinner1.getSelectedItem() == null) {
                        AppUtil.displayDialog(getContext(), "Nominal Tidak Boleh Kosong!");
                        return;
                    } else {
                        nominal = NumberTextWatcherForThousand.trimCommaOfString(spinner1.getSelectedItem().toString());
                    }
                }

                if (et1.getText().toString().length() == 0) {
                    til1.setError(til1.getHint() + " Tidak Boleh Kosong!");
                    return;
                } else {
                    id_pelanggan = et1.getText().toString().trim();
                    Log.d("Cek id_pelanggan", String.valueOf(id_pelanggan.length()));

                    if (pageId.equalsIgnoreCase("Token Listrik")) {
                        if (id_pelanggan.length() != 11) {
                            jenisid = "Nomor Pelanggan";
                            idtype = "1";
                            Log.d("Test", "!=11");
                        } else {
                            jenisid = "Nomor Meter";
                            idtype = "0";
                            Log.d("Test", "=11");
                        }
                    }
                }

                if (pageId.equalsIgnoreCase("Top Up GO-JEK")) {
                    if (et2.getText().toString().length() == 0) {
                        til2.setError("Nominal Tidak Boleh Kosong!");
                        return;
                    } else {
                        nominal_gopay = NumberTextWatcherForThousand.trimCommaOfString(et2.getText().toString());
                    }
                }

                SoapActivity action = new SoapActivity();
                action.execute();

            }
        });

    }

    @Override
    public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
            AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
            builder.setCancelable(false);
            builder.setMessage("Apakah anda yakin akan membatalkan proses Pembelian " + pageId + "?");
            builder.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                    confirmationDialog.dismiss();
                }
            });
            builder.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                    confirmationDialog.show();
                }
            });
            if (AlertDialog == null || !AlertDialog.isShowing()) {
                AlertDialog = builder.create();
                AlertDialog.show();
            }
            return true;
        }
        return false;
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        if (et1.getText().toString().length() > 0) {
            til1.setError(null);
        }
    }

    @Override
    public void afterTextChanged(Editable s) {

    }

    @Override
    public void getFav(String label, String category) {

    }

    @Override
    public void onclikFav(String label, String kode_produk) {
        RealmResults<Favorite> fav = realm.where(Favorite.class)
                .equalTo("menutrx", "6000")
                .equalTo("jenispembayaran", "CASH")
                .equalTo("namafav", label)
                .equalTo("data1", kode_produk)
                .findAll();

        if (fav.size() > 0) {
            et1.setText(fav.get(0).getData3());
            AppUtil.closeListDialog();
        }
    }

    @Override
    public void deleteFav(String jenispembayaran, String menutrx, String submenutrx, String label) {

    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {

    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        return false;
    }

    @Override
    public void onConfirmClick(String pin) {

    }

    @Override
    public void onCancelClick() {

    }

    public void getFavData(String kode_produk) {
        RealmResults<Favorite> fav = realm.where(Favorite.class)
                .equalTo("menutrx", "6000")
                .equalTo("jenispembayaran", "CASH")
                .equalTo("data4", kode_produk)
                .findAll();

        Log.d("Cek fav", fav.toString());

        if (fav.size() > 0) {
            favDatas.clear();
            favDatas.addAll(fav);
        } else {
            favDatas.clear();
        }
    }

    private class SoapActivity extends AsyncTask<Void, Void, Void> {


        @Override
        protected void onPreExecute() {

        }

        @Override
        protected Void doInBackground(Void... params) {
            convertir();
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            if (msgErr.equalsIgnoreCase("true")) {
                AppUtil.displayDialog(getContext(), message);
            }
        }

    }

    private void convertir() {

        try {
            String cmd;
            if (pageId.equalsIgnoreCase("Pulsa")) {
                if (product.equalsIgnoreCase("Pulsa Telkomsel")) {
                    jenis_pembelian = "SIMPATI";
                } else if (product.equalsIgnoreCase("Pulsa XL")) {
                    jenis_pembelian = "XL";
                } else if (product.equalsIgnoreCase("Pulsa Esia")) {
                    jenis_pembelian = "ESIA";
                } else if (product.equalsIgnoreCase("Pulsa Smartfren")) {
                    jenis_pembelian = "SMARTFREN";
                } else if (product.equalsIgnoreCase("Pulsa Indosat Ooredoo")) {
                    jenis_pembelian = "INDOSAT";
                }
            } else if (pageId.equalsIgnoreCase("Paket Data")) {
                if (product.equalsIgnoreCase("Paket Telkomsel")) {
                    jenis_pembelian = "TSELDATA";
                } else if (product.equalsIgnoreCase("Paket Indosat")) {
                    jenis_pembelian = "INDOSATDATA";
                }
            } else if (pageId.equalsIgnoreCase("Top Up GO-JEK")) {
                if (product.equalsIgnoreCase("GOPAY")) {
                    jenis_pembelian = "GOPAY";
                } else if (product.equalsIgnoreCase("GOPAY DRIVER")) {
                    jenis_pembelian = "GOPAYDRIVER";
                }
            }

            getActivity().runOnUiThread(new Runnable() {
                public void run() {
                    AppUtil.initDialogProgress(getContext());
                    AppUtil.showDialog();
                }
            });

            String dsn = AppUtil.getDsn();
            String phone_id = "62" + dsn.substring(1, dsn.length());

            JSONObject cmdparam = new JSONObject();
            if (pageId.equalsIgnoreCase("Token Listrik")) {
                cmd = "prepaid_verify";
                cmdparam.put("id", id_pelanggan);
                cmdparam.put("idtype", idtype);
                cmdparam.put("merchantid", "6010");
                cmdparam.put("branch_id", "0001");
            } else {
                cmd = "beli_verify";
                if (pageId.equalsIgnoreCase("Top Up GO-JEK")) {
                    cmdparam.put("message", AppUtil.API_MSG_PURCHASE + " " + jenis_pembelian + " " + id_pelanggan + " " + nominal_gopay);
                } else if (product.equalsIgnoreCase("Paket Indosat")) {
                    cmdparam.put("message", AppUtil.API_MSG_PURCHASE + " " + jenis_pembelian + " " + id_pelanggan + " " + nominal + " " + paket_type);
                } else {
                    cmdparam.put("message", AppUtil.API_MSG_PURCHASE + " " + jenis_pembelian + " " + id_pelanggan + " " + nominal);
                }
                cmdparam.put("msisdn", phone_id);
            }

            String soapRes = AppUtil.soapReq(AppUtil.getDsn(), cmd, cmdparam);

            if (soapRes.equalsIgnoreCase("")) {
                AppUtil.hideDialog();
                getActivity().runOnUiThread(new Runnable() {
                    public void run() {
                        AppUtil.displayDialog(getContext(), "Gagal terhubung ke server");
                    }
                });
            } else {
                json = new JSONObject(soapRes);
                resdata = json.getJSONObject("resdata");
                rcode = resdata.getString("rcode");
                msgErr = "false";

                if (!(pageId.equalsIgnoreCase("Token Listrik"))) {
                    message = resdata.getString("message");
                } else {
                    params = resdata.getJSONObject("params");
                    id = params.getString("id");
                    idtype = params.getString("idtype");
                    merchantid = params.getString("merchantid");
                    branch_id = params.getString("branch_id");
                    bit11Biller = params.getString("bit11Biller");
                    bit42Biller = params.getString("bit42Biller");
                    bit60Biller = params.getString("bit60Biller");
                    bit61Biller = params.getString("bit61Biller");
                    meter_id = params.getString("meter_id");
                    subscr_id = params.getString("subscr_id");
                    swref = params.getString("swref");
                    owner = params.getString("owner");
                    tariff = params.getString("tariff");
                    power = params.getString("power");
                    bit62Biller = params.getString("bit62Biller");
                    nut = params.getString("nut");
                    lon = params.getJSONObject("lon");
                    bit127Biller = params.getString("bit127Biller");
                    adminfee = params.getString("adminfee");
                    purchasetype = params.getString("purchasetype");

                    int nom = Integer.parseInt(nominal);
                    final String jml = NumberFormat.getNumberInstance(Locale.US).format(nom);

                    message = "Anda akan membeli Token Listrik, " + jenisid + " " + id + ", " + owner + ", " + tariff + "/ " + power + "VA, Rp. " + jml  + ". Jika benar masukkan PIN Anda";
                }

                if (rcode.equalsIgnoreCase("00")) {
                    msgErr = "false";
                    new Thread() {
                        public void run() {
                            FPembelianTunai.this.getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    AppUtil.hideDialog();
                                    try {
                                        LayoutInflater li = LayoutInflater.from(getContext());
                                        final View confirmDialog = li.inflate(R.layout.dialog_confirm_agen, null);
                                        btnConfirm = (Button) confirmDialog.findViewById(R.id.btn_submit);
                                        final TextView confirm = (TextView) confirmDialog.findViewById(R.id.txt_title_top);
                                        final EditText pin = (EditText) confirmDialog.findViewById(R.id.txt_pin);

                                        if (!(pageId.equalsIgnoreCase("Token Listrik"))) {
                                            String dialog = message.toString();
                                            confirm.setText(dialog.substring(0, dialog.indexOf("benar")) + " benar masukkan PIN Anda");
                                        } else {
                                            confirm.setText(message);
                                        }
                                        pin.addTextChangedListener(new TextWatcher() {
                                            @Override
                                            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                                            }

                                            @Override
                                            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                                                if (charSequence.toString().trim().length() == 6) {
                                                    btnConfirm.setEnabled(true);
                                                    btnConfirm.setBackgroundResource(R.drawable.button_shape_blue);
                                                } else {
                                                    btnConfirm.setEnabled(false);
                                                    btnConfirm.setBackgroundResource(R.drawable.button_shape_accent);
                                                }
                                            }

                                            @Override
                                            public void afterTextChanged(Editable editable) {
                                            }
                                        });

                                        AlertDialog.Builder alert = new AlertDialog.Builder(getContext());
                                        alert.setView(confirmDialog);
                                        alert.setCancelable(false);
                                        confirmationDialog = alert.create();
                                        confirmationDialog.setOnKeyListener(FPembelianTunai.this);
                                        confirmationDialog.show();

                                        btnConfirm.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View view) {

                                                AppUtil.initDialogProgress(getContext());
                                                AppUtil.showDialog();

                                                Thread thread = new Thread(new Runnable() {

                                                    @Override
                                                    public void run() {

                                                        try {
                                                            String cmd;
                                                            String dsn = AppUtil.getDsn();
                                                            String phone_id = "62" + dsn.substring(1, dsn.length());

                                                            JSONObject cmdparam = new JSONObject();

                                                            if (pageId.equalsIgnoreCase("Token Listrik")) {
                                                                kode_transaksi = "6001";
                                                            } else if (pageId.equalsIgnoreCase("Pulsa")) {
                                                                kode_transaksi = "6002";
                                                            } else if (pageId.equalsIgnoreCase("Paket Data")) {
                                                                kode_transaksi = "6003";
                                                            } else if (pageId.equalsIgnoreCase("Top Up GO-JEK")) {
                                                                kode_transaksi = "6004";
                                                            }

                                                            if (pageId.equalsIgnoreCase("Token Listrik")) {
                                                                JSONObject params = new JSONObject();

                                                                params.put("id", id);
                                                                params.put("idtype", idtype);
                                                                params.put("merchantid", merchantid);
                                                                params.put("branch_id", branch_id);
                                                                params.put("bit11Biller", bit11Biller);
                                                                params.put("bit42Biller", bit42Biller);
                                                                params.put("bit60Biller", bit60Biller);
                                                                params.put("bit61Biller", bit61Biller);
                                                                params.put("meter_id", meter_id);
                                                                params.put("subscr_id", subscr_id);
                                                                params.put("swref", swref);
                                                                params.put("owner", owner);
                                                                params.put("tariff", tariff);
                                                                params.put("power", power);
                                                                params.put("bit62Biller", bit62Biller);
                                                                params.put("nut", nut);
                                                                params.put("lon", lon);
                                                                params.put("bit127Biller", bit127Biller);
                                                                params.put("nominal", nominal);
                                                                params.put("adminfee", adminfee);
                                                                params.put("purchasetype", purchasetype);
                                                                params.put("src_accno", AppUtil.getAcc());

                                                                cmd = "prepaid_save";
                                                                cmdparam.put("msgrequest", params);
                                                                cmdparam.put("jenistransaksi", kode_transaksi);
                                                            } else {
                                                                cmd = "beli_save";
                                                                cmdparam.put("message", "PIN " + pin.getText().toString());
                                                                cmdparam.put("msisdn", phone_id);
                                                                cmdparam.put("jenistransaksi", kode_transaksi);
                                                                cmdparam.put("idpelanggan", id_pelanggan);
                                                            }

                                                            String soapRes = AppUtil.soapReq(AppUtil.getDsn(), cmd, cmdparam);

                                                            if (soapRes.equalsIgnoreCase("")) {
                                                                AppUtil.hideDialog();
                                                                getActivity().runOnUiThread(new Runnable() {
                                                                    public void run() {
                                                                        AppUtil.displayDialog(getContext(), "Gagal terhubung ke server");
                                                                    }
                                                                });
                                                            } else {
                                                                json = new JSONObject(soapRes);
                                                                resdata = json.getJSONObject("resdata");
                                                                rcode = resdata.getString("rcode");

                                                                if (pageId.equalsIgnoreCase("Token Listrik")) {
                                                                    int nom = Integer.parseInt(nominal);
                                                                    final String jml = NumberFormat.getNumberInstance(Locale.US).format(nom);
                                                                    int fee = Integer.parseInt(adminfee);
                                                                    final String admfee = NumberFormat.getNumberInstance(Locale.US).format(fee);

                                                                    Calendar calendar = Calendar.getInstance();
                                                                    SimpleDateFormat mdformat = new SimpleDateFormat("dd/MM/yy");
                                                                    SimpleDateFormat mtformat = new SimpleDateFormat("HH:mm");

                                                                    final String kwh, token;

                                                                    params = resdata.getJSONObject("params");
                                                                    kwh = params.getString("kwh");
                                                                    token = params.getString("token");

                                                                    String bit1 = token.substring(0, 4);
                                                                    String parsbit1 = token.substring(4, token.length());
                                                                    String bit2 = parsbit1.substring(0, 4);
                                                                    String parsbit2 = parsbit1.substring(4, parsbit1.length());
                                                                    String bit3 = parsbit2.substring(0, 4);
                                                                    String parsbit3 = parsbit2.substring(4, parsbit2.length());
                                                                    String bit4 = parsbit3.substring(0, 4);
                                                                    String parsbit4 = parsbit3.substring(4, parsbit3.length());
                                                                    String bit5 = parsbit4.substring(0, 4);

                                                                    String bittoken = bit1 + " " + bit2 + " " + bit3 + " " + bit4 + " " + bit5;

                                                                    Log.d("Cek bit", bit1 + bit2 + bit3 + bit4 + bit5);

                                                                    message = mdformat.format(calendar.getTime()) + " " + mtformat.format(calendar.getTime()) + "\n" + swref + "\n" + owner +
                                                                            "\n" + meter_id + "\n" + subscr_id + "\n" + tariff + " / " + power + "VA \n" + ", Rp. " + jml + "\n" + "kWh " + kwh +
                                                                            "\n" + "Stroom/Token " + bittoken + "\n" + "Adm Rp. " + admfee + "\n" + "Sukses";
                                                                } else {
                                                                    message = resdata.getString("message");
                                                                }

                                                                if (rcode.equalsIgnoreCase("00")) {

                                                                    //khusus edc brissmart
                                                                    String date = AppUtil.getDateFormat();
//
                                                                    Bundle datas = new Bundle();
                                                                    datas.putString("menu_id", pageId);
                                                                    datas.putString("tid", tid);
                                                                    datas.putString("mid", "4228267000001");
                                                                    datas.putString("type_card", "-");
                                                                    datas.putString("date", date);
                                                                    datas.putString("msg", message);

                                                                    getActivity().runOnUiThread(new Runnable() {
                                                                        public void run() {

                                                                            AppUtilPrint.printStruk(getContext(), datas);

                                                                            AppUtil.hideDialog();
                                                                            et1.getText().clear();
                                                                            AppUtil.displayDialog(getContext(), message);
                                                                            AppUtil.saveIntoInbox(message, pageId, AppUtil.getDsn());
                                                                            confirmationDialog.dismiss();
                                                                        }
                                                                    });
                                                                } else {
                                                                    if (pageId.equalsIgnoreCase("Token Listrik")) {
                                                                        if (rcode.equalsIgnoreCase("13") || rcode.equalsIgnoreCase("55")) {
                                                                            message = "Kode PIN Salah atau Tidak Aktif";
                                                                            getActivity().runOnUiThread(new Runnable() {
                                                                                public void run() {
                                                                                    AppUtil.hideDialog();
                                                                                    AppUtil.displayDialog(getContext(), message);
                                                                                    pin.getText().clear();
                                                                                }
                                                                            });
                                                                        } else {
                                                                            if (rcode.equalsIgnoreCase("12")) {
                                                                                message = "Invalid Transaction";
                                                                            } else if (rcode.equalsIgnoreCase("03")) {
                                                                                message = "Transaksi E-Channel Tidak Terdaftar";
                                                                            } else if (rcode.equalsIgnoreCase("05")) {
                                                                                message = "Transaksi Core Banking gagal";
                                                                            } else if (rcode.equalsIgnoreCase("63")) {
                                                                                message = "Nilai Rupiah yang Anda Masukkan Salah";
                                                                            } else if (rcode.equalsIgnoreCase("14")) {
                                                                                message = jenisid + " yang Anda Masukkan Salah, Mohon Teliti Kembali";
                                                                            } else if (rcode.equalsIgnoreCase("17")) {
                                                                                message = "Nominal Pembelian Tidak Terdaftar";
                                                                            } else if (rcode.equalsIgnoreCase("21")) {
                                                                                message = "Tagihan Bulan Berjalan Belum Tersedia";
                                                                            } else if (rcode.equalsIgnoreCase("30")) {
                                                                                message = "Message Format Error";
                                                                            } else if (rcode.equalsIgnoreCase("43")) {
                                                                                message = "Pick Up, Stolen Card";
                                                                            } else if (rcode.equalsIgnoreCase("47")) {
                                                                                message = "Total kWh Melebihi Batas Maksimum";
                                                                            } else if (rcode.equalsIgnoreCase("51")) {
                                                                                message = "Saldo Tidak Mencukupi";
                                                                            } else if (rcode.equalsIgnoreCase("52")) {
                                                                                message = "No Checking Account";
                                                                            } else if (rcode.equalsIgnoreCase("53")) {
                                                                                message = "No Savings Account";
                                                                            } else if (rcode.equalsIgnoreCase("54")) {
                                                                                message = "Expired Card / Target";
                                                                            } else if (rcode.equalsIgnoreCase("57")) {
                                                                                message = "Transaction Not Permitted To Cardholder";
                                                                            } else if (rcode.equalsIgnoreCase("75")) {
                                                                                message = "Pembelian Minimal RP. 20 Ribu";
                                                                            } else if (rcode.equalsIgnoreCase("76")) {
                                                                                message = "Invalid To Account";
                                                                            } else if (rcode.equalsIgnoreCase("77")) {
                                                                                message = "Invalid From account";
                                                                            } else if (rcode.equalsIgnoreCase("82")) {
                                                                                message = "Time Out at Issuer System";
                                                                            } else if (rcode.equalsIgnoreCase("88")) {
                                                                                message = "Tagihan Sudah Terbayar";
                                                                            } else if (rcode.equalsIgnoreCase("91")) {
                                                                                message = "Issuer or Switch is Inoperative";
                                                                            } else if (rcode.equalsIgnoreCase("94")) {
                                                                                message = "Duplicate Transmission/Request/Reversal Message";
                                                                            } else if (rcode.equalsIgnoreCase("96")) {
                                                                                message = "System Malfunction / System Error";
                                                                            } else {
                                                                                message = "Transaksi Gagal";
                                                                            }

                                                                            getActivity().runOnUiThread(new Runnable() {
                                                                                public void run() {
                                                                                    AppUtil.hideDialog();
                                                                                    AppUtil.displayDialog(getContext(), message);
                                                                                    confirmationDialog.dismiss();
                                                                                }
                                                                            });
                                                                        }
                                                                    } else {
                                                                        if (rcode.equalsIgnoreCase("98")) {
                                                                            getActivity().runOnUiThread(new Runnable() {
                                                                                public void run() {
                                                                                    AppUtil.hideDialog();
                                                                                    AppUtil.displayDialog(getContext(), message);
                                                                                    pin.getText().clear();

                                                                                }
                                                                            });
                                                                        } else {
                                                                            getActivity().runOnUiThread(new Runnable() {
                                                                                public void run() {
                                                                                    AppUtil.hideDialog();
                                                                                    AppUtil.displayDialog(getContext(), message);
                                                                                    confirmationDialog.dismiss();
                                                                                }
                                                                            });
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        } catch (Exception ex) {
                                                            msgErr = "true";
                                                            AppUtil.hideDialog();
                                                            Log.d("THREAD", ex.toString());
                                                            message = "Transaksi tidak dapat diproses";
                                                        }
                                                    }
                                                });
                                                thread.start();
                                            }
                                        });

                                    } catch (Exception e) {
                                        AppUtil.hideDialog();
                                        msgErr = "true";
                                        message = "Transaksi tidak dapat diproses";
                                        Log.d("THREAD 1", e.toString());
                                    }
                                }
                            });
                        }
                    }.start();
                } else {
                    if (pageId.equalsIgnoreCase("Token Listrik")) {
                        if (rcode.equalsIgnoreCase("12")) {
                            message = "Invalid Transaction";
                        } else if (rcode.equalsIgnoreCase("03")) {
                            message = "Transaksi E-Channel Tidak Terdaftar";
                        } else if (rcode.equalsIgnoreCase("05")) {
                            message = "Transaksi Core Banking gagal";
                        } else if (rcode.equalsIgnoreCase("09") || rcode.equalsIgnoreCase("14") || rcode.equalsIgnoreCase("77")) {
                            message = jenisid + " yang Anda Masukkan Salah, Mohon Teliti Kembali";
                        } else if (rcode.equalsIgnoreCase("13")) {
                            message = "Kode PIN Salah atau Tidak Aktif";
                        } else if (rcode.equalsIgnoreCase("63")) {
                            message = "Nilai Rupiah yang Anda Masukkan Salah";
                        } else if (rcode.equalsIgnoreCase("16")) {
                            message = "Konsumen " + jenisid + " " + id_pelanggan + " diblokir, Hubungi PLN";
                        } else if (rcode.equalsIgnoreCase("21")) {
                            message = "Tagihan Bulan Berjalan Belum Tersedia";
                        } else if (rcode.equalsIgnoreCase("51")) {
                            message = "Saldo Tidak Mencukupi";
                        } else if (rcode.equalsIgnoreCase("75")) {
                            message = "Pembelian Minimal RP. 20 Ribu";
                        } else if (rcode.equalsIgnoreCase("88")) {
                            message = "Tagihan Sudah Terbayar";
                        } else if (rcode.equalsIgnoreCase("27")) {
                            message = "Akun Nasabah Tabungan Cerdas Tidak Aktif";
                        } else if (rcode.equalsIgnoreCase("25")) {
                            message = "Akun Tidak Aktif";
                        } else {
                            message = "Transaksi Gagal";
                        }
                    }

                    getActivity().runOnUiThread(new Runnable() {
                        public void run() {
                            AppUtil.hideDialog();
                            AppUtil.displayDialog(getContext(), message);
                        }
                    });
                }
            }
        } catch (Exception ex) {
            msgErr = "true";
            AppUtil.hideDialog();
            Log.d("THREAD 2", ex.toString());
            message = "Transaksi tidak dapat diproses";
        }
    }
}

