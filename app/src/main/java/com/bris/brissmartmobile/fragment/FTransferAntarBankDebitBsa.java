package com.bris.brissmartmobile.fragment;

import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.bris.brissmartmobile.R;
import com.bris.brissmartmobile.listener.FavClickListener;
import com.bris.brissmartmobile.model.Favorite;
import com.bris.brissmartmobile.util.AppPreferences;
import com.bris.brissmartmobile.util.AppUtil;
import com.bris.brissmartmobile.util.AppUtilPrint;
import com.bris.brissmartmobile.util.Menu;
import com.bris.brissmartmobile.util.NumberTextWatcherForThousand;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by valeputra on 6/22/17.
 */

public class FTransferAntarBankDebitBsa extends Fragment implements FavClickListener, DialogInterface.OnKeyListener {

    JSONObject json, resdata;
    String tid, pageId, cmsg;
    private View view;
    private EditText etRekTujuan, etNominal, etRekBsa;
    private TextInputLayout tilNamaBank, tilRekTujuan, tilNominal, tilRekBsa;
    private AutoCompleteTextView actvNamaBank;
    private ImageButton btnFav;
    private Button btn1, btnConfirm;

    String rcode, err, rekbsa, kodebank, rektujuan, nominal;
    private JSONObject params;
    private String accname, tsession, dest_accno, amount, dest_custname, adminfee, bit61;
    JSONArray banks;
    String idBankLain, nameBankLain, networkPriorityBankLain;

    private Realm realm;

    private String[] bank;
    private String[] id_bank, name, networkPriority;

    private List<Favorite> favDatas = new ArrayList<>();

    private AlertDialog AlertDialog = null,
            confirmationDialog = null;

    private AppPreferences appPref;

    public FTransferAntarBankDebitBsa() {
    }

    public View onCreateView(final LayoutInflater inflater, @Nullable final ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.activity_transfer_antar_bank_tunai, container, false);

        pageId = getArguments().getString(Menu.MENU_ID);
        AppUtil.toolbarRegular(getContext(), view, pageId);

        appPref = new AppPreferences(getContext());
        tid = appPref.getDeviceTid();
        Log.d("TID TRANSFER", tid);

        tilRekBsa = (TextInputLayout) view.findViewById(R.id.til_rekening_asal);
        etRekBsa = (EditText) view.findViewById(R.id.et_rekening_asal);
        etRekBsa.setFocusable(false);

        tilRekTujuan = (TextInputLayout) view.findViewById(R.id.til_rekening_tujuan);
        etRekTujuan = (EditText) view.findViewById(R.id.et_rekening_tujuan);
        etRekTujuan.setFocusable(false);

        tilNominal = (TextInputLayout) view.findViewById(R.id.til_nominal_transfer);
        etNominal = (EditText) view.findViewById(R.id.et_nominal_transfer);
        etNominal.addTextChangedListener(new NumberTextWatcherForThousand(etNominal));
        etNominal.setFocusable(false);

        btn1 = (Button) view.findViewById(R.id.btn_submit);
        btnFav = (ImageButton) view.findViewById(R.id.btn_daftar_fav);

        tilNamaBank = (TextInputLayout) view.findViewById(R.id.til_nama_bank);
        actvNamaBank = (AutoCompleteTextView) view.findViewById(R.id.act_nama_bank);
        actvNamaBank.setFocusable(false);

        setAutoCompleteText();

        etRekBsa.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                etRekBsa.setFocusableInTouchMode(true);

                return false;
            }
        });

        etRekTujuan.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                etRekTujuan.setFocusableInTouchMode(true);

                return false;
            }
        });

        etNominal.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                etNominal.setFocusableInTouchMode(true);

                return false;
            }
        });

        realm = Realm.getDefaultInstance();

        try {
            onActionButton();
        } catch (Exception e) {
            e.printStackTrace();
            Log.d("Error", e.toString());
            AppUtil.displayDialog(getContext(), "Maaf, Terjadi Kesalahan");
        }

        return view;
    }

    private void setAutoCompleteText() {
        final Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {

                    JSONObject cmdparam = new JSONObject();
                    String soapRes = AppUtil.soapReq(AppUtil.getDsn(), "getbanklist", cmdparam);

                    if (soapRes.equalsIgnoreCase("")) {
                        AppUtil.hideDialog();
                        getActivity().runOnUiThread(new Runnable() {
                            public void run() {
                                AppUtil.displayDialog(getContext(), "Gagal terhubung ke server");
                            }
                        });
                    } else {
                        json = new JSONObject(soapRes);
                        resdata = json.getJSONObject("resdata");

                        banks = resdata.getJSONArray("list");
                        Log.d("List bank", banks.toString());

                        bank = new String[banks.length()];
                        id_bank = new String[banks.length()];
                        name = new String[banks.length()];
                        networkPriority = new String[banks.length()];
                        for (int i = 0; i < banks.length(); i++) {
                            JSONObject jo = banks.getJSONObject(i);
                            bank[i] = jo.getString("Id") + " - " + jo.getString("Name");
                            id_bank[i] = jo.getString("Id");
                            name[i] = jo.getString("Name");
                            networkPriority[i] = jo.getString("NetworkPriority");
                        }
                        Log.d("bank", bank[2].toString());

                        actvNamaBank.setThreshold(1);

                        getActivity().runOnUiThread(new Runnable() {
                            public void run() {
                                if (banks.equals("")) {
                                    AppUtil.displayDialog(getContext(), "Transaksi tidak dapat dilakukan");
                                } else {
                                    ArrayAdapter<String> bankListAdapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_list_item_1,
                                            bank);
                                    bankListAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                    actvNamaBank.setAdapter(bankListAdapter);
                                }
                            }
                        });


                        actvNamaBank.setOnTouchListener(new View.OnTouchListener() {
                            @Override
                            public boolean onTouch(View v, MotionEvent event) {
                                actvNamaBank.setFocusableInTouchMode(true);
                                actvNamaBank.showDropDown();
                                actvNamaBank.requestFocus();
                                return false;
                            }
                        });

                        actvNamaBank.addTextChangedListener((TextWatcher) getContext());

                        actvNamaBank.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> parent, View arg1, int position, long id) {
                                String bankLain = actvNamaBank.getText().toString();
                                int posisi = 0;
                                for (int i = 0; i < bank.length; i++) {
                                    if (bank[i].equalsIgnoreCase(bankLain)) {
                                        posisi = i;
                                    }
                                }
                                idBankLain = id_bank[posisi];
                                nameBankLain = name[posisi];
                                networkPriorityBankLain = networkPriority[posisi];
                            }
                        });

                        actvNamaBank.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                            @Override
                            public void onFocusChange(View v, boolean hasFocus) {
                                String val = actvNamaBank.getText().toString() + "";
                                Boolean code = Arrays.asList(bank).contains(val);
                                if (!hasFocus)
                                    if (!code) {
                                        tilNamaBank.setErrorEnabled(true);
                                        tilNamaBank.setError("Kode Bank tidak valid");
                                    } else {
                                        tilNamaBank.setErrorEnabled(false);
                                        tilNamaBank.setError(null);
                                    }
                            }
                        });
                        actvNamaBank.addTextChangedListener((TextWatcher) getContext());
                    }
                } catch (Exception ex) {
                    Log.d("THREAD a", ex.toString());
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            AppUtil.displayDialog(getContext(), "Transaksi tidak dapat dilakukan");
                        }
                    });
                }
            }
        });
        thread.start();
    }

    private void onActionButton() {
        btnFav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    rekbsa = etRekBsa.getText().toString().trim();
                    rektujuan = etRekTujuan.getText().toString().trim();
                    String bankLain = actvNamaBank.getText().toString();
                    int posisi = 0;
                    for (int i = 0; i < bank.length; i++) {
                        if (bank[i].equalsIgnoreCase(bankLain)) {
                            posisi = i;
                        }
                    }
                    idBankLain = id_bank[posisi];
                    nameBankLain = name[posisi];
                    networkPriorityBankLain = networkPriority[posisi];
                    final String kode_bank = idBankLain + "|" + nameBankLain + "|" + networkPriorityBankLain;

                    if (rektujuan.equalsIgnoreCase("")) {
                        getFavData();
                        AppUtil.displayDialogFavorit(getContext(), favDatas, FTransferAntarBankDebitBsa.this, "FAVTRANSA");
                        return;
                    }

                    AlertDialog dialogAlert;
                    AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                    builder.setCancelable(true);
                    builder.setMessage("Simpan data transaksi ini sebagai data favorit?");
                    builder.setPositiveButton("Simpan", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface alertdialog, int which) {
                            alertdialog.cancel();

                            if (AppUtil.checkFavorite("DEBIT", rektujuan, kode_bank, AppUtil.getDsn())) {
                                FragmentManager manager = getActivity().getSupportFragmentManager();
                                Fragment fragment;
                                FragmentTransaction transaction = manager.beginTransaction();
                                fragment = new FInsertUpdateDataFavorit();
                                Bundle bundle = new Bundle();
                                bundle.putString("title", getString(R.string.txt_title_menu_transfer_antar_bank));
                                bundle.putString("jenispembayaran", "DEBIT");
                                bundle.putString("bsaacc", rekbsa);
                                bundle.putString("bank", actvNamaBank.getText().toString());
                                bundle.putString("data3", rektujuan);
                                bundle.putString("menutrx", "8000");
                                bundle.putString("submenutrx", "8009");
                                bundle.putString("namajenisfav", "Transfer Bank Lain");

                                fragment.setArguments(bundle);
                                transaction.replace(R.id.fragment_payment_general, fragment);
                                transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
                                transaction.addToBackStack(null);
                                transaction.commit();


                            } else {
                                AppUtil.displayDialog(getContext(), "Data favorit Telah Ada");
                            }
                        }
                    });
                    builder.setNegativeButton("Pilih favorit", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface alertdialog, int which) {
                            alertdialog.cancel();
                            getFavData();
                            AppUtil.displayDialogFavorit(getContext(), favDatas, FTransferAntarBankDebitBsa.this, "FAVTRANSA");
                        }
                    });

                    dialogAlert = builder.create();
                    dialogAlert.show();
                } catch (Exception e) {
                    e.printStackTrace();
                    AppUtil.displayDialog(getContext(), "Maaf, Terjadi Kesalahan");
                }
            }
        });

        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                kodebank = actvNamaBank.getText().toString().trim();
                Boolean isValid = Arrays.asList(bank).contains(kodebank);

                int posisi = 0;
                for (int i = 0; i < bank.length; i++) {
                    if (bank[i].equalsIgnoreCase(actvNamaBank.getText().toString().trim())) {
                        posisi = i;
                    }
                }
                idBankLain = id_bank[posisi];
                nameBankLain = name[posisi];
                networkPriorityBankLain = networkPriority[posisi];

                rekbsa = etRekBsa.getText().toString();
                rektujuan = etRekTujuan.getText().toString();
                nominal = NumberTextWatcherForThousand.trimCommaOfString(etNominal.getText().toString().trim());

                if (!isValid) {
                    tilNamaBank.setError("Kode Bank Tidak Valid!");
                    return;
                } else {
                    int index = kodebank.indexOf(" ");
                    kodebank = kodebank.substring(0, index);
                }
                if (etRekBsa.getText().toString().trim().length() == 0) {
                    tilRekBsa.setError(tilRekBsa.getHint() + " Tidak Boleh Kosong!");
                }
                if (etRekTujuan.getText().toString().trim().length() == 0) {
                    tilRekTujuan.setError("Nomor Rekening Tujuan Tidak Boleh Kosong!");
                    return;
                }
                if (etNominal.getText().toString().length() == 0) {
                    tilNominal.setError(tilNominal.getHint() + " Tidak Boleh Kosong!");
                    return;
                }

                SoapActivity action = new SoapActivity();
                action.execute();
            }
        });

    }

    @Override
    public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
            AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
            builder.setCancelable(false);
            builder.setMessage("Apakah anda yakin akan membatalkan proses Pembayaran " + pageId + "?");
            builder.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                    confirmationDialog.dismiss();
                }
            });
            builder.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                    confirmationDialog.show();
                }
            });
            if (AlertDialog == null || !AlertDialog.isShowing()) {
                AlertDialog = builder.create();
                AlertDialog.show();
            }
            return true;
        }
        return false;
    }

    @Override
    public void getFav(String label, String category) {

    }

    @Override
    public void onclikFav(String label, String kode_produk) {
        RealmResults<Favorite> fav = realm.where(Favorite.class)
                .equalTo("namafav", label)
                .equalTo("data1", kode_produk)
                .findAll();
        Log.d("Cek fav", fav.toString());

        if (fav.size() > 0) {
            String data1 = fav.get(0).getData1();
            String kodebank = data1.substring(0, data1.indexOf("|"));
            String namaprioritybank = data1.substring(data1.indexOf("|") + 1, data1.length());
            Log.d("Cek namaprioritybank", namaprioritybank);
            String namabank = namaprioritybank.substring(0, namaprioritybank.indexOf("|"));
            etRekBsa.setText(fav.get(0).getBsaacc());
            actvNamaBank.setText(kodebank + " - " + namabank);
            etRekTujuan.setText(fav.get(0).getData3());
            AppUtil.closeListDialog();
        }
    }

    @Override
    public void deleteFav(String jenispembayaran, String menutrx, String submenutrx, String label) {

    }

    public void getFavData() {
        favDatas.clear();
        Realm mRealm = Realm.getDefaultInstance();
        favDatas = mRealm.copyFromRealm(
                mRealm.where(Favorite.class)
                        .equalTo("menutrx", "8000")
                        .equalTo("submenutrx", "8009")
                        .equalTo("jenispembayaran", "DEBIT")
                        .findAll());
        Log.d("Cek getFavData", favDatas.toString());
        mRealm.close();
    }

    private class SoapActivity extends AsyncTask<Void, Void, Void> {


        @Override
        protected void onPreExecute() {

        }

        @Override
        protected Void doInBackground(Void... params) {
            convertir();
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
//            if (msgErr.equalsIgnoreCase("true")) {
//                AppUtil.displayDialog(getContext(), message);
//            }
        }

    }

    private void convertir() {

        try {
            getActivity().runOnUiThread(new Runnable() {
                public void run() {
                    AppUtil.initDialogProgress(getContext());
                    AppUtil.showDialog();
                }
            });

            JSONObject cmdparam = new JSONObject();
            cmdparam.put("merchantid", "6014");
            cmdparam.put("merchant", "6014");
            cmdparam.put("branch_id", "0001");
            cmdparam.put("src_accno", "");
            cmdparam.put("bankcode", idBankLain);
            cmdparam.put("bank_code", idBankLain);
            cmdparam.put("refnumber", "");
            cmdparam.put("dest_accno", rektujuan);
            cmdparam.put("amount", nominal);
            cmdparam.put("networkpriority", networkPriorityBankLain);
            cmdparam.put("vdsn", rekbsa);
            String soapRes = AppUtil.soapReq(AppUtil.getDsn(), "trfbanklain_verifybyacc", cmdparam);

            if (soapRes.equalsIgnoreCase("")) {
                AppUtil.hideDialog();
                getActivity().runOnUiThread(new Runnable() {
                    public void run() {
                        AppUtil.displayDialog(getContext(), "Gagal terhubung ke server");
                    }
                });
            } else {
                json = new JSONObject(soapRes);
                resdata = json.getJSONObject("resdata");
                rcode = resdata.getString("rcode");

                if (rcode.equalsIgnoreCase("00")) {
                    accname = resdata.getString("accname");
                    tsession = resdata.getString("tsession");
                    params = resdata.getJSONObject("params");
                    dest_accno = params.getString("dest_accno");
                    amount = params.getString("amount");
                    dest_custname = params.getString("dest_custname");
                    adminfee = params.getString("adminfee");
                    bit61 = params.getString("BIT61");
                    new Thread() {
                        public void run() {
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    AppUtil.hideDialog();
                                    try {
                                        LayoutInflater li = LayoutInflater.from(getContext());
                                        final View confirmDialog = li.inflate(R.layout.dialog_confirm_bsa, null);
                                        btnConfirm = (Button) confirmDialog.findViewById(R.id.btn_submit);
                                        final TextView confirm = (TextView) confirmDialog.findViewById(R.id.txt_title_top);
                                        final EditText pin = (EditText) confirmDialog.findViewById(R.id.txt_pin);

                                        final int nominal_transfer = Integer.parseInt(NumberTextWatcherForThousand.trimCommaOfString(amount));
                                        int fee = Integer.parseInt(NumberTextWatcherForThousand.trimCommaOfString(adminfee));
                                        int jml_total = nominal_transfer+fee;
                                        final String total = NumberFormat.getNumberInstance(Locale.US).format(jml_total);
                                        final String adminfee = NumberFormat.getNumberInstance(Locale.US).format(fee);

                                        confirm.setText(rekbsa + " akan melakukan Transfer ke Bank " + nameBankLain + " rek " + rektujuan + "an." + dest_custname + " sebesar Rp. " + etNominal.getText().toString() + ", biaya Administrasi Rp. " + adminfee + ". Total Rp. " + total + ". Jika benar masukkan PIN yang dikirim melalui SMS");
                                        pin.addTextChangedListener(new TextWatcher() {
                                            @Override
                                            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                                            }

                                            @Override
                                            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                                                if (charSequence.toString().trim().length() == 7) {
                                                    btnConfirm.setEnabled(true);
                                                    btnConfirm.setBackgroundResource(R.drawable.button_shape_blue);
                                                } else {
                                                    btnConfirm.setEnabled(false);
                                                    btnConfirm.setBackgroundResource(R.drawable.button_shape_accent);
                                                }
                                            }

                                            @Override
                                            public void afterTextChanged(Editable editable) {
                                            }
                                        });

                                        AlertDialog.Builder alert = new AlertDialog.Builder(getContext());
                                        alert.setView(confirmDialog);
                                        alert.setCancelable(false);
                                        confirmationDialog = alert.create();
                                        confirmationDialog.setOnKeyListener(FTransferAntarBankDebitBsa.this);
                                        confirmationDialog.show();

                                        btnConfirm.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View view) {

                                                AppUtil.initDialogProgress(getContext());
                                                AppUtil.showDialog();

                                                Thread thread = new Thread(new Runnable() {

                                                    @Override
                                                    public void run() {
                                                        try {
                                                            JSONObject cmdparam = new JSONObject();
                                                            cmdparam.put("merchantid", "6014");
                                                            cmdparam.put("merchant", "6014");
                                                            cmdparam.put("branch_id", "0001");
                                                            cmdparam.put("src_accno", "");
                                                            cmdparam.put("bankcode", idBankLain);
                                                            cmdparam.put("bank_code", idBankLain);
                                                            cmdparam.put("refnumber", "");
                                                            cmdparam.put("dest_accno", rektujuan);
                                                            cmdparam.put("amount", nominal);
                                                            cmdparam.put("bit61", bit61);
                                                            cmdparam.put("accname", accname);
                                                            cmdparam.put("byPass", "");
                                                            cmdparam.put("networkpriority", networkPriorityBankLain);
                                                            cmdparam.put("vdsn", rekbsa);
                                                            cmdparam.put("idpelanggan", nameBankLain + " Rekening Tujuan " + rektujuan + " Nominal " + nominal);
                                                            cmdparam.put("jenistransaksi", "8009");
                                                            cmdparam.put("tpin", pin.getText().toString());
                                                            cmdparam.put("tsession", tsession);
                                                            String soapRes = AppUtil.soapReq(AppUtil.getDsn(), "trfbanklain_savebyacc", cmdparam);

                                                            if (soapRes.equalsIgnoreCase("")) {
                                                                AppUtil.hideDialog();
                                                                getActivity().runOnUiThread(new Runnable() {
                                                                    public void run() {
                                                                        AppUtil.displayDialog(getContext(), "Gagal terhubung ke server");
                                                                    }
                                                                });
                                                            } else {
                                                                json = new JSONObject(soapRes);
                                                                resdata = json.getJSONObject("resdata");
                                                                rcode = resdata.getString("rcode");

                                                                if (rcode.equalsIgnoreCase("00")) {
                                                                    final String refno;

                                                                    params = resdata.getJSONObject("params");
                                                                    refno = params.getString("refno");

                                                                    Calendar calendar = Calendar.getInstance();
                                                                    SimpleDateFormat mdformat = new SimpleDateFormat("dd/MM/yy");
                                                                    SimpleDateFormat mtformat = new SimpleDateFormat("HH:mm");

                                                                    cmsg = mdformat.format(calendar.getTime()) + " " + mtformat.format(calendar.getTime()) + "Trasfer dari rekening " + rekbsa + " ke Bank " + nameBankLain + " rek " + rektujuan + "an." + dest_custname + " sebesar Rp. " + etNominal.getText().toString() + ", biaya Administrasi Rp. " + adminfee + ". Total Rp. " + total + " berhasil. No. reff. " + refno;

                                                                    //khusus edc brissmart
                                                                    String date = AppUtil.getDateFormat();
//
                                                                    Bundle datas = new Bundle();
                                                                    datas.putString("menu_id", pageId);
                                                                    datas.putString("tid", tid);
                                                                    datas.putString("mid", "4228267000001");
                                                                    datas.putString("type_card", "-");
                                                                    datas.putString("date", date);
                                                                    datas.putString("msg", cmsg);

                                                                    AppUtil.hideDialog();
                                                                    getActivity().runOnUiThread(new Runnable() {
                                                                        public void run() {

                                                                            AppUtilPrint.printStruk(getContext(), datas);

                                                                            etRekBsa.getText(). clear();
                                                                            actvNamaBank.getText().clear();
                                                                            etRekTujuan.getText().clear();
                                                                            etNominal.getText().clear();
                                                                            actvNamaBank.setFocusable(false);
                                                                            etRekBsa.setOnTouchListener(new View.OnTouchListener() {
                                                                                @Override
                                                                                public boolean onTouch(View v, MotionEvent event) {
                                                                                    etRekBsa.setFocusableInTouchMode(true);
                                                                                    return false;
                                                                                }
                                                                            });
                                                                            actvNamaBank.setOnTouchListener(new View.OnTouchListener() {
                                                                                @Override
                                                                                public boolean onTouch(View v, MotionEvent event) {
                                                                                    actvNamaBank.setFocusableInTouchMode(true);
                                                                                    return false;
                                                                                }
                                                                            });
                                                                            etRekTujuan.setOnTouchListener(new View.OnTouchListener() {
                                                                                @Override
                                                                                public boolean onTouch(View v, MotionEvent event) {
                                                                                    etRekTujuan.setFocusableInTouchMode(true);
                                                                                    return false;
                                                                                }
                                                                            });
                                                                            etNominal.setOnTouchListener(new View.OnTouchListener() {
                                                                                @Override
                                                                                public boolean onTouch(View v, MotionEvent event) {
                                                                                    etNominal.setFocusableInTouchMode(true);
                                                                                    return false;
                                                                                }
                                                                            });
                                                                            AppUtil.displayDialog(getContext(), cmsg);
                                                                            AppUtil.saveIntoInbox(cmsg, pageId, AppUtil.getDsn());
                                                                            confirmationDialog.dismiss();
                                                                        }
                                                                    });
                                                                } else if (rcode.equalsIgnoreCase("98")) {
                                                                    cmsg = "Kode PIN Salah atau Tidak Aktif";
                                                                    AppUtil.hideDialog();
                                                                    getActivity().runOnUiThread(new Runnable() {
                                                                        public void run() {
                                                                            AppUtil.displayDialog(getContext(), cmsg);
                                                                            pin.getText().clear();

                                                                        }
                                                                    });
                                                                } else {
                                                                    cmsg = "Transaksi tidak dapat dilakukan";
                                                                    AppUtil.hideDialog();
                                                                    getActivity().runOnUiThread(new Runnable() {
                                                                        public void run() {
                                                                            AppUtil.displayDialog(getContext(), cmsg);
                                                                            confirmationDialog.dismiss();
                                                                        }
                                                                    });
                                                                }
                                                            }
                                                        } catch (Exception ex) {
                                                            AppUtil.hideDialog();
                                                            Log.d("THREAD", ex.toString());
                                                            getActivity().runOnUiThread(new Runnable() {
                                                                public void run() {
                                                                    AppUtil.displayDialog(getContext(), "Transaksi tidak dapat dilakukan");
                                                                }
                                                            });
                                                        }
                                                    }
                                                });
                                                thread.start();
                                            }
                                        });
                                    } catch (Exception e) {
                                        AppUtil.hideDialog();
                                        Log.d("THREAD 1", e.toString());
                                        getActivity().runOnUiThread(new Runnable() {
                                            public void run() {
                                                AppUtil.displayDialog(getContext(), "Transaksi tidak dapat dilakukan");
                                            }
                                        });
                                    }
                                }
                            });
                        }
                    }.start();
                } else {
                    AppUtil.hideDialog();
                    cmsg = "Transaksi tidak dapat dilakukan";
                    Log.d("else", "gagal");
                    getActivity().runOnUiThread(new Runnable() {
                        public void run() {
                            AppUtil.displayDialog(getContext(), cmsg);
                        }
                    });
                }
            }
        } catch (Exception ex) {
            AppUtil.hideDialog();
            Log.d("THREAD a", ex.toString());
            getActivity().runOnUiThread(new Runnable() {
                public void run() {
                    AppUtil.displayDialog(getContext(), "Transaksi tidak dapat dilakukan");
                }
            });
        }
    }
}

