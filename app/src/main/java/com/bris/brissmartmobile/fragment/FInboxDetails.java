package com.bris.brissmartmobile.fragment;

import android.app.PendingIntent;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.telephony.SmsManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bris.brissmartmobile.R;
import com.bris.brissmartmobile.model.History;
import com.bris.brissmartmobile.util.AppUtil;
import com.bris.brissmartmobile.util.AppUtilPrint;

import java.io.File;

import io.realm.Realm;

public class FInboxDetails extends Fragment {

    private TextView tv_date, tv_msg;
    private long trxdate;
    private Realm realm;
    private RelativeLayout relativeLayout;
    private static final int RESULT_CONTACT_PICK = 12580;

    public FInboxDetails() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        trxdate = getArguments().getLong("trxdate");
        View view = inflater.inflate(R.layout.fragment_detail_inbox, container, false);
        relativeLayout = (RelativeLayout) view.findViewById(R.id.relative_struk_inbox);
        realm = Realm.getDefaultInstance();
        tv_date = (TextView) view.findViewById(R.id.tv_detail_date_inbox);
        tv_msg = (TextView) view.findViewById(R.id.tv_detail_message_inbox);
        Button btn_share = (Button) view.findViewById(R.id.btn_share_struk);
        Button btn_forward = (Button) view.findViewById(R.id.btn_share_struk_sms);
        getDetailInbox(trxdate);

        btn_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AppUtil.store(AppUtil.getScreenShot(relativeLayout), String.valueOf(trxdate) + ".png");
                File file = new File(AppUtil.dirPath , String.valueOf(trxdate) + ".png");
                shareImage(file);
            }
        });

        btn_forward.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //launcPicker();
                // get message text
                String msg =  tv_msg.getText().toString();
                String date = tv_date.getText().toString();
                // direct to message
//                forwardSMS(sms);

                // print Struk
                Bundle datas = new Bundle();
                datas.putString("tid", "08267001");
                datas.putString("mid", "4228267000001");
                datas.putString("type_card", "inbox");
                datas.putString("date", date);
                datas.putString("msg", msg);

                AppUtilPrint.printStruk(getContext(), datas);
            }
        });

        return view;
    }

    private void getDetailInbox(long trxdate){
        History inbox = realm.where(History.class).equalTo("trxdate", trxdate).findFirst();
        tv_date.setText(inbox.getDatedisplay());
        tv_msg.setText(inbox.getMessage());
    }

    private void shareImage(File file){
        Uri uri = Uri.fromFile(file);
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_SEND);
        intent.setType("image/*");

        intent.putExtra(Intent.EXTRA_SUBJECT, "");
        intent.putExtra(Intent.EXTRA_TEXT, "");
        intent.putExtra(Intent.EXTRA_STREAM, uri);
        try {
            startActivity(Intent.createChooser(intent, "Share Struk"));
        } catch (ActivityNotFoundException e) {
            AppUtil.showToastShort(getContext(), "Tidak ada aplikasi yang tersedia");
        }
    }

    private void launcPicker(){
        Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.CommonDataKinds.Phone.CONTENT_URI);
        startActivityForResult(intent, RESULT_CONTACT_PICK);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode){
            case RESULT_CONTACT_PICK:
                prepareSend(data);
        }
    }

    private void prepareSend(Intent data){
        Cursor cursor = null;
        try {
            String phoneNo = null ;
            String name = null;

            Uri uri = data.getData();

            cursor = getActivity().getContentResolver().query(uri, null, null, null, null);
            cursor.moveToFirst();
            int  phoneIndex =cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
            int  nameIndex =cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME);
            phoneNo = cursor.getString(phoneIndex);
            name = cursor.getString(nameIndex);

            AlertDialog AlertDialog = null;
            android.support.v7.app.AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
            builder.setCancelable(false);
            builder.setMessage("Apakah anda yakin akan mengirim SMS ke " + name + "" +
                    " dengan pesan : " + tv_msg.getText().toString().trim());
            final String finalPhoneNo = phoneNo;
            builder.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    sendSMS(finalPhoneNo,tv_msg.getText().toString().trim());
                    dialog.cancel();
                }
            });
            builder.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                    dialog.cancel();
                }
            });
            if(AlertDialog == null || !AlertDialog.isShowing()) {
                AlertDialog = builder.create();
                AlertDialog.show();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void forwardSMS(String txt) {
//        Intent sendIntent = new Intent();
//        sendIntent.setAction(Intent.ACTION_SEND);
//        sendIntent.putExtra(Intent.EXTRA_TEXT, txt);
//        sendIntent.setType("text/plain");
//        startActivity(sendIntent);

        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SENDTO);
        sendIntent.putExtra(Intent.EXTRA_TEXT, txt);
        sendIntent.setType("text/plain");
        startActivity(sendIntent);
    }

    private void sendSMS(String  phoneNumber, String  message)
    {
        String  SENT = "SMS_SENT";
        String  DELIVERED = "SMS_DELIVERED";

        PendingIntent sentPI = PendingIntent.getBroadcast(getContext(), 0,
                new Intent(SENT), 0);

        PendingIntent deliveredPI = PendingIntent.getBroadcast(getContext(), 0,
                new Intent(DELIVERED), 0);

//        //---when the SMS has been sent---
//        getContext().registerReceiver(new BroadcastReceiver(){
//            @Override
//            public void onReceive(Context arg0, Intent arg1) {
//                switch (getResultCode())
//                {
//                    case Activity.RESULT_OK:
//                        Toast.makeText(getContext(), "SMS terkirim",
//                                Toast.LENGTH_SHORT).show();
//                        break;
//                    case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
//                        Toast.makeText(getContext(), "Generic failure",
//                                Toast.LENGTH_SHORT).show();
//                        break;
//                    case SmsManager.RESULT_ERROR_NO_SERVICE:
//                        Toast.makeText(getContext(), "No service",
//                                Toast.LENGTH_SHORT).show();
//                        break;
//                    case SmsManager.RESULT_ERROR_NULL_PDU:
//                        Toast.makeText(getContext(), "Null PDU",
//                                Toast.LENGTH_SHORT).show();
//                        break;
//                    case SmsManager.RESULT_ERROR_RADIO_OFF:
//                        Toast.makeText(getContext(), "Radio off",
//                                Toast.LENGTH_SHORT).show();
//                        break;
//                }
//            }
//        }, new IntentFilter(SENT));
//
//        //---when the SMS has been delivered---
//        getContext().registerReceiver(new BroadcastReceiver(){
//            @Override
//            public void onReceive(Context  arg0, Intent arg1) {
//                switch (getResultCode())
//                {
//                    case Activity.RESULT_OK:
//                        Toast.makeText(getContext(), "SMS terkirim",
//                                Toast.LENGTH_SHORT).show();
//                        break;
//                    case Activity.RESULT_CANCELED:
//                        Toast.makeText(getContext(), "SMS tidak terkirim",
//                                Toast.LENGTH_SHORT).show();
//                        break;
//                }
//            }
//        }, new IntentFilter(DELIVERED));

        SmsManager sms = SmsManager.getDefault();
        sms.sendTextMessage(phoneNumber, null, message, sentPI, deliveredPI);
    }

}
