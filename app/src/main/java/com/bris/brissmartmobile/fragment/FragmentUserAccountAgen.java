package com.bris.brissmartmobile.fragment;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bris.brissmartmobile.R;
import com.bris.brissmartmobile.activity.AGantiPasswordAgen;
import com.bris.brissmartmobile.model.UserBrissmart;
import com.bris.brissmartmobile.util.AppUtil;
import com.bris.brissmartmobile.util.CustomImageViewRounded;
import com.bris.brissmartmobile.util.Menu;

import org.json.JSONObject;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Random;

import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by valeputra on 6/22/17.
 */

public class FragmentUserAccountAgen extends Fragment {
    private TextView tv_user, tv_nama, tv_user_hp, tv_user_password, tv_user_rekening, tv_user_status,
            tv_user_alamat, tv_user_bisnis;
    private Button btn_ganti_pasword;
    private CustomImageViewRounded ivUserAvatar;
    private ImageView img_user;
    private Realm realm;
    private View view;
    private String status;
    private JSONObject json, resdata;
    private String responseJSON, rcode, name, phonenumber, acccore,
            agenstatus, address, business;

    public FragmentUserAccountAgen() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_user_account_agen, container, false);
        tv_user = (TextView) view.findViewById(R.id.tv_user);
        tv_nama = (TextView) view.findViewById(R.id.tv_nama);
        tv_user_hp = (TextView) view.findViewById(R.id.tv_user_hp);
        tv_user_password = (TextView) view.findViewById(R.id.tv_user_password);
        tv_user_rekening = (TextView) view.findViewById(R.id.tv_user_rekening);
        tv_user_status = (TextView) view.findViewById(R.id.tv_user_status);
        tv_user_alamat = (TextView) view.findViewById(R.id.tv_user_alamat);
        tv_user_bisnis = (TextView) view.findViewById(R.id.tv_user_bisnis);
        btn_ganti_pasword = (Button) view.findViewById(R.id.ganti_password);
        img_user = (ImageView) view.findViewById(R.id.img_user);

        ivUserAvatar = (CustomImageViewRounded) view.findViewById(R.id.iv_user_profile);
        img_user.setImageResource(R.drawable.ico_user_circle);
//        progressBarAvatar = (ProgressBar) view.findViewById(R.id.progress_avatar);

        btn_ganti_pasword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle datas = new Bundle();
                Intent intent = new Intent(getContext(), AGantiPasswordAgen.class);
                datas.putString("status", "nondirect");
                intent.putExtras(datas);
                startActivity(intent);
            }
        });

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Thread thread = new Thread(new Runnable() {

            @Override
            public void run() {
                SoapActivity action = new SoapActivity();
                action.execute();
            }
        });
        thread.start();

    }

    private class SoapActivity extends AsyncTask<Void, Void, Void> {


        @Override
        protected void onPreExecute() {

        }

        @Override
        protected Void doInBackground(Void... params) {
            Thread thread = new Thread(new Runnable() {

                @Override
                public void run() {
                    convertir();
                }
            });
            thread.start();
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
        }
    }

    private void convertir() {

        try {

            final JSONObject cmdparam = new JSONObject();
            cmdparam.put("vdsn", AppUtil.getDsn());
            cmdparam.put("dsn", AppUtil.getDsn());
            String soapRes = AppUtil.soapReq(AppUtil.getDsn(), "getprofilagen", cmdparam);

            if (soapRes.equalsIgnoreCase("")) {
                AppUtil.displayDialog(getContext(), responseJSON);
                responseJSON = "Gagal terhubung ke server";
            } else {
                json = new JSONObject(soapRes);
                resdata = json.getJSONObject("resdata");
                rcode = resdata.getString("rcode");

                if (rcode.equalsIgnoreCase("00")) {
                    name = resdata.getString("name");
                    phonenumber = resdata.getString("phonenumber");
                    acccore = resdata.getString("acccore");
                    agenstatus = resdata.getString("agenstatus");
                    address = resdata.getString("address");
                    business = resdata.getString("business");

                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            String statusagen;
                            if (agenstatus.equalsIgnoreCase("1")) {
                                statusagen = "Waiting Approve";
                            } else if (agenstatus.equalsIgnoreCase("2")) {
                                statusagen = "Aktif";
                            } else if (agenstatus.equalsIgnoreCase("3")) {
                                statusagen = "Tutup";
                            } else if (agenstatus.equalsIgnoreCase("4")) {
                                statusagen = "Terblokir";
                            } else {
                                statusagen = "Rejected";
                            }

                            tv_user.setText(getStatus());
                            tv_nama.setText(name);
                            tv_user_hp.setText(phonenumber);
                            tv_user_rekening.setText(acccore);
                            tv_user_status.setText(statusagen);
                            tv_user_alamat.setText(address);
                            tv_user_bisnis.setText(business);
                        }
                    });

                } else {
                    Log.d("else", "gagal");
                    responseJSON = "Gagal mendapatkan profil";
                    AppUtil.displayDialog(getContext(), responseJSON);
                }
            }
        } catch (Exception ex) {
            Log.d("THREAD 2", ex.toString());
            responseJSON = "Gagal terhubung ke server";
            AppUtil.displayDialog(getContext(), responseJSON);
        }
    }

    public String getStatus() {
        realm = Realm.getDefaultInstance();
        RealmResults<UserBrissmart> user = realm.where(UserBrissmart.class).findAll();
        status = (user.get(0).getStatus().isEmpty()) ? status.toUpperCase() : user.get(0).getStatus();
        return status;
    }

    @Override
    public void onResume() {
        super.onResume();
        setUserInfo();
    }

    private void setUserInfo() {
//        if (!(mainActivity.userAvatar == null)) {
//            progressBarAvatar.setVisibility(View.GONE);
//            ivUserAvatar.setImageBitmap(ConverterBase64.decodeBase64(mainActivity.userAvatar));
//        }
//
//        if (! (mainActivity.userAvatar == null))
//            tvUsername.setText(mainActivity.userName);
//
//        tvPhoneNum.setText("+"+ mainActivity.userPhone);
//        tvLastTrx.setText(setLastTrx());
    }

//    private void goToEditUserProfile() {
//        Bundle userDatas = new Bundle();
//        Intent intentProfile = new Intent(getContext(), UserProfileActivity.class);
//        userDatas.putString("name", mainActivity.userName);
//        userDatas.putString("phone", mainActivity.userPhone);
//        intentProfile.putExtras(userDatas);
//        startActivity(intentProfile);
//    }

//    private String setLastTrx() {
//        return (appPref.getLastTrx() == "") ? "Belum Ada" : appPref.getLastTrx();
//    }
//
//    private ArrayList<ListMenu> getListAcc(RealmResults<Account> acc) {
//        ArrayList<ListMenu> items = new ArrayList<>();
//        for (int i=0 ; i<acc.size() ; i++) {
//            items.add(new ListMenu(0, acc.get(i).getAccount() + " (" + acc.get(i).getAlias() + ")", 0));
//        }
//        return items;
//    }
//
//    private void setMultiAccStatus() {
//        acc = realm.where(Account.class).findAll();
//
//        if (acc.size() > 1) {
//            setDefaultMultiAcc(acc);
//            statusMultiAcc = "Aktif";
//        } else {
//            ImageView iv = (ImageView) view.findViewById(R.id.ic_next_multiacc);
//            iv.setVisibility(View.GONE);
//            statusMultiAcc = "Belum Teregister";
//        }
//
//        tvMultiAccStatus.setText(statusMultiAcc);
//    }

//    private String getPhoneNum() {
//        RealmResults<User> user = realm.where(User.class).findAll();
//        phoneNumber = ( user.get(0).getMsisdn().isEmpty() ) ? phoneNumber.toUpperCase() : user.get(0).getMsisdn();
//        return phoneNumber;
//    }

//    private String getUsername() {
//        if (! (AppPreferences.getUserName(getContext()).isEmpty() || AppPreferences.getUserName(getContext()) == null) ) {
//            userName =  AppPreferences.getUserName(getContext()).toString();
//        }
//        return userName;
//    }

    /****************************** ADAPTER **********************************/

//    private class AccDialogAdapter extends BaseAdapter {
//        private LayoutInflater layoutinflater;
//        private List<ListMenu> listStorage;
//        private Context dContext;
//        private AccDialogAdapter.ViewHolder listViewHolder;
//
//        public AccDialogAdapter(Context context, List<ListMenu> acc) {
//            this.dContext = context;
//            layoutinflater =(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//            listStorage = acc;
//        }
//
//        @Override
//        public int getCount() {
//            return listStorage.size();
//        }
//
//        @Override
//        public Object getItem(int position) {
//            return position;
//        }
//
//        @Override
//        public long getItemId(int position) {
//            return position;
//        }
//
//        @Override
//        public View getView(int position, View convertView, ViewGroup parent) {
//            if (convertView == null) {
//                listViewHolder = new ViewHolder();
//                convertView = layoutinflater.inflate(R.layout.lv_content_alistview_construct, parent, false);
//                listViewHolder.mIcon = (ImageView) convertView.findViewById(R.id.iv_icon_submenu);
//                listViewHolder.mTitle = (TextView) convertView.findViewById(R.id.tv_title_submenu);
//                listViewHolder.mIconNext = (ImageView) convertView.findViewById(R.id.iv_icon_next_submenu);
//
//                listViewHolder.mIcon.setVisibility(View.GONE);
//                listViewHolder.mIconNext.setVisibility(View.GONE);
//
//                convertView.setTag(listViewHolder);
//            } else {
//                listViewHolder = (AccDialogAdapter.ViewHolder) convertView.getTag();
//            }
//
//            listViewHolder.mIcon.setImageResource(listStorage.get(position).getIcon());
//            listViewHolder.mTitle.setText(listStorage.get(position).getTitle());
//            listViewHolder.mIconNext.setImageResource(listStorage.get(position).getIcon_next());
//
//            if (String.valueOf(position).equalsIgnoreCase(AppPreferences.getSettingUserPrefByType(dContext, Constants.Preferences.SETTINGS_KEY[0]))) {
//                listViewHolder.mTitle.setTextColor(ContextCompat.getColor(dContext, R.color.orange));
//                listViewHolder.mTitle.setTypeface(null, Typeface.BOLD);
//            } else {
//                listViewHolder.mTitle.setTextColor(ContextCompat.getColor(dContext, R.color.colorAccent));
//                listViewHolder.mTitle.setTypeface(null, Typeface.NORMAL);
//            }
//
//            return convertView;
//        }
//
//        private class ViewHolder {
//            ImageView mIcon;
//            TextView mTitle;
//            ImageView mIconNext;
//        }
//    }
}
