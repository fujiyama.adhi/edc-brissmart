package com.bris.brissmartmobile.fragment;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;

import com.bris.brissmartmobile.R;
import com.bris.brissmartmobile.util.FormValidationUtil;
import com.bris.brissmartmobile.util.Menu;
import com.bris.brissmartmobile.util.NothingSelectedSpinnerAdapter;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;


/**
 * Create by valeputra
 * Credits : akbaranjas
 */

public class FBukaRekeningBio extends Fragment {
    private View view;
    private EditText et_tempat_lahir, et_tgl_lahir, et_nama_ibu;
    private TextInputLayout til_tempat_lahir, til_tgl_lahir, til_nama_ibu;
    private String page_id, idtype, idcard, name;
    private String tempat_lahir, tgl_lahir, nama_ibu;
    private Spinner spinner_jenis_kelamin;
    private Button btn1;
    private String jenis_kelamin;
    private Calendar myCalendar;

    public FBukaRekeningBio() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.activity_buka_rekening_bio, container, false);

        idtype = getArguments().getString("idtype");
        idcard = getArguments().getString("idcard");
        name = getArguments().getString("name");
        page_id = getArguments().getString(Menu.MENU_ID);

        String[] arrayJenisKelamin = new String[]{
                "Laki-laki", "Perempuan"
        };

        til_tempat_lahir = (TextInputLayout) view.findViewById(R.id.til_tempat_lahir);
        til_tgl_lahir = (TextInputLayout) view.findViewById(R.id.til_tgl_lahir);
        til_nama_ibu = (TextInputLayout) view.findViewById(R.id.til_nama_ibu);

        et_tempat_lahir = (EditText) view.findViewById(R.id.et_tempat_lahir);
        et_nama_ibu = (EditText) view.findViewById(R.id.et_nama_ibu);

        spinner_jenis_kelamin = (Spinner) view.findViewById(R.id.spinner_jenis_kelamin);
        ArrayAdapter<String> adapterJenisKelamin = new ArrayAdapter<String>(getContext(),
                android.R.layout.simple_spinner_item, arrayJenisKelamin);
        adapterJenisKelamin.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_jenis_kelamin.setAdapter(new NothingSelectedSpinnerAdapter(
                adapterJenisKelamin,
                R.layout.spinner_jenis_kelamin_nothing_selected,
                getContext()
        ));

        myCalendar = Calendar.getInstance();
        et_tgl_lahir = (EditText) view.findViewById(R.id.et_tgl_lahir);
        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }

        };

        et_tgl_lahir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                new DatePickerDialog(getContext(), date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        btn1 = (Button) view.findViewById(R.id.btn_submit);

        try {
            onActionButton();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return view;
    }

    private void onActionButton() {
        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (spinner_jenis_kelamin.getSelectedItem() == null) {
                    FormValidationUtil.displayCustomDialog(getContext(),
                            getString(R.string.dialog_msg_jenis_kelamin),
                            getString(R.string.dialog_header_txt_alert));
                    return;
                } else {
                    if (spinner_jenis_kelamin.getSelectedItem().toString().equalsIgnoreCase("Laki-laki")) {
                        jenis_kelamin = "M";
                    } else {
                        jenis_kelamin = "F";
                    }
                }

                if (et_tempat_lahir.getText().toString().length() == 0) {
                    til_tempat_lahir.setError("Tempat Lahir Tidak Boleh Kosong!");
                    return;
                } else {
                    tempat_lahir = et_tempat_lahir.getText().toString();
                }

                if (et_tgl_lahir.getText().toString().length() == 0) {
                    til_tgl_lahir.setError("Tanggal Lahir Tidak Boleh Kosong!");
                    return;
                } else {
                    tgl_lahir = et_tgl_lahir.getText().toString();
                }

                if (et_nama_ibu.getText().toString().length() == 0) {
                    til_nama_ibu.setError("Nama Gadis Ibu Kandung Tidak Boleh Kosong!");
                    return;
                } else {
                    nama_ibu = et_nama_ibu.getText().toString();
                }

                Fragment fr = new FBukaRekeningBio2();
                Bundle datas = new Bundle();
                datas.putString("idtype", idtype);
                datas.putString("idcard", idcard);
                datas.putString("name", name);
                datas.putString("jenis_kelamin", jenis_kelamin);
                datas.putString("tempat_lahir", tempat_lahir);
                datas.putString("tgl_lahir", tgl_lahir);
                datas.putString("nama_ibu", nama_ibu);
                datas.putString(Menu.MENU_ID, page_id);
                fr.setArguments(datas);
                openFragmentSliding(fr);
            }
        });
    }

    private void updateLabel() {
        String myFormat = "dd-MM-yyyy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        et_tgl_lahir.setText(sdf.format(myCalendar.getTime()));
    }

    public void openFragmentSliding(Fragment fragment) {
        FragmentManager manager = getActivity().getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit)
                .replace(R.id.framelayout, fragment)
                .addToBackStack(null)
                .commit();
    }
}