package com.bris.brissmartmobile.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.bris.brissmartmobile.R;
import com.bris.brissmartmobile.model.Favorite;
import com.bris.brissmartmobile.model.FavoriteRequest;
import com.bris.brissmartmobile.model.FavoriteUpdateRequest;
import com.bris.brissmartmobile.util.ApiInterface;
import com.bris.brissmartmobile.util.AppUtil;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;

import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FInsertUpdateDataFavorit extends Fragment implements View.OnClickListener {
    private String namafav = null, menutrx = null, jenispembayaran = null,
            data1 = null, data4 = "", pageTitle = null, submenutrx = null, namajenisfav = null,
            title = "";
    private View view;
    private TextView tv_title;
    private TextInputLayout til_label, til_norek_bsa, til_detail, til_kode_bank;
    private EditText edit_label;
    private EditText edit_norek_bsa, edit_detail;
    private AutoCompleteTextView edit_kode_bank_fav;
    private Button btnOK;
    private Realm realm;
    private String[] listPembayaran = null;
    private String[] listPembelian = null;

    JSONObject json, resdata;
    String responseJSON, msgErr, rcode, tsession, cmsg;
    JSONArray banks;
    ArrayList responseList;
    private String jsonData;
    private String[] bank;
    private String[] id_bank, name, networkPriority;
    private String idBankLain, nameBankLain, networkPriorityBankLain;

    public FInsertUpdateDataFavorit() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_insert_fav, container, false);

        title = getArguments().getString("title");
        namafav = getArguments().getString("namafav");
        menutrx = getArguments().getString("menutrx");
        jenispembayaran = getArguments().getString("jenispembayaran");
        Log.d("Cek jenispembayaran", jenispembayaran);
        data1 = getArguments().getString("data1");
        namajenisfav = getArguments().getString("namajenisfav");
        if (getArguments().containsKey("submenutrx")) {
            submenutrx = getArguments().getString("submenutrx");
            Log.d("Cek FIU submenutrx", submenutrx);
        }
        if (getArguments().containsKey("data4")) {
            data4 = getArguments().getString("data4");
        }

        tv_title = (TextView) view.findViewById(R.id.title_insert_favourite);
        til_label = (TextInputLayout) view.findViewById(R.id.til_fav_label);
        til_norek_bsa = (TextInputLayout) view.findViewById(R.id.til_fav_rek_bsa);
        til_detail = (TextInputLayout) view.findViewById(R.id.til_fav_detail);
        til_kode_bank = (TextInputLayout) view.findViewById(R.id.til_kode_bank);
        edit_label = (EditText) view.findViewById(R.id.edittext_i_label_fav);
        edit_norek_bsa = (EditText) view.findViewById(R.id.et_fav_rek_bsa);
        edit_detail = (EditText) view.findViewById(R.id.edittext_i_detail_fav);
        edit_kode_bank_fav = (AutoCompleteTextView) view.findViewById(R.id.edit_kode_bank_fav);
        btnOK = (Button) view.findViewById(R.id.btn_ok_insert_fav);

        if (!(jenispembayaran.equalsIgnoreCase("DEBIT"))) {
            til_norek_bsa.setVisibility(View.GONE);
            edit_norek_bsa.setVisibility(View.GONE);
        }

        listPembayaran = getResources().getStringArray(R.array.product_pembayaran_all);
        listPembelian = getResources().getStringArray(R.array.product_pembelian_all);

        realm = Realm.getDefaultInstance();

        // custom View
        customToolbar();
        customField();

        btnOK.setOnClickListener(FInsertUpdateDataFavorit.this);

        return view;
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btn_ok_insert_fav) {
            btnOkAction();
        }
    }

    String bundleDetail = "";

    private void setFormLabel() {

        if (namafav == null) {
            tv_title.setText("Tambah data favorit " + title);
            btnOK.setText("Tambah");
        } else {
            tv_title.setText("Ubah data favorit " + title);
            btnOK.setText("Ubah");
            getFavdata(namafav, menutrx, jenispembayaran, data1);
        }

        // For Edit action
        if (getArguments().containsKey("data3")) {
            bundleDetail = getArguments().getString("data3");
            edit_detail.setText(getArguments().getString("data3"));
        }
        if (getArguments().containsKey("bank")) {
            edit_kode_bank_fav.setText(getArguments().getString("bank"));
        }
        if (getArguments().containsKey("bsaacc")) {
            edit_norek_bsa.setText(getArguments().getString("bsaacc"));
        }
    }

    private void setFieldHint() {

        til_norek_bsa.setHint("Nomor Rekening Tabungan Cerdas");

//        if (menutrx.equalsIgnoreCase("8000")) {
//            if (submenutrx.equalsIgnoreCase("8001") || submenutrx.equalsIgnoreCase("8002")) {
//                til_norek_bsa.setHint("Nomor Rekening Asal");
//            } else {
//                til_norek_bsa.setHint("Nomor Rekening Tabungan Cerdas");
//            }
//        } else {
//            til_norek_bsa.setHint("Nomor Rekening Tabungan Cerdas");
//        }

        if (menutrx.equalsIgnoreCase("8000")) {
            if (submenutrx.equalsIgnoreCase("8009")) {
                Thread thread = new Thread(new Runnable() {

                    @Override
                    public void run() {
                        try {
                            JSONObject cmdparam = new JSONObject();
                            jsonData = AppUtil.soapReq(AppUtil.getDsn(), "getbanklist", cmdparam);
                            json = new JSONObject(jsonData);
                            resdata = json.getJSONObject("resdata");
                            banks = resdata.getJSONArray("list");
                            Log.d("List bank", banks.toString());

                            responseList = new ArrayList<>();
                            bank = new String[banks.length()];
                            id_bank = new String[banks.length()];
                            name = new String[banks.length()];
                            networkPriority = new String[banks.length()];
                            for (int i = 0; i < banks.length(); i++) {
                                JSONObject jo = banks.getJSONObject(i);
                                bank[i] = jo.getString("Id") + " - " + jo.getString("Name");
                                id_bank[i] = jo.getString("Id");
                                name[i] = jo.getString("Name");
                                networkPriority[i] = jo.getString("NetworkPriority");
                            }
                        } catch (Exception ex) {
                            Log.d("THREAD a", ex.toString());
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    AppUtil.displayDialog(getContext(), "Transaksi tidak dapat dilakukan");
                                }
                            });
                        }

                        edit_kode_bank_fav.setThreshold(1);

                        getActivity().runOnUiThread(new Runnable() {
                            public void run() {
                                ArrayAdapter<String> bankListAdapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_list_item_1,
                                        bank);
                                bankListAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                edit_kode_bank_fav.setAdapter(bankListAdapter);
                            }
                        });

                        edit_kode_bank_fav.setOnFocusChangeListener(new View.OnFocusChangeListener() {

                            @Override
                            public void onFocusChange(View v, boolean hasFocus) {
                                String val = edit_kode_bank_fav.getText().toString() + "";
                                Boolean code = Arrays.asList(bank).contains(val);
                                if (!hasFocus)
                                    if (!code) {
                                        til_kode_bank.setErrorEnabled(true);
                                        til_kode_bank.setError("Kode Bank tidak valid");
                                    } else {
                                        til_kode_bank.setErrorEnabled(false);
                                        til_kode_bank.setError(null);
                                    }
                            }
                        });

                        edit_kode_bank_fav.setOnTouchListener(new View.OnTouchListener() {
                            @Override
                            public boolean onTouch(View v, MotionEvent event) {
                                edit_kode_bank_fav.setFocusableInTouchMode(true);
                                edit_kode_bank_fav.showDropDown();
                                edit_kode_bank_fav.requestFocus();
                                return false;
                            }
                        });
                    }
                });
                thread.start();
            } else {
                til_kode_bank.setVisibility(View.GONE);
            }
            til_detail.setHint("Nomor Rekening");
        } else if (menutrx.equalsIgnoreCase("6000")) {
            til_kode_bank.setVisibility(View.GONE);
            if (title.equalsIgnoreCase(listPembelian[0])) {
                til_detail.setHint("Nomor Meter");
            } else {
                til_detail.setHint("Nomor Handphone");
            }

        } else if (menutrx.equalsIgnoreCase("7000")) {
            if (submenutrx.equalsIgnoreCase("7006") || submenutrx.equalsIgnoreCase("7007") || submenutrx.equalsIgnoreCase("7008")) {
                Thread thread = new Thread(new Runnable() {

                    @Override
                    public void run() {
                        try {
                            String kode = null;
                            if (submenutrx.equalsIgnoreCase("7006")) {
                                til_kode_bank.setHint("Pilih Institusi");
                                kode = "2";
                            } else if (submenutrx.equalsIgnoreCase("7008")) {
                                til_kode_bank.setHint("Pilih Asuransi");
                                kode = "1";
                            } else if (submenutrx.equalsIgnoreCase("7007")) {
                                if (data4.equalsIgnoreCase("S")) {
                                    til_kode_bank.setHint("Pilih Sekolah");
                                    kode = "3";
                                } else if (data4.equalsIgnoreCase("P")) {
                                    til_kode_bank.setHint("Pilih Perguruan Tinggi");
                                    kode = "4";
                                }
                            }

                            jsonData = AppUtil.setAutoCompleteInstitusi(kode);
                            json = new JSONObject(jsonData);
                            Log.d("json", json.toString());
                            banks = json.getJSONArray("data");
                            Log.d("array", banks.toString());

                            //responseList = new ArrayList<>();
                            bank = new String[banks.length()];
                            id_bank = new String[banks.length()];
                            name = new String[banks.length()];
                            for (int i = 0; i < banks.length(); i++) {
                                JSONObject jo = banks.getJSONObject(i);
                                bank[i] = jo.getString("institusi_code") + " - " + jo.getString("institusi_name");
                                id_bank[i] = jo.getString("institusi_code");
                                name[i] = jo.getString("institusi_name");
                            }
                        } catch (Exception ex) {
                            Log.d("THREAD a", ex.toString());
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    AppUtil.displayDialog(getContext(), "Transaksi tidak dapat dilakukan");
                                }
                            });
                        }

                        edit_kode_bank_fav.setThreshold(1);

                        getActivity().runOnUiThread(new Runnable() {
                            public void run() {
                                ArrayAdapter<String> bankListAdapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_list_item_1,
                                        bank);
                                bankListAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                edit_kode_bank_fav.setAdapter(bankListAdapter);
                            }
                        });

                        edit_kode_bank_fav.setOnFocusChangeListener(new View.OnFocusChangeListener() {

                            @Override
                            public void onFocusChange(View v, boolean hasFocus) {
                                String val = edit_kode_bank_fav.getText().toString() + "";
                                Boolean code = Arrays.asList(bank).contains(val);
                                if (!hasFocus)
                                    if (!code) {
                                        til_kode_bank.setErrorEnabled(true);
                                        til_kode_bank.setError("Kode Bank tidak valid");
                                    } else {
                                        til_kode_bank.setErrorEnabled(false);
                                        til_kode_bank.setError(null);
                                    }
                            }
                        });

                        edit_kode_bank_fav.setOnTouchListener(new View.OnTouchListener() {
                            @Override
                            public boolean onTouch(View v, MotionEvent event) {
                                edit_kode_bank_fav.setFocusableInTouchMode(true);
                                edit_kode_bank_fav.showDropDown();
                                edit_kode_bank_fav.requestFocus();
                                return false;
                            }
                        });
                    }
                });
                thread.start();
            } else {
                til_kode_bank.setVisibility(View.GONE);
            }

            if (title.equalsIgnoreCase(listPembayaran[0]) || title.equalsIgnoreCase(listPembayaran[1])) {
                til_detail.setHint("Nomor Pelanggan");
            } else if (title.equalsIgnoreCase(listPembayaran[2]) || title.equalsIgnoreCase(listPembayaran[3])) {
                til_detail.setHint("Nomor Telepon");
            } else if (title.equalsIgnoreCase(listPembayaran[4]) || title.equalsIgnoreCase(listPembayaran[5])
                    || title.equalsIgnoreCase(listPembayaran[6])) {
                til_detail.setHint("Nomor Handphone");
            } else if (title.equalsIgnoreCase(listPembayaran[7]) || title.equalsIgnoreCase(listPembayaran[8])
                    || title.equalsIgnoreCase(listPembayaran[9]) || title.equalsIgnoreCase(listPembayaran[10])) {
                til_detail.setHint("Nomor Pelanggan");
            } else if (title.equalsIgnoreCase(listPembayaran[11])) {
                til_detail.setHint("Kode Bayar");
            } else {
                til_detail.setHint("Nomor ID");
            }
        } else if (menutrx.equalsIgnoreCase("1000")) {
            til_kode_bank.setVisibility(View.GONE);
            til_detail.setHint("Nomor Rekening Tabungan Cerdas");
        } else if (menutrx.equalsIgnoreCase("9000")) {
            til_kode_bank.setVisibility(View.GONE);
            til_detail.setHint("Nomor HP");
            if (getArguments().containsKey("data4")) {
                edit_label.setText(getArguments().getString("data4"));
            }
            til_label.setHint("Nama Donatur");
        }
    }

    private void customField() {
        setFormLabel();
        setFieldHint();
    }

    private void getFavdata(String namafav, String menutrx, String jenispembayaran, String data1) {
        Favorite fav;
        RealmResults<Favorite> favTest;
        if (menutrx.equalsIgnoreCase("8000") || submenutrx.equalsIgnoreCase("7006") || submenutrx.equalsIgnoreCase("7008")) {
            fav = realm.where(Favorite.class)
                    .equalTo("namafav", namafav)
                    .equalTo("menutrx", menutrx)
                    .equalTo("submenutrx", submenutrx)
                    .equalTo("jenispembayaran", jenispembayaran)
                    .equalTo("data1", data1)
                    .findFirst();

            favTest = realm.where(Favorite.class)
                    .equalTo("namafav", namafav)
                    .equalTo("menutrx", menutrx)
                    .equalTo("submenutrx", submenutrx)
                    .equalTo("jenispembayaran", jenispembayaran)
                    .equalTo("data1", data1)
                    .findAll();

            if (submenutrx.equalsIgnoreCase("8009")) {
                String favData1 = fav.getData1();
                String kodebank = favData1.substring(0, favData1.indexOf("|"));
                String namaprioritybank = favData1.substring(favData1.indexOf("|") + 1, favData1.length());
                String namabank = namaprioritybank.substring(0, namaprioritybank.indexOf("|"));
                edit_kode_bank_fav.setText(kodebank + " - " + namabank);
            } else if (submenutrx.equalsIgnoreCase("7006") || submenutrx.equalsIgnoreCase("7008")) {
                String favData1 = fav.getData1();
                String kodeinstitusi = favData1.substring(0, favData1.indexOf("|"));
                String namainstitusi = favData1.substring(favData1.indexOf("|") + 1, favData1.length());
                edit_kode_bank_fav.setText(kodeinstitusi + " - " + namainstitusi);
            }
        } else if (submenutrx.equalsIgnoreCase("7007")) {
            fav = realm.where(Favorite.class)
                    .equalTo("namafav", namafav)
                    .equalTo("menutrx", menutrx)
                    .equalTo("submenutrx", submenutrx)
                    .equalTo("jenispembayaran", jenispembayaran)
                    .equalTo("data4", data4)
                    .findFirst();

            favTest = realm.where(Favorite.class)
                    .equalTo("namafav", namafav)
                    .equalTo("menutrx", menutrx)
                    .equalTo("submenutrx", submenutrx)
                    .equalTo("jenispembayaran", jenispembayaran)
                    .equalTo("data4", data4)
                    .findAll();

            String favData1 = fav.getData1();
            String kodeinstitusi = favData1.substring(0, favData1.indexOf("|"));
            String namainstitusi = favData1.substring(favData1.indexOf("|") + 1, favData1.length());
            edit_kode_bank_fav.setText(kodeinstitusi + " - " + namainstitusi);
        }
        else {
            fav = realm.where(Favorite.class)
                    .equalTo("namafav", namafav)
                    .equalTo("menutrx", menutrx)
                    .equalTo("jenispembayaran", jenispembayaran)
                    .equalTo("data1", data1)
                    .findFirst();

            favTest = realm.where(Favorite.class)
                    .equalTo("namafav", namafav)
                    .equalTo("menutrx", menutrx)
                    .equalTo("jenispembayaran", jenispembayaran)
                    .equalTo("data1", data1)
                    .findAll();
        }
        Log.d("TEST fav", String.valueOf(favTest));

        //String[] date = fav.getTimealarm().split(":");
        edit_label.setText(namafav);
        edit_detail.setText(fav.getData3());

        if (jenispembayaran.equalsIgnoreCase("DEBIT")) {
            edit_norek_bsa.setText(fav.getBsaacc());
        }
    }

    //    private boolean InsertUpdateFavdataTrf(String command) {
    private boolean InsertUpdateFavdataTrf(String command) {

        String bankLain = edit_kode_bank_fav.getText().toString();
        final String id = bankLain.substring(0, bankLain.indexOf(" -"));
        int posisi = 0;
        for (int i = 0; i < id_bank.length; i++) {
            if (id_bank[i].equalsIgnoreCase(id)) {
                posisi = i;
            }
        }
        idBankLain = id_bank[posisi];
        nameBankLain = name[posisi];
        networkPriorityBankLain = networkPriority[posisi];
        final String dataBank = idBankLain + "|" + nameBankLain + "|" + networkPriorityBankLain;

        if (command.equalsIgnoreCase("update")) {
            Log.d("Cek Update", "Mlebu");

            Thread thread = new Thread(new Runnable() {
                @Override
                public void run() {
                    Log.d("Cek Update", "Mlebu Run");
                    Realm backgroundRealm = Realm.getDefaultInstance();
                    backgroundRealm.executeTransactionAsync(new Realm.Transaction() {
                        @Override
                        public void execute(Realm realm) {
                            Favorite fav = realm.where(Favorite.class)
                                    .equalTo("menutrx", menutrx)
                                    .equalTo("submenutrx", submenutrx)
                                    .equalTo("jenispembayaran", jenispembayaran)
                                    .equalTo("namafav", namafav)
                                    .equalTo("data1", dataBank)
                                    .findFirst();

                            Log.d("Cek Update fav", fav.toString());

                            fav.setNamafav(edit_label.getText().toString());
                            fav.setData1(dataBank);
                            fav.setData3(edit_detail.getText().toString());
                            if (jenispembayaran.equalsIgnoreCase("DEBIT")) {
                                fav.setBsaacc(edit_norek_bsa.getText().toString());
                            }

                            FavoriteUpdateRequest favoriteUpdateRequest;

                            if (jenispembayaran.equalsIgnoreCase("DEBIT")) {
                                favoriteUpdateRequest = new FavoriteUpdateRequest(fav.getIdfav(), jenispembayaran, edit_norek_bsa.getText().toString(), dataBank, "", edit_detail.getText().toString(),
                                        "", edit_label.getText().toString(), AppUtil.getDsn(), menutrx, submenutrx, namajenisfav, "");
                            } else {
                                favoriteUpdateRequest = new FavoriteUpdateRequest(fav.getIdfav(), jenispembayaran, "", dataBank, "", edit_detail.getText().toString(),
                                        "", edit_label.getText().toString(), AppUtil.getDsn(), menutrx, submenutrx, namajenisfav, "");
                            }

                            realm.commitTransaction();

                            try {
                                ApiInterface apiInterface = AppUtil.getClientBrissmart().create(ApiInterface.class);
                                Call<ResponseBody> call = apiInterface.updateFavorit(favoriteUpdateRequest);
                                call.enqueue(new retrofit2.Callback<ResponseBody>() {
                                    @Override
                                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                                        Log.d("Response insertFavorite", response.body().toString());
                                    }

                                    @Override
                                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                                        getActivity().runOnUiThread(new Runnable() {
                                            public void run() {
                                                AppUtil.displayDialog(getContext(), "Gagal terhubung ke server");
                                            }
                                        });
                                        Log.d("Failure insertFavorite", t.toString());
                                    }
                                });
                            } catch (Exception e) {
                                Log.e("TESST catch", e.toString());
                                getActivity().runOnUiThread(new Runnable() {
                                    public void run() {
                                        AppUtil.displayDialog(getContext(), "Gagal terhubung ke server");
                                    }
                                });
                            }
                        }
                    });
                    backgroundRealm.close();
                }
            });
            thread.start();

        } else {
            Log.d("Cek Insert", "Mlebu");

            Thread thread = new Thread(new Runnable() {
                @Override
                public void run() {
                    Realm backgroundRealm = Realm.getDefaultInstance();
                            backgroundRealm.executeTransactionAsync(new Realm.Transaction() {
                                @Override
                                public void execute(Realm realm) {
                            Log.d("Cek Insert Thread", "Mlebu");

                            RealmQuery<Favorite> chkFav = realm.where(Favorite.class)
                                    .equalTo("menutrx", menutrx)
                                    .equalTo("submenutrx", submenutrx)
                                    .equalTo("jenispembayaran", jenispembayaran)
                                    .equalTo("data1", dataBank)
                                    .equalTo("data3", edit_detail.getText().toString());
                            RealmQuery<Favorite> favInEachCategory = realm.where(Favorite.class)
                                    .equalTo("menutrx", menutrx)
                                    .equalTo("submenutrx", submenutrx);

                            Log.d("Cek Realm Favorit", chkFav.toString());
                            Log.d("Cek Realm Favorit", favInEachCategory.toString());

                            if (favInEachCategory.count() >= AppUtil.MAX_FAVORIT_EACH_CATEG) {
                                AppUtil.showToastLong(getContext(), "Maaf Anda hanya bisa memiliki "
                                        + AppUtil.MAX_FAVORIT_EACH_CATEG + " favorit tiap kategorinya");
                            } else if (chkFav.count() > 0) {
                                AppUtil.displayDialog(getContext(), "Data favorit Telah Ada");
                            } else {
                                Log.d("Cek Insert True", "Mlebu");

//                                realm.beginTransaction();
                                Favorite fav = realm.createObject(Favorite.class);
                                fav.setJenispembayaran(jenispembayaran);
                                if (jenispembayaran.equalsIgnoreCase("DEBIT")) {
                                    fav.setBsaacc(edit_norek_bsa.getText().toString());
                                }
                                fav.setData1(dataBank);
                                fav.setData3(edit_detail.getText().toString());
                                fav.setNamafav(edit_label.getText().toString());
                                fav.setUseragen(AppUtil.getDsn());
                                fav.setMenutrx(menutrx);
                                fav.setSubmenutrx(submenutrx);
                                fav.setNamajnsfav(namajenisfav);

//                                realm.beginTransaction();
                                realm.commitTransaction();

                                Log.d("Cek Realm Favorit", fav.toString());
                            }
                        }
                    });
                    backgroundRealm.close();
                }
            });
            thread.start();

            FavoriteRequest favoriteRequest;
            if (jenispembayaran.equalsIgnoreCase("DEBIT")) {
                favoriteRequest = new FavoriteRequest(jenispembayaran, edit_norek_bsa.getText().toString(), dataBank, "", edit_detail.getText().toString(),
                        "", edit_label.getText().toString(), AppUtil.getDsn(), menutrx, submenutrx, namajenisfav, "");
            } else {
                favoriteRequest = new FavoriteRequest(jenispembayaran, "", dataBank, "", edit_detail.getText().toString(),
                        "", edit_label.getText().toString(), AppUtil.getDsn(), menutrx, submenutrx, namajenisfav, "");
            }

            try {
                ApiInterface apiInterface = AppUtil.getClientBrissmart().create(ApiInterface.class);
                Log.e("TESST catch", "TTTTTT 1");
                Call<ResponseBody> call = apiInterface.insertFavorit(favoriteRequest);
                Log.e("TESST catch", "TTTTTT 2");
                call.enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        Log.d("Response insertFavorite", response.body().toString());
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        Log.d("Failure insertFavorite", t.toString());
                        getActivity().runOnUiThread(new Runnable() {
                            public void run() {
                                AppUtil.displayDialog(getContext(), "Gagal terhubung ke server");
                            }
                        });
                    }
                });
            } catch (Exception e) {
                Log.e("TESST catch", e.toString());
                getActivity().runOnUiThread(new Runnable() {
                    public void run() {
                        AppUtil.displayDialog(getContext(), "Gagal terhubung ke server");
                    }
                });
            }
        }
        return true;
    }

    private boolean InsertUpdateFavdata(String namafav,
                                        String menutrx,
                                        String jenispembayaran,
                                        String command,
                                        String data3,
                                        String data1,
                                        String timealarm) {

        // update or insert
        if (command.equalsIgnoreCase("update")) {
            Favorite fav = realm.where(Favorite.class)
                    .equalTo("namafav", getArguments().getString("namafav"))
                    .equalTo("menutrx", menutrx)
                    .equalTo("jenispembayaran", jenispembayaran)
                    .equalTo("data1", data1)
                    .findFirst();

            realm.beginTransaction();
            fav.setNamafav(namafav);
            fav.setData3(data3);
            if (jenispembayaran.equalsIgnoreCase("DEBIT")) {
                fav.setBsaacc(edit_norek_bsa.getText().toString());
            }
            if (submenutrx.equalsIgnoreCase("7006") || submenutrx.equalsIgnoreCase("7007") || submenutrx.equalsIgnoreCase("7008")) {
                String bankLain = edit_kode_bank_fav.getText().toString();
                String id = bankLain.substring(0, bankLain.indexOf(" "));
                int posisi = 0;
                for (int i = 0; i < id_bank.length; i++) {
                    if (id_bank[i].equalsIgnoreCase(id)) {
                        posisi = i;
                    }
                }
                idBankLain = id_bank[posisi];
                nameBankLain = name[posisi];
                String dataBank = idBankLain + "|" + nameBankLain;
                fav.setData1(dataBank);
            }
            if (menutrx.equalsIgnoreCase("9000")) {
                fav.setData4(namafav);
            }

            try {
                FavoriteUpdateRequest favoriteUpdateRequest;
                if (jenispembayaran.equalsIgnoreCase("DEBIT")) {
                    favoriteUpdateRequest = new FavoriteUpdateRequest(fav.getIdfav(), jenispembayaran, edit_norek_bsa.getText().toString(), data1, "", edit_detail.getText().toString(),
                            data4, edit_label.getText().toString(), AppUtil.getDsn(), menutrx, submenutrx, namajenisfav, "");
                } else {
                    favoriteUpdateRequest = new FavoriteUpdateRequest(fav.getIdfav(), jenispembayaran, "", data1, "", edit_detail.getText().toString(),
                            data4, edit_label.getText().toString(), AppUtil.getDsn(), menutrx, submenutrx, namajenisfav, "");
                }
                ApiInterface apiInterface = AppUtil.getClientBrissmart().create(ApiInterface.class);
                Log.e("TESST catch", "TTTTTT 1");
                Call<ResponseBody> call = apiInterface.updateFavorit(favoriteUpdateRequest);
                Log.e("TESST catch", "TTTTTT 2");
                call.enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        Log.d("Response insertFavorite", String.valueOf(response.body()));
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        Log.d("Failure insertFavorite", t.toString());
                        getActivity().runOnUiThread(new Runnable() {
                            public void run() {
                                AppUtil.displayDialog(getContext(), "Gagal terhubung ke server");
                            }
                        });
                    }
                });
            } catch (Exception e) {
                Log.e("TESST catch", e.toString());
                getActivity().runOnUiThread(new Runnable() {
                    public void run() {
                        AppUtil.displayDialog(getContext(), "Gagal terhubung ke server");
                    }
                });
            }

            realm.commitTransaction();
        } else {
            // iki lo insert e minh
            RealmQuery<Favorite> chkFav = realm.where(Favorite.class)
                    .equalTo("menutrx", menutrx)
                    .equalTo("jenispembayaran", jenispembayaran)
                    .equalTo("data1", data1)
//                    .equalTo("namafav", namafav)
                    .equalTo("data3", data3);

            RealmQuery<Favorite> favInEachCategory = realm.where(Favorite.class)
                    .equalTo("menutrx", menutrx);

            if (chkFav.count() > 0) {
                AppUtil.displayDialog(getContext(), "Data favorit Telah Ada");
                return false;
            }
            if (favInEachCategory.count() >= AppUtil.MAX_FAVORIT_EACH_CATEG) {
                AppUtil.showToastLong(getContext(), "Maaf Anda hanya bisa memiliki "
                        + AppUtil.MAX_FAVORIT_EACH_CATEG + " favorit tiap kategorinya");
                return false;
            }

            realm.beginTransaction();
            Favorite fav = realm.createObject(Favorite.class);
            fav.setJenispembayaran(jenispembayaran);
            if (jenispembayaran.equalsIgnoreCase("DEBIT")) {
                fav.setBsaacc(edit_norek_bsa.getText().toString());
            }
            if (submenutrx.equalsIgnoreCase("7006") || submenutrx.equalsIgnoreCase("7007") || submenutrx.equalsIgnoreCase("7008")) {
                String bankLain = edit_kode_bank_fav.getText().toString();
                String id = bankLain.substring(0, bankLain.indexOf(" "));
                int posisi = 0;
                for (int i = 0; i < id_bank.length; i++) {
                    if (id_bank[i].equalsIgnoreCase(id)) {
                        posisi = i;
                    }
                }
                idBankLain = id_bank[posisi];
                nameBankLain = name[posisi];
                String dataBank = idBankLain + "|" + nameBankLain;
                data1 = dataBank;
            }
            fav.setData1(data1);
            if (menutrx.equalsIgnoreCase("9000")) {
                data4 = namafav;
            }
            fav.setData4(data4);
            fav.setData3(data3);
            fav.setNamafav(namafav);
            fav.setUseragen(AppUtil.getDsn());
            fav.setMenutrx(menutrx);
            fav.setSubmenutrx(submenutrx);
            fav.setNamajnsfav(namajenisfav);
            realm.commitTransaction();

            try {

                FavoriteRequest favoriteRequest;
                if (jenispembayaran.equalsIgnoreCase("DEBIT")) {
                    favoriteRequest = new FavoriteRequest(jenispembayaran, edit_norek_bsa.getText().toString(), data1, "", edit_detail.getText().toString(),
                            data4, edit_label.getText().toString(), AppUtil.getDsn(), menutrx, submenutrx, namajenisfav, "");
                } else {
                    favoriteRequest = new FavoriteRequest(jenispembayaran, "", data1, "", edit_detail.getText().toString(),
                            data4, edit_label.getText().toString(), AppUtil.getDsn(), menutrx, submenutrx, namajenisfav, "");
                }
                ApiInterface apiInterface = AppUtil.getClientBrissmart().create(ApiInterface.class);
                Log.e("TESST catch", "TTTTTT 1");
                Call<ResponseBody> call = apiInterface.insertFavorit(favoriteRequest);
                Log.e("TESST catch", "TTTTTT 2");
                call.enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        Log.d("Response insertFavorite", String.valueOf(response.body()));
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        Log.d("Failure insertFavorite", t.toString());
                        getActivity().runOnUiThread(new Runnable() {
                            public void run() {
                                AppUtil.displayDialog(getContext(), "Gagal terhubung ke server");
                            }
                        });
                    }
                });
            } catch (Exception e) {
                Log.e("TESST catch", e.toString());
                getActivity().runOnUiThread(new Runnable() {
                    public void run() {
                        AppUtil.displayDialog(getContext(), "Gagal terhubung ke server");
                    }
                });
            }
        }
        return true;
    }

    public static void hideKeyboard(Activity activity) {
        InputMethodManager inputManager = (InputMethodManager) activity
                .getSystemService(Context.INPUT_METHOD_SERVICE);

        // check if no view has focus:
        View currentFocusedView = activity.getCurrentFocus();
        if (currentFocusedView != null) {
            inputManager.hideSoftInputFromWindow(currentFocusedView.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

    private void btnOkAction() {
        final String command;
        final String hour;
        final String minute;
        String reminderformattime = "00:00";

        // check for error input
        if (formValidation() > 0) return;

        if (btnOK.getText().toString().equalsIgnoreCase("Ubah")) {
            command = "update";
        } else {
            command = "insert";
        }

        hideKeyboard(getActivity());

        AlertDialog dialogAlert = null;
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setCancelable(false);
        builder.setMessage("Apakah Anda Yakin Untuk " + btnOK.getText() + " Data ?");
        final String finalReminderformattime = reminderformattime;
        builder.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface alertdialog, int which) {
                alertdialog.cancel();

                AppUtil.initDialogProgress(getContext());
                AppUtil.showDialog();

                updateLocalFav(command, finalReminderformattime);

                AppUtil.hideDialog();
                FragmentManager fm = getFragmentManager();
                fm.popBackStack();

            }
        });

        builder.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface alertdialog, int which) {
                alertdialog.cancel();
            }
        });

        if (dialogAlert == null || !dialogAlert.isShowing()) {
            dialogAlert = builder.create();
            dialogAlert.show();
        }
    }

    private void updateLocalFav(String command, String finalReminderformattime) {
        if (menutrx.equalsIgnoreCase("8000") && submenutrx.equalsIgnoreCase("8001")) {
            if (!InsertUpdateFavdata(edit_label.getText().toString(),
                    menutrx, jenispembayaran, command, edit_detail.getText().toString(), data1, finalReminderformattime)) {
                return;
            }
        }
        if (menutrx.equalsIgnoreCase("8000") && submenutrx.equalsIgnoreCase("8002")) {
            if (!InsertUpdateFavdata(edit_label.getText().toString(),
                    menutrx, jenispembayaran, command, edit_detail.getText().toString(), data1, finalReminderformattime)) {
                return;
            }
        } else if (menutrx.equalsIgnoreCase("8000") && submenutrx.equalsIgnoreCase("8009")) {
            if (!InsertUpdateFavdataTrf(command)) {
                return;
            }
        } else if (menutrx.equalsIgnoreCase("6000")) {
            if (!InsertUpdateFavdata(edit_label.getText().toString(),
                    menutrx, jenispembayaran, command, edit_detail.getText().toString(), data1, finalReminderformattime)) {
                return;
            }
        } else if (menutrx.equalsIgnoreCase("7000")) {
            if (submenutrx.equalsIgnoreCase("7006") || submenutrx.equalsIgnoreCase("7007") || submenutrx.equalsIgnoreCase("7008")) {
                String bankLain = edit_kode_bank_fav.getText().toString();
                String id = bankLain.substring(0, bankLain.indexOf(" "));
                int posisi = 0;
                for (int i = 0; i < id_bank.length; i++) {
                    if (id_bank[i].equalsIgnoreCase(id)) {
                        posisi = i;
                    }
                }
                idBankLain = id_bank[posisi];
                nameBankLain = name[posisi];
                String dataBank = idBankLain + "|" + nameBankLain;
                data1 = dataBank;
            }
            if (!InsertUpdateFavdata(edit_label.getText().toString(),
                    menutrx, jenispembayaran, command, edit_detail.getText().toString(), data1, finalReminderformattime)) {
                return;
            }
        } else if (menutrx.equalsIgnoreCase("1000")) {
            if (!InsertUpdateFavdata(edit_label.getText().toString(),
                    menutrx, jenispembayaran, command, edit_detail.getText().toString(), data1, finalReminderformattime)) {
                return;
            }
        } else if (menutrx.equalsIgnoreCase("9000")) {
            if (!InsertUpdateFavdata(edit_label.getText().toString(),
                    menutrx, jenispembayaran, command, edit_detail.getText().toString(), data1, finalReminderformattime)) {
                return;
            }
        }
    }

    private int formValidation() {
        int errorCount = 0;
        if (edit_label.getText().length() == 0) {
            til_label.setError(til_label.getHint() + "tidak boleh kosong");
            errorCount++;
        } else if (edit_detail.getText().length() == 0) {
            til_detail.setError(til_detail.getHint() + " Kosong");
            errorCount++;
        }

        if (jenispembayaran.equalsIgnoreCase("DEBIT")) {
            if (edit_norek_bsa.getText().length() == 0) {
                til_norek_bsa.setError(til_norek_bsa.getHint() + " Kosong");
                errorCount++;
            }
        }

        return errorCount;
    }

    private void customToolbar() {
        Toolbar toolbar = (Toolbar) view.findViewById(R.id.tb_credential_page);
        TextView tv_toolbar = (TextView) view.findViewById(R.id.tv_tb_credential_page);
        tv_toolbar.setText(title);
        toolbar.setNavigationIcon(R.mipmap.abc_ic_ab_back_mtrl_am_alpha);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().onBackPressed();
            }
        });
    }
}