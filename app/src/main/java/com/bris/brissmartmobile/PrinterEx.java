package com.bris.brissmartmobile;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.DashPathEffect;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.RemoteException;
import android.util.Log;

import com.vfi.smartpos.deviceservice.aidl.FontFamily;
import com.vfi.smartpos.deviceservice.aidl.PrinterConfig;

import java.io.ByteArrayOutputStream;

/**
 * Created by Simon on 2019/2/20.
 */

public class PrinterEx {

    private static final String TAG = "PrinterEx";
    private static final int MAX_WIDTH = 384;

    int offsetY;
    int offsetX;
    Canvas canvas;
    Bitmap bitmap;
    Paint paint;

    public PrinterEx() {

        offsetY = 0;
        offsetX = 0;

        paint = new Paint();
        paint.setStrokeWidth(1);
        paint.setTextSize(50);
        paint.setColor(Color.BLACK);

        bitmap = Bitmap.createBitmap(MAX_WIDTH, MAX_WIDTH * 12, Bitmap.Config.ARGB_8888);
        bitmap.eraseColor(Color.WHITE);

        canvas = new Canvas(bitmap);
        writeRuler(0);
    }

    public int addText(String text, int fontSize, boolean isBold, int alignType) throws RemoteException {
        boolean isNewLine = true;
        if (isBold){
            Typeface typeface = Typeface.create(Typeface.createFromFile(PrinterFonts.path + PrinterFonts.FONT_MONO), Typeface.BOLD);
            paint.setTypeface(typeface);
        } else {
            paint.setTypeface(Typeface.createFromFile(PrinterFonts.path + PrinterFonts.FONT_MONO));
        }
        Paint.Align align = Paint.Align.LEFT;
        int x = 0;
        if (alignType == FontFamily.CENTER) {
            align = Paint.Align.CENTER;
            x = MAX_WIDTH / 2;
        } else if (alignType == FontFamily.RIGHT) {
            align = Paint.Align.RIGHT;
            x = MAX_WIDTH;
        }

        paint.setTextSize(fontSize);
        paint.setTextAlign(align);
        offsetY += fontSize;
        int width = (int) paint.measureText( text,0, text.length() );
        Log.d(TAG, "pixel "+width+" of " + text + ", try print at [" + offsetX + ", " + offsetY + "]");
        switch (alignType){
            case FontFamily.LEFT:
                if( width + offsetX > MAX_WIDTH ) {
                    Log.d(TAG, "Over size, write in new line");
                    offsetY += fontSize;
                    offsetX = 0;
                } else {
                    offsetX += width;
                }
                break;
            case FontFamily.CENTER:
                if( width + offsetX*2 > MAX_WIDTH ) {
                    Log.d(TAG, "Over size, write in new line");
                    offsetY += fontSize;
                    offsetX = 0;
                } else {
                    offsetX = (MAX_WIDTH/2+width/2);
                }
                break;
            case FontFamily.RIGHT:
                if( width + offsetX > MAX_WIDTH ) {
                    Log.d(TAG, "Over size, write in new line");
                    offsetY += fontSize;
                    offsetX = 0;
                } else {
                    offsetX = MAX_WIDTH;
                }
                break;

        }
        canvas.drawText(text, x, offsetY, paint);
        if( isNewLine ) {
            offsetX = 0;
            offsetY += 2;
        } else {
            offsetY -= fontSize;
        }
        return offsetX;
    }

    public int addText(Bundle fontFormat, String text) throws RemoteException {
        String fontStyle = fontFormat.getString("fontStyle", "");

        //
        boolean isBoldFont = fontFormat.getBoolean("bold", false);
        boolean isNewLine = fontFormat.getBoolean("newline", true);
        int offset = fontFormat.getInt("offset", 0);
        int typeFaceStyle = Typeface.NORMAL;


        int fontSize = fontFormat.getInt(PrinterConfig.BUNDLE_PRINT_FONT);

        if (fontSize == FontFamily.MIDDLE) {
            fontSize = 24;
        }
        else if (fontSize == FontFamily.SMALL) {
            fontSize = 16;
        } else if (fontSize == FontFamily.BIG) {
            fontSize = 24;
//            entity.setMultipleHeight(2);
//            entity.setMultipleWidth(1);
            fontSize = 28;
            paint.setTextScaleX(0.5f);
            isBoldFont = true;
        } else if (fontSize == 3) {
            fontSize = 32;
        } else if (fontSize == 4) {
            fontSize = 32;
//            entity.setMultipleHeight(2);
//            entity.setMultipleWidth(1);
            isBoldFont = true;
        } else if (fontSize == 5) {
            fontSize = 48;
        }

        if( isBoldFont ){
            typeFaceStyle = Typeface.BOLD;
        }

        Log.d(TAG, "fontStyle:" + fontStyle );
        paint.setTypeface(Typeface.createFromFile(fontStyle));

        int alignType = fontFormat.getInt(PrinterConfig.BUNDLE_PRINT_ALIGN);
        Paint.Align align = Paint.Align.LEFT;
        int x = 0;
        if (alignType == FontFamily.CENTER) {
            align = Paint.Align.CENTER;
            x = MAX_WIDTH / 2;
        } else if (alignType == FontFamily.RIGHT) {
            align = Paint.Align.RIGHT;
            x = MAX_WIDTH;
        }

        paint.setTextSize(fontSize);
        paint.setTextAlign(align);
        offsetY += fontSize;
        int width = (int) paint.measureText( text,0, text.length() );
        Log.d(TAG, "pixel "+width+" of " + text + ", try print at [" + offsetX + ", " + offsetY + "]");
        switch (alignType){
            case FontFamily.LEFT:
                if( width + offsetX > MAX_WIDTH ) {
                    Log.d(TAG, "Over size, write in new line");
                    offsetY += fontSize;
                    offsetX = 0;
                } else {
                    offsetX += width;
                }
                break;
            case FontFamily.CENTER:
                if( width + offsetX*2 > MAX_WIDTH ) {
                    Log.d(TAG, "Over size, write in new line");
                    offsetY += fontSize;
                    offsetX = 0;
                } else {
                    offsetX = (MAX_WIDTH/2+width/2);
                }
                break;
            case FontFamily.RIGHT:
                if( width + offsetX > MAX_WIDTH ) {
                    Log.d(TAG, "Over size, write in new line");
                    offsetY += fontSize;
                    offsetX = 0;
                } else {
                    offsetX = MAX_WIDTH;
                }
                break;

        }
        canvas.drawText(text, x, offsetY, paint);
        if( isNewLine ) {
            offsetX = 0;
            offsetY += 2;
        } else {
            offsetY -= fontSize;
        }
        return offsetX;
    }

    public void addNewLine(int fontSize){
        offsetY += fontSize;
    }

    public void addTextInLine(Bundle fontFormat, String left, String center, String right, int mode) throws RemoteException {

    }
    public void addImage(Bundle format, byte[] imageData) throws RemoteException {
        Bitmap bitmap = BitmapFactory.decodeByteArray(imageData, 0, imageData.length );
        addImage( format, bitmap );
    }
    public void addImage(Bundle format, Bitmap bitmap) throws RemoteException {
        int offset = format.getInt("offset");

        canvas.drawBitmap( bitmap, offset, offsetY, null );
        offsetY += bitmap.getHeight();
        offsetY += 2;

    }
    public void addLine(Bundle format, int width ) throws RemoteException {
        paint.setPathEffect(null);
        paint.setStrokeWidth(width);
        canvas.drawLine(1,offsetY,MAX_WIDTH-1, offsetY, paint );
        offsetY += width;
        offsetY += 2;
    }
    public void writeRuler( int mode ){
        int x = 0;
        int y = 0;
        paint.setStrokeWidth(1);
        int c = paint.getColor();
        paint.setColor(Color.GREEN);
        paint.setPathEffect(new DashPathEffect(new float[]{16f,16f}, 0));
        for( x = -8; x <= MAX_WIDTH;  ){
            canvas.drawLine(x,0,x, MAX_WIDTH, paint );
            canvas.drawLine(0,x,MAX_WIDTH, x, paint );

            x+= 32;
        }
        paint.setColor(c);
    }

    public Bitmap getBitmap(){
        Bitmap newBitmap = Bitmap.createBitmap(bitmap, 0,0,MAX_WIDTH, offsetY);
        return newBitmap;
    }
    public int getHeight() {
        return offsetY;
    }

    public byte[] getData() {
        return getBytesByBitmap(bitmap);
    }

    public byte[] getBytesByBitmap(Bitmap bitmap) {

        Bitmap newBitmap = Bitmap.createBitmap(bitmap, 0,0,MAX_WIDTH, offsetY);

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        newBitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        return baos.toByteArray();

    }


}
