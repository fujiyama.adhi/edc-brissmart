package com.bris.brissmartmobile.promo;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.LinearLayout;

import com.bris.brissmartmobile.R;
import com.bris.brissmartmobile.model.BankingPromoResponse;
import com.bris.brissmartmobile.util.ApiClientAdapter;
import com.bris.brissmartmobile.util.AppUtil;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by valeputra on 7/18/17.
 */

public class PromoActivity extends AppCompatActivity {
    private RecyclerView rvPromo;
    private PromoAdapter adapter;
    private LinearLayoutManager layoutManager;
    private Toolbar tb_promo;
    private ApiClientAdapter apiClientAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_apromo);
        rvPromo = (RecyclerView) findViewById(R.id.lv_promo);

        AppUtil.customMainFonts(getAssets(), this.findViewById(android.R.id.content));
        customToolbar();

        getPromoList();
    }

    private void getPromoList() {
        AppUtil.initDialogProgress(PromoActivity.this);

        apiClientAdapter = new ApiClientAdapter(PromoActivity.this);
        Call<BankingPromoResponse> call = apiClientAdapter.getApiInterface().getPromo("all");
        call.enqueue(new Callback<BankingPromoResponse>() {
            @Override
            public void onResponse(Call<BankingPromoResponse> call, Response<BankingPromoResponse> response) {
                AppUtil.hideDialog();
//                dialog.hideDialog();
                if (response.body().getData().size() > 0) {
                    List<BankingPromoResponse.DataPromo> mPromo = response.body().getData();
                    adapter = new PromoAdapter(mPromo);
                    layoutManager = new LinearLayoutManager(PromoActivity.this, LinearLayoutManager.VERTICAL, false);
                    rvPromo.setLayoutManager(layoutManager);
                    rvPromo.setAdapter(adapter);
                } else {
                    // no promo registered now
                    LinearLayout lempty = (LinearLayout) findViewById(R.id.content_empty);
                    lempty.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onFailure(Call<BankingPromoResponse> call, Throwable t) {
                AppUtil.hideDialog();
//                dialog.hideDialog();
                LinearLayout lfailure = (LinearLayout) findViewById(R.id.connection_failure);
                lfailure.setVisibility(View.VISIBLE);
            }
        });
    }

    private void customToolbar() {
        tb_promo = (Toolbar) findViewById(R.id.tb_info_promo);
        tb_promo.setNavigationIcon(R.mipmap.abc_ic_ab_back_mtrl_am_alpha);
        setSupportActionBar(tb_promo);
        tb_promo.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

}
