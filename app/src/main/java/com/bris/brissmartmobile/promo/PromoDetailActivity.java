package com.bris.brissmartmobile.promo;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bris.brissmartmobile.R;
import com.squareup.picasso.Picasso;

/**
 * Created by ryputra on 07/03/2018.
 */

public class PromoDetailActivity extends AppCompatActivity {
    public String title, imgUrl, detail, createAt, validUntil, due, pageCategory;
    private TextView tv_title, tv_detail, tv_valid_until;
    private ImageView iv_promo_detail;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_apromo_detail);
        iv_promo_detail = (ImageView) findViewById(R.id.iv_promo_detail);
        tv_title = (TextView) findViewById(R.id.tv_promo_detail_title);
        tv_detail = (TextView) findViewById(R.id.tv_promo_detail_detail);
        tv_valid_until = (TextView) findViewById(R.id.tv_promo_valid_until);
        customToolbar();

        // get data bundle
        Bundle bundle = getIntent().getExtras() ;
        title = bundle.getString("promo_title");
        imgUrl = bundle.getString("promo_imgUrl");
        detail = bundle.getString("promo_detail");
        createAt = bundle.getString("promo_create_at");
        validUntil = bundle.getString("promo_valid_until");
        due = bundle.getString("promo_due");
        pageCategory = bundle.getString("category");

        main();
    }

    private void main() {
        Picasso.with(PromoDetailActivity.this)
                .load(imgUrl)
                .placeholder(R.drawable.banner_placeholder)
                .into(iv_promo_detail);
        tv_title.setText(title);
        tv_detail.setText(detail);
        tv_valid_until.setText(validUntil);
    }

    public void customToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.tb_regular);
        ImageView btnBack = (ImageView) findViewById(R.id.btn_back);
        TextView tvPageTitle = (TextView) findViewById(R.id.tv_page_title);

        tvPageTitle.setText("BRIS Promo");
        setSupportActionBar(toolbar);

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }
}
