package com.bris.brissmartmobile.promo;

/**
 * Created by valeputra on 7/4/17.
 */

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bris.brissmartmobile.R;
import com.bris.brissmartmobile.model.BankingPromoResponse;
import com.squareup.picasso.Picasso;

import java.util.List;

public class PromoAdapter extends RecyclerView.Adapter<PromoAdapter.ViewHolder> {
    private List<BankingPromoResponse.DataPromo> mPromo;
    private View view;

    public PromoAdapter(List<BankingPromoResponse.DataPromo> mPromo) {
        this.mPromo = mPromo;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.content_apromo, parent, false);
        ViewHolder vh = new ViewHolder(view);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        BankingPromoResponse.DataPromo item = mPromo.get(position);
        holder.bindPromo(item);
    }

    @Override
    public int getItemCount() {
        return mPromo.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView tvTitle;
        private ImageView ivPromo;
        private BankingPromoResponse.DataPromo mPromo;

        public ViewHolder(View itemView) {
            super(itemView);
            tvTitle = (TextView) itemView.findViewById(R.id.tv_promo_title);
            ivPromo = (ImageView) itemView.findViewById(R.id.iv_promo);
            itemView.setOnClickListener(this);
        }

        public void bindPromo(BankingPromoResponse.DataPromo item) {
            mPromo = item;
            Picasso.with(ivPromo.getContext())
                    .load(item.getImgUrl())
                    .placeholder(R.drawable.banner_placeholder)
                    .into(ivPromo);
            tvTitle.setText(item.getTitle());
        }

        @Override
        public void onClick(View v) {
            Context context = itemView.getContext();
            Intent intentPromoDetail = new Intent(context, PromoDetailActivity.class);
            intentPromoDetail.putExtra("promo_title", mPromo.getTitle());
            intentPromoDetail.putExtra("promo_imgUrl", mPromo.getImgUrl());
            intentPromoDetail.putExtra("promo_detail", mPromo.getDetail());
            intentPromoDetail.putExtra("category", "");
            context.startActivity(intentPromoDetail);
        }
    }
}