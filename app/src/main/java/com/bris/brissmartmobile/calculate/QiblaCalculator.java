package com.bris.brissmartmobile.calculate;

import java.text.ParseException;
import java.util.Locale;

/**
 * Created by ryputra on 18/10/2017.
 */

public class QiblaCalculator
{

    public static  double  MECCA_LATITUDE = 21.41666;
    public static double  MECCA_LONGITUDE = 39.81666;

    public static double doCalculate(double cityLat,double cityLong)
    {
        double a = cityLat ;
        double b = MECCA_LATITUDE;
        double c = (cityLong - MECCA_LONGITUDE);

        double cotan = (MyMath.dSin(a)*MyMath.dCos(c) - MyMath.dCos(a) * MyMath.dTan(b)) / MyMath.dSin(c);
        double newAngle = MyMath.dACot(cotan);

        if(cityLong > MECCA_LONGITUDE)
            newAngle += 180;

        double qiblaDegree = 0.0;

        qiblaDegree = getQiblaDegree(qiblaDegree, newAngle);

        return qiblaDegree;
    }

    private static double getQiblaDegree(double qiblaDegree, double newAngle) {
        try {
            qiblaDegree = Double.parseDouble(String.format("%.1f", newAngle));
        } catch (Exception e) {
            qiblaDegree = getQiblaDegreeParse(qiblaDegree, newAngle);
        }

        return qiblaDegree;
    }

    private static double getQiblaDegreeParse(double qiblaDegree, double newAngle) {
        java.text.NumberFormat nf = java.text.NumberFormat.getInstance(Locale.FRANCE);
        Number number = null;

        try {
            number = nf.parse(String.format("%.1f", new Double(newAngle)));
            qiblaDegree = number.doubleValue();
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return qiblaDegree;
    }

}

