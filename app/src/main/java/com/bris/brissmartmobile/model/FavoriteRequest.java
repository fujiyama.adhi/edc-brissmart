package com.bris.brissmartmobile.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by akbaranjas on 31/03/17.
 */

public class FavoriteRequest {

    @SerializedName("jenispembayaran")
    private String jenispembayaran;
    @SerializedName("bsaacc")
    private String bsaacc;
    @SerializedName("data1")
    private String data1;
    @SerializedName("data2")
    private String data2;
    @SerializedName("data3")
    private String data3;
    @SerializedName("data4")
    private String data4;
    @SerializedName("namafav")
    private String namafav;
    @SerializedName("useragen")
    private String useragen;
    @SerializedName("menutrx")
    private String menutrx;
    @SerializedName("submenutrx")
    private String submenutrx;
    @SerializedName("namajnsfav")
    private String namajnsfav;
    @SerializedName("tanggalfav")
    private String tanggalfav;

    public FavoriteRequest() {}

    public FavoriteRequest(String jenispembayaran, String bsaacc, String data1, String data2, String data3, String data4, String namafav, String useragen, String menutrx, String submenutrx, String namajnsfav, String tanggalfav) {
        this.jenispembayaran = jenispembayaran;
        this.bsaacc = bsaacc;
        this.data1 = data1;
        this.data2 = data2;
        this.data3 = data3;
        this.data4 = data4;
        this.namafav = namafav;
        this.useragen = useragen;
        this.menutrx = menutrx;
        this.submenutrx = submenutrx;
        this.namajnsfav = namajnsfav;
        this.tanggalfav = tanggalfav;
    }

    public String getJenispembayaran() {
        return jenispembayaran;
    }

    public void setJenispembayaran(String jenispembayaran) {
        this.jenispembayaran = jenispembayaran;
    }

    public String getBsaacc() {
        return bsaacc;
    }

    public void setBsaacc(String bsaacc) {
        this.bsaacc = bsaacc;
    }

    public String getData1() {
        return data1;
    }

    public void setData1(String data1) {
        this.data1 = data1;
    }

    public String getData2() {
        return data2;
    }

    public void setData2(String data2) {
        this.data2 = data2;
    }

    public String getData3() {
        return data3;
    }

    public void setData3(String data3) {
        this.data3 = data3;
    }

    public String getData4() {
        return data4;
    }

    public void setData4(String data4) {
        this.data4 = data4;
    }

    public String getNamafav() {
        return namafav;
    }

    public void setNamafav(String namafav) {
        this.namafav = namafav;
    }

    public String getUseragen() {
        return useragen;
    }

    public void setUseragen(String useragen) {
        this.useragen = useragen;
    }

    public String getMenutrx() {
        return menutrx;
    }

    public void setMenutrx(String menutrx) {
        this.menutrx = menutrx;
    }

    public String getSubmenutrx() {
        return submenutrx;
    }

    public void setSubmenutrx(String submenutrx) {
        this.submenutrx = submenutrx;
    }

//    public String getKodeproduk() {
//        return kodeproduk;
//    }
//
//    public void setKodeproduk(String kodeproduk) {
//        this.kodeproduk = kodeproduk;
//    }

    public String getNamajnsfav() {
        return namajnsfav;
    }

    public void setNamajnsfav(String namajnsfav) {
        this.namajnsfav = namajnsfav;
    }

    public String getTanggalfav() {
        return tanggalfav;
    }

    public void setTanggalfav(String tanggalfav) {
        this.tanggalfav = tanggalfav;
    }
}
