package com.bris.brissmartmobile.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ryputra on 16/01/2018.
 */

public class OAuthGrantAccessResponse {
    @SerializedName("status")
    public String status;
    @SerializedName("message")
    public String message;
    @SerializedName("data")
    public DataToken dataToken;

    public String getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }

    public DataToken getDataToken() {
        return dataToken;
    }

    public class DataToken {
        @SerializedName("token_type")
        public String tokenType;
        @SerializedName("access_token")
        public String accessToken;
        @SerializedName("refresh_token")
        public String refreshToken;
        @SerializedName("expires_in")
        public String expiresIn;

        public String getTokenType() {
            return tokenType;
        }
        public String getAccessToken() {
            return accessToken;
        }
        public String getRefreshToken() {
            return refreshToken;
        }
        public String getExpiresIn() {
            return expiresIn;
        }
    }
}
