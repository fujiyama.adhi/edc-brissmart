package com.bris.brissmartmobile.model;

/**
 * Created by ryputra on 18/10/2017.
 */

public class LocationInfo {
    public double longitude , latitude;
    public int number , mazhab , way , dls , timeZone;
    public String country , country_code , locality , city ;

    public LocationInfo(double latitude , double longitude , String country ,
                        String country_code , String city , String locality,
                        int mazhab , int way , int dls , int timeZone)
    {
        this.longitude = longitude ;
        this.latitude  = latitude ;
        this.country = country ;
        this.country_code = country_code;
        this.city = city ;
        this.locality = locality ;
        this.mazhab = mazhab ;
        this.way = way ;
        this.dls = dls ;
        this.timeZone = timeZone ;
    }
}

