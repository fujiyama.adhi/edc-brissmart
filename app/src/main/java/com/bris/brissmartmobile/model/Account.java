package com.bris.brissmartmobile.model;

import io.realm.RealmObject;
import io.realm.annotations.Required;

/**
 * Created by akbaranjas on 23/03/17.
 */

public class Account extends RealmObject {

    @Required
    private String phone_id;
    @Required
    private String alias;
    @Required
    private String account;

    public Account() {
        super();
    }

    public String getPhone_id() {
        return phone_id;
    }

    public void setPhone_id(String phone_id) {
        this.phone_id = phone_id;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    @Override
    public String toString() {
        return alias + " (" + account + ")";
    }
}
