package com.bris.brissmartmobile.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by ryputra on 23/01/2018.
 */

public class BankingPromoResponse {
    @SerializedName("status")
    private int status;
    @SerializedName("message")
    private String message;
    @SerializedName("data")
    private List<DataPromo> data;

    public int getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }

    public List<DataPromo> getData() {
        return data;
    }

    public class DataPromo {
        @SerializedName("promo_id")
        private int id;
        @SerializedName("promo_title")
        private String title;
        @SerializedName("promo_img_url")
        private String imgUrl;
        @SerializedName("promo_detail")
        private String detail;

        public int getId() {
            return id;
        }

        public String getTitle() {
            return title;
        }

        public String getImgUrl() {
            return imgUrl;
        }

        public String getDetail() {
            return detail;
        }
    }
}