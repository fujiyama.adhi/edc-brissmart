package com.bris.brissmartmobile.model;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.Required;

/**
 * Created by valeputra on 8/1/17.
 */

public class Surah extends RealmObject {

   /* @PrimaryKey
    private long id;*/
    public String noSurah;
    @Required
    public String surah;
    @Required
    public String pathSurah;
    public RealmList<Ayat> ayats;

    public Surah() {}

    public Surah(String noSurah, String surah, String pathSurah) {
        this.noSurah = noSurah;
        this.surah = surah;
        this.pathSurah = pathSurah;
    }

   /* public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }*/

    public String getNoSurah() {
        return noSurah;
    }

    public void setNoSurah(String noSurah) {
        this.noSurah = noSurah;
    }

    public String getSurah() {
        return surah;
    }

    public void setSurah(String surah) {
        this.surah = surah;
    }

    public String getPathSurah() {
        return pathSurah;
    }

    public void setPathSurah(String pathSurah) {
        this.pathSurah = pathSurah;
    }

    public RealmList<Ayat> getAyats() {
        return ayats;
    }

    public void setAyats(RealmList<Ayat> ayats) {
        this.ayats = ayats;
    }
}
