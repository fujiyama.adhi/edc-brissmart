package com.bris.brissmartmobile.model;

/**
 * Created by ryputra on 22/09/2017.
 */

public class CustomMarker {
    private String mLabel;
   // private String mDistance;
    private String mAddress;
    private String mIcon;
    private Double mLatitude;
    private Double mLongitude;

    public CustomMarker(String label, String address, String icon, Double latitude, Double longitude)
    {
        this.mLabel = label;
        this.mAddress = address;
        //this.mDistance = distance;
        this.mLatitude = latitude;
        this.mLongitude = longitude;
        this.mIcon = icon;
    }

    public String getmLabel() {
        return mLabel;
    }

    public void setmLabel(String mLabel)
    {
        this.mLabel = mLabel;
    }

//    public String getmDistance() {
//        return mDistance;
//    }
//
//    public void setmDistance(String mDistance) {
//        this.mDistance = mDistance;
//    }

    public String getmIcon()
    {
        return mIcon;
    }

    public void setmIcon(String icon)
    {
        this.mIcon = icon;
    }

    public Double getmLatitude()
    {
        return mLatitude;
    }

    public void setmLatitude(Double mLatitude)
    {
        this.mLatitude = mLatitude;
    }

    public Double getmLongitude()
    {
        return mLongitude;
    }

    public void setmLongitude(Double mLongitude)
    {
        this.mLongitude = mLongitude;
    }

    public String getmAddress() {
        return mAddress;
    }

    public void setmAddress(String mAddress) {
        this.mAddress = mAddress;
    }
}
