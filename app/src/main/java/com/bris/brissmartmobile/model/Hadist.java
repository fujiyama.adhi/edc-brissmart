package com.bris.brissmartmobile.model;

import io.realm.RealmObject;
import io.realm.annotations.Required;

/**
 * Created by valeputra on 7/27/17.
 */

public class Hadist extends RealmObject {
    @Required
    public String posid;
    @Required
    public String ayat;
    @Required
    public String latin;
    @Required
    public String arti;
    @Required
    public String periwayat;

    public String getPosid() {
        return posid;
    }

    public void setPosid(String posid) {
        this.posid = posid;
    }

    public String getAyat() {
        return ayat;
    }

    public void setAyat(String ayat) {
        this.ayat = ayat;
    }

    public String getLatin() {
        return latin;
    }

    public void setLatin(String latin) {
        this.latin = latin;
    }

    public String getArti() {
        return arti;
    }

    public void setArti(String arti) {
        this.arti = arti;
    }

    public String getPeriwayat() {
        return periwayat;
    }

    public void setPeriwayat(String periwayat) {
        this.periwayat = periwayat;
    }
}
