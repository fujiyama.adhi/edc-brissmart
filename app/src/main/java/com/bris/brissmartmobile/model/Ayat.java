package com.bris.brissmartmobile.model;

import io.realm.RealmObject;

/**
 * Created by valeputra on 8/2/17.
 */

public class Ayat extends RealmObject {

    public String number;
    public String text;
    public String numberInSurah;
    public String juz;
    public String page;
    public String ruku;
    public String hizbQuarter;
    public String meaning;
    public boolean sajda;

    public Ayat () {}

    public Ayat(String number, String text, String numberInSurah, String meaning) {
        this.number = number;
        this.text = text;
        this.numberInSurah = numberInSurah;
        this.meaning = meaning;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getNumberInSurah() {
        return numberInSurah;
    }

    public void setNumberInSurah(String numberInSurah) {
        this.numberInSurah = numberInSurah;
    }

    public String getJuz() {
        return juz;
    }

    public void setJuz(String juz) {
        this.juz = juz;
    }

    public String getPage() {
        return page;
    }

    public void setPage(String page) {
        this.page = page;
    }

    public String getRuku() {
        return ruku;
    }

    public void setRuku(String ruku) {
        this.ruku = ruku;
    }

    public String getHizbQuarter() {
        return hizbQuarter;
    }

    public void setHizbQuarter(String hizbQuarter) {
        this.hizbQuarter = hizbQuarter;
    }

    public boolean isSajda() {
        return sajda;
    }

    public void setSajda(boolean sajda) {
        this.sajda = sajda;
    }

    public String getMeaning() {
        return meaning;
    }

    public void setMeaning(String meaning) {
        this.meaning = meaning;
    }
}
