package com.bris.brissmartmobile.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by valeputra on 8/17/17.
 */

public class ItemSholat {
    @SerializedName("date_for")
    private String date;
    @SerializedName("fajr")
    private String fajr;
    @SerializedName("shurooq")
    private String shurooq;
    @SerializedName("dhuhr")
    private String dhuhr;
    @SerializedName("asr")
    private String asr;
    @SerializedName("maghrib")
    private String magrib;
    @SerializedName("isha")
    private String isha;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getFajr() {
        return fajr;
    }

    public void setFajr(String fajr) {
        this.fajr = fajr;
    }

    public String getShurooq() {
        return shurooq;
    }

    public void setShurooq(String shurooq) {
        this.shurooq = shurooq;
    }

    public String getDhuhr() {
        return dhuhr;
    }

    public void setDhuhr(String dhuhr) {
        this.dhuhr = dhuhr;
    }

    public String getAsr() {
        return asr;
    }

    public void setAsr(String asr) {
        this.asr = asr;
    }

    public String getMagrib() {
        return magrib;
    }

    public void setMagrib(String magrib) {
        this.magrib = magrib;
    }

    public String getIsha() {
        return isha;
    }

    public void setIsha(String isha) {
        this.isha = isha;
    }
}
