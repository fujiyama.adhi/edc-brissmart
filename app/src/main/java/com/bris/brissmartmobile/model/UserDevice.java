package com.bris.brissmartmobile.model;

import io.realm.RealmObject;
import io.realm.annotations.Required;

/**
 * Created by akbaranjas on 22/03/17.
 */

public class UserDevice extends RealmObject {
    @Required
    private String vdsn;
    @Required
    private String user;
    @Required
    private String device_id;
    @Required
    private String imei;
    @Required
    private String grant_access_code;

    public String getVdsn() {
        return vdsn;
    }

    public void setVdsn(String vdsn) {
        this.vdsn = vdsn;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getDevice_id() {
        return device_id;
    }

    public void setDevice_id(String device_id) {
        this.device_id = device_id;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public String getGrant_access_code() {
        return grant_access_code;
    }

    public void setGrant_access_code(String grant_access_code) {
        this.grant_access_code = grant_access_code;
    }
}
