package com.bris.brissmartmobile.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by akbaranjas on 31/03/17.
 */

public class ValidateCardRequest {

    @SerializedName("cardNumber")
    private String cardNumber;
    @SerializedName("cardPin")
    private String cardPin;

    public ValidateCardRequest(String cardNumber, String cardPin) {
        this.cardNumber = cardNumber;
        this.cardPin = cardPin;
    }
}
