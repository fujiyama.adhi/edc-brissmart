package com.bris.brissmartmobile.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ryputra on 17/01/2018.
 */

public class OAuthGrantAccessRequest {
    @SerializedName("client_id")
    private String clientId;
    @SerializedName("client_secret")
    private String clientSecret;
    @SerializedName("device_id")
    private String deviceId;
    @SerializedName("msisdn")
    private String msisdn;
    @SerializedName("actcode")
    private String actcode;

    public OAuthGrantAccessRequest(String clientId, String clientSecret, String deviceId, String msisdn, String actcode) {
        this.clientId = clientId;
        this.clientSecret = clientSecret;
        this.deviceId = deviceId;
        this.msisdn = msisdn;
        this.actcode = actcode;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public void setClientSecret(String clientSecret) {
        this.clientSecret = clientSecret;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public void setActcode(String actcode) {
        this.actcode = actcode;
    }
}
