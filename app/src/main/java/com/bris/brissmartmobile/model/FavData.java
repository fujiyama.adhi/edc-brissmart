package com.bris.brissmartmobile.model;

import com.bris.brissmartmobile.util.AppUtil;
import com.bris.brissmartmobile.util.GetCodeMenu;

import io.realm.RealmObject;
import io.realm.annotations.Required;

/**
 * Created by akbaranjas on 11/04/17.
 */

public class FavData extends RealmObject{
    @Required
    private String label;
    @Required
    private String detail;
    @Required
    private String category;
    private String kode_produk;
    private boolean isalarm;
    private String timealarm;

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getKode_produk() {
        return kode_produk;
    }

    public void setKode_produk(String kode_produk) {
        this.kode_produk = kode_produk;
    }

    public boolean isalarm() {
        return isalarm;
    }

    public void setIsalarm(boolean isalarm) {
        this.isalarm = isalarm;
    }

    public String getTimealarm() {
        return timealarm;
    }

    public void setTimealarm(String timealarm) {
        this.timealarm = timealarm;
    }

    @Override
    public String toString() {
        String message = null;
        if(category.equalsIgnoreCase("TRF")){
            message = label + "--" + detail + "--" + timealarm;
        }else if(category.equalsIgnoreCase("TRFA")){
            message = label + "--" + detail + "--" + kode_produk + "--" + timealarm;
        }else if(category.equalsIgnoreCase("BELI")){
            message = GetCodeMenu.getCodeFromValueBeli(kode_produk) + AppUtil.SPECIAL_CHAR_PIPE + label + "--" + detail + "--" + timealarm;
        }else if(category.equalsIgnoreCase("BAYAR")){
            message = GetCodeMenu.getCodeFromValueBayar(kode_produk) + AppUtil.SPECIAL_CHAR_PIPE + label + "--" + detail + "--" + timealarm;
        }

        return message;
    }
}
