package com.bris.brissmartmobile.model;

public class TerminaID {

    private Integer id;
    private String tid;
    private String sn;
    private Integer status;

    public TerminaID(Integer id, String tid, String sn, Integer status) {
        this.id = id;
        this.tid = tid;
        this.sn = sn;
        this.status = status;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTid() {
        return tid;
    }

    public void setTid(String tid) {
        this.tid = tid;
    }

    public String getSn() {
        return sn;
    }

    public void setSn(String sn) {
        this.sn = sn;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

}
