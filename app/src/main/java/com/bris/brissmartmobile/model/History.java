package com.bris.brissmartmobile.model;

import io.realm.RealmObject;
import io.realm.annotations.Required;

/**
 * Created by akbaranjas on 31/03/17.
 */

public class History extends RealmObject {

    private long trxdate;
    @Required
    private String datedisplay;
    @Required
    private String message;
    @Required
    private String statusread;
    private String menuId;
    private String dsn;

    public History() {
        super();
    }

    public String getDatedisplay() {
        return datedisplay;
    }

    public void setDatedisplay(String datedisplay) {
        this.datedisplay = datedisplay;
    }

    public String getStatusread() {
        return statusread;
    }

    public void setStatusread(String statusread) {
        this.statusread = statusread;
    }

    public long getTrxdate() {
        return trxdate;
    }

    public void setTrxdate(long trxdate) {
        this.trxdate = trxdate;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMenuId() {
        return menuId;
    }

    public void setMenuId(String menuId) {
        this.menuId = menuId;
    }

    public String getDsn() {
        return dsn;
    }

    public void setDsn(String dsn) {
        this.dsn = dsn;
    }
}
