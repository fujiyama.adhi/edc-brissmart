package com.bris.brissmartmobile.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by akbaranjas on 31/03/17.
 */

public class ValidateCardResponse {
    @SerializedName("status")
    private String status;
    @SerializedName("message")
    private String message;
    @SerializedName("msisdn")
    private String msisdn;

    public ValidateCardResponse(String status, String message, String msisdn) {
        this.status = status;
        this.message = message;
        this.msisdn = msisdn;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }
}
