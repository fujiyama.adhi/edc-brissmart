package com.bris.brissmartmobile.model;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;
import io.realm.annotations.Required;

/**
 * Created by ryputra on 13/10/2017.
 */

public class Quotes extends RealmObject {
    @Required
    @SerializedName("id")
    public String id;
    @Required
    @SerializedName("ayat")
    public String ayat;
    @Required
    @SerializedName("latin")
    public String latin;
    @Required
    @SerializedName("arti")
    public String arti;
    @Required
    @SerializedName("periwayat")
    public String periwayat;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAyat() {
        return ayat;
    }

    public void setAyat(String ayat) {
        this.ayat = ayat;
    }

    public String getLatin() {
        return latin;
    }

    public void setLatin(String latin) {
        this.latin = latin;
    }

    public String getArti() {
        return arti;
    }

    public void setArti(String arti) {
        this.arti = arti;
    }

    public String getPeriwayat() {
        return periwayat;
    }

    public void setPeriwayat(String periwayat) {
        this.periwayat = periwayat;
    }
}
