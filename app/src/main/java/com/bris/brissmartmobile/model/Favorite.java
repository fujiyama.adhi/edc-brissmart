package com.bris.brissmartmobile.model;

import io.realm.RealmObject;

/**
 * Created by akbaranjas on 31/03/17.
 */

public class Favorite extends RealmObject {

    private String idfav;
    private String jenispembayaran;
    private String bsaacc;
    private String data1;
    private String data2;
    private String data3;
    private String data4;
    private String namafav;
    private String useragen;
    private String menutrx;
    private String submenutrx;
    private String namajnsfav;
    private String tanggalfav;

    public Favorite() {
        super();
    }

    public String getIdfav() {
        return idfav;
    }

    public void setIdfav(String idfav) {
        this.idfav = idfav;
    }

    public String getJenispembayaran() {
        return jenispembayaran;
    }

    public void setJenispembayaran(String jenispembayaran) {
        this.jenispembayaran = jenispembayaran;
    }

    public String getBsaacc() {
        return bsaacc;
    }

    public void setBsaacc(String bsaacc) {
        this.bsaacc = bsaacc;
    }

    public String getData1() {
        return data1;
    }

    public void setData1(String data1) {
        this.data1 = data1;
    }

    public String getData2() {
        return data2;
    }

    public void setData2(String data2) {
        this.data2 = data2;
    }

    public String getData3() {
        return data3;
    }

    public void setData3(String data3) {
        this.data3 = data3;
    }

    public String getData4() {
        return data4;
    }

    public void setData4(String data4) {
        this.data4 = data4;
    }

    public String getNamafav() {
        return namafav;
    }

    public void setNamafav(String namafav) {
        this.namafav = namafav;
    }

    public String getUseragen() {
        return useragen;
    }

    public void setUseragen(String useragen) {
        this.useragen = useragen;
    }

    public String getMenutrx() {
        return menutrx;
    }

    public void setMenutrx(String menutrx) {
        this.menutrx = menutrx;
    }

    public String getSubmenutrx() {
        return submenutrx;
    }

    public void setSubmenutrx(String submenutrx) {
        this.submenutrx = submenutrx;
    }

    public String getNamajnsfav() {
        return namajnsfav;
    }

    public void setNamajnsfav(String namajnsfav) {
        this.namajnsfav = namajnsfav;
    }

    public String getTanggalfav() {
        return tanggalfav;
    }

    public void setTanggalfav(String tanggalfav) {
        this.tanggalfav = tanggalfav;
    }
}
