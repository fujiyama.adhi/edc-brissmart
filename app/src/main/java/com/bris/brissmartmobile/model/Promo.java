package com.bris.brissmartmobile.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by valeputra on 7/20/17.
 */

public class Promo implements Parcelable {
    private long promo_id;
    private String promo_title;
    private String promo_img_url;
    private String promo_detail;
    private String promo_create_at;
    private String promo_valid_until;
    private String is_headline;

    public Promo(long promo_id, String promo_title, String promo_img_url, String promo_detail, String promo_create_at, String promo_valid_until, String is_headline) {
        this.promo_id = promo_id;
        this.promo_title = promo_title;
        this.promo_img_url = promo_img_url;
        this.promo_detail = promo_detail;
        this.promo_create_at = promo_create_at;
        this.promo_valid_until = promo_valid_until;
        this.is_headline = is_headline;
    }

    protected Promo(Parcel in) {
        promo_id = in.readLong();
        promo_title = in.readString();
        promo_img_url = in.readString();
        promo_detail = in.readString();
        promo_create_at = in.readString();
        promo_valid_until = in.readString();
        is_headline = in.readString();
    }

    public static final Creator<Promo> CREATOR = new Creator<Promo>() {
        @Override
        public Promo createFromParcel(Parcel in) {
            return new Promo(in);
        }

        @Override
        public Promo[] newArray(int size) {
            return new Promo[size];
        }
    };

    public long getPromo_id() {
        return promo_id;
    }

    public void setPromo_id(long promo_id) {
        this.promo_id = promo_id;
    }

    public String getPromo_title() {
        return promo_title;
    }

    public void setPromo_title(String promo_title) {
        this.promo_title = promo_title;
    }

    public String getPromo_img_url() {
        return promo_img_url;
    }

    public void setPromo_img_url(String promo_img_url) {
        this.promo_img_url = promo_img_url;
    }

    public String getPromo_detail() {
        return promo_detail;
    }

    public void setPromo_detail(String promo_detail) {
        this.promo_detail = promo_detail;
    }

    public String getPromo_create_at() {
        return promo_create_at;
    }

    public void setPromo_create_at(String promo_create_at) {
        this.promo_create_at = promo_create_at;
    }

    public String getPromo_valid_until() {
        return promo_valid_until;
    }

    public void setPromo_valid_until(String promo_valid_until) {
        this.promo_valid_until = promo_valid_until;
    }

    public String getIs_headline() {
        return is_headline;
    }

    public void setIs_headline(String is_headline) {
        this.is_headline = is_headline;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(promo_id);
        dest.writeString(promo_title);
        dest.writeString(promo_img_url);
        dest.writeString(promo_detail);
        dest.writeString(promo_create_at);
        dest.writeString(promo_valid_until);
        dest.writeString(is_headline);
    }
}
