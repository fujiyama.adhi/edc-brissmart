package com.bris.brissmartmobile.model;

import io.realm.RealmObject;
import io.realm.annotations.Required;

/**
 * Created by akbaranjas on 22/03/17.
 */

public class User extends RealmObject {
    @Required
    private String msisdn;
    @Required
    private String phone_id;
    @Required
    private String name;

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public String getPhone_id() {
        return phone_id;
    }

    public void setPhone_id(String phone_id) {
        this.phone_id = phone_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
