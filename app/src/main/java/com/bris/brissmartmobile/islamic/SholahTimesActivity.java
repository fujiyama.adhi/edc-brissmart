package com.bris.brissmartmobile.islamic;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bris.brissmartmobile.R;
import com.bris.brissmartmobile.util.Constants;
import com.bris.brissmartmobile.util.AppPreferences;
import com.bris.brissmartmobile.util.AppUtil;
import com.bris.brissmartmobile.util.GPSTracker;
import com.bris.brissmartmobile.util.LocationPermission;
import com.bris.brissmartmobile.util.PrayTime;
import com.google.android.gms.location.LocationListener;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

/**
 * Created by ryputra on 02/03/2018.
 */

public class SholahTimesActivity extends AppCompatActivity implements LocationListener {
    private PrayTime prayers;
    private Toolbar transparencyToolbar;
    private TextView tvLocation;
    private TextView mFajr, mSunrise, mDhuhr, mAsr, mSunset, mMaghrib, mIsha, mDate;
    private RelativeLayout mlayoutDate;
    private double timezone;
    private double latitude, longitude;
    private int year, month, day;
    private String dayName;

    private AppPreferences appPref;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ajadwal_sholat);
        customToolbar(getString(R.string.menu_title_jadwal_sholat));

        tvLocation = (TextView) findViewById(R.id.tv_state);

        mlayoutDate = (RelativeLayout) findViewById(R.id.layout_date);
        mFajr = (TextView) findViewById(R.id.fajr_value);
        mDhuhr = (TextView) findViewById(R.id.Dhuhr_value);
        mAsr = (TextView) findViewById(R.id.Asr_value);
        mMaghrib = (TextView) findViewById(R.id.Maghrib_value);
        mIsha = (TextView) findViewById(R.id.Isha_value);
        mDate = (TextView) findViewById(R.id.date_value);

        appPref = new AppPreferences(SholahTimesActivity.this);

        timezone = (Calendar.getInstance().getTimeZone().getOffset(Calendar.getInstance().getTimeInMillis())) / (1000 * 60 * 60);

        prayers = new PrayTime();
        prayers.setTimeFormat(prayers.Time24); //format waktu
        prayers.setCalcMethod(prayers.Makkah); //kalulasi menurut
        prayers.setAsrJuristic(prayers.Shafii);
        prayers.setAdjustHighLats(prayers.MidNight);
        prayers.setTimeZone(prayers.getTimeZone());
        prayers.setFajrAngle(21.9);
        prayers.setIshaAngle(18.6);

        int[] offsets = {0, 0, 0, 0, 0, 0, 0}; // {Fajr,Sunrise,Dhuhr,Asr,Sunset,Maghrib,Isha}
        prayers.tune(offsets);

        Calendar cal = Calendar.getInstance();
        year = cal.get(Calendar.YEAR);
        month = cal.get(Calendar.MONTH);
        day = cal.get(Calendar.DAY_OF_MONTH);
        dayName = cal.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.LONG, Locale.getDefault()).toString();

        GPSTracker gps = new GPSTracker(this);
        if (gps.canGetLocation()) {
            latitude = gps.getLatitude();
            longitude = gps.getLongitude();
        }

        tvLocation.setText("Lat: "+latitude + ", Lng: "+longitude);

        mDate.setText(setDateFormat());

        if (latitude != 0.0 && longitude != 0.0) {
            ShowPrayTime(year, month, day);
            mlayoutDate.setOnClickListener(new View.OnClickListener() {
                @SuppressWarnings("deprecation")
                @Override
                public void onClick(View v) {
                    showDialog(0);
                }
            });
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (appPref.getLocationConfig() == null) {
            LocationPermission loc = new LocationPermission(SholahTimesActivity.this);
            loc.updateLocation(0);
        }
    }

    private String setDateFormat() {
        return AppUtil.getDayIndonesia(dayName) + ", " +day + " " + AppUtil.getMonthIndonesiaById(month) + " " + year;
    }

//    public void dialogEnabledLocationService() {
//        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
//        dialog.setMessage(this.getResources().getString(R.string.gps_network_not_enabled));
//        dialog.setPositiveButton(this.getResources().getString(R.string.btn_txt_izinkan),
//                new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
//                        // TODO Auto-generated method stub
//                        Intent myIntent = new Intent( Settings.ACTION_LOCATION_SOURCE_SETTINGS);
//                        startActivity(myIntent);
//                    }
//                });
//        dialog.setNegativeButton(this.getString(R.string.btn_cancel), new DialogInterface.OnClickListener() {
//
//            @Override
//            public void onClick(DialogInterface paramDialogInterface, int paramInt) {
//                // TODO Auto-generated method stub
//            }
//        });
//        dialog.show();
//    }

    private void ShowPrayTime(int year, int month, int day) {
        ArrayList<String> prayerTimes = prayers.getPrayerTimes(year, month, day, latitude, longitude, timezone);

        mFajr.setText(prayerTimes.get(0));
        mDhuhr.setText(prayerTimes.get(2));
        mAsr.setText(prayerTimes.get(3));
        mMaghrib.setText(prayerTimes.get(4));
        mIsha.setText(prayerTimes.get(6));
    }

    @Deprecated
    protected Dialog onCreateDialog(int id) {
        return new DatePickerDialog(this, datePickerListener, year, month, day);
    }

    private DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {
        public void onDateSet(DatePicker view, int selectedYear, int selectedMonth, int selectedDay) {
            ShowPrayTime(selectedYear, selectedMonth, selectedDay);
            year	= selectedYear;
            month	= selectedMonth;
            day		= selectedDay;
        }
    };

    @Override
    public void onLocationChanged(Location location) {}

    public void customToolbar(final String title) {
        Toolbar toolbar = (Toolbar) findViewById(R.id.tb_regular);
        ImageView btnBack = (ImageView) findViewById(R.id.btn_back);
        TextView tvPageTitle = (TextView) findViewById(R.id.tv_page_title);
        ImageView btnUpdateLoc = (ImageView) findViewById(R.id.btn_custom);

        btnUpdateLoc.setImageResource(R.drawable.ico_refresh);
        btnUpdateLoc.setVisibility(View.VISIBLE);

        tvPageTitle.setText(title);
        toolbar.setBackgroundColor(ContextCompat.getColor(SholahTimesActivity.this, R.color.colorPrimaryIslamic));
        setSupportActionBar(toolbar);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(ContextCompat.getColor(this, Constants.Color.ISLAMIC));
        }

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        btnUpdateLoc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LocationPermission location = new LocationPermission(SholahTimesActivity.this);
                location.updateLocation(0);
            }
        });
    }
}
