package com.bris.brissmartmobile.islamic.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.support.constraint.ConstraintLayout;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.bris.brissmartmobile.R;
import com.bris.brissmartmobile.model.Ayat;

import java.util.List;

/**
 * Created by valeputra on 8/2/17.
 */

public class AyatAdapter extends BaseAdapter {
    private Context context;
    private LayoutInflater layoutinflaters;
    private List<Ayat> listStorage;

    public AyatAdapter(Context context, List<Ayat> customizedListView) {
        this.context = context;
        layoutinflaters =(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        listStorage = customizedListView;
    }

    @Override
    public int getCount() {
        return listStorage.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final AyatAdapter.ViewHolder listViewHolder;
        ViewHolder holder;

        Typeface type = Typeface.createFromAsset(context.getAssets(), "fonts/LateefRegOT.ttf");

        if(convertView == null) {
            listViewHolder = new AyatAdapter.ViewHolder();
            convertView = layoutinflaters.inflate(R.layout.content_asurah_container, parent, false);
            listViewHolder.ayahArabic = (TextView)convertView.findViewById(R.id.tv_ayat_arabic);
            listViewHolder.meaning = (TextView)convertView.findViewById(R.id.tv_ayat_mean);
            listViewHolder.numberSurah = (TextView)convertView.findViewById(R.id.tv_number_ayah);
            listViewHolder.layout = (ConstraintLayout) convertView.findViewById(R.id.item_layout_ayah);

            listViewHolder.ayahArabic.setTypeface(type);

            convertView.setTag(listViewHolder);
        }else{
            listViewHolder = (AyatAdapter.ViewHolder)convertView.getTag();
        }

        listViewHolder.ayahArabic.setText(listStorage.get(position).getText());
        listViewHolder.meaning.setText(listStorage.get(position).getMeaning());
        listViewHolder.numberSurah.setText(listStorage.get(position).getNumberInSurah());

        if (Integer.parseInt(listStorage.get(position).getNumberInSurah()) % 2 == 0) {
            listViewHolder.layout.setBackgroundColor(ContextCompat.getColor(context, R.color.white));
        } else {
            listViewHolder.layout.setBackgroundColor(ContextCompat.getColor(context, R.color.colorBodyIslamic));
        }

        return convertView;
    }

    private class ViewHolder {
        ConstraintLayout layout;
        TextView ayahArabic;
        TextView meaning;
        TextView numberSurah;
    }
}
