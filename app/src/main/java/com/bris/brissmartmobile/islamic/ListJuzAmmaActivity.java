package com.bris.brissmartmobile.islamic;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.bris.brissmartmobile.R;
import com.bris.brissmartmobile.model.Surah;
import com.bris.brissmartmobile.util.Constants;
import com.bris.brissmartmobile.util.RealmImporter;
import com.bris.brissmartmobile.islamic.adapter.ListJuzAmmaAdapter;
import com.bris.brissmartmobile.util.Menu;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by valeputra on 8/1/17.
 */

public class ListJuzAmmaActivity extends AppCompatActivity {
    private static Realm realm;
    private RealmResults<Surah> surah;
    private ListView lv_juzamma;
    private List<Surah> listSurahJuzAmma;
    private ListJuzAmmaAdapter adapter;
    private String pageTitle = "";

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alist_juz_amma);
        pageTitle = getIntent().getExtras().getString(Menu.MENU_ID);
        customToolbarIslamic(pageTitle);
//        AppUtil.toolbarRegularWithCustomStatusBar(ListJuzAmmaActivity.this, pageTitle, ContextCompat.getColor(this, R.color.colorPrimaryIslamic));

        main();
    }

    private void main() {
        // realm start
        realm = Realm.getDefaultInstance();
        surah = getDaftarSurah();

        // if data have been sets dont insertAccount to realm
        // import hadist json to realm
        if(surah.size()< 1) {
            RealmImporter realmImporter = new RealmImporter(getResources());
            realmImporter.importFromJson("juz_amma");
        }

        // generate listview using adapter
        lv_juzamma = (ListView) findViewById(R.id.lv_list_juzamma);
        listSurahJuzAmma = getJuzAmmaList(surah);
        adapter = new ListJuzAmmaAdapter(getApplicationContext(), listSurahJuzAmma);
        lv_juzamma.setAdapter(adapter);

        onAction();
    }

    private void onAction() {
        // on item listview clicked
        lv_juzamma.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                // get textview by item listview that is clicked
                String no_surah = ((TextView) view.findViewById(R.id.tv_no_surah)).getText().toString();
                String nama_surah = ((TextView) view.findViewById(R.id.tv_nama_surah)).getText().toString();
                Intent intent = new Intent(ListJuzAmmaActivity.this, SurahContainerActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString("noSurah", no_surah);
                bundle.putString("namaSurah", nama_surah);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });
    }

    private RealmResults<Surah> getDaftarSurah() {
        return realm.where(Surah.class).findAll();
    }

    public void customToolbarIslamic(final String title) {
        Toolbar toolbar = (Toolbar) findViewById(R.id.tb_regular);
        ImageView btnBack = (ImageView) findViewById(R.id.btn_back);
        TextView tvPageTitle = (TextView) findViewById(R.id.tv_page_title);

        tvPageTitle.setText(title);
        toolbar.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimaryIslamic));
        setSupportActionBar(toolbar);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(ContextCompat.getColor(this, Constants.Color.ISLAMIC));
        }

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    private List<Surah> getJuzAmmaList(RealmResults<Surah> surah) {
        List<Surah> data = new ArrayList<>();
        for (int i=0 ; i<surah.size() ; i++) {
            data.add(new Surah(
                    surah.get(i).getNoSurah(),
                    surah.get(i).getSurah(),
                    surah.get(i).getPathSurah()
            ));
        }
        return data;
    }
}
