package com.bris.brissmartmobile.islamic;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.bris.brissmartmobile.R;
import com.bris.brissmartmobile.util.Constants;
import com.bris.brissmartmobile.model.Ayat;
import com.bris.brissmartmobile.util.RealmImporter;
import com.bris.brissmartmobile.islamic.adapter.AyatAdapter;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by valeputra on 8/1/17.
 */

public class SurahContainerActivity extends AppCompatActivity {

    private static Realm realm;
    private Toolbar tb_list_surah;
    private TextView tv_nama_surah;
    private RealmResults<Ayat> ayat;
    private String noSurah;
    private String namaSurah;
    private ListView lv_ayat;
    private List<Ayat> listAyat;
    private AyatAdapter adapter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_asurah_container);

        Bundle bundle = getIntent().getExtras();
        noSurah = bundle.getString("noSurah");
        namaSurah = bundle.getString("namaSurah");
        customToolbarIslamic("Surah " + namaSurah);

        main();
    }

    private void main() {
        try {
            realm = Realm.getDefaultInstance();
            ayat = getAllAyat();

            // if data have been sets dont insertAccount to realm
            if(ayat.size() < 1) {
                // import hadist json to realm
                RealmImporter realmImporter = new RealmImporter(getResources());
                realmImporter.importFromJson(noSurah);
            }

            // set adapter
            lv_ayat = (ListView) findViewById(R.id.lv_ayat);
            listAyat = getListAyat(ayat);
            adapter = new AyatAdapter(getApplicationContext(), listAyat);
            lv_ayat.setAdapter(adapter);

            // add listview header (basmallah)
            if (!noSurah.equalsIgnoreCase("1")) {
                LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View vbasmallah = inflater.inflate(R.layout.header_ayah_listview, lv_ayat, false);
                TextView tvBasmallah = (TextView) vbasmallah.findViewById(R.id.basmallah);
                Typeface type = Typeface.createFromAsset(getAssets(), "fonts/LateefRegOT.ttf");
                tvBasmallah.setTypeface(type);
                lv_ayat.addHeaderView(vbasmallah);
            }

        } catch (Exception e) {}
    }

    private RealmResults<Ayat> getAllAyat() {
        return realm.where(Ayat.class).findAll();
    }

    private List<Ayat> getListAyat(RealmResults<Ayat> ayat) {
        List<Ayat> data = new ArrayList<>();
        for (int i=0 ; i<ayat.size() ; i++) {
            data.add(new Ayat(
                    ayat.get(i).getNumber(),
                    ayat.get(i).getText(),
                    ayat.get(i).getNumberInSurah(),
                    ayat.get(i).getMeaning()
            ));
        }
        return data;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        clearRealmObj();
    }

    @Override
    protected void onStop() {
        super.onStop();
        clearRealmObj();
    }

    private void clearRealmObj() {
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                RealmResults<Ayat> rows = realm.where(Ayat.class).findAll();
                rows.deleteAllFromRealm();
            }
        });
    }

    public void customToolbarIslamic(final String title) {
        Toolbar toolbar = (Toolbar) findViewById(R.id.tb_regular);
        ImageView btnBack = (ImageView) findViewById(R.id.btn_back);
        TextView tvPageTitle = (TextView) findViewById(R.id.tv_page_title);

        tvPageTitle.setText(title);
        toolbar.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimaryIslamic));
        setSupportActionBar(toolbar);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(ContextCompat.getColor(this, Constants.Color.ISLAMIC));
        }

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }
}
