package com.bris.brissmartmobile.islamic.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.bris.brissmartmobile.R;
import com.bris.brissmartmobile.model.Surah;
import com.bris.brissmartmobile.util.AppUtil;

import java.util.List;

/**
 * Created by valeputra on 8/1/17.
 */

public class ListJuzAmmaAdapter extends BaseAdapter {

    private Context context;
    private LayoutInflater layoutinflaters;
    private List<Surah> listStorage;

    public ListJuzAmmaAdapter(Context context, List<Surah> customizedListView) {
        this.context = context;
        layoutinflaters =(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        listStorage = customizedListView;
    }

    @Override
    public int getCount() {
        return listStorage.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ListJuzAmmaAdapter.ViewHolder listViewHolder;

        if(convertView == null){
            listViewHolder = new ListJuzAmmaAdapter.ViewHolder();
            convertView = layoutinflaters.inflate(R.layout.content_alistview_juzamma, parent, false);
            AppUtil.customMainFonts(context.getAssets(), convertView);  // Custom Fonts
            listViewHolder.namaSurah = (TextView)convertView.findViewById(R.id.tv_nama_surah);
            listViewHolder.noSurah = (TextView)convertView.findViewById(R.id.tv_no_surah);
            convertView.setTag(listViewHolder);

        }else{
            listViewHolder = (ListJuzAmmaAdapter.ViewHolder)convertView.getTag();
        }
        listViewHolder.namaSurah.setText(listStorage.get(position).getSurah());
        listViewHolder.noSurah.setText(listStorage.get(position).getNoSurah());

        return convertView;
    }

    private class ViewHolder {
        TextView namaSurah;
        TextView noSurah;
    }
}
