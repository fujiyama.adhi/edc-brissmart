package com.bris.brissmartmobile.islamic;


import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bris.brissmartmobile.R;
import com.bris.brissmartmobile.util.AppUtil;
import com.bris.brissmartmobile.util.Constants;
import com.bris.brissmartmobile.util.AppPreferences;
import com.bris.brissmartmobile.model.LocationInfo;
import com.bris.brissmartmobile.util.LocationPermission;
import com.bris.brissmartmobile.util.Menu;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;

/**
 * Created by ryputra on 18/10/2017.
 */

public class QiblaActivity extends AppCompatActivity implements SensorEventListener,
        OnMapReadyCallback,
        com.google.android.gms.location.LocationListener {

    private TextView Quibladegree;
    private RelativeLayout compass, compassMapContainer, pointerMap, compassMain, innerPosition;
    private ImageView indicator, redCircle, compassLevel, pointerIndicator, smallCircleLevel, errorImage, pointerMapQuibla, pointerIndicatorInner;
    private float currentDegree = 0f;
    private SensorManager mSensorManager;
    private Sensor mAccelerometer, mMagnetometer;
    private float[] mLastAccelerometer = new float[3];
    private float[] mLastMagnetometer = new float[3];
    private float[] mR = new float[9];
    private float[] mOrientation = new float[3];
    private boolean mLastAccelerometerSet = false, switchView = false,
            pointerPosition = true, mLastMagnetometerSet = false, start = false, mapReady = false;
    private double previousAzimuthInDegrees = 0f;
    private long SensorSendTime;
    private float pointerFirstPositionX, pointerFirstPositionY, smallCircleRadius, newX, newY;
    private double lastRoll, lastPitch, lastTime;
    private GoogleMap googleMap;
//    private GoogleApiClient mGoogleApiClient;
//    private LocationRequest mLocationRequest;
    private double fusedLatitude = 0.0;
    private double fusedLongitude = 0.0;
    private String pageTitle;
    public AppPreferences appPref;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_aqibla_compass);
//        customToolbarIslamic();
        init();
    }

    private void init() {
        pageTitle = getIntent().getExtras().getString(Menu.MENU_ID);
        customToolbar(pageTitle);
//        AppUtil.toolbarRegularWithCustomStatusBar(QiblaActivity.this, pageTitle, ContextCompat.getColor(this, R.color.colorPrimaryIslamic));


        appPref = new AppPreferences(QiblaActivity.this);
        //init compass activity views
        Quibladegree = (TextView) findViewById(R.id.angle);
        Quibladegree.setText(appPref.getQiblaDegree() + getString(R.string.angle_from_north));
        indicator = (ImageView) findViewById(R.id.imageView2);
        compass = (RelativeLayout) findViewById(R.id.compassContainer);
        compassMapContainer = (RelativeLayout) findViewById(R.id.compassMapContainer);
        compassMain = (RelativeLayout) findViewById(R.id.compassMain);
        smallCircleLevel = (ImageView) findViewById(R.id.smallCircle);
        innerPosition = (RelativeLayout) findViewById(R.id.innerplace);
        pointerIndicatorInner = (ImageView) findViewById(R.id.poinerInner);
        redCircle = (ImageView) findViewById(R.id.red_circle);
        errorImage = (ImageView) findViewById(R.id.error);

        //init sensor services
        mSensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        mAccelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        mMagnetometer = mSensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
        compassLevel = (ImageView) findViewById(R.id.compassLevel);

        Log.d("sensor acceloro", String.valueOf(mAccelerometer));
        Log.d("sensor mMagnetometer", String.valueOf(mMagnetometer));

        if (mAccelerometer != null || mMagnetometer != null) {
            //animate compass pointer
            RotateAnimation ra = new RotateAnimation(currentDegree, appPref.getQiblaDegree(),
                    Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
            ra.setDuration(400);
            ra.setFillAfter(true);
            indicator.startAnimation(ra);
            pointerIndicatorInner.startAnimation(ra);
        } else {
            AppUtil.displayDialogBasic(this, "Maaf, perangkat Anda tidak mendukung Kompas", "Arah Kiblat");
        }
    }


    @Override
    public void onSensorChanged(SensorEvent event) {
        double startTime = System.currentTimeMillis();

        if (event.sensor == mAccelerometer) {
            mLastAccelerometer = event.values;
            mLastAccelerometerSet = true;
        } else if (event.sensor == mMagnetometer) {
            mLastMagnetometer = event.values;
            mLastMagnetometerSet = true;
        }

        if (mLastAccelerometerSet && mLastMagnetometerSet) {
            boolean success = SensorManager.getRotationMatrix(mR, null, mLastAccelerometer, mLastMagnetometer);
            SensorManager.getOrientation(mR, mOrientation);
            float azimuthInRadians = mOrientation[0];
            double azimuthInDegress = -(float) (Math.toDegrees(azimuthInRadians) + 360) % 360;

            if (Math.abs(azimuthInDegress - previousAzimuthInDegrees) > 300) {
                previousAzimuthInDegrees = azimuthInDegress;
            }

            azimuthInDegress = lowPass(azimuthInDegress, previousAzimuthInDegrees, startTime);

            if (mapReady) updateCamera((float) azimuthInDegress);

            RotateAnimation ra = new RotateAnimation(
                    (float) previousAzimuthInDegrees,
                    (float) azimuthInDegress,
                    Animation.RELATIVE_TO_SELF, 0.5f,
                    Animation.RELATIVE_TO_SELF,
                    0.5f);

            ra.setDuration(500);
            ra.setFillAfter(true);
            compass.startAnimation(ra);
            innerPosition.startAnimation(ra);

            previousAzimuthInDegrees = azimuthInDegress;


            if (pointerPosition == true) {
                pointerFirstPositionX = compassLevel.getX();
                pointerFirstPositionY = compassLevel.getY();
                smallCircleRadius = smallCircleLevel.getX();
                pointerPosition = false;
            }

            if (success) {
                float orientation[] = new float[3];
                SensorManager.getOrientation(mR, orientation);
                double yaw = orientation[0] * 57.2957795f;
                double pitch = orientation[1] * 57.2957795f;
                double roll = orientation[2] * 57.2957795f;
                if (pitch > 90) pitch -= 180;
                if (pitch < -90) pitch += 180;
                if (roll > 90) roll -= 180;
                if (roll < -90) roll += 180;

                double time = System.currentTimeMillis();

                if (!start) {
                    lastTime = time;
                    lastRoll = roll;
                    lastPitch = pitch;
                }
                start = true;


                double dt = (time - lastTime) / 1000.0;
                roll = lowPassPointerLevel(roll, lastRoll, dt);
                pitch = lowPassPointerLevel(pitch, lastPitch, dt);
                lastTime = time;
                lastRoll = roll;
                lastPitch = pitch;

                newX = (float) (pointerFirstPositionX + pointerFirstPositionX * roll / 90.0);
                newY = (float) (pointerFirstPositionY + pointerFirstPositionY * pitch / 90.0);

                compassLevel.setX(newX);
                compassLevel.setY(newY);

                if (smallCircleRadius / 3 < Math.sqrt((roll * roll) + (pitch * pitch))) {
                    compassLevel.setImageResource(R.drawable.ic_error_pointer);
                } else {
                    compassLevel.setImageResource(R.drawable.ic_level_pointer);
                }
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (appPref.getLocationConfig() == null) {
            LocationPermission loc = new LocationPermission(QiblaActivity.this);
            loc.updateLocation(0);
        } else {
            // gagal medapatkan arah kiblat
        }
        mSensorManager.registerListener(this, mAccelerometer, SensorManager.SENSOR_DELAY_GAME);
        mSensorManager.registerListener(this, mMagnetometer, SensorManager.SENSOR_DELAY_GAME);
    }

    @Override
    public void onPause() {
        super.onPause();
        mSensorManager.unregisterListener(this, mAccelerometer);
        mSensorManager.unregisterListener(this, mMagnetometer);
    }


    private double lowPassPointerLevel(double input, double lastOutput, double dt) {
        final double lagConstant = 0.25;
        double alpha = dt / (lagConstant + dt);
        return alpha * input + (1 - alpha) * lastOutput;
    }

    private void updateCamera(float bearing) {
        LocationInfo location = appPref.getLocationConfig(); // set location config ?
        CameraPosition oldPos = googleMap.getCameraPosition();

        CameraPosition pos = CameraPosition.builder(oldPos)
                .target(new LatLng(getFusedLatitude(), getFusedLongitude()))
                .zoom(17)
                .bearing(360-bearing)
                .build();
        googleMap.moveCamera(CameraUpdateFactory.newCameraPosition(pos));
    }

    private double lowPass(double input, double lastOutput, double dt) {
        double elapsedTime = dt - SensorSendTime;
        Log.d("TIMESEND", elapsedTime + "");
        SensorSendTime = (long) dt;
        elapsedTime = elapsedTime / 1000;
        final double lagConstant = 1;
        double alpha = elapsedTime / (lagConstant + elapsedTime);
        return alpha * input + (1 - alpha) * lastOutput;
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    @Override
    public void onLocationChanged(Location location) {
        setFusedLatitude(location.getLatitude());
        setFusedLongitude(location.getLongitude());
        appPref.setQiblatDegree((float) bearing(location.getLatitude(), location.getLongitude()));
    }

    private float bearing(double myLat, double myLong) {
        double lngKabah = 39.8256498;
        double latKabah = 21.4225031;

        double dLong = lngKabah - myLong;
        double dLon = Math.toRadians(dLong);
        double lat1 = Math.toRadians(myLat);
        double lat2 = Math.toRadians(latKabah);
        double y = Math.sin(dLon) * Math.cos(lat2);
        double x = Math.cos(lat1)*Math.sin(lat2) -
                Math.sin(lat1)*Math.cos(lat2)*Math.cos(dLon);

        double brng = Math.atan2(y, x);

        Log.d("bearing", String.valueOf(Math.toDegrees(brng)));

        return Float.parseFloat(String.format("%.1f", Math.toDegrees(brng)));
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        mapReady = true;
    }

    public void setFusedLatitude(double lat) {
        fusedLatitude = lat;
    }

    public void setFusedLongitude(double lon) {
        fusedLongitude = lon;
    }

    public double getFusedLatitude() {
        return fusedLatitude;
    }

    public double getFusedLongitude() {
        return fusedLongitude;
    }

//    private void customToolbarIslamic() {
//        Toolbar tb_islamic = (Toolbar) findViewById(R.id.tb_transparent);
//        tb_islamic.setNavigationIcon(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
//        setSupportActionBar(tb_islamic);
//        tb_islamic.setNavigationOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                onBackPressed();
//            }
//        });
//    }

    public void customToolbar(final String title) {
        Toolbar toolbar = (Toolbar) findViewById(R.id.tb_regular);
        ImageView btnBack = (ImageView) findViewById(R.id.btn_back);
        TextView tvPageTitle = (TextView) findViewById(R.id.tv_page_title);
        ImageView btnUpdateLoc = (ImageView) findViewById(R.id.btn_custom);

        btnUpdateLoc.setImageResource(R.drawable.ico_refresh);
        btnUpdateLoc.setVisibility(View.VISIBLE);

        tvPageTitle.setText(title);
        toolbar.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimaryIslamic));
        setSupportActionBar(toolbar);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(ContextCompat.getColor(this, Constants.Color.ISLAMIC));
        }

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        btnUpdateLoc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LocationPermission location = new LocationPermission(QiblaActivity.this);
                location.updateLocation(0);
            }
        });
    }
}
