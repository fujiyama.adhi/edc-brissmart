package com.bris.brissmartmobile.islamic.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bris.brissmartmobile.R;
import com.bris.brissmartmobile.model.Quotes;

import io.realm.RealmResults;

/**
 * Created by ryputra on 12/10/2017.
 */

public class QuotesAdapter extends PagerAdapter {
    private Context context;
    private LayoutInflater layoutInflater;
    private RealmResults<Quotes> listStorage;
    private View view;

    public QuotesAdapter(Context context, RealmResults<Quotes> motivationQoutes) {
        this.context = context;
        this.listStorage = motivationQoutes;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        QuotesAdapter.ViewHolder listViewHolder;
        listViewHolder = new ViewHolder();
        layoutInflater = (LayoutInflater) container.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = layoutInflater.inflate(R.layout.item_quotes, container, false);

        Typeface type = Typeface.createFromAsset(context.getAssets(), "fonts/LateefRegOT.ttf");

        listViewHolder.ayat = (TextView) view.findViewById(R.id.tv_ayat);
        listViewHolder.terjemahan = (TextView) view.findViewById(R.id.tv_terjemahan);
        listViewHolder.periwayat = (TextView) view.findViewById(R.id.tv_periwayat);

        listViewHolder.ayat.setText(listStorage.get(position).getAyat());
        listViewHolder.ayat.setTypeface(type);
        listViewHolder.terjemahan.setText(listStorage.get(position).getArti());
        listViewHolder.periwayat.setText(listStorage.get(position).getPeriwayat());

        /** required to show each pager **/
        ((ViewPager) container).addView(view);

        return view;
    }

    @Override
    public int getCount() {
        return listStorage.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return (view == object);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        ((ViewPager) container).removeView((RelativeLayout) object);
    }

    static class ViewHolder{
        TextView ayat;
        TextView terjemahan;
        TextView periwayat;
    }
}
