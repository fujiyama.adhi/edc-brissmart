package com.bris.brissmartmobile.islamic;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.BounceInterpolator;
import android.view.animation.Interpolator;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bris.brissmartmobile.R;
import com.bris.brissmartmobile.util.googlemapapi.GoogleMapsV2Response;
import com.bris.brissmartmobile.util.ApiInterface;
import com.bris.brissmartmobile.util.Constants;
import com.bris.brissmartmobile.model.CustomMarker;
import com.bris.brissmartmobile.util.AppUtil;
import com.bris.brissmartmobile.util.Menu;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by valeputra on 7/24/17.
 */

public class MasjidNearmeActivity extends FragmentActivity implements OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener {

    private static final int PROXIMITY_RADIUS = 600;
    private static final String qCategory = "mosque";
    private double markerLat, markerLng;
    private double myLat, myLng;
    private FloatingActionButton fabMyLoc;
    private SupportMapFragment mapFragment;
    private LocationRequest mLocationRequest;
    private GoogleApiClient mGoogleApiClient;
    private LatLng myLatLng;
    private GoogleMap mMap;
    private Marker myLocationMarker = null;
    private String markerName, markerVicinity;
    private LocationManager locateManager;
    private HashMap<Marker, CustomMarker> mMarkersHashMap;
    private ArrayList<CustomMarker> mMyMarkersArray = new ArrayList<CustomMarker>();
    private String pageTitle;

    public MasjidNearmeActivity() {}

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_amasjid_nearme);
        pageTitle = getIntent().getExtras().getString(Menu.MENU_ID);
        fabMyLoc = (FloatingActionButton) findViewById(R.id.fab_maps_myloc);

//        AppUtil.toolbarRegularWithCustomStatusBar(MasjidNearmeActivity.this, pageTitle, ContextCompat.getColor(this, R.color.menuOrange));

        customToolbarIslamic();

        // build case for Android M
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            checkLocationPermission();
        }

        // location gps setting
        locateManager = (LocationManager)getSystemService(LOCATION_SERVICE);
        if (!locateManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            dialogEnabledLocationService();
        }

        // show error dialog if Google Play Services not available
        if (!isGooglePlayServicesAvailable()) {
            Log.d("onCreate", "Google Play Services not available. Ending Test case.");
            finish();
        } else {
            mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
            mapFragment.getMapAsync(this);
            onActionFab();
        }
    }

    private void onActionFab() {
        fabMyLoc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    AppUtil.showToastShort(getApplicationContext(), getString(R.string.alert_my_location));
                    if(locateManager.isProviderEnabled( LocationManager.GPS_PROVIDER)) {
                        showMyLocation(myLatLng);
                    }
                } catch (Exception e) {
                    AppUtil.showToastShort(getApplicationContext(), getString(R.string.alert_error_default));
                }
            }
        });
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);                                                     // map type gesture
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            mMap.setMyLocationEnabled(true);                                                            // mylocation default googlemaps
            mMap.getUiSettings().setMyLocationButtonEnabled(false);
//            mMap.getUiSettings().setMapToolbarEnabled(false);
            buildGoogleApiClient();
        }
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }

    private void showMarkerInRadius() {
        mMarkersHashMap = new HashMap<Marker, CustomMarker>();

        ApiInterface apiInterface = AppUtil.getRetrofitMasjidNearme().create(ApiInterface.class);
        Call<GoogleMapsV2Response> call = apiInterface.getMasjidNearme(qCategory, myLat + "," + myLng, PROXIMITY_RADIUS);
        call.enqueue(new Callback<GoogleMapsV2Response>() {
            @Override
            public void onResponse(Call<GoogleMapsV2Response> call, Response<GoogleMapsV2Response> response) {
                try {
                    mMap.clear();
                    for (int i = 0; i < response.body().getResults().size(); i++) {
                        markerLat = response.body().getResults().get(i).getGeometry().getLocation().getLat();
                        markerLng = response.body().getResults().get(i).getGeometry().getLocation().getLng();
                        markerName = response.body().getResults().get(i).getName();
                        markerVicinity = response.body().getResults().get(i).getVicinity();

                        // insertAccount marker value
                        mMyMarkersArray.add(new CustomMarker(markerName, markerVicinity, "ico_nav_konten", markerLat, markerLng));
                    }
                    plotMarkers(mMyMarkersArray);

                } catch (Exception e) {
                    Log.d("onResponse", "There is an error");
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<GoogleMapsV2Response> call, Throwable t) {
                Log.d("onFailure", t.toString());
            }
        });

    }

    private void plotMarkers(ArrayList<CustomMarker> markers) {
        if(markers.size() > 0) {
            for (CustomMarker myMarker : markers) {
                // Create user marker with custom icon and other options
                MarkerOptions markerOption = new MarkerOptions().position(new LatLng(myMarker.getmLatitude(), myMarker.getmLongitude()));
                markerOption.icon(BitmapDescriptorFactory.fromResource(R.drawable.marker_mosque));

                Marker currMarker = mMap.addMarker(markerOption);

                // animated marker
                dropMarkerAnimation(currMarker);

                // set marker info window
                mMarkersHashMap.put(currMarker, myMarker);
                mMap.setInfoWindowAdapter(new MarkerInfoWindowAdapter());

                // move to myLocation
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(myLatLng, 15));
            }
        }
    }

    // check google play services availability
    private boolean isGooglePlayServicesAvailable() {
        GoogleApiAvailability googleAPI = GoogleApiAvailability.getInstance();
        int result = googleAPI.isGooglePlayServicesAvailable(this);
        if (result != ConnectionResult.SUCCESS) {
            if (googleAPI.isUserResolvableError(result)) {
                googleAPI.getErrorDialog(this, result,
                        0).show();
            }
            return false;
        }
        return true;
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(1000);
        mLocationRequest.setFastestInterval(1000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        }
    }

    @Override
    public void onConnectionSuspended(int i) {}

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {}

    @Override
    public void onLocationChanged(Location location) {
        //place marker at current position
        if (myLocationMarker != null) {
            myLocationMarker.remove();
        }
        //Toast.makeText(this,"Location Changed",Toast.LENGTH_SHORT).show();

        myLat = location.getLatitude();
        myLng = location.getLongitude();
        myLatLng = new LatLng(myLat, myLng);

        // generate marker within radius
        showMarkerInRadius();

        //zoom to my current position:
        showMyLocation(myLatLng);

        //stop location updates
        if (mGoogleApiClient != null) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
        }
    }

    public void showMyLocation(LatLng myLatLng) {
        // animate to my location
        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(myLatLng)      // Sets the center of the map to Mountain View
                .zoom(15)                   // Sets the zoom
                .tilt(30)                   // Sets the tilt of the camera to 30 degrees
                .build();                   // Creates a CameraPosition from the builder
        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
    }

    private void dropMarkerAnimation(final Marker marker) {
        final Handler handler = new Handler();
        final long start = SystemClock.uptimeMillis();
        final long duration = 1500;
        final Interpolator interpolator = new BounceInterpolator();
        handler.post(new Runnable() {
            @Override
            public void run() {
                long elapsed = SystemClock.uptimeMillis() - start;
                float t = Math.max(
                        1 - interpolator.getInterpolation((float) elapsed
                                / duration), 0);
                marker.setAnchor(0.5f, 1.0f + 14 * t);

                if (t > 0.0) {
                    // Post again 15ms later.
                    handler.postDelayed(this, 15);
                } else {
                    marker.showInfoWindow();

                }
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted. Do the
                    // contacts-related task you need to do.
                    if (ContextCompat.checkSelfPermission(this,
                            Manifest.permission.ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED) {

                        if (mGoogleApiClient == null) {
                            buildGoogleApiClient();
                        }
                        mMap.setMyLocationEnabled(true);
                    }

                } else {
                    // Permission denied, Disable the functionality that depends on this permission.
                    Toast.makeText(this, "permission denied", Toast.LENGTH_LONG).show();
                }
                return;
            }

            // other 'case' lines to check for other permissions this app might request.
            // You can add here other case statements according to your requirement.
        }
    }

    public void dialogEnabledLocationService() {
        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setCancelable(false);
        dialog.setMessage(this.getResources().getString(R.string.gps_network_not_enabled));
        dialog.setPositiveButton(this.getResources().getString(R.string.btn_txt_izinkan),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                        // TODO Auto-generated method stub
                        Intent myIntent = new Intent( Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivity(myIntent);
                    }
                });
        dialog.setNegativeButton(this.getString(R.string.btn_cancel), new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                // TODO Auto-generated method stub
                onBackPressed();
            }
        });
        dialog.show();
    }

    public class MarkerInfoWindowAdapter implements GoogleMap.InfoWindowAdapter {

        public MarkerInfoWindowAdapter() {}

        @Override
        public View getInfoWindow(Marker marker)
        {
            return null;
        }

        @Override
        public View getInfoContents(Marker marker) {
            View v  = getLayoutInflater().inflate(R.layout.window_info_marker, null);
            CustomMarker myMarker = mMarkersHashMap.get(marker);
            ImageView markerIcon = (ImageView) v.findViewById(R.id.marker_icon);
            TextView markerLabel = (TextView)v.findViewById(R.id.marker_label);
            TextView markerAddress = (TextView)v.findViewById(R.id.marker_address);
            markerIcon.setImageResource(AppUtil.getMapsMarkerIcon(myMarker.getmIcon()));
            markerIcon.setBackgroundResource(R.drawable.custom_icon_bg_orange);
            markerLabel.setText(myMarker.getmLabel());
            markerAddress.setText(myMarker.getmAddress());
            return v;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        mGoogleApiClient.disconnect();
    }

    private void customToolbarIslamic() {
        Toolbar tb_islamic = (Toolbar) findViewById(R.id.tb_islamic);
        TextView tv_tb_islamic = (TextView) findViewById(R.id.tv_tb_islamic);
        tb_islamic.setNavigationIcon(R.mipmap.abc_ic_ab_back_mtrl_am_alpha);
        tv_tb_islamic.setText(pageTitle);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(ContextCompat.getColor(MasjidNearmeActivity.this, Constants.Color.ISLAMIC));
        }

        tb_islamic.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;
    public boolean checkLocationPermission(){
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            // Asking user if explanation is needed
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

                //Prompt the user once explanation has been shown
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);

            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            }
            return false;
        } else {
            return true;
        }
    }
}