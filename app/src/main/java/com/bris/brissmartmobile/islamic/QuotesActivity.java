package com.bris.brissmartmobile.islamic;

import android.app.ProgressDialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bris.brissmartmobile.R;
import com.bris.brissmartmobile.util.Constants;
import com.bris.brissmartmobile.util.Menu;
import com.bris.brissmartmobile.util.SupportHadistResponse;
import com.bris.brissmartmobile.util.ApiClientAdapter;
import com.bris.brissmartmobile.util.AppPreferences;
import com.bris.brissmartmobile.model.Quotes;
import com.bris.brissmartmobile.util.AppUtil;
import com.bris.brissmartmobile.islamic.adapter.QuotesAdapter;

import net.i2p.android.ext.floatingactionbutton.FloatingActionButton;

import java.util.List;
import java.util.Random;

import io.realm.Realm;
import io.realm.RealmResults;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

;

/**
 * Created by ryputra on 12/10/2017.
 */

public class QuotesActivity extends AppCompatActivity {
    private Realm realm;
    private ProgressDialog mProgressDialog;
    private QuotesAdapter quoteAdapter;
    private TextView tvPosition;
    private FloatingActionButton fabCopy, fabShare;
    private ApiClientAdapter apiClientAdapter;
    private AppPreferences appPref;
    private RealmResults<Quotes> quotes;
    private String pageTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_amotivasi_islami);
        tvPosition = (TextView) findViewById(R.id.tv_quote_pos);
        realm = Realm.getDefaultInstance();
        apiClientAdapter = new ApiClientAdapter(QuotesActivity.this);
        appPref = new AppPreferences(QuotesActivity.this);
        pageTitle = getIntent().getExtras().getString(Menu.MENU_ID);
        customToolbarIslamic(pageTitle);
//        AppUtil.toolbarRegularWithCustomStatusBar(QuotesActivity.this, "Motivasi Islami", ContextCompat.getColor(this, R.color.colorPrimaryIslamic));


        initQuoteListOrUpdate();

        quotes = getAllQuotes();
        if (quotes.size() > 0)
            viewPagerQuotes(quotes);
        else
            onBackPressed();
    }

    @RequiresApi(api = Build.VERSION_CODES.HONEYCOMB)
    private void copyToClipboard(String kutipan) {
        ClipboardManager clipboard = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            ClipData clip = ClipData.newPlainText("Copy Hadist", kutipan);
            clipboard.setPrimaryClip(clip);
        } else {
            clipboard.setText(kutipan);
        }
        Toast.makeText(QuotesActivity.this, "Copied to clipboard", Toast.LENGTH_LONG).show();
    }

    private void shareQuote(String kutipan) {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, kutipan);
        sendIntent.setType("text/plain");
        startActivity(sendIntent);
    }

    private void initQuoteListOrUpdate() {
        AppUtil.showDialog(mProgressDialog);
        Call<SupportHadistResponse> responseCall = apiClientAdapter.getApiInterface().getHadist();
        responseCall.enqueue(new Callback<SupportHadistResponse>() {
            @Override
            public void onResponse(Call<SupportHadistResponse> call, Response<SupportHadistResponse> response) {
                AppUtil.hideDialog(mProgressDialog);
                if (response.isSuccessful()) {
                    String hadistVersion =  response.body().getMessage();
                    if (!appPref.getQuotesVersion().equals(hadistVersion)) {
                        saveToLocalDB(response.body().getData(), hadistVersion);
                    }
                }
                else {
                    AppUtil.showToastLong(getBaseContext(), "Gagal Sinkronisasi Data");
                }
            }

            @Override
            public void onFailure(Call<SupportHadistResponse> call, Throwable t) {
                AppUtil.hideDialog(mProgressDialog);
                AppUtil.showToastLong(QuotesActivity.this, "Pertama kali menggunakan, anda perlu terkonesi ke server");
            }
        });
    }

    private void viewPagerQuotes(final RealmResults<Quotes> items) {
        final ViewPager kutipanPager = (ViewPager) findViewById(R.id.vp_kutipan);
        quoteAdapter = new QuotesAdapter(getBaseContext(), items);
        kutipanPager.setAdapter(quoteAdapter);

        ViewPager.OnPageChangeListener pageChangeListener = new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(final int position) {
                tvPosition.setText((position+1) +"/"+ items.size());

                // fab action
                fabCopy = (FloatingActionButton) findViewById(R.id.fab_copy);
                fabShare = (FloatingActionButton) findViewById(R.id.fab_share);
                fabCopy.setOnClickListener(new View.OnClickListener() {
                    @RequiresApi(api = Build.VERSION_CODES.HONEYCOMB)
                    @Override
                    public void onClick(View view) {
                        copyToClipboard(getPositionSharedQuote((position+1)+""));
                    }
                });

                fabShare.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        shareQuote(getPositionSharedQuote((position+1)+""));
                    }
                });
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        };

        // Random pager position that first shown
        Random rand = new Random();
        int pos = rand.nextInt(items.size() - 0) + 0;
        kutipanPager.setOnPageChangeListener(pageChangeListener);
        pageChangeListener.onPageSelected(pos);
        kutipanPager.setCurrentItem(pos);
    }

    public void customToolbarIslamic(final String title) {
        Toolbar toolbar = (Toolbar) findViewById(R.id.tb_regular);
        ImageView btnBack = (ImageView) findViewById(R.id.btn_back);
        TextView tvPageTitle = (TextView) findViewById(R.id.tv_page_title);

        tvPageTitle.setText(title);
        toolbar.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimaryIslamic));
        setSupportActionBar(toolbar);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(ContextCompat.getColor(this, Constants.Color.ISLAMIC));
        }

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    private void saveToLocalDB(List<SupportHadistResponse.DataHadist> dataHadist, String last_version) {

        if (getAllQuotes() != null) {
            clearQuote();
        }

        for (int i=0; i<dataHadist.size(); i++) {
            addQuote(
                    dataHadist.get(i).getId()+"",
                    dataHadist.get(i).getAyah(),
                    dataHadist.get(i).getSpelling(),
                    dataHadist.get(i).getTranslation(),
                    dataHadist.get(i).getNarrators()
            );
        }

        appPref.setQuotesVersion(last_version);
        Log.d("Quotes", "version is created");

        // restart activity
        Intent i = new Intent(QuotesActivity.this, QuotesActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(i);
    }

    public void addQuote(String id, String ayat, String latin, String arti, String periwayat) {
        realm.beginTransaction();
        Quotes quotes = realm.createObject(Quotes.class);
        quotes.setId(id);
        quotes.setAyat(ayat);
        quotes.setLatin(latin);
        quotes.setArti(arti);
        quotes.setPeriwayat(periwayat);
        realm.commitTransaction();
    }

    private RealmResults<Quotes> getAllQuotes() {
        quotes = realm.where(Quotes.class).findAll();
        return quotes;
    }

    private void clearQuote() {
        realm.beginTransaction();
        quotes.deleteAllFromRealm();
        realm.commitTransaction();
    }

    private String getPositionSharedQuote(String id) {
        RealmResults<Quotes> quotes = realm.where(Quotes.class).equalTo("id", id).findAll();
        Log.d("return", String.valueOf(quotes));
        return quotes.get(0).getAyat() + "\n"
                + quotes.get(0).getArti()
                + quotes.get(0).getPeriwayat()
                + "\n\n" + "Hi, I'm using BRISyariah Motivation Quotes";
    }
}
