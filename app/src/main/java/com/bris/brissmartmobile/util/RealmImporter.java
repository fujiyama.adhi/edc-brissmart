package com.bris.brissmartmobile.util;

import android.content.res.Resources;
import android.util.Log;

import com.bris.brissmartmobile.R;
import com.bris.brissmartmobile.model.Ayat;
import com.bris.brissmartmobile.model.Hadist;
import com.bris.brissmartmobile.model.Surah;

import java.io.InputStream;

import io.realm.Realm;


/**
 * Created by valeputra on 7/27/17.
 */

public class RealmImporter {

    Resources resources;

    public RealmImporter(Resources resources) {
        this.resources = resources;
    }

    public void importFromJson(final String type) {
        Realm realm = Realm.getDefaultInstance();

        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                InputStream inputStream = null;

                Class classRealmObj = null;
                if ("hadist".equalsIgnoreCase(type)) {
                    inputStream = resources.openRawResource(R.raw.alhadist);
                    classRealmObj = Hadist.class;

                } else if("juz_amma".equalsIgnoreCase(type)) {
                    inputStream = resources.openRawResource(R.raw.list_surah);
                    classRealmObj = Surah.class;

                } else { // each of surah
                    Integer typeInt = Integer.parseInt(type);
                    inputStream = setSurahInputStream(inputStream, typeInt);
                    classRealmObj = Ayat.class;
                }

                try {
                    realm.createAllFromJson(classRealmObj, inputStream);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        Log.d("Realm", "createAllFromJson Task completed");
    }

    private InputStream setSurahInputStream(InputStream inputStream, Integer typeInt) {
        switch (typeInt) {
            case 1:
                inputStream = resources.openRawResource(R.raw.surah_1);
                break;
            case 78:
                inputStream = resources.openRawResource(R.raw.surah_78);
                break;
            case 79:
                inputStream = resources.openRawResource(R.raw.surah_79);
                break;
            case 80:
                inputStream = resources.openRawResource(R.raw.surah_80);
                break;
            case 81:
                inputStream = resources.openRawResource(R.raw.surah_81);
                break;
            case 82:
                inputStream = resources.openRawResource(R.raw.surah_82);
                break;
            case 83:
                inputStream = resources.openRawResource(R.raw.surah_83);
                break;
            case 84:
                inputStream = resources.openRawResource(R.raw.surah_84);
                break;
            case 85:
                inputStream = resources.openRawResource(R.raw.surah_85);
                break;
            case 86:
                inputStream = resources.openRawResource(R.raw.surah_86);
                break;
            case 87:
                inputStream = resources.openRawResource(R.raw.surah_87);
                break;
            case 88:
                inputStream = resources.openRawResource(R.raw.surah_88);
                break;
            case 89:
                inputStream = resources.openRawResource(R.raw.surah_89);
                break;
            case 90:
                inputStream = resources.openRawResource(R.raw.surah_90);
                break;
            case 91:
                inputStream = resources.openRawResource(R.raw.surah_91);
                break;
            case 92:
                inputStream = resources.openRawResource(R.raw.surah_92);
                break;
            case 93:
                inputStream = resources.openRawResource(R.raw.surah_93);
                break;
            case 94:
                inputStream = resources.openRawResource(R.raw.surah_94);
                break;
            case 95:
                inputStream = resources.openRawResource(R.raw.surah_95);
                break;
            case 96:
                inputStream = resources.openRawResource(R.raw.surah_96);
                break;
            case 97:
                inputStream = resources.openRawResource(R.raw.surah_97);
                break;
            case 98:
                inputStream = resources.openRawResource(R.raw.surah_98);
                break;
            case 99:
                inputStream = resources.openRawResource(R.raw.surah_99);
                break;
            case 100:
                inputStream = resources.openRawResource(R.raw.surah_100);
                break;
            case 101:
                inputStream = resources.openRawResource(R.raw.surah_101);
                break;
            case 102:
                inputStream = resources.openRawResource(R.raw.surah_102);
                break;
            case 103:
                inputStream = resources.openRawResource(R.raw.surah_103);
                break;
            case 104:
                inputStream = resources.openRawResource(R.raw.surah_104);
                break;
            case 105:
                inputStream = resources.openRawResource(R.raw.surah_105);
                break;
            case 106:
                inputStream = resources.openRawResource(R.raw.surah_106);
                break;
            case 107:
                inputStream = resources.openRawResource(R.raw.surah_107);
                break;
            case 108:
                inputStream = resources.openRawResource(R.raw.surah_108);
                break;
            case 109:
                inputStream = resources.openRawResource(R.raw.surah_109);
                break;
            case 110:
                inputStream = resources.openRawResource(R.raw.surah_110);
                break;
            case 111:
                inputStream = resources.openRawResource(R.raw.surah_111);
                break;
            case 112:
                inputStream = resources.openRawResource(R.raw.surah_112);
                break;
            case 113:
                inputStream = resources.openRawResource(R.raw.surah_113);
                break;
            case 114:
                inputStream = resources.openRawResource(R.raw.surah_114);
                break;
        }

        return inputStream;
    }
}

