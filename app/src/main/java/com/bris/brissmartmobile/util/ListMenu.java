package com.bris.brissmartmobile.util;

/**
 * Created by valeputra on 7/5/17.
 */

public class ListMenu {
    private int icon;
    private String title;
    private int icon_next;

    public ListMenu(int icon, String title, int icon_next) {
        this.icon = icon;
        this.title = title;
        this.icon_next = icon_next;
    }

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getIcon_next() {
        return icon_next;
    }

    public void setIcon_next(int icon_next) {
        this.icon_next = icon_next;
    }
}
