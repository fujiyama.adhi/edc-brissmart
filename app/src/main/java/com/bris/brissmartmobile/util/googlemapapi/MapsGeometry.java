package com.bris.brissmartmobile.util.googlemapapi;

import com.bris.brissmartmobile.model.Location;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by valeputra on 7/24/17.
 */

public class MapsGeometry {
    @SerializedName("location")
    @Expose
    private Location location;

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }
}
