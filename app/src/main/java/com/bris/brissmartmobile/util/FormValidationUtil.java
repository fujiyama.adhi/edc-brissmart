package com.bris.brissmartmobile.util;

import android.content.Context;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.bris.brissmartmobile.R;

/**
 * Created by ryputra on 19/10/2017.
 */

public class FormValidationUtil {

    public static final int minLengthPhoneNum = 9;

    public static void displayCustomDialog(Context context, String message, String header){
        LayoutInflater li = LayoutInflater.from(context);
        View msgDialog = li.inflate(R.layout.custom_dialog_message, null);
        Button btnOkMessage = (Button) msgDialog.findViewById(R.id.btn_close);
        TextView txt_header = (TextView) msgDialog.findViewById(R.id.dialog_txt_header);
        TextView txt_msg_display = (TextView) msgDialog.findViewById(R.id.dialog_txt_msg_info);

        txt_header.setText(header);
        txt_msg_display.setText(message);

        AlertDialog.Builder alert = new AlertDialog.Builder(context);
        alert.setView(msgDialog);
        alert.setCancelable(false);
        final AlertDialog dialog = alert.create();

        btnOkMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    public static boolean isPhoneNumValid(String phoneNum) {
        if (phoneNum.length() < minLengthPhoneNum || !phoneNum.substring(0,2).equals("08")) {
            return false;
        }
        return true;
    }

    public static void validationField(Context context, String field, TextInputLayout til) {
        if(field.trim().length() < 1) til.setError(context.getString(R.string.alert_field_empty));
        return;
    }
}
