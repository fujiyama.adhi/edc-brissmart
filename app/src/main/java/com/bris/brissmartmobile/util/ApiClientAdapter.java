package com.bris.brissmartmobile.util;

import android.content.Context;

import com.bris.brissmartmobile.model.OAuthRefreshAccessRequest;
import com.bris.brissmartmobile.model.OAuthRefreshAccessResponse;
import com.google.gson.Gson;

import java.io.IOException;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by ryputra on 16/01/2018.
 */

public class ApiClientAdapter {
    private static Retrofit restClientAdapter;
    private ApiInterface apiInterface;
    public static final String API_BASE_URL = UriService.BaseUrl.URL;
    public static final GsonConverterFactory gsonFactory = GsonConverterFactory.create(new Gson());

    public ApiInterface getApiInterface() {
        return apiInterface;
    }

    private String getGrantAccessTokenItem(Context context, String itemKey) {
        AppPreferences appPreferences = new AppPreferences(context);
        HashMap<String, String> item = appPreferences.getGrantAccess();
        return item.get(itemKey) != null ? item.get(itemKey) : "";
    }

    public ApiClientAdapter(final Context context) {
//        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
//        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        final OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Interceptor.Chain chain) throws IOException {
                Request request = chain.request();

                // Build init request
                Request.Builder requestBuilder = request.newBuilder();
                setAuthHeader(requestBuilder, getGrantAccessTokenItem(context, Constants.Preferences.ACCESS_TOKEN)); //write current token to request

                request = requestBuilder.build();
                Response response = chain.proceed(request); //perform request, here original request will be executed

                if (context != null) {
                    switch (response.code()) {
                        case 401:
                            synchronized (httpClient) { //perform all 401 in sync blocks, to avoid multiply token updates
                                setAuthHeader(requestBuilder, getNewAccessToken(context)); //set auth token to updated
                                request = requestBuilder.build();
                                return chain.proceed(request);
                            }
                        case 400:
                            AppUtil.displayCustomDialog(context, "Sesi habis, anda perlu login kembali.", "Sesi Habis");
                            break;
                    }
                }

                return response;
            }

            private void setAuthHeader(Request.Builder builder, String token) {
                if (token != null) {//Add Auth token to each request if authorized
                    builder.header("Content-Type", "application/json");
                    builder.header("Authorization", String.format("Bearer %s", token));
                }
            }

            private String getNewAccessToken(Context context) throws IOException {
                // Ger refresh token
                AppPreferences appPreferences = new AppPreferences(context);
                String refreshToken = appPreferences.getRefreshToken();

                // Call service
                String clientId = Keys.Client.CLIENT_ID;
                String secretId = Keys.Client.CLIENT_SECRET;
                OAuthRefreshAccessRequest refreshRequest = new OAuthRefreshAccessRequest(clientId, secretId, refreshToken);
                ApiClientAdapter apiClientAdapter = new ApiClientAdapter(context);
                Call<OAuthRefreshAccessResponse> call = apiClientAdapter.getApiInterface().getNewAccessToken(refreshRequest);
                OAuthRefreshAccessResponse responseResult = call.execute().body();

                if (responseResult != null) {
                    // store new access token
                    appPreferences.setGrantAccess(
                            responseResult.getDataToken().getTokenType(),
                            responseResult.getDataToken().getAccessToken(),
                            responseResult.getDataToken().getRefreshToken(),
                            responseResult.getDataToken().getExpiresIn()
                    );

                    return responseResult.getDataToken().getAccessToken();
                } else {
                    return "";
                }
            }
        })
                .connectTimeout(30, TimeUnit.SECONDS)
//        .addInterceptor(interceptor)
                .readTimeout(30,TimeUnit.SECONDS);

        OkHttpClient client = httpClient.build();
        restClientAdapter = new Retrofit.Builder()
                .baseUrl(API_BASE_URL)
                .addConverterFactory(new NullOnEmptyConverterFactory())
                .addConverterFactory(gsonFactory)
                .client(client)
                .build();

        apiInterface = restClientAdapter.create(ApiInterface.class);
    }
}