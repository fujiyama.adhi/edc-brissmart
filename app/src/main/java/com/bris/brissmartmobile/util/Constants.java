package com.bris.brissmartmobile.util;

import com.bris.brissmartmobile.R;

/**
 * Created by ryputra on 15/01/2018.
 */

public class Constants {

    public static class AppConfig {
        public static final int APP_LIFETIMES = 120000; // foreground counter ms / 2 mnt
        //        public static final int APP_LIFETIMES = 10000; // test 10 s
        public static final int APP_BG_LIFETIMES = 30000; // background counter ms
    }

    public static class Preferences {
        public static final String MAIN_CONFIG = "mobileBrisPref";
        public static final String APP_VERSION = "app_version";
        public static final String QUOTES_VERSION = "qoutes_version";
        public static final String LAST_TRANSACTION = "last_trx";
        public static final String QIBLAT_DEGREE = "qiblat_degree";
        public static final String USER_LOCATION_INFO = "user_loc_info";
        public static final String NOTIFICATION = "notif_";
        public static final String USERINFO = "user_info_";
        public static final String BANKINFO = "bank_info_";
        public static final String SETTING_USER_PREF = "setting_user_pref_";
        public static final String[] SETTINGS_KEY = {"acc_default"};
        public static final String IS_DIALOG_CALLBRIS_ENABLE = "is_dialog_callbris_enable";
        public static final String TID_DEVICE = "device_tid";

        public static final String TOKEN_BUNDLE = "token_bundle";
        public static final String TOKEN_TYPE = "token_type";
        public static final String ACCESS_TOKEN = "access_token";
        public static final String REFRESH_TOKEN = "refresh_token";
        public static final String EXPIRES_IN = "expires_in";

        public static final String AVATAR_NAME = "avatar_name";
        public static final String AVATAR_IMG = "avatar_image";

        public static final String USER_AUTH = "user_auth";

        public static final String IS_APP_LOCKED = "is_app_locked";
        public static final String APP_TIME_UNLOCKED = "app_time_unlocked";

        public static final String HEADLINE_PROMO = "headline_promo";
    }

    public class Color {
        public static final int ACCOUNT_INFO = R.color.menuBlue;
        public static final int TRANSFER = R.color.menuGreen;
        public static final int PAYMENT = R.color.menuOrange;
        public static final int PURCHASE = R.color.menuRed;
//        public static final int PRIMARY = R.color.colorPrimary;
        public static final int PRIMARY = R.color.black;
        public static final int ISLAMIC = R.color.colorPrimaryIslamic;
    }

    public class Command {
        public static final String SALDO = "SALDO";
        public static final String MUTASI = "MUTASI";
        public static final String MUTASIMOB = "MUTASIMOB";
        public static final String TRF = "TRANSFER";
        public static final String TRFA = "TRANSFERBANKLAIN";
        public static final String PURCHASE = "BELI";
        public static final String PAYMENT = "BAYAR";
        public static final String GANTIPIN = "GANTI PIN";
        public static final String PIN = "PIN ";
    }

    public class DeviceInfo {
        public static final String DEVICE_ID = "device_id";
        public static final String IMEI = "imei";
    }
}
