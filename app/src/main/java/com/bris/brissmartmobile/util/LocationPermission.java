package com.bris.brissmartmobile.util;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;

import com.bris.brissmartmobile.R;
import com.bris.brissmartmobile.calculate.QiblaCalculator;
import com.bris.brissmartmobile.model.LocationInfo;
import com.bris.brissmartmobile.services.FusedLocationServices;
import com.google.android.gms.location.LocationListener;

import java.io.IOException;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

/**
 * Created by ryputra on 20/10/2017.
 */

public class LocationPermission implements LocationListener {
    public static final int REQUEST_CODE = 1;
    private static final int REQUEST_GPS_LOCATION = 113;
    public static Context mContext;

    public LocationPermission(Context context) {
        this.mContext = context;
    }

    public static boolean gpsEnabledInLocation(final Context context, int flag) {
        final LocationManager manager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);

        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            final AlertDialog.Builder builder = new AlertDialog.Builder(context);

            if (flag > 0) {
                if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_FINE_LOCATION)
                        != PackageManager.PERMISSION_GRANTED &&
                        ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_COARSE_LOCATION)
                                != PackageManager.PERMISSION_GRANTED)
                {
                    Log.d("FragmentContent Dialog Permission", "CHECKED");
                    final AlertDialog.Builder builderPermission = new AlertDialog.Builder(context);
                    builderPermission.setMessage("Mohon beri izin Bris Online mendapatkan lokasi Anda untuk menggunakan fitur Jadwal Sholat.")
                            .setPositiveButton("Izinkan", new DialogInterface.OnClickListener() {
                                public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                                    final AlertDialog alert = builder.create();
                                    alert.show();
                                    ActivityCompat.requestPermissions((Activity) mContext,
                                            new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION},
                                            REQUEST_GPS_LOCATION);
                                }
                            })
                            .setNegativeButton("Nanti", new DialogInterface.OnClickListener() {
                                public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                                    dialog.dismiss();
                                }
                            });
                }

                Log.d("FragmentContent Dialog Permission2", "CHECKED "+flag);

                builder.setMessage(context.getString(R.string.dialog_alert_gps))
                    .setPositiveButton(context.getString(R.string.btn_yes), new DialogInterface.OnClickListener() {
                        public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                            ((Activity) context).startActivityForResult(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS), REQUEST_CODE);
                        }
                    })
                    .setNegativeButton("Batal", new DialogInterface.OnClickListener() {
                        public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                            dialog.dismiss();
                        }
                    });
                final AlertDialog alert = builder.create();
                alert.show();

            } else {

                if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_FINE_LOCATION)
                        != PackageManager.PERMISSION_GRANTED &&
                        ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_COARSE_LOCATION)
                                != PackageManager.PERMISSION_GRANTED) {
                    final AlertDialog.Builder builderPermission = new AlertDialog.Builder(context);
                    builderPermission.setCancelable(false);
                    builderPermission.setMessage("Beri izin Bris Online mendapatkan lokasi Anda untuk menggunakan fitur Jadwal Sholat dan Arah Kiblat.")
                            .setPositiveButton("Izinkan", new DialogInterface.OnClickListener() {
                                public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                                    ActivityCompat.requestPermissions((Activity) mContext,
                                            new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION},
                                            REQUEST_GPS_LOCATION);

                                    setRequestGpsLocation(context);
                                }
                            })
                            .setNegativeButton(context.getString(R.string.btn_no), new DialogInterface.OnClickListener() {
                                public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                                    dialog.dismiss();
                                }
                            });

                    final AlertDialog alert2 = builderPermission.create();
                    alert2.show();

                } else {
                    setRequestGpsLocation(context);
                }
            }
            return false;
        }

        return true;
    }

    private static FusedLocationServices gps = null;
    private static ProgressDialog progressDialog = null;
    private static void getLocation(int flag) {
        if (LocationPermission.gpsEnabledInLocation(mContext, flag)) {
            gps = new FusedLocationServices(mContext, new LocationPermission(mContext));

            Log.d("Cek lat", String.valueOf(gps.getFusedLatitude()));
            Log.d("Cek long", String.valueOf(gps.getFusedLongitude()));
            Log.d("Cek isGoogleApiClientConnected", String.valueOf(gps.isGoogleApiClientConnected()));
            Log.d("Cek OIIIIIIIIIIII 2", String.valueOf(gps.toString()));

            if (gps != null) {
                progressDialog = new ProgressDialog(mContext);
                progressDialog.setIndeterminate(true);
                progressDialog.setCancelable(false);
                progressDialog.setMessage("Harap Tunggu");
                progressDialog.show();

                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        progressDialog.dismiss();
                    }
                }, 2000);
            }
        }
    }


    private Location currLocation = null;
    @Override
    public void onLocationChanged(Location location) {
        Log.d("Cek OIIIIIIIIIIII", "Mlebu wes");

        if (location != null && currLocation == null) {
            currLocation = location;
            gps.setFusedLatitude(location.getLatitude());
            gps.setFusedLongitude(location.getLongitude());

            if (gps.getFusedLatitude() != 0 && gps.getFusedLongitude() != 0) {
                Log.d("Cek onLocationChanged", "Mlebu");
                manageUserLocation();
                gps.stopFusedLocation();
            }
        }
    }

    private LocationInfo locationInfo;
    private void manageUserLocation() {
        Geocoder geocoder = new Geocoder(mContext, Locale.getDefault());
        try {
            if (geocoder.isPresent()) {
                List<Address> addresses = geocoder.getFromLocation(gps.getFusedLatitude(), gps.getFusedLongitude(), 1);
                Log.d("Cek addresses", addresses.toString());
                if (addresses != null) {
                    Address userAddress = addresses.get(0);
                    Calendar cal = Calendar.getInstance();
                    int dst = cal.getTimeZone().getDSTSavings();    // Returns the amount of time to be added to local standard time
                    int timeZone = cal.getTimeZone().getRawOffset() / (1000 * 60 * 60);

                    // save to pref
                    locationInfo = new LocationInfo(
                            userAddress.getLatitude(), userAddress.getLongitude(),
                            userAddress.getCountryName(), userAddress.getCountryCode(),
                            userAddress.getAdminArea(), userAddress.getLocality(),
                            0, 0, dst, timeZone
                    );

                    locationInfo.dls = dst;
                    locationInfo.mazhab = 0;
                    locationInfo.way = 0;

                    AppPreferences appPref = new AppPreferences(mContext);
                    appPref.setLocationConfig(locationInfo);
                    appPref.setQiblatDegree((float) QiblaCalculator.doCalculate(gps.getFusedLatitude(), gps.getFusedLongitude()));
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
            AppUtil.showToastShort(mContext, "Gagal mendapatkan lokasi");
        }
    }

    /**
     * @Param flag (0=auto, 1= manual)
     **/
    public static void updateLocation(int flag) {
        try {
//            if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_FINE_LOCATION)
//                    != PackageManager.PERMISSION_GRANTED &&
//                    ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_COARSE_LOCATION)
//                            != PackageManager.PERMISSION_GRANTED) {
//                ActivityCompat.requestPermissions((Activity) mContext,
//                        new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION},
//                        REQUEST_GPS_LOCATION);
//            } else {
                getLocation(flag);  //start to detect user loaction
//            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.d("Cek catch updateLocation", "Mlebu");
        }
    }

    public static void setRequestGpsLocation(final Context context) {
        final AlertDialog.Builder builder2 = new AlertDialog.Builder(context);
        builder2.setCancelable(true);
        builder2.setMessage(context.getString(R.string.dialog_alert_gps))
                .setPositiveButton(context.getString(R.string.btn_yes), new DialogInterface.OnClickListener() {
                    public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        ((Activity) context).startActivityForResult(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS), REQUEST_CODE);
                    }
                })
                .setNegativeButton(context.getString(R.string.btn_no), new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        dialog.dismiss();
                    }
                });

        final AlertDialog alert2 = builder2.create();
        alert2.show();
    }

}
