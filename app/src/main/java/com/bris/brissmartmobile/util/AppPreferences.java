package com.bris.brissmartmobile.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;

import com.bris.brissmartmobile.model.LocationInfo;
import com.google.gson.Gson;

import java.util.HashMap;

/**
 * Created by ryputra on 13/10/2017.
 */


public class AppPreferences {

    private SharedPreferences sharedPref;
    private SharedPreferences.Editor spEditor;
    private Context mContext;

    private String TOKEN_TYPE = Constants.Preferences.TOKEN_TYPE;
    private String ACCESS_TOKEN = Constants.Preferences.ACCESS_TOKEN;
    private String REFRESH_TOKEN = Constants.Preferences.REFRESH_TOKEN;
    private String EXPIRES_IN = Constants.Preferences.EXPIRES_IN;

    public AppPreferences(Context context) {
        this.mContext = context;
        sharedPref = mContext.getSharedPreferences(Constants.Preferences.MAIN_CONFIG, Context.MODE_PRIVATE);
        spEditor = sharedPref.edit();
    }

    /********** APP VERS **************/
    public void setAppVersion(String version) {
        spEditor.putString(Constants.Preferences.APP_VERSION, version);
        spEditor.commit();
    }

    public String getAppVersion() {
        return sharedPref.getString(Constants.Preferences.APP_VERSION, "");
    }

    /*************** TID **************/
    public void setDeviceTid(String tid) {
        spEditor.putString(Constants.Preferences.TID_DEVICE, tid);
        spEditor.commit();
    }

    public String getDeviceTid() {
        return sharedPref.getString(Constants.Preferences.TID_DEVICE, "");
    }

    /********** USER **************/
    public void setUsername(String uname) {
        spEditor.putString(Constants.Preferences.USERINFO + "name", uname);
        spEditor.commit();
    }

    public String getUserName() {
        return sharedPref.getString(Constants.Preferences.USERINFO + "name", "");
    }

    public void setAvatarImage(Bitmap bitmap, String fileName) {
        if (bitmap != null) {
            spEditor.putString(Constants.Preferences.USERINFO + Constants.Preferences.AVATAR_IMG, ConverterBase64.encodeTobase64(bitmap));
        }

        spEditor.putString(Constants.Preferences.USERINFO + Constants.Preferences.AVATAR_NAME, fileName);
        spEditor.commit();
    }

    public HashMap<String, String> getAvatarImage() {
        HashMap<String, String> avatar = new HashMap<>();
        avatar.put(Constants.Preferences.AVATAR_NAME, sharedPref.getString(Constants.Preferences.USERINFO + Constants.Preferences.AVATAR_NAME, null));
        avatar.put(Constants.Preferences.AVATAR_IMG, sharedPref.getString(Constants.Preferences.USERINFO + Constants.Preferences.AVATAR_IMG, null));
        return avatar;
    }

    /********** AUTH **************/
    public void setUserAuth(String xpin) {
        spEditor.putString(Constants.Preferences.USER_AUTH, xpin);
        spEditor.commit();
    }

    public String getUserAuth() {
        SharedPreferences sharedPref = mContext.getSharedPreferences
                (Constants.Preferences.MAIN_CONFIG, Context.MODE_PRIVATE);
        return sharedPref.getString(Constants.Preferences.USER_AUTH, "");
    }

    /********** SETTING **************/
    public void setIsAppLocked(boolean isLocked) {
        spEditor.putBoolean(Constants.Preferences.IS_APP_LOCKED, isLocked);
        spEditor.commit();
    }

    public boolean getIsAppLocked() {
        return sharedPref.getBoolean(Constants.Preferences.IS_APP_LOCKED, false);
    }

    public void setAppTimeUnlocked(long time) {
        spEditor.putLong(Constants.Preferences.APP_TIME_UNLOCKED, time);
        spEditor.commit();
    }

    public long getAppTimeUnlocked() {
        return sharedPref.getLong(Constants.Preferences.APP_TIME_UNLOCKED, 0);
    }

    public void setSettingUserPrefByType(String key, String val) {
        spEditor.putString(Constants.Preferences.SETTING_USER_PREF + key, val);
        spEditor.commit();
    }

    public static String getSettingUserPrefByType(Context context, String key) {
        SharedPreferences sharedPref = context.getSharedPreferences
                (Constants.Preferences.MAIN_CONFIG, Context.MODE_PRIVATE);
        return sharedPref.getString(Constants.Preferences.SETTING_USER_PREF + key, "");
    }

    /********** VERSION SOURCE **************/
    public void setQuotesVersion(String version) {
        spEditor.putString(Constants.Preferences.QUOTES_VERSION, version);
        spEditor.commit();
    }

    public String getQuotesVersion() {
        return sharedPref.getString(Constants.Preferences.QUOTES_VERSION, "");
    }

    /********** LAST TRX **************/
    public void setLastTrx(String times) {
        spEditor.putString(Constants.Preferences.LAST_TRANSACTION, times);
        spEditor.commit();
    }

    public String getLastTrx() {
        return sharedPref.getString(Constants.Preferences.LAST_TRANSACTION, "");
    }

    /********** CONTENT - QIBLA **************/
    public void setQiblatDegree(float degree) {
        spEditor.putFloat(Constants.Preferences.QIBLAT_DEGREE, degree);
        spEditor.commit();
    }

    public float getQiblaDegree() {
        return sharedPref.getFloat(Constants.Preferences.QIBLAT_DEGREE, -1);
    }

    /********** CONTENT - USER LOC **************/
    public void setLocationConfig(LocationInfo locationConfig) {
        Gson gson = new Gson();
        String json = gson.toJson(locationConfig);
        spEditor.putString(Constants.Preferences.USER_LOCATION_INFO, json);
        spEditor.commit();
    }

    public LocationInfo getLocationConfig() {
        Gson gson = new Gson();
        String json = sharedPref.getString(Constants.Preferences.USER_LOCATION_INFO, "");
        LocationInfo locationInfo = gson.fromJson(json, LocationInfo.class);
        return locationInfo;
    }

    public void removeLocationConfig() {
        sharedPref.edit().remove(Constants.Preferences.USER_LOCATION_INFO).commit();
    }

    /********** NOTIFICATION **************/
    public static void disableNotif(Context context, String notifId) {
        SharedPreferences.Editor editor = context.getSharedPreferences
                (Constants.Preferences.MAIN_CONFIG, Context.MODE_PRIVATE).edit();
        editor.putBoolean(Constants.Preferences.NOTIFICATION + notifId, false);
        editor.commit();
    }

    public boolean isNotifEnabled(Context context, String notifId) {
        return sharedPref.getBoolean(Constants.Preferences.NOTIFICATION + notifId, true);
    }

    /************** TOKEN ***************/
    public void setGrantAccess(String type, String accessToken, String refreshToken, String expiresTime) {
        spEditor.putString(TOKEN_TYPE, type);
        spEditor.putString(ACCESS_TOKEN, accessToken);
        spEditor.putString(REFRESH_TOKEN, refreshToken);
        spEditor.putString(EXPIRES_IN, expiresTime);
        spEditor.commit();
    }

    public HashMap<String, String> getGrantAccess() {
        HashMap<String, String> grantAccess = new HashMap<>();
        grantAccess.put(TOKEN_TYPE, sharedPref.getString(TOKEN_TYPE, null));
        grantAccess.put(ACCESS_TOKEN, sharedPref.getString(ACCESS_TOKEN, null));
        grantAccess.put(REFRESH_TOKEN, sharedPref.getString(REFRESH_TOKEN, null));
        grantAccess.put(EXPIRES_IN, sharedPref.getString(EXPIRES_IN, null));
        return grantAccess;
    }

    /********** KODE BANK **********/
    public void setKodeBank (String kode_bank, String network_priority) {
        spEditor.putString(Constants.Preferences.BANKINFO + "Id", kode_bank);
        spEditor.putString(Constants.Preferences.BANKINFO + "NetworkPriority", network_priority);
        spEditor.commit();
    }

    public HashMap<String, String> getKodeBank() {
        HashMap<String, String> bank = new HashMap<>();
        bank.put("kode_bank", sharedPref.getString(Constants.Preferences.BANKINFO + "Id", null));
        bank.put("network_priority", sharedPref.getString(Constants.Preferences.BANKINFO + "NetworkPriority", null));
        return bank;
    }

    /************** BANKING ***************/
//    public void setHeadlinePromoList(List<BankingPromoResponse.DataPromo> dataPromos) {
//        Gson gson = new Gson();
//        String jsonFavorites = gson.toJson(dataPromos);
//        spEditor.putString(Constants.Preferences.HEADLINE_PROMO, jsonFavorites);
//    }
//
//    public List<BankingPromoResponse.DataPromo> getHeadlinePromoList() {
//
//        String jsonPromo = mContext.getSharedPreferences(Constants.Preferences.MAIN_CONFIG,
//                Context.MODE_PRIVATE).getString(Constants.Preferences.HEADLINE_PROMO, null);
//
//        Gson gson = new Gson();
//        BankingPromoResponse.DataPromo[] promoItems = gson.fromJson(jsonPromo, BankingPromoResponse.DataPromo[].class);
//        List<BankingPromoResponse.DataPromo> promos = Arrays.asList(promoItems);
//        return promos;
//    }

    public String getRefreshToken() {
        return sharedPref.getString(Constants.Preferences.REFRESH_TOKEN, "");
    }

    public static void clearAll(Context context) {
        SharedPreferences pref = context.getSharedPreferences(Constants.Preferences.MAIN_CONFIG, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.clear();
        editor.commit();
    }

    public void removeByKey(String key) {
        sharedPref.edit().remove(key).commit();
    }

}
