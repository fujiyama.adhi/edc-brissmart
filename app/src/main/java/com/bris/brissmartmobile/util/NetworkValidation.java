package com.bris.brissmartmobile.util;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;

import com.bris.brissmartmobile.R;
import com.bris.brissmartmobile.calculate.QiblaCalculator;
import com.bris.brissmartmobile.database.ConfigSharedPref;
import com.bris.brissmartmobile.model.LocationInfo;
import com.bris.brissmartmobile.services.FusedLocationServices;
import com.google.android.gms.location.LocationListener;

import java.io.IOException;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

/**
 * Created by ryputra on 20/10/2017.
 */

public class NetworkValidation implements LocationListener {
    public static final int REQUEST_CODE = 1;
    public static Context mContext;

    public NetworkValidation (Context context) {
        this.mContext = context;
    }

    public static boolean gpsEnabledInLocation(final Context context) {
        final LocationManager manager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            final AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setMessage(context.getString(R.string.dialog_alert_gps))
                    .setPositiveButton(context.getString(R.string.btn_yes), new DialogInterface.OnClickListener() {
                        public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                            ((Activity)context).startActivityForResult(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS) , REQUEST_CODE);
                        }
                    })
                    .setNeutralButton(context.getString(R.string.btn_no), new DialogInterface.OnClickListener() {
                        public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                            dialog.dismiss();
                        }
                    })
                    .setNegativeButton("Tidak, Jangan Tanya Lagi", new DialogInterface.OnClickListener() {
                        public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                            ConfigSharedPref.disableNotif(context, "gps");
                            dialog.dismiss();
                        }
                    });
            final AlertDialog alert = builder.create();
            alert.show();
            return false;
        }

        return true;
    }

    private static final int REQUEST_GPS_LOCATION = 113;
    public static void setUpLocationPermission() {
        if (ConfigSharedPref.getLocationConfig(mContext) == null) {
            if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_FINE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED &&
                    ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_COARSE_LOCATION)
                            != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions((Activity) mContext,
                        new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION},
                        REQUEST_GPS_LOCATION);
            } else {
                try {
                    getLocation();  //start to detect user loaction
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private static FusedLocationServices gps = null;
    private static ProgressDialog progressDialog = null;
    private static void getLocation() {
        if (NetworkValidation.gpsEnabledInLocation(mContext)) {
            gps = new FusedLocationServices(mContext, new NetworkValidation(mContext));

            if (gps != null) {
                progressDialog = new ProgressDialog(mContext);
                progressDialog.setIndeterminate(true);
                progressDialog.setCancelable(false);
                progressDialog.setMessage("Harap Tunggu");
                progressDialog.show();

                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        progressDialog.dismiss();
                    }
                }, 1000);
            }

            Log.d("loc GPS", String.valueOf(gps));
        }
    }

    private Location currLocation = null;
    @Override
    public void onLocationChanged(Location location) {
        Log.d("loc GPS", String.valueOf(location));
        if (location != null && currLocation == null) {
            currLocation = location;
            gps.setFusedLatitude(location.getLatitude());
            gps.setFusedLongitude(location.getLongitude());

            Log.d("Location gps.getFusedLatitude", "lat:" +gps.getFusedLatitude());

            if (gps.getFusedLatitude() != 0 && gps.getFusedLongitude() != 0) {

                // user location
                manageUserLocation();

                gps.stopFusedLocation();
            }
        }
    }

    private LocationInfo locationInfo;
    private void manageUserLocation() {
        Geocoder geocoder = new Geocoder(mContext, Locale.getDefault());
        try {
            if (geocoder.isPresent()) {
                Log.d("loc Geocoder", "its supported");
                List<Address> addresses = geocoder.getFromLocation(gps.getFusedLatitude(), gps.getFusedLongitude(), 1);
                Log.d("loc geocoder - address", String.valueOf(addresses));
                if (addresses != null) {
                    Log.d("loc geocoder - address - not null", String.valueOf(addresses));
                    Address userAddress = addresses.get(0);
                    Calendar cal = Calendar.getInstance();
                    int dst = cal.getTimeZone().getDSTSavings();    // Returns the amount of time to be added to local standard time
                    int timeZone = cal.getTimeZone().getRawOffset() / (1000 * 60 * 60);

                    Log.d("locInfo - latitude", userAddress.getLatitude() + "");
                    Log.d("locInfo - long", "" + userAddress.getLongitude());
                    Log.d("locInfo - countryName", "" + userAddress.getCountryName());
                    Log.d("locInfo - contryCode", "" + userAddress.getCountryCode());
                    Log.d("locInfo - subAdmin, AdminArea", "" + userAddress.getSubAdminArea() + ", " + userAddress.getAdminArea());
                    Log.d("locInfo - locality", "" + userAddress.getLocality());
                    Log.d("locInfo - dst", "" + dst);
                    Log.d("locInfo - timeZone", "" + timeZone);

                    // save to pref
                    locationInfo = new LocationInfo(
                            userAddress.getLatitude(), userAddress.getLongitude(),
                            userAddress.getCountryName(), userAddress.getCountryCode(),
                            userAddress.getAdminArea(), userAddress.getLocality(),
                            0, 0, dst, timeZone
                    );

                    locationInfo.dls = dst;
                    locationInfo.mazhab = 0;
                    locationInfo.way = 0;

                    ConfigSharedPref.setLocationConfig(mContext, locationInfo);
                    ConfigSharedPref.setQiblatDegree(mContext,
                            (float) QiblaCalculator.doCalculate(gps.getFusedLatitude(), gps.getFusedLongitude())
                    );
                }
                else {
                    Log.d("loc geocoder - address - null", String.valueOf(addresses));
                    setDefaultLocation();
                }
            }
            else {
                setDefaultLocation();
            }
        } catch (IOException e) {
            e.printStackTrace();
            Log.d("loc geocoder - address - failure", "failure");
            setDefaultLocation();
        }
    }

    private void setDefaultLocation() {
        Log.d("loc Geocoder", "its unsupported");
        // save to pref
        locationInfo = new LocationInfo(
                -6.175264, 106.827153,
                "Indonesia", "ID",
                "", "",
                0, 0, 0, 7
        );

        locationInfo.dls = 0;
        locationInfo.mazhab = 0;
        locationInfo.way = 0;

        ConfigSharedPref.setLocationConfig(mContext, locationInfo);
        ConfigSharedPref.setQiblatDegree(mContext,
                (float) QiblaCalculator.doCalculate((float) gps.getFusedLatitude(), (float) gps.getFusedLongitude())
        );
    }

}
