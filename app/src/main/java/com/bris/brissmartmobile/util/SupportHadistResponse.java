package com.bris.brissmartmobile.util;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by ryputra on 23/01/2018.
 */

public class SupportHadistResponse {
    @SerializedName("status")
    private int status;
    @SerializedName("message")
    private String message;
    @SerializedName("data")
    private List<DataHadist> data;

    public int getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }

    public List<DataHadist> getData() {
        return data;
    }

    public class DataHadist {
        @SerializedName("id")
        private int id;
        @SerializedName("text")
        private String ayah;
        @SerializedName("spelling")
        private String spelling;
        @SerializedName("translation")
        private String translation;
        @SerializedName("narrators")
        private String narrators;

        public int getId() {
            return id;
        }

        public String getAyah() {
            return ayah;
        }

        public String getSpelling() {
            return spelling;
        }

        public String getTranslation() {
            return translation;
        }

        public String getNarrators() {
            return narrators;
        }
    }
}
