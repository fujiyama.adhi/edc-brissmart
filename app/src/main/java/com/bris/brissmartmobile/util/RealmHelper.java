package com.bris.brissmartmobile.util;

/**
 * Created by akbaranjas on 23/03/17.
 */

public interface RealmHelper {
    void insertUser(String msisdn, String phone_id) ;
    void insertAcc(String phone_id, String alias, String account);
    void getUser();
    void getAccount();
    void deleteAccount();
    void deleteUser();
}