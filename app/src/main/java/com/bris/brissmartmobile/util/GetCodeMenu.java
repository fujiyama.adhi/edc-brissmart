package com.bris.brissmartmobile.util;

import android.support.annotation.Nullable;

import com.bris.brissmartmobile.R;
import com.bris.brissmartmobile.app.BRISSMARTMobileRealmApp;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by akbaranjas on 13/04/17.
 * Updated by valeputra
 */

public class GetCodeMenu {
    private static HashMap<String,String> mapBayar = new HashMap<>();
    private static HashMap<String,String> mapBeli = new HashMap<>();
    private static HashMap<String,String> mapSyntaxBayar = new HashMap<>();
    private static HashMap<String,String> mapSyntaxBeli = new HashMap<>();

    private static Map<String,HashMap<String,String>> mapSyntaxBeli2 = new HashMap<>();
    private static HashMap<String,String> innerSyntaxBeli = new HashMap<>();

    static {
        init();
    }

    private static void init() {

        String[] arrFavBeli = BRISSMARTMobileRealmApp.getContext().getResources().getStringArray(R.array.product_pembelian_all);
        String[] arrFavBayar = BRISSMARTMobileRealmApp.getContext().getResources().getStringArray(R.array.product_pembayaran_all);

        int counterBeli = 1;
        for (String arr : arrFavBeli) {
            mapBeli.put(String.valueOf(counterBeli), arr);
            counterBeli++;
        }

        int counterBayar = 1;
        for (String arr : arrFavBayar) {
            mapBayar.put(String.valueOf(counterBayar), arr);
            counterBayar++;
        }

        /********* New Mapping *********/
        innerSyntaxBeli.put("TOKEN", "1");
        mapSyntaxBeli2.put("PLN Prepaid (Token)", innerSyntaxBeli);

        // BELI
        mapSyntaxBeli.put("PLN Prepaid (Token)","TOKEN");
        mapSyntaxBeli.put("Pulsa Telkomsel","SIMPATI");
        mapSyntaxBeli.put("Pulsa XL","XL");
        mapSyntaxBeli.put("Pulsa Esia","ESIA");
        mapSyntaxBeli.put("Pulsa Smartfren","SMARTFREN");
        mapSyntaxBeli.put("Pulsa IM3","IM3");
        mapSyntaxBeli.put("Pulsa StarOne","STARONE");
        mapSyntaxBeli.put("Paket Telkomsel","TSELDATA");
        mapSyntaxBeli.put("GOPAY","GOPAY");
        mapSyntaxBeli.put("GOPAY DRIVER","GOPAYDRIVER");
        mapSyntaxBeli.put("Pulsa Mentari","MENTARI");

        // BAYAR
        mapSyntaxBayar.put("Tagihan PLN","PLN");
        mapSyntaxBayar.put("PLN Non Tagihan Listrik","NONTAG");
        mapSyntaxBayar.put("Telepon Rumah","TELKOM");
        mapSyntaxBayar.put("Telkom Flexi","TELKOM");
        mapSyntaxBayar.put("Telkomsel Halo","HALO");
        mapSyntaxBayar.put("XL","XL");
        mapSyntaxBayar.put("ESIA","ESIA");
        mapSyntaxBayar.put("Smartfren","SMARTFREN");
        mapSyntaxBayar.put("Speedy","TELKOM");
        mapSyntaxBayar.put("Transvision","TELKOM");
        mapSyntaxBayar.put("BIG TV","BIGTV");
        mapSyntaxBayar.put("Indovision","INDOVISION");
//        mapSyntaxBayar.put("Institusi","SPP");
        mapSyntaxBayar.put("Pendidikan","SPP");
        mapSyntaxBayar.put("KAI","KAI");
        mapSyntaxBayar.put("Asuransi","SPP");
        mapSyntaxBayar.put("Institusi","SPP");

        mapSyntaxBayar.put("Tokopedia", "TOKOPEDIA");
    }

    public static String getCodeBeli(String param){
        return mapBeli.get(param);
    }

    public static String getCodeBayar(String param){
        return mapBayar.get(param);
    }

    public static String getSyntaxBeli(String param){
        return mapSyntaxBeli.get(param);
    }

    public static String getSyntaxBayar(String param){
        return mapSyntaxBayar.get(param);
    }

    @Nullable
    public static String getCodeFromValueBeli(String value) {
        for (String o : mapBeli.keySet()) {
            if (mapBeli.get(o).equals(value)) {
                return o;
            }
        }
        return null;
    }

    @Nullable
    public static String getCodeFromValueBayar(String value) {
        for (String o : mapBayar.keySet()) {
            if (mapBayar.get(o).equals(value)) {
                return o;
            }
        }
        return null;
    }
}
