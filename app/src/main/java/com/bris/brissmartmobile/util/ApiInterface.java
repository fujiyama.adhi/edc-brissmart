package com.bris.brissmartmobile.util;

import com.bris.brissmartmobile.model.BankingPromoResponse;
import com.bris.brissmartmobile.model.FavoriteRequest;
import com.bris.brissmartmobile.model.FavoriteUpdateRequest;
import com.bris.brissmartmobile.model.OAuthRefreshAccessRequest;
import com.bris.brissmartmobile.model.OAuthRefreshAccessResponse;
import com.bris.brissmartmobile.model.Promo;
import com.bris.brissmartmobile.model.ValidateCardRequest;
import com.bris.brissmartmobile.model.ValidateCardResponse;
import com.bris.brissmartmobile.util.googlemapapi.GoogleMapsV2Response;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by akbaranjas on 24/03/17.
 * Updated by valeputra
 */

/* Interface API Service */
public interface ApiInterface {

    @POST("user/favourite/get/{msisdn}")
    Call<Void> getFavData(@Path("msisdn") String msisdn);

    @GET("user/promo/{cases}")
    Call<List<Promo>> getPromoByCase(@Path("cases") String cases);

    @GET("api/place/nearbysearch/json?sensor=true&key=AIzaSyBdBktY8r528-ftk3RqbPajbIJxbBegSDM")
    Call<GoogleMapsV2Response> getMasjidNearme(@Query("type") String type, @Query("location") String location, @Query("radius") int radius);

    @POST(UriService.OAuth.refresh_access)
    Call<OAuthRefreshAccessResponse> getNewAccessToken(@Body OAuthRefreshAccessRequest refreshAccessRequest);

    @GET(UriService.Support.hadist)
    Call<SupportHadistResponse> getHadist();

    @GET(UriService.Banking.promo + "/{promo_type}")
    Call<BankingPromoResponse> getPromo(@Path("promo_type") String promoType);

    @POST("addfavorit")
    Call<ResponseBody> insertFavorit(@Body FavoriteRequest favoriteRequest);

    @POST("updatefavorit")
    Call<ResponseBody> updateFavorit(@Body FavoriteUpdateRequest favoriteUpdateRequest);

    @POST("validatecard")
    Call<ValidateCardResponse> validateCard(@Body ValidateCardRequest validateCardRequest);

}
