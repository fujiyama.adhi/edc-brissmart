package com.bris.brissmartmobile.util;

/**
 * Created by ryputra on 06/10/2017.
 */

public class ListMenuBackground {

    private int icon;
    private String title;
    private int imgBackground;

    public ListMenuBackground(int icon, String title, int imgBackground) {
        this.icon = icon;
        this.title = title;
        this.imgBackground = imgBackground;
    }

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getImgBackground() {
        return imgBackground;
    }

    public void setImgBackground(int imgBackground) {
        this.imgBackground = imgBackground;
    }
}
