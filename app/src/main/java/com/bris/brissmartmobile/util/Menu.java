package com.bris.brissmartmobile.util;

import android.content.Context;

import com.bris.brissmartmobile.R;

import java.util.List;

/**
 * Created by ryputra on 27/10/2017.
 */

public class Menu {

    public static String MENU_ID = "menu_id";

    public static void homeTop(Context context, List<ListMenuBackground> menu) {
        menu.add(new ListMenuBackground(R.drawable.ico_account_info, context.getString(R.string.menu_title_rekening), R.mipmap.ico_next));
        menu.add(new ListMenuBackground(R.drawable.ico_transfer, context.getString(R.string.menu_title_transaksi), R.mipmap.ico_next));
        menu.add(new ListMenuBackground(R.drawable.ico_payment, context.getString(R.string.menu_title_pembayaran), R.mipmap.ico_next));
        menu.add(new ListMenuBackground(R.drawable.ico_purchase, context.getString(R.string.menu_title_pembelian), R.mipmap.ico_next));

    }

    public static void homeBottom(Context context, List<ListMenu> menu) {
        menu.add(new ListMenu(R.drawable.test_ico_donasi, context.getString(R.string.menu_title_donasi), R.mipmap.ico_next));
        menu.add(new ListMenu(R.drawable.test_ico_favorit, context.getString(R.string.menu_title_favorit), R.mipmap.ico_next));
        menu.add(new ListMenu(R.drawable.test_ico_inbox, context.getString(R.string.menu_title_inbox), R.mipmap.ico_next));
        menu.add(new ListMenu(R.drawable.test_ico_promo, context.getString(R.string.menu_title_promo), R.mipmap.ico_next));
    }

    public static void homeTopBsa(Context context, List<ListMenuBackground> menu) {
        menu.add(new ListMenuBackground(R.drawable.ico_saldo, context.getString(R.string.submenu_title_cek_saldo), R.mipmap.ico_next));
        menu.add(new ListMenuBackground(R.drawable.ico_account_info, context.getString(R.string.submenu_title_cek_mutasi), R.mipmap.ico_next));
        menu.add(new ListMenuBackground(R.drawable.ico_transfer, context.getString(R.string.menu_title_transfer_bsa), R.mipmap.ico_next));
    }

    public static void homeBottomBsa(Context context, List<ListMenu> menu) {
        menu.add(new ListMenu(R.drawable.test_ico_cari_agen, context.getString(R.string.menu_title_cari_agen_bsa), R.mipmap.ico_next));
        menu.add(new ListMenu(R.drawable.test_ico_favorit, context.getString(R.string.menu_title_favorit), R.mipmap.ico_next));
        menu.add(new ListMenu(R.drawable.test_ico_inbox, context.getString(R.string.menu_title_inbox), R.mipmap.ico_next));
        menu.add(new ListMenu(R.drawable.test_ico_promo, context.getString(R.string.menu_title_promo), R.mipmap.ico_next));
    }

    public static void submenuRekening(Context context, List<ListViewMenu> submenu) {
        submenu.add(new ListViewMenu(R.drawable.ico_saldonew, context.getString(R.string.submenu_title_cek_saldo), R.mipmap.ico_next));
        submenu.add(new ListViewMenu(R.drawable.ico_mutasinew, context.getString(R.string.submenu_title_cek_mutasi), R.mipmap.ico_next));
    }

    public static void submenuTransferBsa(Context context, List<ListViewMenu> submenu) {
        submenu.add(new ListViewMenu(R.drawable.ico_subtransfer, context.getString(R.string.submenu_title_transfer_rekening_tabungan_cerdas), R.mipmap.ico_next));
        submenu.add(new ListViewMenu(R.drawable.ico_subtransfer, context.getString(R.string.submenu_title_transfer_rekening_bris), R.mipmap.ico_next));
    }

    public static void submenuTransaksi(Context context, List<ListViewMenu> submenu) {
        submenu.add(new ListViewMenu(R.drawable.ico_swipe_cardnew, context.getString(R.string.submenu_title_buka_rekening), R.mipmap.ico_next));
        submenu.add(new ListViewMenu(R.drawable.ico_transfer2, context.getString(R.string.submenu_title_setor_tunai), R.mipmap.ico_next));
        submenu.add(new ListViewMenu(R.drawable.ico_setornew, context.getString(R.string.submenu_title_tarik_tunai), R.mipmap.ico_next));
        submenu.add(new ListViewMenu(R.drawable.ico_tranfer_innew, context.getString(R.string.submenu_title_transfer_rekening_cerdas), R.mipmap.ico_next));
//        submenu.add(new ListViewMenu(R.mipmap.ico_subtransfer, context.getString(R.string.submenu_title_transfer_rekening_bris), R.mipmap.ico_next));
//        submenu.add(new ListViewMenu(R.mipmap.ico_subtransfer, context.getString(R.string.submenu_title_transfer_antar_bank), R.mipmap.ico_next));
    }

    public static void subMenuPembayaran (Context context, List<ListViewMenu> submenu) {
        submenu.add(new ListViewMenu(R.drawable.ico_plnnew, context.getString(R.string.submenu_title_tagihan_pln), R.mipmap.ico_next));
        submenu.add(new ListViewMenu(R.drawable.ico_tlpnew, context.getString(R.string.submenu_title_telepon), R.mipmap.ico_next));
        submenu.add(new ListViewMenu(R.drawable.ico_internetnew, context.getString(R.string.submenu_title_internet), R.mipmap.ico_next));
        submenu.add(new ListViewMenu(R.drawable.ico_tvnew, context.getString(R.string.submenu_title_tv_berbayar), R.mipmap.ico_next));
        submenu.add(new ListViewMenu(R.drawable.ico_kainew, context.getString(R.string.submenu_title_tiket_kai), R.mipmap.ico_next));
        submenu.add(new ListViewMenu(R.drawable.ico_tokpednew, context.getString(R.string.submenu_title_tokopedia), R.mipmap.ico_next));
        submenu.add(new ListViewMenu(R.drawable.ico_institutnew, context.getString(R.string.submenu_title_institusi), R.mipmap.ico_next));
        submenu.add(new ListViewMenu(R.drawable.ico_pendidikannew, context.getString(R.string.submenu_title_pendidikan), R.mipmap.ico_next));
        submenu.add(new ListViewMenu(R.drawable.ico_asuransinew, context.getString(R.string.submenu_title_asuransi), R.mipmap.ico_next));
    }

    public static void subMenuPembelian (Context context, List<ListViewMenu> submenu) {
        submenu.add(new ListViewMenu(R.drawable.ico_plnnew, context.getString(R.string.submenu_title_token_listrik), R.mipmap.ico_next));
        submenu.add(new ListViewMenu(R.drawable.ico_pulsanew, context.getString(R.string.submenu_title_pulsa), R.mipmap.ico_next));
        submenu.add(new ListViewMenu(R.drawable.ico_datanew, context.getString(R.string.submenu_title_paket_data), R.mipmap.ico_next));
        submenu.add(new ListViewMenu(R.drawable.ico_gojeknew, context.getString(R.string.submenu_title_gojek), R.mipmap.ico_next));
    }

    public static void subMenuDonasi (Context context, List<ListViewMenu> submenu) {
        submenu.add(new ListViewMenu(R.drawable.ico_donasi_baznas, context.getString(R.string.submenu_title_donasi_baznas), R.mipmap.ico_next));
        submenu.add(new ListViewMenu(R.drawable.ico_donasi_dompet_dhuafa, context.getString(R.string.submenu_title_donasi_dompet_dhuafa), R.mipmap.ico_next));
        submenu.add(new ListViewMenu(R.drawable.ico_donasi_griya_yatim, context.getString(R.string.submenu_title_donasi_griya_yatim), R.mipmap.ico_next));
        submenu.add(new ListViewMenu(R.drawable.ico_donasi_zis_bris, context.getString(R.string.submenu_title_donasi_zis_bris), R.mipmap.ico_next));
        submenu.add(new ListViewMenu(R.drawable.ico_donasi_bazis_dki, context.getString(R.string.submenu_title_donasi_bazis_dki), R.mipmap.ico_next));
        submenu.add(new ListViewMenu(R.drawable.ico_donasi_madani_bali, context.getString(R.string.submenu_title_donasi_madani_bali), R.mipmap.ico_next));
        submenu.add(new ListViewMenu(R.drawable.ico_donasi_bm_hidayatullah, context.getString(R.string.submenu_title_donasi_bm_hidayatullah), R.mipmap.ico_next));
        submenu.add(new ListViewMenu(R.drawable.ico_donasi_lazisnu_jatim, context.getString(R.string.submenu_title_donasi_lazisnu_jatim), R.mipmap.ico_next));
    }

    public static void submenuFavorite(Context context, List<ListViewMenu> submenu) {
        submenu.add(new ListViewMenu(R.drawable.test_ico_favorit, context.getString(R.string.submenu_title_bsafavorit), R.mipmap.ico_next));
        submenu.add(new ListViewMenu(R.drawable.test_ico_favorit, context.getString(R.string.submenu_title_transferfavorit), R.mipmap.ico_next));
        submenu.add(new ListViewMenu(R.drawable.test_ico_favorit, context.getString(R.string.submenu_title_pembayaranfavorit), R.mipmap.ico_next));
        submenu.add(new ListViewMenu(R.drawable.test_ico_favorit, context.getString(R.string.submenu_title_pembelianfavorit), R.mipmap.ico_next));
        submenu.add(new ListViewMenu(R.drawable.test_ico_favorit, context.getString(R.string.submenu_title_donasifavorit), R.mipmap.ico_next));
    }

    public static void submenuFavoriteBsa(Context context, List<ListViewMenu> submenu) {
        submenu.add(new ListViewMenu(R.drawable.test_ico_favorit, context.getString(R.string.submenu_title_rekbsa), R.mipmap.ico_next));
        submenu.add(new ListViewMenu(R.drawable.test_ico_favorit, context.getString(R.string.submenu_title_rekbris), R.mipmap.ico_next));
    }

    public static void content(Context context, List<ListMenu> menu) {
        menu.add(new ListMenu(R.drawable.ico_masjid, context.getString(R.string.menu_title_masjid_terdekat), R.mipmap.ico_next));
        menu.add(new ListMenu(R.drawable.ico_quotes, context.getString(R.string.menu_title_motivasi_islami), R.mipmap.ico_next));
        menu.add(new ListMenu(R.drawable.ico_quran, context.getString(R.string.menu_title_juz_amma), R.mipmap.ico_next));
        menu.add(new ListMenu(R.drawable.ico_kaaba, context.getString(R.string.menu_title_arahkiblat), R.mipmap.ico_next));
    }
}
