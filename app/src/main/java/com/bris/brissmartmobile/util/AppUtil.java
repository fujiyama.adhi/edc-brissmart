package com.bris.brissmartmobile.util;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.telephony.TelephonyManager;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bris.brissmartmobile.R;
import com.bris.brissmartmobile.activity.AGantiPasswordAgen;
import com.bris.brissmartmobile.activity.AGantiPasswordBsa;
import com.bris.brissmartmobile.activity.ALoginScreen;
import com.bris.brissmartmobile.activity.AMainAgen;
import com.bris.brissmartmobile.activity.AMainBsa;
import com.bris.brissmartmobile.activity.ARegistrasiDevice;
import com.bris.brissmartmobile.activity.AUser;
import com.bris.brissmartmobile.adapter.FFavoriteAdapter;
import com.bris.brissmartmobile.adapter.ItemMutationAdapter;
import com.bris.brissmartmobile.listener.FavClickListener;
import com.bris.brissmartmobile.model.Favorite;
import com.bris.brissmartmobile.model.History;
import com.bris.brissmartmobile.model.UserBrissmart;
import com.bris.brissmartmobile.model.UserDevice;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONObject;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class AppUtil {
//    public static final String HOST = "http://mobile.brisyariah.co.id:55045/api/v1/";
//    public static final String HOST_RESOURCES = "http://mobile.brisyariah.co.id:55045/api/v1/";
    public static final String HOST = "http://10.1.25.46:55045/api/v1/";
    public static final String HOST_RESOURCES = "http://10.1.25.46:55045/api/v1/";
//    public static final String HOST_LAKU_PANDAI = "http://10.0.76.133:8080";
//    public static final String HOST_LAKU_PANDAI_2 = "http://10.0.76.133:8000";
    public static final String HOST_LAKU_PANDAI = "http://10.1.25.11:8080";
    public static final String HOST_LAKU_PANDAI_2 = "http://10.1.25.11";
//    public static final String HOST_LAKU_PANDAI = "https://mbs.brisyariah.co.id:55057";
//    public static final String HOST_LAKU_PANDAI_2 = "https://mbs.brisyariah.co.id:55059";
    public static final String BASE_URL_LAKU_PANDAI = HOST_LAKU_PANDAI_2 + "/rest_server/getservice/";

    public static final String HOST_TID = "http://10.0.76.77:8888";
    public static final String BASE_URL_TID = HOST_TID + "/edc_android/Termid/Inquiry/";

    public static final String BASE_URL = HOST + "/mobilebrisapi/rest/";
    public static final String BASE_URL_RESOURCES = HOST_RESOURCES + "/mobilebris/resources/";
    public static final String BASE_URL_GMAPS = "https://maps.googleapis.com/maps/";

    public static final String API_MSG_TRF = "TRANSFER";
    public static final String API_MSG_TRFA = "TRANSFERBANKLAIN";
    public static final String API_MSG_PURCHASE = "BELI";
    public static final String API_MSG_PAYMENT = "BAYAR";
    public static final String API_MSG_GANTIPIN = "GANTI PIN";

    public static String dirPath = Environment.getExternalStorageDirectory() + "/mobileBRIS";
    public static final String SPECIAL_CHAR_PIPE = "%7C";
    public static final int MAX_FAVORIT_EACH_CATEG = 100;

    private static Realm realm;
    private static Retrofit retrofit = null;
    private static AlertDialog alertDialog = null;
    private static AlertDialog listAlertDialog = null;
    private static ProgressDialog mProgressDialog = null;

    public static class GenerateRandomString {

        public static final String DATA = "0123456789";
        public static Random RANDOM = new Random();

        public static String randomString(int len) {
            StringBuilder sb = new StringBuilder(len);

            for (int i = 0; i < len; i++) {
                sb.append(DATA.charAt(RANDOM.nextInt(DATA.length())));
            }

            return sb.toString();
        }
    }

    public static String soapReq(String dsn, String cmd, JSONObject cmdparam) {
        Log.d("Util soapReq", "Mlebu");

        int Timeout = 600000;
        String SOAP_ACTION = "http://lakupandai.fbg.bris.com/sendRequestLKP";
        String METHOD_NAME = "sendRequestLKP";
        String NAMESPACE = "http://lakupandai.fbg.bris.com";
        String URL = HOST_LAKU_PANDAI + "/lakupandaiapp/services/lakupandaiGW?wsdl";

        SoapSerializationEnvelope soapEnvelope;
        HttpTransportSE transport;
        SoapPrimitive resultString;
        String response;

        try {
            SoapObject Request = new SoapObject(NAMESPACE, METHOD_NAME);

            Calendar calendar = Calendar.getInstance();
            SimpleDateFormat mdformat = new SimpleDateFormat("yyyyMMdd");
            SimpleDateFormat mtformat = new SimpleDateFormat("HHmmss");

            String msgid = GenerateRandomString.randomString(6);

            JSONObject request = new JSONObject();

            request.put("pcode", "LKP");
            request.put("msgid", msgid);
            request.put("channel", "1002");
            request.put("terminal", "10102");
            request.put("dsn", dsn);
            request.put("date", mdformat.format(calendar.getTime()));
            request.put("time", mtformat.format(calendar.getTime()));
            request.put("cmd", cmd);
            request.put("cmdparam", cmdparam);

            Request.addProperty("msg", request.toString());
            Log.d("postData util ", request.toString());

            soapEnvelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
            soapEnvelope.setOutputSoapObject(Request);

            transport = new HttpTransportSE(URL, Timeout);
            transport.call(SOAP_ACTION, soapEnvelope);

            resultString = (SoapPrimitive) soapEnvelope.getResponse();
            Log.d("soapResult util ", resultString.toString());
            response = resultString.toString();

        } catch (Exception e) {
            Log.d("THREAD util", e.toString());
            response = "";
        }
        return response;
    }

    public static String soapReqTID(String dsn, String cmd, JSONObject cmdparam) {
        Log.d("Util soapReq", "Mlebu");

        int Timeout = 600000;
        String SOAP_ACTION = "http://lakupandai.fbg.bris.com/sendRequestLKP";
        String METHOD_NAME = "sendRequestLKP";
        String NAMESPACE = "http://lakupandai.fbg.bris.com";
        String URL = HOST_LAKU_PANDAI + "/lakupandaiapp/services/lakupandaiGW?wsdl";

        SoapSerializationEnvelope soapEnvelope;
        HttpTransportSE transport;
        SoapPrimitive resultString;
        String response;

        try {
            SoapObject Request = new SoapObject(NAMESPACE, METHOD_NAME);

            Calendar calendar = Calendar.getInstance();
            SimpleDateFormat mdformat = new SimpleDateFormat("yyyyMMdd");
            SimpleDateFormat mtformat = new SimpleDateFormat("HHmmss");

            String msgid = GenerateRandomString.randomString(6);

            JSONObject request = new JSONObject();

            request.put("pcode", "LKP");
            request.put("msgid", msgid);
            request.put("channel", "1002");
            request.put("terminal", "10102");
            request.put("dsn", dsn);
            request.put("date", mdformat.format(calendar.getTime()));
            request.put("time", mtformat.format(calendar.getTime()));
            request.put("cmd", cmd);
            request.put("cmdparam", cmdparam);

            Request.addProperty("msg", request.toString());
            Log.d("postData util ", request.toString());

            soapEnvelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
            soapEnvelope.setOutputSoapObject(Request);

            transport = new HttpTransportSE(URL, Timeout);
            transport.call(SOAP_ACTION, soapEnvelope);

            resultString = (SoapPrimitive) soapEnvelope.getResponse();
            Log.d("soapResult util ", resultString.toString());
            response = resultString.toString();

        } catch (Exception e) {
            Log.d("THREAD util", e.toString());
            response = "";
        }
        return response;
    }

    /********************************* RETROFIT OKTTP ********************************************/
    public static final GsonConverterFactory gsonFactory = GsonConverterFactory.create();

    public static final Gson gson = new GsonBuilder().setLenient().create();
    public static final GsonConverterFactory gsonConvertFactory = GsonConverterFactory.create(gson);

    public static Retrofit getClient() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient client = new OkHttpClient.Builder()
                .connectTimeout(30, TimeUnit.SECONDS)
                .addInterceptor(interceptor)
                .readTimeout(30, TimeUnit.SECONDS)
                .build();

        retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(new NullOnEmptyConverterFactory())
                .addConverterFactory(gsonFactory)
                .client(client)
                .build();
        return retrofit;
    }

    public static Retrofit getClientBrissmart() {

        final OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Interceptor.Chain chain) throws IOException {
                Request request = chain.request();

                // Build init request
                Request.Builder requestBuilder = request.newBuilder();
                request = requestBuilder.build();
                Response response = chain.proceed(request); //perform request, here original request will be executed
                return response;
            }
        })
                .connectTimeout(30, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS);

        OkHttpClient client = httpClient.build();
        retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL_LAKU_PANDAI)
                .addConverterFactory(new NullOnEmptyConverterFactory())
                .addConverterFactory(gsonFactory)
                .client(client)
                .build();

        return retrofit;
    }

    public static Retrofit getRetrofitMasjidNearme() {

        final OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Interceptor.Chain chain) throws IOException {
                Request request = chain.request();

                // Build init request
                Request.Builder requestBuilder = request.newBuilder();
                request = requestBuilder.build();
                Response response = chain.proceed(request); //perform request, here original request will be executed
                return response;
            }
        })
                .connectTimeout(30, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS);

        OkHttpClient client = httpClient.build();
        retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL_GMAPS)
                .addConverterFactory(new NullOnEmptyConverterFactory())
                .addConverterFactory(gsonFactory)
                .client(client)
                .build();

        return retrofit;
    }

    public static String getDsn() {
        String dsn = null;
        realm = Realm.getDefaultInstance();
        RealmResults<UserBrissmart> user = realm.where(UserBrissmart.class).findAll();
        dsn = (user.get(0).getDsn().isEmpty()) ? dsn.toUpperCase() : user.get(0).getDsn();
        return dsn;
    }

    public static String getAcc() {
        String acc = null;
        realm = Realm.getDefaultInstance();
        RealmResults<UserBrissmart> user = realm.where(UserBrissmart.class).findAll();
        acc = (user.get(0).getAcc().isEmpty()) ? acc.toUpperCase() : user.get(0).getAcc();
        return acc;
    }

    public static void toolbarRegular(final Context context, String title) {
        Toolbar toolbar = (Toolbar) ((AppCompatActivity) context).findViewById(R.id.tb_regular);
        ImageView btnBack = (ImageView) ((AppCompatActivity) context).findViewById(R.id.btn_back);
        TextView tvPageTitle = (TextView) ((AppCompatActivity) context).findViewById(R.id.tv_page_title);

        tvPageTitle.setText(title);
//        setToolbarColor(toolbar, title, context);
        ((AppCompatActivity) context).setSupportActionBar(toolbar);


        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((AppCompatActivity) context).onBackPressed();
            }
        });
    }

    public static void toolbarRegular(final Context context, View view, String title) {
        Toolbar toolbar = (Toolbar) view.findViewById(R.id.tb_regular);
        ImageView btnBack = (ImageView) view.findViewById(R.id.btn_back);
        TextView tvPageTitle = (TextView) view.findViewById(R.id.tv_page_title);

        Log.d("TEST tb title", title);

        tvPageTitle.setText(title);

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((AppCompatActivity) context).onBackPressed();
            }
        });
    }

    public static void showDialog() {
        if (mProgressDialog != null && !mProgressDialog.isShowing()) mProgressDialog.show();
    }

    public static void hideDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) mProgressDialog.dismiss();
    }

    public static void initDialogProgress(Context context) {
        mProgressDialog = new ProgressDialog(context);
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setCancelable(false);
        mProgressDialog.setMessage("Loading . .");
        showDialog();
    }

    public static void initDialogProgressSendingSMS(Context context) {
        mProgressDialog = new ProgressDialog(context);
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setCancelable(false);
        mProgressDialog.setMessage("Mengirim SMS . .");
        showDialog();
    }

    public static void setToolbarColor(Toolbar toolbar, String pageTitle, Context context) {
        if (pageTitle.equalsIgnoreCase(context.getString(R.string.menu_title_informasi_rekening))) {
            toolbar.setBackgroundResource(R.color.colorPrimary);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                Window window = ((AppCompatActivity) context).getWindow();
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                window.setStatusBarColor(ContextCompat.getColor(context, Constants.Color.PRIMARY));
            }

        } else if (pageTitle.equalsIgnoreCase(context.getString(R.string.menu_title_transfer))) {
            toolbar.setBackgroundResource(R.color.menuGreen);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                Window window = ((AppCompatActivity) context).getWindow();
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                window.setStatusBarColor(ContextCompat.getColor(context, Constants.Color.TRANSFER));
            }

        } else if (pageTitle.equalsIgnoreCase(context.getString(R.string.menu_title_pembayaran)) ||
                pageTitle.equalsIgnoreCase(context.getString(R.string.menu_title_juz_amma)) ||
                pageTitle.equalsIgnoreCase(context.getString(R.string.menu_title_arahkiblat)) ||
                pageTitle.equalsIgnoreCase(context.getString(R.string.menu_title_jadwal_sholat)) ||
                pageTitle.equalsIgnoreCase(context.getString(R.string.menu_title_motivasi_islami))) {
            toolbar.setBackgroundResource(R.color.menuOrange);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                Window window = ((AppCompatActivity) context).getWindow();
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                window.setStatusBarColor(ContextCompat.getColor(context, Constants.Color.PAYMENT));
            }

        } else if (pageTitle.equalsIgnoreCase(context.getString(R.string.menu_title_pembelian))) {
            toolbar.setBackgroundResource(R.color.menuRed);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                Window window = ((AppCompatActivity) context).getWindow();
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                window.setStatusBarColor(ContextCompat.getColor(context, Constants.Color.PURCHASE));
            }
        }
    }

    private static String jsonData = "";

    public static String setAutoCompleteInstitusi(String kode) {
        try {
            OkHttpClient client = new OkHttpClient();
            okhttp3.Request request = new Request.Builder()
                    .url(BASE_URL_LAKU_PANDAI + "listinstitusi?type=" + kode)
                    .build();
            Response responses = null;

            Log.e("URL", BASE_URL_LAKU_PANDAI + "listinstitusi?type=" + kode);

            try {
                responses = client.newCall(request).execute();
            } catch (IOException e) {
                e.printStackTrace();
            }
            jsonData = responses.body().string();
        } catch (Exception ex) {
            Log.d("Catch act prop", ex.toString());
            jsonData = "";
        }
        return jsonData;
    }

    public static String setAutoCompletePropinsi() {
        try {
            OkHttpClient client = new OkHttpClient();
            okhttp3.Request request = new Request.Builder()
                    .url(BASE_URL_LAKU_PANDAI + "provinsi")
                    .build();
            Response responses = null;

            try {
                responses = client.newCall(request).execute();
            } catch (IOException e) {
                e.printStackTrace();
            }
            jsonData = responses.body().string();
        } catch (Exception ex) {
            Log.d("Catch act prop", ex.toString());
            jsonData = "";
        }
        return jsonData;
    }

    public static String setAutoCompleteKota(String id_propinsi) {
        try {
            OkHttpClient client = new OkHttpClient();
            okhttp3.Request request = new Request.Builder()
                    .url(BASE_URL_LAKU_PANDAI + "kabupaten?idprov=" + id_propinsi)
                    .build();
            Response responses = null;

            try {
                responses = client.newCall(request).execute();
            } catch (IOException e) {
                e.printStackTrace();
            }
            jsonData = responses.body().string();
        } catch (Exception ex) {
            Log.d("Catch act kota", ex.toString());
            jsonData = "";
        }

        return jsonData;
    }

    public static String setAutoCompleteKecamatan(String id_propinsi, String id_kota) {
        try {
            OkHttpClient client = new OkHttpClient();
            okhttp3.Request request = new Request.Builder()
                    .url(BASE_URL_LAKU_PANDAI + "kecamatan?idprov=" + id_propinsi + "&idkab=" + id_kota)
                    .build();
            Response responses = null;

            try {
                responses = client.newCall(request).execute();
            } catch (IOException e) {
                e.printStackTrace();
            }
            jsonData = responses.body().string();
        } catch (Exception ex) {
            Log.d("Catch act kota", ex.toString());
            jsonData = "";
        }

        return jsonData;
    }

    public static String setAutoCompleteKelurahan(String id_propinsi, String id_kota, String id_kecamatan) {
        try {
            OkHttpClient client = new OkHttpClient();
            okhttp3.Request request = new Request.Builder()
                    .url(BASE_URL_LAKU_PANDAI + "kelurahan?idprov=" + id_propinsi + "&idkab=" + id_kota + "&idkec=" + id_kecamatan)
                    .build();
            Response responses = null;

            try {
                responses = client.newCall(request).execute();
            } catch (IOException e) {
                e.printStackTrace();
            }
            jsonData = responses.body().string();
        } catch (Exception ex) {
            Log.d("Catch act kota", ex.toString());
            jsonData = "";
        }

        return jsonData;
    }

    public static String setTID(String sn) {

        try {
            OkHttpClient client = new OkHttpClient();
            okhttp3.Request request = new Request.Builder()
                    .url(BASE_URL_TID + sn)
                    .build();
            Response responses = null;

            try {
                responses = client.newCall(request).execute();
            } catch (IOException e) {
                e.printStackTrace();
            }
            jsonData = responses.body().string();
        } catch (Exception ex) {
            Log.d("Catch act tid", ex.toString());
            jsonData = "";
        }

        return jsonData;

    }

    public static boolean deleteFavorite(String id_fav) {
        try {
            OkHttpClient client = new OkHttpClient();
            okhttp3.Request request = new Request.Builder()
                    .url(AppUtil.BASE_URL_LAKU_PANDAI + "delfavorit?idfav=" + id_fav)
                    .build();
            okhttp3.Response responses;
            try {
                responses = client.newCall(request).execute();
                Log.d("Response ", responses.toString());

                return true;

            } catch (IOException e) {
                e.printStackTrace();
                Log.d("Catch deleteFav", e.toString());
                return false;
            }
        } catch (Exception ex) {
            Log.d("Catch deleteFav 2", ex.toString());
            return false;
        }
    }

    public static Retrofit getClientResources() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder()
                .retryOnConnectionFailure(true)
                .readTimeout(30, TimeUnit.SECONDS)
                .connectTimeout(30, TimeUnit.SECONDS)
                .addInterceptor(interceptor)
                .build();

        retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL_RESOURCES)
                .addConverterFactory(gsonFactory)
                .client(client)
                .build();
        return retrofit;
    }

    public static void showToastShort(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }

    public static void showToastLong(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
    }

    public static String getDateFormat() {
        String result = "";
        Date date = new Date();
        String dayText = (String) DateFormat.format("EEEE", date);
        String dayNumber = (String) DateFormat.format("dd", date);
        String monthString = (String) DateFormat.format("MMMM", date);
        String year = (String) DateFormat.format("yyyy", date);
        String hour = (String) DateFormat.format("HH", date);
        String minute = (String) DateFormat.format("mm", date);
        result = getDayIndonesia(dayText) + "," + dayNumber + " " + getMonthIndonesia(monthString)
                + " " + year + "-" + hour + ":" + minute;
        return result;
    }

    public static String getDayIndonesia(String day) {
        String result = "";
        HashMap<String, String> mapDay = new HashMap<>();
        mapDay.put("Sunday", "Minggu");
        mapDay.put("Monday", "Senin");
        mapDay.put("Tuesday", "Selasa");
        mapDay.put("Wednesday", "Rabu");
        mapDay.put("Thursday", "Kamis");
        mapDay.put("Friday", "Jumat");
        mapDay.put("Saturday", "Sabtu");

        result = mapDay.get(day);
        return result != null ? result : day;
    }

    public static String getMonthIndonesia(String month) {
        String result = "";
        HashMap<String, String> mapMonth = new HashMap<>();
        mapMonth.put("January", "Januari");
        mapMonth.put("February", "Februari");
        mapMonth.put("March", "Maret");
        mapMonth.put("April", "April");
        mapMonth.put("May", "Mei");
        mapMonth.put("June", "Juni");
        mapMonth.put("July", "Juli");
        mapMonth.put("August", "Agustus");
        mapMonth.put("September", "September");
        mapMonth.put("October", "Oktober");
        mapMonth.put("November", "November");
        mapMonth.put("December", "Desember");

        result = mapMonth.get(month);
        return result != null ? result : month;
    }

    public static String getMonthIndonesiaById(int month) {
        String[] months = {
                "Januari", "Februari",
                "Maret", "April",
                "Mei", "Juni",
                "Juli", "Augustus",
                "September", "Oktober",
                "November", "Desember"
        };

        return months[month];
    }

    public static void displayDialogCall(final Context context, String message) {
        LayoutInflater li = LayoutInflater.from(context);
        View msgDialog = li.inflate(R.layout.custom_dialog_call, null);
        TextView btnClose = (Button) msgDialog.findViewById(R.id.btn_close);
        TextView btnCall = (Button) msgDialog.findViewById(R.id.btn_call);
        TextView txt_msg_display = (TextView) msgDialog.findViewById(R.id.dialog_txt_msg_info);
        txt_msg_display.setText(message);
        AlertDialog.Builder alert = new AlertDialog.Builder(context);
        alert.setView(msgDialog);
        alert.setCancelable(true);
        final AlertDialog dialog = alert.create();
        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        btnCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:0211500789"));
                context.startActivity(intent);
                dialog.dismiss();
//
//                Intent intent = new Intent(Intent.ACTION_DIAL);
//                intent.setData(Uri.parse("tel:0211500789"));
//                if (ActivityCompat.checkSelfPermission(context,
//                        Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
//                    return;
//                }
//                context.startActivity(intent);
//                dialog.dismiss();
            }
        });
        dialog.show();
    }

    public static void displayDialog(final Context context, String message) {
        LayoutInflater li = LayoutInflater.from(context);
        View msgDialog = li.inflate(R.layout.custom_dialog_message, null);
        Button btnOkMessage = (Button) msgDialog.findViewById(R.id.btn_close);
        TextView txt_msg_display = (TextView) msgDialog.findViewById(R.id.dialog_txt_msg_info);
        txt_msg_display.setText(message);
        AlertDialog.Builder alert = new AlertDialog.Builder(context);
        alert.setView(msgDialog);
        alert.setCancelable(false);
        final AlertDialog dialog = alert.create();

        btnOkMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    public static void displayDialogCard(final Context context, String message) {
        LayoutInflater li = LayoutInflater.from(context);
        View msgDialog = li.inflate(R.layout.custom_dialog_message, null);
        Button btnOkMessage = (Button) msgDialog.findViewById(R.id.btn_close);
        TextView txt_msg_display = (TextView) msgDialog.findViewById(R.id.dialog_txt_msg_info);
        txt_msg_display.setText(message);
        AlertDialog.Builder alert = new AlertDialog.Builder(context);
        alert.setView(msgDialog);
        alert.setCancelable(false);
        final AlertDialog dialog = alert.create();

        btnOkMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                ((AppCompatActivity) context).finish();
            }
        });

        dialog.show();
    }



    public static void displayDialogChangeDevice(final Context context, String message) {
        LayoutInflater li = LayoutInflater.from(context);
        View msgDialog = li.inflate(R.layout.custom_dialog_message, null);
        Button btnOkMessage = (Button) msgDialog.findViewById(R.id.btn_close);
        TextView txt_msg_display = (TextView) msgDialog.findViewById(R.id.dialog_txt_msg_info);
        txt_msg_display.setText(message);
        AlertDialog.Builder alert = new AlertDialog.Builder(context);
        alert.setView(msgDialog);
        alert.setCancelable(false);
        final AlertDialog dialog = alert.create();
        dialog.show();

        btnOkMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                realm = Realm.getDefaultInstance();
                realm.beginTransaction();
                RealmResults<UserDevice> acc = realm.where(UserDevice.class).findAll();
                acc.deleteAllFromRealm();
                realm.commitTransaction();

                openActivityAndClearAllPrevious(context, AUser.class);
            }
        });
    }

    public static void displayDialogChangePinSucsess(final Context context, String message, final String user) {
        LayoutInflater li = LayoutInflater.from(context);
        View msgDialog = li.inflate(R.layout.custom_dialog_message, null);
        Button btnOkMessage = (Button) msgDialog.findViewById(R.id.btn_close);
        TextView txt_msg_display = (TextView) msgDialog.findViewById(R.id.dialog_txt_msg_info);
        txt_msg_display.setText(message);
        AlertDialog.Builder alert = new AlertDialog.Builder(context);
        alert.setView(msgDialog);
        alert.setCancelable(false);
        final AlertDialog dialog = alert.create();
        dialog.show();

        btnOkMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                try {
                    Intent intent;
                    Bundle datas = new Bundle();
                    if (user.equalsIgnoreCase("agen")) {
                        intent = new Intent(context, AMainAgen.class);
                    } else {
                        intent = new Intent(context, AMainBsa.class);
                    }
                    datas.putString("source", "ganti password");
                    intent.putExtras(datas);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    context.startActivity(intent);
                    ((AppCompatActivity) context).finish();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public static void displayDialogChangePin(final Context context, String message, final String user) {
        LayoutInflater li = LayoutInflater.from(context);
        View msgDialog = li.inflate(R.layout.custom_dialog_message, null);
        Button btnOkMessage = (Button) msgDialog.findViewById(R.id.btn_close);
        TextView txt_msg_display = (TextView) msgDialog.findViewById(R.id.dialog_txt_msg_info);
        txt_msg_display.setText(message);
        AlertDialog.Builder alert = new AlertDialog.Builder(context);
        alert.setView(msgDialog);
        alert.setCancelable(false);
        final AlertDialog dialog = alert.create();

        dialog.show();

        btnOkMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                String vdsn = null;
                realm = Realm.getDefaultInstance();
                realm.beginTransaction();
                RealmResults<UserDevice> acc = realm.where(UserDevice.class).findAll();
                vdsn = (acc.get(0).getVdsn().isEmpty()) ? vdsn.toUpperCase() : acc.get(0).getVdsn();
                    try {
                        Intent intent;
                        if (user.equalsIgnoreCase("agen")) {
                            intent = new Intent(context, AGantiPasswordAgen.class);
                        } else {
                            intent = new Intent(context, AGantiPasswordBsa.class);
                        }
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        Bundle datas = new Bundle();
                        datas.putString("status", "direct");
                        datas.putString("vdsn", vdsn);
                        datas.putString("user", user);
                        intent.putExtras(datas);
                        context.startActivity(intent);
                        ((AppCompatActivity) context).finish();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

            }
        });
    }

    public static void openActivityAndClearAllPrevious(final Context context, Class<?> cls) {
        try {
            Intent intent = new Intent(context, cls);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            context.startActivity(intent);
            ((AppCompatActivity) context).finish();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void displayDialogBasic(final Context context, String message, final String header) {
        LayoutInflater li = LayoutInflater.from(context);
        View msgDialog = li.inflate(R.layout.custom_dialog_message, null);
        Button btnOkMessage = (Button) msgDialog.findViewById(R.id.btn_close);
        TextView txt_header = (TextView) msgDialog.findViewById(R.id.dialog_txt_header);
        TextView txt_msg_display = (TextView) msgDialog.findViewById(R.id.dialog_txt_msg_info);
        txt_header.setText(header);
        txt_msg_display.setText(message);
        AlertDialog.Builder alert = new AlertDialog.Builder(context);
        alert.setView(msgDialog);
        alert.setCancelable(false);
        final AlertDialog dialog = alert.create();

        btnOkMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    public static void DialogMutation(final Context context, String[] idate, String[] isign, String[] iamount, final String balance) {
        LayoutInflater li = LayoutInflater.from(context);
        View msgDialog = li.inflate(R.layout.custom_dialog_mutasi, null);
        ImageView btnClose = (ImageView) msgDialog.findViewById(R.id.btn_close);
        RecyclerView rcListMutation = (RecyclerView) msgDialog.findViewById(R.id.rv_list_mutation);
//        final TextView tvBalance = (TextView) msgDialog.findViewById(R.id.tv_balance);

        AlertDialog.Builder alert = new AlertDialog.Builder(context);
        alert.setView(msgDialog);
        alert.setCancelable(true);
        alertDialog = alert.create();

//        int saldo = Integer.parseInt(balance);
//        final String jml = NumberFormat.getNumberInstance(Locale.US).format(saldo);
//        final String jml_saldo = jml.replace(",", ".");

//        final Handler handler = new Handler();
//        handler.postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                Animation anim = AnimationUtils.loadAnimation(context, R.anim.fadeinup);
//                anim.setInterpolator((new AccelerateDecelerateInterpolator()));
//                anim.setFillAfter(true);
//                tvBalance.startAnimation(anim);
//                tvBalance.setText("Rp " + jml_saldo);
//            }
//        }, 600);

        ItemMutationAdapter adapter = new ItemMutationAdapter(idate, isign, iamount, context);
        rcListMutation.setHasFixedSize(true);
        rcListMutation.setLayoutManager(new LinearLayoutManager(context));
        rcListMutation.setAdapter(adapter);
        adapter.notifyDataSetChanged();
        alertDialog.show();

        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
            }
        });
    }

    public static void displayCustomDialog(final Context context, String message, final String header) {
        LayoutInflater li = LayoutInflater.from(context);
        View msgDialog = li.inflate(R.layout.custom_dialog_message, null);
        Button btnOkMessage = (Button) msgDialog.findViewById(R.id.btn_close);
        TextView txt_header = (TextView) msgDialog.findViewById(R.id.dialog_txt_header);
        TextView txt_msg_display = (TextView) msgDialog.findViewById(R.id.dialog_txt_msg_info);

        txt_header.setText(header);
        txt_msg_display.setText(message);
        AlertDialog.Builder alert = new AlertDialog.Builder(context);
        alert.setView(msgDialog);
        alert.setCancelable(false);
        final AlertDialog dialog = alert.create();

        btnOkMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (header.equals(context.getString(R.string.alert_rooted_phone))) {
                    Intent i = new Intent(Intent.ACTION_MAIN);
                    i.addCategory(Intent.CATEGORY_HOME);
                    i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    context.startActivity(i);
                } else if (header.equals(context.getString(R.string.alert_header_gagal_login))) {
                    Intent alogin = new Intent(context, ALoginScreen.class);
                    context.startActivity(alogin);
                }
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    public static void saveIntoInbox(String message, String menuId, String dsn) {

        Date date = new Date();
        long trxdate = date.getTime();

        realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        History history = realm.createObject(History.class);
        history.setTrxdate(trxdate);
        history.setMessage(message);
        history.setDatedisplay(getDateFormat());
        history.setStatusread("0");
        history.setMenuId(menuId);
        history.setDsn(dsn);
        realm.commitTransaction();
    }

    public static void saveMutasiIntoInbox(int mutation, String[] idate, String[] isign, String[] iamount, String balance, String menuId, String dsn) {

        Date date = new Date();
        long trxdate = date.getTime();
        String[] detail = new String[mutation];
        int[] amount = new int[mutation];
        String[] amount_str = new String[mutation];
        String[] amount_txt = new String[mutation];
        String msg, mutasi, message;

        realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        History history = realm.createObject(History.class);
        history.setTrxdate(trxdate);

        for (int i = 0; i < mutation; i++) {
            amount[i] = Integer.parseInt(iamount[i]);
            amount_str[i] = NumberFormat.getNumberInstance(Locale.US).format(amount[i]);
            amount_txt[i] = amount_str[i].replace(",", ".");
            if (isign[i].equalsIgnoreCase("D")) {
                detail[i] = idate[i] + "  " + isign[i] + "  - " + amount_txt[i];
            } else {
                detail[i] = idate[i] + "  " + isign[i] + "  " + amount_txt[i];
            }
        }

        msg = Arrays.toString(detail);
        mutasi = msg.substring(1, msg.length() - 1);
//        message = mutasi.replace(",", "\n") + "\n" + "Saldo : " + balance;
        message = mutasi.replace(",", "\n");

        history.setMessage(message);
        history.setDatedisplay(getDateFormat());
        history.setStatusread("0");
        history.setMenuId(menuId);
        history.setDsn(dsn);
        realm.commitTransaction();
    }

    public static void showNoConnectionDialog(Context ctx1) {
        final Context ctx = ctx1;
        AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
        builder.setCancelable(true);
        builder.setMessage(R.string.no_connection);
        builder.setTitle(R.string.no_connection_title);
        builder.setPositiveButton(R.string.settings, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                ctx.startActivity(new Intent(Settings.ACTION_WIRELESS_SETTINGS));
            }
        });
        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                return;
            }
        });
        builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
            public void onCancel(DialogInterface dialog) {
                return;
            }
        });

        builder.show();
    }

    public static Bitmap getScreenShot(View view) {
        View screenView = view;
        screenView.setDrawingCacheEnabled(true);
        Bitmap bitmap = Bitmap.createBitmap(screenView.getDrawingCache());
        screenView.setDrawingCacheEnabled(false);
        return bitmap;
    }

    public static void store(Bitmap bm, String fileName) {
        File dir = new File(dirPath);
        if (!dir.exists())
            dir.mkdirs();
        File file = new File(dirPath, fileName);
        try {
            FileOutputStream fOut = new FileOutputStream(file);
            bm.compress(Bitmap.CompressFormat.PNG, 85, fOut);
            fOut.flush();
            fOut.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // Exit
    public static void showQuitAppDialog(final Context context) {
        AlertDialog alertDialog = null;
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setCancelable(false);
        builder.setMessage("Apakah anda yakin akan keluar dari aplikasi ?");
        builder.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface alertdialog, int which) {
                ((Activity) context).finish();
            }
        });
        builder.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface alertdialog, int which) {
                alertdialog.cancel();
            }
        });
        alertDialog = builder.create();
        alertDialog.show();
    }

    public static void showLogOutDialog(final Context context) {
        AlertDialog alertDialog = null;
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setCancelable(false);
        builder.setMessage("Apakah anda yakin Logout dari aplikasi ?");
        builder.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface alertdialog, int which) {
                ((Activity) context).finish();
            }
        });
        builder.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface alertdialog, int which) {
                alertdialog.cancel();
            }
        });
        alertDialog = builder.create();
        alertDialog.show();
    }


    public static void displayDialogFavorit(Context context, List<Favorite> favDatas, FavClickListener favClickListener, String menu) {
        RecyclerView rclistfav;
        TextView tv_no_data;
        FFavoriteAdapter favAdapter;
        Button btnButton;
        LayoutInflater li = LayoutInflater.from(context);
        View msgDialog = li.inflate(R.layout.custom_dialog_list_fav_trans, null);
        rclistfav = (RecyclerView) msgDialog.findViewById(R.id.rv_list_trans_fav);
        tv_no_data = (TextView) msgDialog.findViewById(R.id.tv_no_data);
        btnButton = (Button) msgDialog.findViewById(R.id.btn_ok_dialog_list_fav);

        if (favDatas.size() > 0) {
            tv_no_data.setVisibility(View.GONE);
            rclistfav.setVisibility(View.VISIBLE);
        } else {
            tv_no_data.setVisibility(View.VISIBLE);
            rclistfav.setVisibility(View.GONE);
        }

        AlertDialog.Builder alert = new AlertDialog.Builder(context);
        alert.setView(msgDialog);
        alert.setCancelable(true);
        listAlertDialog = alert.create();
        btnButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listAlertDialog.dismiss();
            }
        });
        favAdapter = new FFavoriteAdapter(favDatas, context, favClickListener, menu);
        rclistfav.setHasFixedSize(true);
        rclistfav.setLayoutManager(new LinearLayoutManager(context));//Linear Items
        rclistfav.setAdapter(favAdapter);
        favAdapter.notifyDataSetChanged();
        listAlertDialog.show();
    }


    public static void closeListDialog() {
        if (listAlertDialog != null) {
            listAlertDialog.dismiss();
        }
    }

    public static boolean checkFavorite(String jenispembayaran, String data3, String data1, String dsn) {
        realm = Realm.getDefaultInstance();

        RealmResults<Favorite> resultsTest = realm.where(Favorite.class).findAll();
        RealmQuery<Favorite> cekFav = realm.where(Favorite.class)
                .equalTo("jenispembayaran", jenispembayaran)
                .equalTo("data1", data1)
                .equalTo("data3", data3);

        RealmResults<Favorite> results = cekFav.findAll();

        if (results.size() > 0) {
            return false;
        }
        if (!realm.isClosed()) {
            realm.close();
        }
        return true;
    }

    public static void showDialog(ProgressDialog mProgressDialog) {

        if (mProgressDialog != null && !mProgressDialog.isShowing())
            mProgressDialog.show();
    }

    public static void hideDialog(ProgressDialog mProgressDialog) {

        if (mProgressDialog != null && mProgressDialog.isShowing())
            mProgressDialog.dismiss();
    }

    public static void customMainFonts(AssetManager assetManager, View view) { // only textview, edittext, and buttons
        CustomFonts customFonts = new CustomFonts(assetManager, "fonts/source-sans-pro.regular.ttf");
        customFonts.replaceFonts((ViewGroup) view);
    }

    public static int getMapsMarkerIcon(String iconName) {
        switch (iconName) {
            case "ico_nav_konten":
                return R.mipmap.ico_masjid;
            case "atm":
                return R.mipmap.marker_atm;
            case "cabang":
                return R.mipmap.marker_branch;
        }
        return 0;
    }

    public static HashMap<String, String> getDeviceInfo(Context context) {
        HashMap<String, String> deviceInfo = new HashMap<>();
        String deviceId = "";
        String imei = "";
        try {
            TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
            deviceId = Settings.Secure.getString(context.getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);

            if (ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
            }
            imei = telephonyManager.getDeviceId().toString();
        } catch (Exception e) {
        }

        deviceInfo.put("device_id", deviceId);
        deviceInfo.put("imei", imei);

        return deviceInfo;
    }

}