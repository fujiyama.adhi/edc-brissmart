package com.bris.brissmartmobile.util;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.RemoteException;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.bris.brissmartmobile.PrinterEx;
import com.bris.brissmartmobile.PrinterFonts;
import com.bris.brissmartmobile.app.BRISSMARTMobileRealmApp;
import com.bris.brissmartmobile.fragment.FPembayaranKartu;
import com.vfi.smartpos.deviceservice.aidl.FontFamily;
import com.vfi.smartpos.deviceservice.aidl.IBeeper;
import com.vfi.smartpos.deviceservice.aidl.IPrinter;
import com.vfi.smartpos.deviceservice.aidl.PrinterConfig;
import com.vfi.smartpos.deviceservice.aidl.PrinterListener;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.StringTokenizer;

public class AppUtilPrint extends  PrinterListener.Stub{

    public static final String TAG = "VFI";
    public static IBeeper iBeeper;
    public static IPrinter printer;
    public static PrinterEx printerEx;
    public static String msg, pesan, sisa;


    //Print function
    public static void printStruk(final Context context, Bundle datas){
        printerEx = new PrinterEx();
        iBeeper = BRISSMARTMobileRealmApp.getInstance().getiBeeper();
        printer = BRISSMARTMobileRealmApp.getInstance().getPrinter();
        PrinterFonts.initialize(context.getAssets());

        try {

            byte[] buffer = readImageFromAssets(context, "bris_logo_print.png");
            if( null != buffer) {
                Bitmap bitmap = BitmapFactory.decodeByteArray(buffer, 0, buffer.length);
                Bundle fmtImage = new Bundle();
                fmtImage.putInt("width", bitmap.getWidth());
                fmtImage.putInt("height", bitmap.getHeight());
                fmtImage.putInt("offset", (384 - bitmap.getWidth())/2);
                printerEx.addImage( fmtImage, buffer );
            }
//            printerEx.addText("KCP Depok Cimanggis", 21, false, FontFamily.CENTER);
//            printerEx.addText("Jln Raya Bogor KM. 31", 21, false, FontFamily.CENTER);
//            printerEx.addText("Kota Depok", 21, false, FontFamily.CENTER);
//            printerEx.addText("Jawa Barat", 21, false, FontFamily.CENTER);
//            printerEx.addNewLine(21);

            printerEx.addText(filler("TID : " + datas.getString("tid"), "", 46), 21, false, FontFamily.LEFT);

            if (datas.getString("type_card").equalsIgnoreCase("-")) {

                printerEx.addText(datas.getString("menu_id"), 24, true, FontFamily.LEFT);
                printerEx.addText(filler("BATCH  : " + "000001" ,       "TRACE NO :   " + "000812", 46), 21, false, FontFamily.LEFT);
//                printerEx.addText(filler("DATE   : " + datas.getString("date"),       "TIME : " + datas.getString("time"), 46), 21, false, FontFamily.LEFT);
                printerEx.addText(filler("REF NO : " + "000000000812", "APPR CODE :   " + "97485C", 46), 21, false, FontFamily.LEFT);
                printerEx.addText(datas.getString("date"), 21, false, FontFamily.LEFT);
                printerEx.addNewLine(21);

                msg = datas.getString("msg");
                Log.d("MSG msg", msg);
                while (msg.length() > 42) {
                    Log.d("MSG status", "Masuk While");

                    pesan = msg.trim().substring(0, 42);
                    Log.d("MSG pesan", pesan);
                    sisa = msg.replace(pesan, "");
                    Log.d("MSG sisa", sisa);

                    printerEx.addText(pesan, 21, false, FontFamily.CENTER);
                    msg = sisa;
                    Log.d("MSG msg sisa", msg);
                }

                printerEx.addText(msg, 21, false, FontFamily.CENTER);


            } else if (datas.getString("type_card").equalsIgnoreCase("CARD") || datas.getString("type_card").equalsIgnoreCase("CHIP")) {

                String card_no = datas.getString("card_no");
                card_no = card_no.substring(0, 6) + "******" + card_no.substring(12, 16);
                printerEx.addText(card_no + "-" +  datas.getString("type_card"), 24, true, FontFamily.LEFT);
                printerEx.addText(datas.getString("type_trx") + " " + datas.getString("prod_id"), 24, true, FontFamily.LEFT);
                printerEx.addText(filler("BATCH  : " + "000001" ,       "TRACE NO :   " + "000812", 46), 21, false, FontFamily.LEFT);
                printerEx.addText(filler("DATE   : " + datas.getString("date"),       "TIME : " + datas.getString("time"), 46), 21, false, FontFamily.LEFT);
                printerEx.addText(filler("REF NO : " + "000000000812", "APPR CODE :   " + "97485C", 46), 21, false, FontFamily.LEFT);
                printerEx.addNewLine(21);

                if (datas.getString("type_trx").equalsIgnoreCase("TRANSFER")) {

                    printerEx.addText("REK. DEBET          : " + "944443001213070311", 21, false, FontFamily.LEFT);
                    printerEx.addText("NAMA NASABAH        : " + datas.getString("nama_pelanggan"), 21, false, FontFamily.LEFT);
                    printerEx.addText("REK. TUJUAN         : " + datas.getString("acc_des"), 21, false, FontFamily.LEFT);
                    printerEx.addText("NAMA NASABAH TUJUAN : " + "FITRIANA RAHMAWATI", 21, false, FontFamily.LEFT);
                    printerEx.addText("NOMINAL             : " + "RP. " + getDecimalFormat(datas.getString("amount")), 21, false, FontFamily.LEFT);

                }

                else if (datas.getString("type_trx").equalsIgnoreCase("PEMBAYARAN")) {

                    printerEx.addText("NO. PELANGGAN       : " + datas.getString("id_pelanggan"), 21, false, FontFamily.LEFT);
                    printerEx.addText("NAMA PELANGGAN      : " + datas.getString("nama_pelanggan"), 21, false, FontFamily.LEFT);
                    printerEx.addText("NOMINAL             : " + "Rp. " + getDecimalFormat(datas.getString("amount")), 21, false, FontFamily.LEFT);
                    printerEx.addText("ADMIN FEE           : " + "Rp. " + getDecimalFormat(datas.getString("fee")), 21, false, FontFamily.LEFT);
                    printerEx.addText("TOTAL BAYAR         : " + "Rp. " + getDecimalFormat(datas.getString("total")), 21, false, FontFamily.LEFT);

                }

            } else {

                printerEx.addText(filler("BATCH  : " + "000001" ,       "TRACE NO :   " + "000812", 46), 21, false, FontFamily.LEFT);
                printerEx.addText(filler("REF NO : " + "000000000812", "APPR CODE :   " + "97485C", 46), 21, false, FontFamily.LEFT);
                printerEx.addText(datas.getString("date"), 21, false, FontFamily.LEFT);
                printerEx.addNewLine(21);

                msg = datas.getString("msg");
                Log.d("MSG msg", msg);
                while (msg.length() > 42) {
                    Log.d("MSG status", "Masuk While");

                    pesan = msg.trim().substring(0, 42);
                    Log.d("MSG pesan", pesan);
                    sisa = msg.replace(pesan, "");
                    Log.d("MSG sisa", sisa);

                    printerEx.addText(pesan, 21, false, FontFamily.CENTER);
                    msg = sisa;
                    Log.d("MSG msg sisa", msg);
                }

                printerEx.addText(msg, 21, false, FontFamily.CENTER);

            }

            printerEx.addNewLine(21);

            printerEx.addText("TERIMA KASIH TELAH MENGGUNAKAN", 21, false, FontFamily.CENTER);
            printerEx.addText("EDC BRISSMART", 21, false, FontFamily.CENTER);
            printerEx.addText("CALL CENTER BRI SYARIAH : 021-1500789", 21, false, FontFamily.CENTER);
            printerEx.addNewLine(21);
            printerEx.addText("--Merchant Copy--", 21, false, FontFamily.CENTER);
            printerEx.addNewLine(21);
            printerEx.addNewLine(21);

            Bundle pexImage = new Bundle();
            pexImage.putInt("offset", 0);
            pexImage.putInt("width", 384);  // bigger then actual, will print the actual
            pexImage.putInt("height", printerEx.getHeight()); // bigger then actual, will print the actual
            printer.addImage(pexImage, printerEx.getData());

            printer.feedLine(5);
            printer.startPrint(new AppUtilPrint());
            ((AppCompatActivity) context).finish();
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    private static String filler(String front, String back, int length){
        StringBuffer buffer = new StringBuffer(length);
        int flength = front.length();
        int blength = back.length();
        int fill = length - (flength + blength);
        buffer.append(front);
        for (int x = 0; x < fill; x++){
            buffer.append(" ");
        }
        buffer.append(back);
        return buffer.toString();
    }


    private static byte[] readImageFromAssets(final Context context, String imageName) {
        InputStream is = null;
        ByteArrayOutputStream out = null;
        try {
            is = context.getAssets().open(imageName);
            out = new ByteArrayOutputStream(1024);
            byte[] temp = new byte[1024];
            int size = 0;
            while ((size = is.read(temp)) != -1) {
                out.write(temp, 0, size);
            }
            byte[] content = out.toByteArray();

            if(is != null) {
                is.close();
            }
            if(out != null) {
                out.close();
            }
            return content;
        } catch (IOException e) {
            e.printStackTrace();
        } finally {

        }
        return null;

    }


    @Override
    public void onError(int error) throws RemoteException {
        Message msg = new Message();
        msg.getData().putString("msg", "print error,errno:" + error);
        handler.sendMessage(msg);
    }

    @Override
    public void onFinish() throws RemoteException {
        Message msg = new Message();
        msg.getData().putString("msg", "print finished");
        iBeeper.startBeep(200);
        handler.sendMessage(msg);
    }

    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            String string = msg.getData().getString("string");
            if( null != string )
                if( string.length()>0)
//                    editText1.setText( string );
                    super.handleMessage(msg);
            Log.d(TAG, msg.getData().getString("msg"));

        }
    };

    public static String getDecimalFormat(String value) {
        StringTokenizer lst = new StringTokenizer(value, ".");
        String str1 = value;
        String str2 = "";
        if (lst.countTokens() > 1)
        {
            str1 = lst.nextToken();
            str2 = lst.nextToken();
        }
        String str3 = "";
        int i = 0;
        int j = -1 + str1.length();
        if (str1.charAt( -1 + str1.length()) == '.')
        {
            j--;
            str3 = ".";
        }
        for (int k = j;; k--)
        {
            if (k < 0)
            {
                if (str2.length() > 0)
                    str3 = str3 + "." + str2;
                return str3;
            }
            if (i == 3)
            {
                str3 = "," + str3;
                i = 0;
            }
            str3 = str1.charAt(k) + str3;
            i++;
        }

    }

    private void formatMsg(String msg) {



    }
}
