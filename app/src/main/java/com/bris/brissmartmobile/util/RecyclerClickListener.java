package com.bris.brissmartmobile.util;

/**
 * Created by valeputra on 8/4/17.
 */

import android.view.View;

public interface RecyclerClickListener {
    void onClick(View view, int position);
    void onLongClick(View view, int position);
}
