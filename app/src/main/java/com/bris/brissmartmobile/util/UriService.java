package com.bris.brissmartmobile.util;

/**
 * Created by ryputra on 16/01/2018.
 */

public class UriService {

    public class BaseUrl {
        public static final String URL = "http://180.250.94.91:55045/api/v1/";  // production
//        public static final String URL = "http://mobile.brisyariah.co.id:55045/api/v1/";  // production
    }

    public class OAuth {
        public static final String refresh_access = "oauth/refresh_access";
    }

    public class Banking {
        public static final String transaction = "banking/transaction";
        public static final String location = "banking/location";
        public static final String promo = "banking/promo";
    }

    public class Support {
        public static final String hadist = "support/hadist";
    }
}
