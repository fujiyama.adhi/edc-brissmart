package com.bris.brissmartmobile.util;

/**
 * Created by valeputra on 6/22/17.
 */

public class ListViewMenu {
    private int icon;
    private String title;
    private int icon_next;

    public ListViewMenu(int icon, String title, int icon_next) {
        this.icon = icon;
        this.title = title;
        this.icon_next = icon_next;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }

    public int getIcon_next() {
        return icon_next;
    }

    public void setIcon_next(int icon_next) {
        this.icon_next = icon_next;
    }
}
