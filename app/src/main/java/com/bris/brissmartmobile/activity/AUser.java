package com.bris.brissmartmobile.activity;

import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.RemoteException;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.bris.brissmartmobile.R;
import com.bris.brissmartmobile.model.Favorite;
import com.bris.brissmartmobile.model.UserBrissmart;
import com.bris.brissmartmobile.model.UserDevice;
import com.bris.brissmartmobile.util.AppUtil;
import com.bris.brissmartmobile.util.BackStackFragment;
import com.bris.brissmartmobile.util.RealmHelper;
import com.vfi.smartpos.deviceservice.aidl.IBeeper;
import com.vfi.smartpos.deviceservice.aidl.IDeviceService;
import com.vfi.smartpos.deviceservice.aidl.IPrinter;
import com.vfi.smartpos.deviceservice.aidl.PrinterConfig;
import com.vfi.smartpos.deviceservice.aidl.PrinterListener;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;

import io.realm.Realm;
import io.realm.RealmResults;
import okhttp3.OkHttpClient;
import okhttp3.Request;

public class AUser extends AppCompatActivity implements View.OnClickListener {

    private Button btnAgen, btnBsa;
    private Realm realm;

    private static final String TAG = "VFI";
    IPrinter printer;
    IBeeper mIBeeper;
    IDeviceService idevice;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user);

        btnAgen = findViewById(R.id.btn_login_agen);
        btnBsa = findViewById(R.id.btn_login_bsa);

        realm = Realm.getDefaultInstance();

        main();
    }

    private void main() {
        if (isOnline()) {
            if(isUserRegistered()) {
                Intent i = new Intent(AUser.this, ALoginScreen.class);
                Bundle datas = new Bundle();
                datas.putString("login", "defaultlogin");
                i.putExtras(datas);
                startActivity(i);
                finish();
            } else {
                btnAgen.setOnClickListener(this);
                btnBsa.setOnClickListener(this);
            }

        } else if (!isOnline()) {
            AppUtil.showNoConnectionDialog(AUser.this);


            
            btnAgen.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    Toast.makeText(AUser.this, "Print", Toast.LENGTH_SHORT).show();
//                    doPrintString(v);
                }
            });


        }
    }

    @Override
    public void onClick(View v) {
        Intent i = new Intent(AUser.this, ARegistrasiDevice.class);
        Bundle datas = new Bundle();

        switch (v.getId()) {
            case R.id.btn_login_agen:
                datas.putString("user", "agen");
                break;

            case R.id.btn_login_bsa:
                datas.putString("user", "bsa");
                break;

            default: break;
        }
        
        i.putExtras(datas);
        startActivity(i);
    }

    @Override
    public void onBackPressed() {
        AppUtil.showQuitAppDialog(AUser.this);
    }

    private boolean isUserRegistered() {
        RealmResults<UserDevice> resUser = realm.where(UserDevice.class).findAll();
        if(resUser.size() > 0) {
            return true;
        }
        return false;
    }

    protected boolean isOnline() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
            return true;
        } else {
            return false;
        }
    }


    public void doPrintString(View view) {
        try {
            Long total = (long) 0;
            Long ppn = (long) 0;
            Bundle format = new Bundle();

            // bundle formate for AddTextInLine
            Bundle fmtAddTextInLine = new Bundle();

            format.putInt(PrinterConfig.addText.FontSize.BundleName, PrinterConfig.addText.FontSize.HUGE_48);
            format.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.CENTER);
            printer.addText(format, "BRI Syariah");


            // space enter below the text
            printer.feedLine(5);

            // start print here
            printer.startPrint(new MyListener());
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        Intent intent = new Intent();
        intent.setAction("com.vfi.smartpos.device_service");
        intent.setPackage("com.vfi.smartpos.deviceservice");
        boolean isSucc = bindService(intent, conn, Context.BIND_AUTO_CREATE);

        if (!isSucc) {
            Log.i("TAG", "deviceService connect fail!");
        } else {
            Log.i("TAG", "deviceService connect success");
        }

    }

    // log & display
    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            String string = msg.getData().getString("string");
            if( null != string )
                if( string.length()>0)
//                    editText1.setText( string );
                    super.handleMessage(msg);
            Log.d(TAG, msg.getData().getString("msg"));
            Toast.makeText(AUser.this, msg.getData().getString("msg"), Toast.LENGTH_SHORT).show();

        }
    };

    // connect service -- start
    private ServiceConnection conn = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            idevice = IDeviceService.Stub.asInterface(service);
            try {
                mIBeeper = idevice.getBeeper();
                printer = idevice.getPrinter();
//                msr = idevice.getMagCardReader();
//                scanner = idevice.getScanner(0);    // 1 for the front, 0 for the rear
//                irfCardReader = idevice.getRFCardReader();
//                iSmartCardReader = idevice.getSmartCardReader(0);

            } catch (RemoteException e) {
                e.printStackTrace();
            }
            Toast.makeText(AUser.this, "bind service sucess", Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {

        }
    };

    class MyListener extends PrinterListener.Stub {
        @Override
        public void onError(int error) throws RemoteException {
            Message msg = new Message();
            msg.getData().putString("msg", "print error,errno:" + error);
            handler.sendMessage(msg);
        }

        @Override
        public void onFinish() throws RemoteException {
            Message msg = new Message();
            msg.getData().putString("msg", "print finished");
            mIBeeper.startBeep(200);
            handler.sendMessage(msg);
        }
    }

}
