package com.bris.brissmartmobile.activity;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.bris.brissmartmobile.R;
import com.bris.brissmartmobile.util.AppUtil;
import com.bris.brissmartmobile.util.Menu;

import org.json.JSONObject;

public class ACekSaldoBsa extends AppCompatActivity {

    private EditText et1;
    private TextInputEditText et2;
    private TextInputLayout til1, til2;
    private Button btn1;
    private String param1, param2;
    private String pageId;
    private JSONObject json, resdata;
    private String responseJSON, msgErr, rcode, cmsg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cek_saldo_bsa);

        til1 = (TextInputLayout) findViewById(R.id.param1Til);
        til1.setHint("Nomor Rekening");
        et1 = (EditText) findViewById(R.id.param1Txt);
        til2 = (TextInputLayout) findViewById(R.id.param2Til);
        til2.setHint("PIN Anda");
        et2 = (TextInputEditText) findViewById(R.id.param2Txt);

        et1.setFocusable(false);
        et1.setText(AppUtil.getDsn());

        et2.setFocusable(false);
        et2.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                et2.setFocusableInTouchMode(true);

                return false;
            }
        });

        btn1 = (Button) findViewById(R.id.btn_submit);

        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                param1 = et1.getText().toString();

                if (et2.getText().toString().length() == 0) {
                    til2.setError("PIN tidak boleh kosong");
                    return;
                } else {
                    param2 = et2.getText().toString();
                }

                SoapActivity action = new SoapActivity();
                action.execute();

            }
        });

        Bundle bundleDatas = getIntent().getExtras();
        pageId = bundleDatas.getString(Menu.MENU_ID);
        AppUtil.toolbarRegular(ACekSaldoBsa.this, pageId);
    }

    private class SoapActivity extends AsyncTask<Void, Void, Void> {


        @Override
        protected void onPreExecute() {

        }

        @Override
        protected Void doInBackground(Void... params) {
            convertir();
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            if (msgErr.equalsIgnoreCase("true")) {
                AppUtil.displayDialog(ACekSaldoBsa.this, responseJSON);
            }
        }
    }

    private void convertir() {
        try {

            runOnUiThread(new Runnable() {
                public void run() {
                    AppUtil.initDialogProgress(ACekSaldoBsa.this);
                    AppUtil.showDialog();
                }
            });

            JSONObject cmdparam = new JSONObject();
            cmdparam.put("vdsn", param1);
            cmdparam.put("tpass", param2);

            Log.d("Cek dsn", AppUtil.getDsn());
            String soapRes = AppUtil.soapReq(AppUtil.getDsn(), "bsa_checkpass", cmdparam);

            if (soapRes.equalsIgnoreCase("")) {
                AppUtil.hideDialog();
                msgErr = "true";
                responseJSON = "Gagal terhubung ke server";
            } else {
                json = new JSONObject(soapRes);
                resdata = json.getJSONObject("resdata");
                rcode = resdata.getString("rcode");
                msgErr = "false";

                if (rcode.equalsIgnoreCase("00")) {
                    Thread thread = new Thread(new Runnable() {

                        @Override
                        public void run() {
                            try {
                                JSONObject cmdparam = new JSONObject();
                                cmdparam.put("vdsn", param1);
                                String soapRes = AppUtil.soapReq(AppUtil.getDsn(), "balance_check", cmdparam);

                                if (soapRes.equalsIgnoreCase("")) {
                                    AppUtil.hideDialog();
                                    msgErr = "true";
                                    responseJSON = "Gagal terhubung ke server";
                                } else {
                                    json = new JSONObject(soapRes);
                                    resdata = json.getJSONObject("resdata");
                                    rcode = resdata.getString("rcode");
                                    cmsg = resdata.getString("cmsg");

                                    if (rcode.equalsIgnoreCase("00")) {
                                        AppUtil.hideDialog();
                                        runOnUiThread(new Runnable() {
                                            public void run() {
                                                et2.getText().clear();
                                                AppUtil.displayDialog(ACekSaldoBsa.this, cmsg);
                                                AppUtil.saveIntoInbox(cmsg, pageId, AppUtil.getDsn());
                                            }
                                        });
                                    } else {
                                        AppUtil.hideDialog();
                                        runOnUiThread(new Runnable() {
                                            public void run() {
                                                AppUtil.displayDialog(ACekSaldoBsa.this, cmsg);
                                            }
                                        });
                                    }
                                }
                            } catch (Exception ex) {
                                msgErr = "true";
                                AppUtil.hideDialog();
                                Log.d("THREAD", ex.toString());
                                responseJSON = "Transaksi gagal";
                            }
                        }
                    });
                    thread.start();
                } else {
                    Log.d("else", "gagal");
                    AppUtil.hideDialog();
                    cmsg = resdata.getString("cmsg");
                    responseJSON = cmsg;
                    msgErr = "true";
                }
            }
        } catch (Exception ex) {
            msgErr = "true";
            AppUtil.hideDialog();
            Log.d("THREAD 2", ex.toString());
            responseJSON = "Transaksi gagal";
        }
    }
}
