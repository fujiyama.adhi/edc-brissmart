package com.bris.brissmartmobile.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.LogPrinter;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.bris.brissmartmobile.R;
import com.bris.brissmartmobile.model.Favorite;
import com.bris.brissmartmobile.model.UserBrissmart;
import com.bris.brissmartmobile.model.UserDevice;
import com.bris.brissmartmobile.util.AppUtil;
import com.bris.brissmartmobile.util.BackStackFragment;
import com.bris.brissmartmobile.util.RealmHelper;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;

import io.realm.Realm;
import io.realm.RealmResults;
import okhttp3.OkHttpClient;
import okhttp3.Request;

public class ALoginScreen extends AppCompatActivity implements RealmHelper {

    private EditText et1, et2;
    private TextInputLayout til1, til2;
    private Button btn1;
    String param1, param2;
    JSONObject json, resdata;
    String user, responseJSON, gcode, rcodeGrant = "", rcode = "", cmsg, name, acc, status, msgErr, login;
    private Realm realm;
    ProgressDialog loading;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        til1 = (TextInputLayout) findViewById(R.id.til_param1Txt);
        et1 = (EditText) findViewById(R.id.param1Txt);
        til2 = (TextInputLayout) findViewById(R.id.til_param2Txt);
        et2 = (EditText) findViewById(R.id.param2Txt);
        btn1 = (Button) findViewById(R.id.btn_submit);

        Bundle bundleDatas = getIntent().getExtras();
        login = bundleDatas.getString("login");
        if (login.equalsIgnoreCase("autologin")) {
            param1 = bundleDatas.getString("vdsn");
            param2 = bundleDatas.getString("vpass");

            loading = ProgressDialog.show(ALoginScreen.this,
                    "Login",
                    "Memproses . . .", false, false);

            loading.show();

            SoapActivity action = new SoapActivity();
            action.execute();

        } else {
            et1.setFocusable(false);
            et1.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {

                    et1.setFocusableInTouchMode(true);

                    return false;
                }
            });

            et2.setFocusable(false);
            et2.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {

                    et2.setFocusableInTouchMode(true);

                    return false;
                }
            });

            btn1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (et1.getText().toString().length() == 0) {
                        til1.setError("Username Tidak Boleh Kosong!");
                        return;
                    } else {
                        param1 = et1.getText().toString();

                    }

                    if (et2.getText().toString().length() == 0) {
                        til2.setError("Password Tidak Boleh Kosong!");
                        return;
                    } else {
                        param2 = et2.getText().toString();
                    }

                    loading = ProgressDialog.show(ALoginScreen.this,
                            "Login",
                            "Memproses . . .", false, false);

                    loading.show();

                    SoapActivity action = new SoapActivity();
                    action.execute();
                }
            });
        }
    }

    private void convertir() {
        try {
            String grant_access_code = null;
            realm = Realm.getDefaultInstance();
            RealmResults<UserDevice> userDevice = realm.where(UserDevice.class).findAll();
            grant_access_code = (userDevice.get(0).getGrant_access_code().isEmpty()) ? grant_access_code.toUpperCase() : userDevice.get(0).getGrant_access_code();
            user = (userDevice.get(0).getUser().isEmpty()) ? user.toUpperCase() : userDevice.get(0).getUser();

            JSONObject cmdparamGrant = new JSONObject();
            cmdparamGrant.put("vdsn", param1);
            cmdparamGrant.put("grant_access_code", grant_access_code);
            String soapResGrant = AppUtil.soapReq(param1, "checkgrantaccesscode_activation", cmdparamGrant);

            if (soapResGrant.equalsIgnoreCase("")) {
                loading.dismiss();
                msgErr = "true";
                responseJSON = "Gagal terhubung ke server";
            } else {
                json = new JSONObject(soapResGrant);
                resdata = json.getJSONObject("resdata");
                rcodeGrant = resdata.getString("rcode");
                msgErr = "false";

                if (rcodeGrant.equalsIgnoreCase("00") || rcodeGrant.equalsIgnoreCase("93")) {
                    if (rcodeGrant.equalsIgnoreCase("93")) {
                        cmsg = resdata.getString("cmsg");
                    }
                    try {
                        JSONObject cmdparam = new JSONObject();
                        cmdparam.put("vdsn", param1);
                        String soapRes = AppUtil.soapReq(param1, "dsn_verify", cmdparam);

                        if (soapRes.equalsIgnoreCase("")) {
                            msgErr = "true";
                            responseJSON = "Gagal terhubung ke server";
                        } else {
                            json = new JSONObject(soapRes);
                            resdata = json.getJSONObject("resdata");
                            rcode = resdata.getString("rcode");
                            msgErr = "false";
                            Log.d("rcode", rcode);

                            if (rcode.equalsIgnoreCase("00")) {
                                msgErr = "false";
                                name = resdata.getString("name");

                                gcode = resdata.getString("gcode");
                                Log.d("Cek gcode", gcode);

                                if (gcode.equalsIgnoreCase("00")) {
                                    msgErr = "true";
                                    responseJSON = "User tidak terdaftar";
                                } else if (gcode.equalsIgnoreCase("01")) {
                                    msgErr = "false";
                                    status = "Agen";

                                    JSONObject cmdparam2 = new JSONObject();
                                    cmdparam2.put("vdsn", param1);
                                    cmdparam2.put("vpass", param2);

                                    String soapRes2 = AppUtil.soapReq(param1, "agen_login", cmdparam2);

                                    if (soapRes2.equalsIgnoreCase("")) {
                                        msgErr = "true";
                                        responseJSON = "Gagal terhubung ke server";
                                    } else {
                                        json = new JSONObject(soapRes2);
                                        resdata = json.getJSONObject("resdata");
                                        rcode = resdata.getString("rcode");
                                        gcode = resdata.getString("gcode");

                                        if (rcode.equalsIgnoreCase("00") && gcode.equalsIgnoreCase("01")) {
                                            loading.dismiss();
                                            msgErr = "false";
                                            acc = resdata.getString("acc");

                                            insertUserBrissmart(name, param1, status, acc);
                                            insertFavorit();

                                            Intent intent = new Intent(ALoginScreen.this, AMainAgen.class);
                                            Bundle datas = new Bundle();
                                            if (rcodeGrant.equalsIgnoreCase("93")) {
                                                datas.putString("source", "login");
                                                datas.putString("rcode", "93");
                                                datas.putString("cmsg", cmsg);
                                                datas.putString("user", user);
                                            } else {
                                                datas.putString("source", "login");
                                                datas.putString("rcode", "00");
                                            }
                                            intent.putExtras(datas);
                                            startActivity(intent);
                                        } else if ((rcode.equalsIgnoreCase("00") && gcode.equalsIgnoreCase("04"))
                                                || (rcode.equalsIgnoreCase("00") && gcode.equalsIgnoreCase("05"))
                                                || (rcode.equalsIgnoreCase("00") && gcode.equalsIgnoreCase("06"))
                                                || (rcode.equalsIgnoreCase("00") && gcode.equalsIgnoreCase("07"))) {
                                            loading.dismiss();
                                            msgErr = "true";
                                            cmsg = resdata.getString("cmsg");
                                            Log.d("err : ", cmsg);
                                            responseJSON = cmsg;
                                            if (rcode.equalsIgnoreCase("00") && gcode.equalsIgnoreCase("04")) {
                                                responseJSON = "Agen diblokir";
                                            } else if (rcode.equalsIgnoreCase("00") && gcode.equalsIgnoreCase("05")) {
                                                responseJSON = "Nasabah BSA diblokir";
                                            } else if (rcode.equalsIgnoreCase("00") && gcode.equalsIgnoreCase("06")) {
                                                responseJSON = "Akun Agen ditutup";
                                            } else if (rcode.equalsIgnoreCase("00") && gcode.equalsIgnoreCase("07")) {
                                                responseJSON = "Account BSA ditutup";
                                            }
                                        } else {
                                            loading.dismiss();
                                            msgErr = "true";
                                            cmsg = resdata.getString("cmsg");
                                            Log.d("err : ", cmsg);
                                            responseJSON = cmsg;
                                        }
                                    }
                                } else if (gcode.equalsIgnoreCase("02") || gcode.equalsIgnoreCase("03")) {
                                    msgErr = "false";
                                    status = "Nasabah BSA";
                                    acc = "";

                                    JSONObject cmdparam3 = new JSONObject();
                                    cmdparam3.put("vdsn", param1);
                                    cmdparam3.put("tpass", param2);

                                    String soapRes2 = AppUtil.soapReq(param1, "bsa_checkpass", cmdparam3);

                                    if (soapRes2.equalsIgnoreCase("")) {
                                        msgErr = "true";
                                        responseJSON = "Gagal terhubung ke server";
                                    } else {
                                        json = new JSONObject(soapRes2);
                                        resdata = json.getJSONObject("resdata");
                                        rcode = resdata.getString("rcode");

                                        if (rcode.equalsIgnoreCase("00")) {
                                            loading.dismiss();
                                            msgErr = "false";

                                            insertUserBrissmart(name, param1, status, acc);
                                            insertFavorit();

                                            Intent intent = new Intent(ALoginScreen.this, AMainBsa.class);
                                            Bundle datas = new Bundle();
                                            if (rcodeGrant.equalsIgnoreCase("93")) {
                                                datas.putString("source", "login");
                                                datas.putString("rcode", "93");
                                                datas.putString("cmsg", cmsg);
                                                datas.putString("user", user);
                                            } else {
                                                datas.putString("source", "login");
                                                datas.putString("rcode", "00");
                                            }
                                            intent.putExtras(datas);
                                            startActivity(intent);
                                        } else {
                                            loading.dismiss();
                                            msgErr = "true";
                                            cmsg = resdata.getString("cmsg");
                                            Log.d("err : ", cmsg);
                                            responseJSON = cmsg;
                                        }
                                    }
                                } else if (gcode.equalsIgnoreCase("04") || gcode.equalsIgnoreCase("05") || gcode.equalsIgnoreCase("06") || gcode.equalsIgnoreCase("07")) {
                                    loading.dismiss();
                                    msgErr = "true";
                                    cmsg = resdata.getString("cmsg");
                                    Log.d("err : ", cmsg);
                                    responseJSON = cmsg;
                                    if (rcode.equalsIgnoreCase("00") && gcode.equalsIgnoreCase("04")) {
                                        responseJSON = "Agen diblokir";
                                    } else if (rcode.equalsIgnoreCase("00") && gcode.equalsIgnoreCase("05")) {
                                        responseJSON = "Nasabah BSA diblokir";
                                    } else if (rcode.equalsIgnoreCase("00") && gcode.equalsIgnoreCase("06")) {
                                        responseJSON = "Akun Agen ditutup";
                                    } else if (rcode.equalsIgnoreCase("00") && gcode.equalsIgnoreCase("07")) {
                                        responseJSON = "Account BSA ditutup";
                                    }
                                }
                            } else {
                                loading.dismiss();
                                msgErr = "true";
                                Log.d("else : ", "gagal");
                                responseJSON = "User tidak terdaftar";
                            }
                        }
                    } catch (Exception ex) {
                        ex.printStackTrace();
                        Log.d("Gagal", ex.toString());
                        responseJSON = "Gagal terhubung ke server";
                        runOnUiThread(new Runnable() {
                            public void run() {
                                loading.dismiss();
                                AppUtil.displayDialog(ALoginScreen.this, responseJSON);
                            }
                        });
                    }
                } else if (rcodeGrant.equalsIgnoreCase("92")) {
                    loading.dismiss();
                    cmsg = resdata.getString("cmsg");

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            AppUtil.displayDialogChangeDevice(ALoginScreen.this, cmsg);
                        }
                    });
                    loading.dismiss();
                    msgErr = "true";
                    cmsg = resdata.getString("cmsg");
                    Log.d("else : ", "gagal");
                    responseJSON = cmsg;
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            Log.d("Gagal", ex.toString());
            responseJSON = "Gagal terhubung ke server";
            runOnUiThread(new Runnable() {
                public void run() {
                    loading.dismiss();
                    AppUtil.displayDialog(ALoginScreen.this, responseJSON);
                }
            });
        }
    }

    @Override
    public void onBackPressed() {
        AppUtil.showQuitAppDialog(ALoginScreen.this);
    }

    private class SoapActivity extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
        }

        @Override
        protected Void doInBackground(Void... params) {
            convertir();
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            if (msgErr.equalsIgnoreCase("true")) {
                AppUtil.displayDialog(ALoginScreen.this, responseJSON);
            }
            if (login.equalsIgnoreCase("defaultlogin")) {
                et1.getText().clear();
                et2.getText().clear();
            }
        }
    }

    @Override
    public void insertUser(String msisdn, String phone_id) {

    }

    @Override
    public void insertAcc(String phone_id, String alias, String account) {

    }

    @Override
    public void getUser() {

    }

    @Override
    public void getAccount() {

    }

    @Override
    public void deleteAccount() {

    }

    @Override
    public void deleteUser() {

    }

    public void insertUserBrissmart(final String name, final String dsn, final String status, final String acc) {
        Log.d("Cek insertUserBrissmart", "Mlebu");
        Log.d("Cek name", name);
        Log.d("Cek dsn", dsn);
        Log.d("Cek status", status);
        Log.d("Cek acc", acc);

        if (login.equalsIgnoreCase("autologin")) {
            Realm realm = Realm.getDefaultInstance();

        }

        Realm realm = Realm.getDefaultInstance();
        RealmResults<UserBrissmart> user = realm.where(UserBrissmart.class).findAll();
        Log.d("Cek realm 1", user.toString());
        if (user.size() > 0) {
            realm.beginTransaction();
            user.deleteAllFromRealm();
            realm.commitTransaction();
        }
        Log.d("Cek realm 2", user.toString());

        realm.beginTransaction();
        Log.d("Cek beginTransaction", "Mlebu");
        UserBrissmart userBrissmart = realm.createObject(UserBrissmart.class);
        userBrissmart.setName(name);
        userBrissmart.setDsn(dsn);
        userBrissmart.setStatus(status);
        userBrissmart.setAcc(acc);
        realm.commitTransaction();
        Log.d("RESULT", "Cek User =" + user.toString());
    }

    public void insertFavorit() {
        Log.d("Cek insertFavorit", "Mlebu");
        realm = Realm.getDefaultInstance();
        RealmResults<Favorite> favorites = realm.where(Favorite.class).findAll();
        if (favorites.size() > 0) {
            realm.beginTransaction();
            favorites.deleteAllFromRealm();
            realm.commitTransaction();
        }

        try {
            OkHttpClient client = new OkHttpClient();
            okhttp3.Request request = new Request.Builder()
                    .url(AppUtil.BASE_URL_LAKU_PANDAI + "getfavorit?dsn=" + AppUtil.getDsn() + "&menutrx=all&submenutrx=all")
                    .build();
            okhttp3.Response responses = null;

            try {
                responses = client.newCall(request).execute();
            } catch (IOException e) {
                e.printStackTrace();
            }
            JSONObject json = new JSONObject(responses.body().string());
            JSONArray jsonArray = json.getJSONArray("data");
            Log.d("Cek jsonArray", jsonArray.toString());
            for (int i = 0; i < jsonArray.length(); i++) {
                realm.beginTransaction();
                Favorite fav = realm.createObject(Favorite.class);
                JSONObject jo = jsonArray.getJSONObject(i);
                fav.setIdfav(jo.getString("IDFAV"));
                fav.setJenispembayaran(jo.getString("JENISPEMBAYARAN"));
                fav.setBsaacc(jo.getString("BSAACC"));
                fav.setData1(jo.getString("DATA1"));
                fav.setData2(jo.getString("DATA2"));
                fav.setData3(jo.getString("DATA3"));
                fav.setData4(jo.getString("DATA4"));
                fav.setNamafav(jo.getString("NAMAFAV"));
                fav.setUseragen(jo.getString("USERAGEN"));
                fav.setMenutrx(jo.getString("MENUTRX"));
                fav.setSubmenutrx(jo.getString("SUBMENUTRX"));
                fav.setNamajnsfav(jo.getString("NAMAJNSFAV"));
                fav.setTanggalfav(jo.getString("TANGGALFAV"));
                realm.commitTransaction();
            }
            RealmResults<Favorite> fav = realm.where(Favorite.class)
                    .findAll();
            Log.d("Cek dataFavorit", fav.toString());
        } catch (Exception ex) {
            Log.d("Catch act prop", ex.toString());
        }
    }
}
