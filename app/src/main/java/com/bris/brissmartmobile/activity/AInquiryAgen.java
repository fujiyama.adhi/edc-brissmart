package com.bris.brissmartmobile.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;

import com.bris.brissmartmobile.R;
import com.bris.brissmartmobile.fragment.FInquiryAgen;
import com.bris.brissmartmobile.util.AppUtil;
import com.bris.brissmartmobile.util.Menu;

import org.json.JSONArray;
import org.json.JSONObject;
import java.util.Arrays;

public class AInquiryAgen extends AppCompatActivity implements TextWatcher, DialogInterface.OnKeyListener {

    private TextInputLayout tilNamaPropinsi, tilNamaKota;
    private String[] propinsi_id, propinsi_nama, propinsi_syiarnoprop, kota_id, kota_nama, kota_syiarnokota;
    private String syiar_noprop, syiar_nokota;
    JSONArray data;
    private AutoCompleteTextView actvNamaPropinsi, actvNamaKota;
    private Button btn_submit;
    String pageId;
    private String jsonData;
    JSONObject json, resdata, cmdparamRes;
    String responseJSON, msgErr, rcode, count, dpage;
    private AlertDialog AlertDialog = null,
            confirmationDialog = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inquiry_agen);

        tilNamaPropinsi = (TextInputLayout) findViewById(R.id.til_nama_propinsi);
        actvNamaPropinsi = (AutoCompleteTextView) findViewById(R.id.act_nama_propinsi);
        actvNamaPropinsi.setFocusable(false);

        tilNamaKota = (TextInputLayout) findViewById(R.id.til_nama_kota);
        actvNamaKota = (AutoCompleteTextView) findViewById(R.id.act_nama_kota);
        actvNamaKota.setFocusable(false);

        btn_submit = (Button) findViewById(R.id.convertirBtn);

        Bundle bundleDatas = getIntent().getExtras();
        pageId = bundleDatas.getString(Menu.MENU_ID);

        AppUtil.toolbarRegular(AInquiryAgen.this, pageId);

        setAutoCompletePropinsi();

        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Thread thread = new Thread(new Runnable() {

                    @Override
                    public void run() {
                        SoapActivity action = new SoapActivity();
                        action.execute();

                        InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(actvNamaPropinsi.getWindowToken(), 0);
                        imm.hideSoftInputFromWindow(actvNamaKota.getWindowToken(), 0);
                    }
                });
                thread.start();
            }
        });
    }

    private void setAutoCompletePropinsi() {

        Thread thread = new Thread(new Runnable() {

            @Override
            public void run() {
                try {
                    jsonData = AppUtil.setAutoCompletePropinsi();
                    Log.d("data", jsonData);
                    json = new JSONObject(jsonData);
                    Log.d("json", json.toString());
                    data = json.getJSONArray("data");
                    Log.d("array", data.toString());

                    propinsi_id = new String[data.length()];
                    propinsi_nama = new String[data.length()];
                    propinsi_syiarnoprop = new String[data.length()];
                    for (int i = 0; i < data.length(); i++) {
                        JSONObject jo = data.getJSONObject(i);
                        propinsi_id[i] = jo.getString("id");
                        propinsi_nama[i] = jo.getString("nama");
                        propinsi_syiarnoprop[i] = jo.getString("syiar_no_prop");
                    }
                } catch (Exception ex) {
                    msgErr = "true";
                    Log.d("THREAD a", ex.toString());
                    responseJSON = ex.toString();
                }

                actvNamaPropinsi.setThreshold(1);

                runOnUiThread(new Runnable() {
                    public void run() {
                        ArrayAdapter<String> bankListAdapter = new ArrayAdapter<String>(AInquiryAgen.this, android.R.layout.simple_list_item_1,
                                propinsi_nama);
                        bankListAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        actvNamaPropinsi.setAdapter(bankListAdapter);
                    }
                });

                actvNamaPropinsi.setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        actvNamaPropinsi.setFocusableInTouchMode(true);
                        actvNamaPropinsi.showDropDown();
                        actvNamaPropinsi.requestFocus();
                        return false;
                    }
                });

                actvNamaPropinsi.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                    @Override
                    public void onFocusChange(View v, boolean hasFocus) {
                        String val = actvNamaPropinsi.getText().toString() + "";
                        Boolean code = Arrays.asList(propinsi_nama).contains(val);
                        if (!hasFocus)
                            if (!code) {
                                tilNamaPropinsi.setErrorEnabled(true);
                                tilNamaPropinsi.setError("Nama Propinsi tidak valid");
                            } else {
                                tilNamaPropinsi.setErrorEnabled(false);
                                tilNamaPropinsi.setError(null);
                            }
                    }
                });

                actvNamaPropinsi.addTextChangedListener(AInquiryAgen.this);

                actvNamaPropinsi.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View arg1, int position, long id) {
                        String prop = actvNamaPropinsi.getText().toString();
                        int posisi = 0;
                        for (int i = 0; i < propinsi_nama.length; i++) {
                            if (propinsi_nama[i].equalsIgnoreCase(prop)) {
                                Log.d("Test getPropinsi", String.valueOf(i));
                                posisi = i;
                            }
                        }
                        String id_propinsi = propinsi_id[posisi];
                        setAutoCompleteKota(id_propinsi);

                        syiar_noprop = propinsi_syiarnoprop[posisi];
                    }
                });
            }
        });
        thread.start();
    }

    private void setAutoCompleteKota(final String id_propinsi) {

        Thread thread = new Thread(new Runnable() {

            @Override
            public void run() {
                try {
                    jsonData = AppUtil.setAutoCompleteKota(id_propinsi);
                    Log.d("data", jsonData);
                    json = new JSONObject(jsonData);
                    Log.d("json", json.toString());
                    data = json.getJSONArray("data");
                    Log.d("array", data.toString());


                    kota_id = new String[data.length()];
                    kota_nama = new String[data.length()];
                    kota_syiarnokota = new String[data.length()];
                    for (int i = 0; i < data.length(); i++) {
                        JSONObject jo = data.getJSONObject(i);
                        kota_id[i] = jo.getString("id");
                        kota_nama[i] = jo.getString("nama");
                        kota_syiarnokota[i] = jo.getString("syiar_no_kab");
                        Log.d("Test kota", kota_nama.toString());
                    }
                } catch (Exception ex) {
                    msgErr = "true";
                    Log.d("THREAD a", ex.toString());
                    responseJSON = ex.toString();
                }

                actvNamaKota.setThreshold(1);

                runOnUiThread(new Runnable() {
                    public void run() {
                        ArrayAdapter<String> bankListAdapter = new ArrayAdapter<String>(AInquiryAgen.this, android.R.layout.simple_list_item_1,
                                kota_nama);
                        bankListAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        actvNamaKota.setAdapter(bankListAdapter);
                    }
                });

                actvNamaKota.setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        actvNamaKota.setFocusableInTouchMode(true);
                        actvNamaKota.showDropDown();
                        actvNamaKota.requestFocus();
                        return false;
                    }
                });

                actvNamaKota.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                    @Override
                    public void onFocusChange(View v, boolean hasFocus) {
                        String val = actvNamaKota.getText().toString() + "";
                        Boolean code = Arrays.asList(kota_nama).contains(val);
                        if (!hasFocus)
                            if (!code) {
                                tilNamaKota.setErrorEnabled(true);
                                tilNamaKota.setError("Nama Kota tidak valid");
                            } else {
                                tilNamaKota.setErrorEnabled(false);
                                tilNamaKota.setError(null);
                            }
                    }
                });

                actvNamaKota.addTextChangedListener(AInquiryAgen.this);

                actvNamaKota.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        String kota = actvNamaKota.getText().toString();
                        int posisi = 0;
                        for (int i = 0; i < kota_nama.length; i++) {
                            if (kota_nama[i].equalsIgnoreCase(kota)) {
                                Log.d("Test getKota", String.valueOf(i));
                                posisi = i;
                            }
                        }
                        syiar_nokota = kota_syiarnokota[posisi];
                    }
                });
            }
        });
        thread.start();
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        if (actvNamaPropinsi.getText().toString().length() > 0) {
            tilNamaPropinsi.setError(null);
        }
        if (actvNamaKota.getText().toString().length() > 0) {
            tilNamaKota.setError(null);
        }
    }

    @Override
    public void afterTextChanged(Editable s) {
//        if (nama_propinsi.contains(" - ")) {
//            String id_propinsi = nama_propinsi.substring(0, nama_propinsi.indexOf(" -"));
//            setAutoCompleteKota(id_propinsi);
//        }
    }

    private void convertir() {

        try {
            JSONObject cmdparam = new JSONObject();
            cmdparam.put("vdsn", AppUtil.getDsn());
            cmdparam.put("dprov", syiar_noprop);
            cmdparam.put("dcity", syiar_nokota);
            cmdparam.put("ddist", "");
            cmdparam.put("dpage", "1");

            String soapRes = AppUtil.soapReq(AppUtil.getDsn(), "agent_search", cmdparam);

            if (soapRes.equalsIgnoreCase("")) {
                AppUtil.hideDialog();
                msgErr = "true";
                responseJSON = "Gagal terhubung ke server";
            } else {
                json = new JSONObject(soapRes);
                resdata = json.getJSONObject("resdata");
                cmdparamRes = json.getJSONObject("cmdparam");
                dpage = cmdparamRes.getString("dpage");
                rcode = resdata.getString("rcode");
                count = resdata.getString("count");

                if(rcode.equalsIgnoreCase("00")){
                    Bundle datas = new Bundle();
                    datas.putString("response", String.valueOf(json));
                    datas.putString("syiar_noprop", syiar_noprop);
                    datas.putString("syiar_nokota", syiar_nokota);
                    datas.putString("dpage", dpage);
                    datas.putString(Menu.MENU_ID, pageId);

                    FInquiryAgen fSubMenu = new FInquiryAgen();
                    FragmentManager manager = getSupportFragmentManager();
                    fSubMenu.setArguments(datas); // insert bundling datas
                    manager.beginTransaction()
                            .replace(R.id.container_inquiry_agen, fSubMenu)
                            .addToBackStack(null)
                            .commit();
                } else {
                    AppUtil.displayDialog(AInquiryAgen.this, "Maaf, Terjadi Kesalahan");
                }
            }

        } catch (Exception ex) {
            Log.d("THREAD a", ex.toString());
            runOnUiThread(new Runnable() {
                public void run() {
                    AppUtil.displayDialog(AInquiryAgen.this, "Maaf, Terjadi Kesalahan");
                }
            });
        }
    }

    @Override
    public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
            AlertDialog.Builder builder = new AlertDialog.Builder(AInquiryAgen.this);
            builder.setCancelable(false);
            builder.setMessage("Apakah anda yakin akan membatalkan proses " + pageId + "?");
            builder.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                    confirmationDialog.dismiss();
                }
            });
            builder.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                    confirmationDialog.show();
                }
            });
            if (AlertDialog == null || !AlertDialog.isShowing()) {
                AlertDialog = builder.create();
                AlertDialog.show();
            }
            return true;
        }
        return false;
    }

    private class SoapActivity extends AsyncTask<Void, Void, Void> {


        @Override
        protected void onPreExecute() {

        }

        @Override
        protected Void doInBackground(Void... params) {
            convertir();
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
        }

    }
}
