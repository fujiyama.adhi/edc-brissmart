package com.bris.brissmartmobile.activity;

import android.os.Bundle;
import android.support.annotation.ColorRes;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;

import com.aurelhubert.ahbottomnavigation.AHBottomNavigation;
import com.aurelhubert.ahbottomnavigation.AHBottomNavigationItem;
import com.bris.brissmartmobile.R;
import com.bris.brissmartmobile.bottomnav.BottomNavigationAdapter;
import com.bris.brissmartmobile.bottomnav.NoSwipePager;
import com.bris.brissmartmobile.fragment.FragmentContentBsa;
import com.bris.brissmartmobile.fragment.FragmentHomeBsa;
import com.bris.brissmartmobile.fragment.FragmentUserAccountBsa;
import com.bris.brissmartmobile.util.AppUtil;
import com.bris.brissmartmobile.util.BackStackFragment;

public class AMainBsa extends AppCompatActivity {

    /******* Pages : Home, Kontent, User Account *******/
    private NoSwipePager viewPager;
    private AHBottomNavigation bottomNavigation;
    private BottomNavigationAdapter pagerAdapter;
    private final int[] colors = {R.color.white, R.color.colorPrimary};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_dashboard);

        Bundle bundleDatas = getIntent().getExtras();
        String source = bundleDatas.getString("source");
        if (source.equalsIgnoreCase("login")) {
            String rcode = bundleDatas.getString("rcode");
            Log.d("Cek rcode", rcode);
            if (rcode.equalsIgnoreCase("93")) {
                final String cmsg = bundleDatas.getString("cmsg");
                Log.d("Cek cmsg", cmsg);
                final String user = bundleDatas.getString("user");
                Log.d("Cek user", user);
                AppUtil.displayDialogChangePin(AMainBsa.this, cmsg, user);
            }
        }

        bottomNavigation = (AHBottomNavigation) findViewById(R.id.bottom_navigation);
        viewPager = (NoSwipePager) findViewById(R.id.viewpager);

        setupViewPager();
        setupBottomNavigation();
    }

    private void setupViewPager() {
        viewPager.setPagingEnabled(false);
        pagerAdapter = new BottomNavigationAdapter(getSupportFragmentManager());

        FragmentHomeBsa fHome = new FragmentHomeBsa();
        FragmentContentBsa fContent = new FragmentContentBsa();
        FragmentUserAccountBsa fUserAccount = new FragmentUserAccountBsa();

        pagerAdapter.addFragments(fHome);
        pagerAdapter.addFragments(fContent);
        pagerAdapter.addFragments(fUserAccount);

        viewPager.setAdapter(pagerAdapter);
    }

    private void setupBottomNavigation() {
        setupBottomNavBehaviors();
        setBottomNavStyle();
        setBottomNavigationItems();
        bottomNavigation.setCurrentItem(0);

        bottomNavigation.setOnTabSelectedListener(new AHBottomNavigation.OnTabSelectedListener() {
            @Override
            public boolean onTabSelected(int position, boolean wasSelected) {
                try {
                    if (!wasSelected) viewPager.setCurrentItem(position);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                return true;
            }
        });
    }

    public void setupBottomNavBehaviors() {
        bottomNavigation.setBehaviorTranslationEnabled(false);

        /*
        Before enabling this. Change MainActivity theme to MyTheme.TranslucentNavigation in
        AndroidManifest.

        Warning: Toolbar Clipping might occur. Solve this by wrapping it in a LinearLayout with a top
        View of 24dp (status bar size) height.
         */
        bottomNavigation.setTranslucentNavigationEnabled(false);
    }

    private void setBottomNavStyle() {
        // Colors for selected (active) and non-selected items.
        bottomNavigation.setColoredModeColors(fetchColor(R.color.menuBlue),
                fetchColor(R.color.fontFormHint));

        //  Enables Reveal effect
        bottomNavigation.setColored(true);

        //  Displays item Title always (for selected and non-selected items)
        bottomNavigation.setTitleState(AHBottomNavigation.TitleState.ALWAYS_SHOW);
    }

    private int fetchColor(@ColorRes int color) {
        return ContextCompat.getColor(this, color);
    }

    private void setBottomNavigationItems() {
        AHBottomNavigationItem home = new AHBottomNavigationItem(R.string.menu_nav_beranda, R.drawable.ico_nav_home, colors[0]);
        AHBottomNavigationItem konten = new AHBottomNavigationItem(R.string.menu_nav_konten, R.drawable.ico_nav_konten, colors[0]);
        AHBottomNavigationItem account = new AHBottomNavigationItem(R.string.menu_nav_akun, R.drawable.ico_nav_user, colors[0]);

        bottomNavigation.addItem(home);
        bottomNavigation.addItem(konten);
        bottomNavigation.addItem(account);
    }

    @Override
    public void onBackPressed() {
        if (!BackStackFragment.handleBackPressed(getSupportFragmentManager())) {
            AppUtil.showLogOutDialog(AMainBsa.this);
        }
    }
}