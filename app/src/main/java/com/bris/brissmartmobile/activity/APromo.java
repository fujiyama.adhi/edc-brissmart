package com.bris.brissmartmobile.activity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;

import com.bris.brissmartmobile.R;
import com.bris.brissmartmobile.adapter.PromoAdapter;
import com.bris.brissmartmobile.model.Promo;
import com.bris.brissmartmobile.util.ApiInterface;
import com.bris.brissmartmobile.util.AppUtil;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by suminiwl
 * Credit : valeputra
 */

public class APromo extends AppCompatActivity {
    private RecyclerView rvPromo;
    private PromoAdapter adapter;
    private LinearLayoutManager layoutManager;
    private ProgressDialog mProgressDialog = null;
    private Toolbar tb_promo;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_apromo);
        rvPromo = (RecyclerView) findViewById(R.id.lv_promo);
        AppUtil.customMainFonts(getAssets(), this.findViewById(android.R.id.content));
        customToolbar();
        mainPromo();
    }

    private void mainPromo() {
        initProgressDialog();
        getPromoList();
    }

    private void initProgressDialog() {
        mProgressDialog = new ProgressDialog(APromo.this);
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setCancelable(false);
        mProgressDialog.setMessage("Loading . .");
    }

    private void getPromoList() {
        showDialog();
        ApiInterface apiInterface =  AppUtil.getClient().create(ApiInterface.class);
        Call<List<Promo>> call = apiInterface.getPromoByCase("all");
        call.enqueue(new Callback<List<Promo>>() {
            @Override
            public void onResponse(Call<List<Promo>> call, Response<List<Promo>> response) {
                hideDialog();
                Log.d("Promo res code", response.code()+"");
                Log.d("Promo res size", response.body().size()+"");
                if (response.body().size() > 0) {
                    Log.d("onResponse", toString());
                    List<Promo> mPromo = response.body();
                    Log.d("RESULT", "DATA="+mPromo);
                    adapter = new PromoAdapter(APromo.this, mPromo);
                    layoutManager = new LinearLayoutManager(APromo.this, LinearLayoutManager.VERTICAL, false);
                    rvPromo.setLayoutManager(layoutManager);
                    rvPromo.setAdapter(adapter);
                } else {
                    // no promo registered now
                    LinearLayout lempty = (LinearLayout) findViewById(R.id.content_empty);
                    lempty.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onFailure(Call<List<Promo>> call, Throwable t) {
                Log.d("onResponse", t.toString());
                hideDialog();
                LinearLayout lfailure = (LinearLayout) findViewById(R.id.connection_failure);
                lfailure.setVisibility(View.VISIBLE);
            }
        });
    }

    private void customToolbar() {
        tb_promo = (Toolbar) findViewById(R.id.tb_info_promo);
        tb_promo.setNavigationIcon(R.mipmap.abc_ic_ab_back_mtrl_am_alpha);
        setSupportActionBar(tb_promo);
        tb_promo.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    public void showDialog() {

        if(mProgressDialog != null && !mProgressDialog.isShowing())
            mProgressDialog.show();
    }

    public void hideDialog() {

        if(mProgressDialog != null && mProgressDialog.isShowing())
            mProgressDialog.dismiss();
    }
}
