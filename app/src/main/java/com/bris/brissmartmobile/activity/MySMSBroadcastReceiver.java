package com.bris.brissmartmobile.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.auth.api.phone.SmsRetriever;
import com.google.android.gms.common.api.CommonStatusCodes;
import com.google.android.gms.common.api.Status;

/**
 * BroadcastReceiver to wait for SMS messages. This can be registered either
 * in the AndroidManifest or at runtime.  Should filter Intents on
 * SmsRetriever.SMS_RETRIEVED_ACTION.
 */
public class MySMSBroadcastReceiver extends BroadcastReceiver {

    private OTPReceiveListener otpReceiver;

    public void initOTPListener(OTPReceiveListener receiver) {
        this.otpReceiver = receiver;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        if (SmsRetriever.SMS_RETRIEVED_ACTION.equals(intent.getAction())) {
            Bundle extras = intent.getExtras();
            Status status = (Status) extras.get(SmsRetriever.EXTRA_STATUS);

            Log.e("onReceive status", String.valueOf(status));

            switch(status.getStatusCode()) {
                case CommonStatusCodes.SUCCESS:
                    try {
                        // Get SMS message contents
                        String otp = (String) extras.get(SmsRetriever.EXTRA_SMS_MESSAGE);
                        // Extract one-time code from the message and complete verification
                        // by sending the code back to your server.

                        Log.e("onReceive SUCCESS", String.valueOf(otp));

                        if (otpReceiver != null) {
                            int start = otp.indexOf("anda ") + 5;
                            int end = start + 4;
                            otp = otp.substring(start, end).trim();

                            Log.e("onReceive otpReceiver", String.valueOf(otp));

                            otpReceiver.onOTPReceived(otp);
                        }
                    } catch (Exception e) {
                        Log.e("Exception", e.getMessage());
                    }

                    break;

                case CommonStatusCodes.TIMEOUT:
                    // Waiting for SMS timed out (5 minutes)
                    // Handle the error ...
                    otpReceiver.onOTPTimeOut();

                    Log.e("onReceive TIMEOUT", "TIMEOUT");
                    break;
            }
        }
    }

    public interface OTPReceiveListener {
        void onOTPReceived(String otp);
        void onOTPTimeOut();
    }
}
