package com.bris.brissmartmobile.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;

import com.bris.brissmartmobile.R;
import com.bris.brissmartmobile.util.AppUtil;

import org.json.JSONObject;

/**
 * Created by suminiwl
 */

public class AGantiPasswordBsa extends AppCompatActivity {
    private TextInputEditText tvCurrPin, tvNewPin, tvConfirmNewPin;
    private String param1, param2, vdsn, direct, user;
    private String responsMsg = "Message is Undefined";
    private Button btnDoGantiPin;
    JSONObject json, resdata;
    String responseJSON, msgErr, rcode, gcode, cmsg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ganti_pin);
        tvCurrPin = (TextInputEditText) findViewById(R.id.et_pin_lama);
        tvNewPin = (TextInputEditText) findViewById(R.id.et_pin_baru);
        tvConfirmNewPin = (TextInputEditText) findViewById(R.id.et_konfirm_pass);
        btnDoGantiPin = (Button) findViewById(R.id.btn_ganti_password);

        Bundle bundleDatas = getIntent().getExtras();
        direct = bundleDatas.getString("status");
        if (direct.equalsIgnoreCase("direct")) {
            vdsn = bundleDatas.getString("vdsn");
            user = bundleDatas.getString("user");
            tvCurrPin.setText("123456");
            tvCurrPin.setFocusable(false);
        } else {
            vdsn = AppUtil.getDsn();
        }

        setEnabled();
        AppUtil.toolbarRegular(AGantiPasswordBsa.this, "Ganti Password");
        main();
    }

    private void main() {
        btnDoGantiPin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (tvCurrPin.getText().toString().trim().isEmpty()) {
                    tvCurrPin.setError("Password lama harus diisi");
                    return;
                } else {
                    param1 = tvCurrPin.getText().toString();
                }

                if (tvNewPin.getText().toString().trim().isEmpty()) {
                    tvNewPin.setError("Password baru harus diisi");
                    return;
                } else {
                    param2 = tvNewPin.getText().toString();
                }

                if (tvConfirmNewPin.toString().toString().trim().isEmpty()) {
                    tvConfirmNewPin.setError("Password konfirmasi harus diisi");
                    return;
                }

                responsMsg = validationInput();

                if (responsMsg.isEmpty()) {
                    SoapActivity action = new SoapActivity();
                    action.execute();
                } else {
                    AppUtil.displayDialog(AGantiPasswordBsa.this, responsMsg);
                }
            }
        });
    }


    private void clearField() {
        tvCurrPin.getText().clear();
        tvNewPin.getText().clear();
        tvConfirmNewPin.getText().clear();
    }

    private void setEnabled() {
        tvCurrPin.setFocusable(false);
        tvCurrPin.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                tvCurrPin.setFocusableInTouchMode(true);

                return false;
            }
        });

        tvNewPin.setFocusable(false);
        tvNewPin.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                tvNewPin.setFocusableInTouchMode(true);

                return false;
            }
        });

        tvConfirmNewPin.setFocusable(false);
        tvConfirmNewPin.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                tvConfirmNewPin.setFocusableInTouchMode(true);

                return false;
            }
        });
    }

    public String validationInput() {
        responsMsg = "";

        if (tvCurrPin.getText().toString().length() != 6 ||
                tvNewPin.getText().toString().length() != 6 ||
                tvConfirmNewPin.getText().toString().length() != 6) {
            responsMsg = "Gagal. Password lama dan baru panjangnya 6 karakter";
            return responsMsg;
        }
        if (!tvNewPin.getText().toString().equalsIgnoreCase(tvConfirmNewPin.getText().toString())) {
            responsMsg = "Gagal. Field Password baru dan Konfirmasi tidak sama";
            return responsMsg;
        }

        return responsMsg;
    }

    private class SoapActivity extends AsyncTask<Void, Void, Void> {


        @Override
        protected void onPreExecute() {

        }

        @Override
        protected Void doInBackground(Void... params) {
            convertir();
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            if (msgErr.equalsIgnoreCase("true")) {
                AppUtil.displayDialog(AGantiPasswordBsa.this, responseJSON);
            }
        }
    }

    private void convertir() {

        try {

            runOnUiThread(new Runnable() {
                public void run() {
                    AppUtil.initDialogProgress(AGantiPasswordBsa.this);
                    AppUtil.showDialog();
                }
            });

            JSONObject cmdparam = new JSONObject();
            cmdparam.put("vdsn", vdsn);
            cmdparam.put("oldtpass", param1);
            cmdparam.put("newtpass", param2);

            String soapRes = AppUtil.soapReq(vdsn, "bsa_changepass", cmdparam);

            if (soapRes.equalsIgnoreCase("")) {
                AppUtil.hideDialog();
                msgErr = "true";
                responseJSON = "Gagal terhubung ke server";
            } else {
                json = new JSONObject(soapRes);
                resdata = json.getJSONObject("resdata");
                rcode = resdata.getString("rcode");
                gcode = resdata.getString("gcode");
                cmsg = resdata.getString("cmsg");
                msgErr = "false";

                if (rcode.equalsIgnoreCase("00")) {
                    AppUtil.hideDialog();
                    msgErr = "false";
                    runOnUiThread(new Runnable() {
                        public void run() {
                            if (gcode.equalsIgnoreCase("00")) {
                                if (direct.equalsIgnoreCase("direct")) {
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            AppUtil.displayDialogChangePinSucsess(AGantiPasswordBsa.this, "PIN Anda Berhasil diganti", user);
                                        }
                                    });
                                } else {
                                    runOnUiThread(new Runnable() {
                                        public void run() {
                                            clearField();
                                            AppUtil.hideDialog();
                                            AppUtil.displayDialog(AGantiPasswordBsa.this, "PIN Anda Berhasil diganti");
                                            hideKeyboard(AGantiPasswordBsa.this);
                                            setEnabled();
                                        }
                                    });
                                }
                            } else {
                                AppUtil.displayDialog(AGantiPasswordBsa.this, cmsg);
                            }

                        }
                    });
                } else {
                    Log.d("else", "gagal");
                    AppUtil.hideDialog();
                    responseJSON = cmsg;
                    msgErr = "true";
                }
            }
        } catch (Exception ex) {
            msgErr = "true";
            AppUtil.hideDialog();
            Log.d("THREAD 2", ex.toString());
            responseJSON = "Ganti Password Gagal";
        }
    }

    public static void hideKeyboard(Activity activity) {
        InputMethodManager inputManager = (InputMethodManager) activity
                .getSystemService(Context.INPUT_METHOD_SERVICE);

        // check if no view has focus:
        View currentFocusedView = activity.getCurrentFocus();
        if (currentFocusedView != null) {
            inputManager.hideSoftInputFromWindow(currentFocusedView.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }
}
