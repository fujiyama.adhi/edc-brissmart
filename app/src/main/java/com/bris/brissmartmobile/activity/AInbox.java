package com.bris.brissmartmobile.activity;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ActionMode;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bris.brissmartmobile.R;
import com.bris.brissmartmobile.adapter.InboxAdapter;
import com.bris.brissmartmobile.fragment.FInboxDetails;
import com.bris.brissmartmobile.listener.InboxListener;
import com.bris.brissmartmobile.model.History;
import com.bris.brissmartmobile.util.AppUtil;
import com.bris.brissmartmobile.util.Menu;
import com.bris.brissmartmobile.util.RecyclerClickListener;
import com.bris.brissmartmobile.util.RecyclerTouchListener;
import com.bris.brissmartmobile.util.ToolbarActionModeCallback;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;

/**
 * Created by suminiwl
 * Credit : valeputra
 */

public class AInbox extends AppCompatActivity implements InboxListener {
    private RecyclerView rv_inbox;
    private List<History> inboxList = new ArrayList<>();
    private InboxAdapter inboxAdapter;
    private Realm realm;
    private ActionMode mActionMode;
    private Toolbar tb_inbox;
    private String pageId;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_inbox);

        Bundle bundleDatas = getIntent().getExtras();
        pageId = bundleDatas.getString(Menu.MENU_ID);
        customToolbar();

        // get history list
        realm = Realm.getDefaultInstance();
        getHistory(AppUtil.getDsn());

        if(inboxList.size() < 1) {
            RelativeLayout no_inbox = (RelativeLayout) findViewById(R.id.no_inbox);
            no_inbox.setVisibility(View.VISIBLE);
        }

        inboxAdapter = new InboxAdapter(inboxList, getApplicationContext(), this);
        rv_inbox = (RecyclerView) findViewById(R.id.rv_inbox);
        rv_inbox.setHasFixedSize(true);
        rv_inbox.setLayoutManager(new LinearLayoutManager(AInbox.this)); //Linear Items
        rv_inbox.setAdapter(inboxAdapter);
        inboxAdapter.notifyDataSetChanged();

        implementRecyclerViewClickListeners();
    }

    private void customToolbar() {
        tb_inbox = (Toolbar) findViewById(R.id.tb_inbox);
        TextView tvToolbar = (TextView) findViewById(R.id.tv_title_inbox);
        tb_inbox.setNavigationIcon(R.mipmap.abc_ic_ab_back_mtrl_am_alpha);
        tvToolbar.setText(pageId);
        setSupportActionBar(tb_inbox);
        tb_inbox.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    private void getHistory(String dsn){
        RealmResults<History> inbox = realm.where(History.class).equalTo("dsn", dsn).findAll().sort("trxdate", Sort.DESCENDING);
        Log.d("JUMLAH HISTORY", String.valueOf(inbox.size()));
        if(inbox.size() > 0){
            inboxList = inbox;
        }
    }

    //Implement item click and long click over recycler view
    private void implementRecyclerViewClickListeners() {
        rv_inbox.addOnItemTouchListener(new RecyclerTouchListener(this, rv_inbox, new RecyclerClickListener() {

            @Override
            public void onClick(View view, int position) {
                //If ActionMode not null select item
                if (mActionMode != null) {
                    onListItemSelect(position);
                    Log.d("position item", position+"");
                }
            }

            @Override
            public void onLongClick(View view, int position) {
                //Select item on long click
                onListItemSelect(position);
                tb_inbox.setVisibility(View.GONE);
            }
        }));
    }

    public void onItemClick(long trxdate) {
        changeMessageStatusRead(trxdate);
        FragmentManager manager = getSupportFragmentManager();
        Fragment fragment;
        FragmentTransaction transaction = manager.beginTransaction();

        // open fragment detail inbox
        fragment = new FInboxDetails();
        Bundle bundle = new Bundle();
        bundle.putLong("trxdate",trxdate);
        fragment.setArguments(bundle);
        transaction.replace(R.id.framelayout_inbox, fragment);
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    private void changeMessageStatusRead(long trxdate) {
        realm.beginTransaction();
        History inbox = realm.where(History.class).equalTo("trxdate", trxdate).findFirst();

        if (inbox.getStatusread() != "1"){
            inbox.setStatusread("1");
            inboxAdapter.notifyDataSetChanged();
        }

        realm.commitTransaction();
    }

    //List item select method
    private void onListItemSelect(int position) {
        inboxAdapter.toggleSelection(position);//Toggle the selection

        boolean hasCheckedItems = inboxAdapter.getSelectedCount() > 0;//Check if any items are already selected or not

        if (hasCheckedItems && mActionMode == null)
            // there are some selected items, start the actionMode
            mActionMode =((AppCompatActivity) this).startSupportActionMode(new ToolbarActionModeCallback(this, inboxAdapter, AInbox.this, inboxList));
        else if (!hasCheckedItems && mActionMode != null)
            // there no selected items, finish the actionMode
            mActionMode.finish();

        if (mActionMode != null)
            //set action mode title on item selection
            mActionMode.setTitle(String.valueOf(inboxAdapter
                    .getSelectedCount()) + " selected");
    }

    //Delete selected rows
    public void deleteRows() {
        SparseBooleanArray selected = inboxAdapter.getSelectedIds(); //Get selected ids

        //Loop all selected ids
        for (int i = (selected.size() - 1); i >= 0; i--) {
            if (selected.valueAt(i)) {

                Log.d("selected delete", String.valueOf(inboxList.get(i)));
                Log.d("selected key", selected.keyAt(i)+"");

                realm.beginTransaction();
                inboxList.get(selected.keyAt(i)).deleteFromRealm();
                inboxAdapter.notifyDataSetChanged();
                realm.commitTransaction();
            }
        }
        finishActionMode();
    }

    public void deleteAllRows() {
        RealmResults<History> results = realm.where(History.class).findAll();
        realm.beginTransaction();
        results.deleteAllFromRealm();
        realm.commitTransaction();
        finishActionMode();
    }

    //Set action mode null after use
    public void setNullToActionMode() {
        if (mActionMode != null)
            mActionMode = null;

        tb_inbox.setVisibility(View.VISIBLE);
    }

    public void finishActionMode() {
        mActionMode.finish();
    }
}
