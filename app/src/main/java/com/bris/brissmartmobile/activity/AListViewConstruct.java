package com.bris.brissmartmobile.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.bris.brissmartmobile.R;
import com.bris.brissmartmobile.adapter.ListViewAdapter;
import com.bris.brissmartmobile.fragment.FCekMutasi;
import com.bris.brissmartmobile.fragment.FCekSaldo;
import com.bris.brissmartmobile.fragment.FDonasi;
import com.bris.brissmartmobile.fragment.FFavorite;
import com.bris.brissmartmobile.fragment.FSubMenuLists;
import com.bris.brissmartmobile.fragment.FTarikTunai;
import com.bris.brissmartmobile.fragment.FTransferRekeningBrisBsa;
import com.bris.brissmartmobile.fragment.FTransferRekeningCerdasBsa;
import com.bris.brissmartmobile.listener.FavClickListener;
import com.bris.brissmartmobile.model.UserBrissmart;
import com.bris.brissmartmobile.util.ListViewMenu;
import com.bris.brissmartmobile.util.Menu;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by suminiwl
 * Credit : valeputra
 */

public class AListViewConstruct extends AppCompatActivity implements FavClickListener, TextWatcher, DialogInterface.OnKeyListener {
    private Realm realm;
    private ListView lv_menu;
    private List<ListViewMenu> submenu;
    private ListViewAdapter adapter;
    private String status, pageId;
    public static final String[] menuContaintsMenu = new String[] {
            "Cek Saldo",
            "Cek Mutasi",
            "Transfer",
            "Transfer dari Rekening Tabungan Cerdas",
            "PLN Postpaid",
            "Telepon",
            "Internet",
            "TV Berbayar",
            "Tiket KAI",
            "Tokopedia",
            "Institusi",
            "Pendidikan",
            "Asuransi",
            "Token Listrik",
            "Pulsa",
            "Paket Data",
            "Top Up GO-JEK"
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alistview_constructor);

        lv_menu = (ListView) findViewById(R.id.lv_submenu_construct);
        submenu = new ArrayList<>();
        Bundle bundleDatas = getIntent().getExtras();
        pageId = bundleDatas.getString(Menu.MENU_ID);
        customToolbar();

        genSubMenu(pageId);

        adapter = new ListViewAdapter(getApplicationContext(), submenu);
        lv_menu.setAdapter(adapter);

        //listview onclick
        lv_menu.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                final TextView subtitle = (TextView) view.findViewById(R.id.tv_title_submenu);
                Bundle datas = new Bundle();
                String menuId = String.valueOf(subtitle.getText());

                datas.putString(Menu.MENU_ID, menuId);

//                Toast.makeText(AListViewConstruct.this, pageId, Toast.LENGTH_SHORT).show();

                // jika menu memiliki sub menu
                if(Arrays.asList(menuContaintsMenu).contains(subtitle.getText().toString())) {
                    // open submenu fragment
                    FSubMenuLists fSubMenu = new FSubMenuLists();
                    FragmentManager manager = getSupportFragmentManager();
                    fSubMenu.setArguments(datas); // insert bundling datas
                    manager.beginTransaction()
                            .replace(R.id.container_listview_root, fSubMenu)
                            .addToBackStack(null)
                            .commit();
                } else {
                    // jika tidak meniliki submenu
                    menuThatNotContainMenu(menuId, datas);
                }
            }
        });
    }

    private void menuThatNotContainMenu(String menuId, Bundle datas) {
        switch (menuId) {
//            case "Cek Mutasi" :
//                goToFCekMutasi(datas);
//                break;
            case "Buka Rekening Tabungan Cerdas" :
                goToABukaRekening(datas);
                break;
            case "Tarik Tunai" :
                goToFTarikTunai(datas);
                break;
            case "Transfer Rekening Tabungan Cerdas" :
                if (!(getStatus().equalsIgnoreCase("Agen"))) {
                    goToFTransferRekeningCerdasBsa(datas);
                }
                break;
            case "Transfer Rekening BRIS" :
                if (!(getStatus().equalsIgnoreCase("Agen"))) {
                    goToFTransferRekeningBrisBsa(datas);
                }
                break;
            case "Baznas":
                goToFDonasi(datas);
                break;
            case "Dompet Dhuafa":
                goToFDonasi(datas);
                break;
            case "Griya Yatim":
                goToFDonasi(datas);
                break;
            case "ZIS BRI Syariah":
                goToFDonasi(datas);
                break;
            case "Bazis DKI Jakarta":
                goToFDonasi(datas);
                break;
            case "Yayasan Dompet Sosial Madani Bali":
                goToFDonasi(datas);
                break;
            case "Yayasan Baitul Maal Hidayatullah":
                goToFDonasi(datas);
                break;
            case "LazisNU Jatim":
                goToFDonasi(datas);
                break;
            case "Rekening Tabungan Cerdas Favorit":
                datas.putString("menutrx", "1000");
                datas.putString("jenispembayaran", "CASH");
                goToFFavorit(datas);
                break;
            case "Transfer Favorit":
                datas.putString("menutrx", "8000");
                goToFFavorit(datas);
                break;
            case "Pembayaran Favorit":
                datas.putString("menutrx", "7000");
                goToFFavorit(datas);
                break;
            case "Pembelian Favorit":
                datas.putString("menutrx", "6000");
                goToFFavorit(datas);
                break;
            case "Donasi Favorit":
                datas.putString("menutrx", "9000");
                goToFFavorit(datas);
                break;
            case "Rekening Tabungan Cerdas":
                datas.putString("menutrx", "8000");
                goToFFavorit(datas);
                break;
            case "Rekening BRI Syariah":
                datas.putString("menutrx", "8000");
                goToFFavorit(datas);
                break;
            default:
                break;
        }
    }

    private void genSubMenu(String pageId) {
        switch (pageId) {
            case "Pembayaran" :
                Menu.subMenuPembayaran(AListViewConstruct.this, submenu);
                break;
            case "Pembelian" :
                Menu.subMenuPembelian(AListViewConstruct.this, submenu);
                break;
            case "Rekening":
                Menu.submenuRekening(AListViewConstruct.this, submenu);
                break;
            case "Transaksi":
                Menu.submenuTransaksi(AListViewConstruct.this, submenu);
                break;
            case "Donasi":
                Menu.subMenuDonasi(AListViewConstruct.this, submenu);
                break;
            case "Transfer":
                Menu.submenuTransferBsa(AListViewConstruct.this, submenu);
                break;
            case "Favorit":
                if (getStatus().equalsIgnoreCase("Agen")) {
                    Menu.submenuFavorite(AListViewConstruct.this, submenu);
                } else {
                    Menu.submenuFavoriteBsa(AListViewConstruct.this, submenu);
                }
                break;
            default:
                break;
        }
    }

//    private void goToFCekMutasi(Bundle data) {
//        Fragment fr = new FCekMutasi();
//        openFragmentSliding(fr, data);
//    }

    private void goToABukaRekening(Bundle datas) {
        Intent iBukaRekening = new Intent(this, ABukaRekening.class);
        iBukaRekening.putExtras(datas);
        startActivity(iBukaRekening);
    }

    private void goToFTarikTunai(Bundle data) {
        Fragment fr = new FTarikTunai();
        openFragmentSliding(fr, data);
    }

    private void goToFTransferRekeningCerdasBsa(Bundle data) {
        Fragment fr = new FTransferRekeningCerdasBsa();
        openFragmentSliding(fr, data);
    }

    private void goToFTransferRekeningBrisBsa(Bundle data) {
        Fragment fr = new FTransferRekeningBrisBsa();
        openFragmentSliding(fr, data);
    }

    private void goToFDonasi(Bundle data) {
        Fragment fr = new FDonasi();
        openFragmentSliding(fr, data);
    }


    private void goToFFavorit(Bundle data) {
        Fragment fr = new FFavorite();
        openFragmentSliding(fr, data);
    }

    public void openFragmentSliding(Fragment fragment, Bundle datas) {
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        fragment.setArguments(datas);
        transaction.setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit)
                .replace(R.id.container_listview_root, fragment)
                .addToBackStack(null)
                .commit();
    }

    private void customToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.tb_listview_constructor);
        TextView tvToolbar = (TextView) findViewById(R.id.tb_title_alistview_construct);
        toolbar.setNavigationIcon(R.mipmap.abc_ic_ab_back_mtrl_am_alpha);
        tvToolbar.setText(pageId);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    public String getStatus() {
        realm = Realm.getDefaultInstance();
        RealmResults<UserBrissmart> user = realm.where(UserBrissmart.class).findAll();
        status = (user.get(0).getStatus().isEmpty()) ? status.toUpperCase() : user.get(0).getStatus();
        return status;
    }

    @Override
    public void getFav(String label, String category) {

    }

    @Override
    public void onclikFav(String label, String kode_produk) {

    }

    @Override
    public void deleteFav(String jenispembayaran, String menutrx, String submenutrx, String label) {

    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {

    }

    @Override
    public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
        return false;
    }
}

