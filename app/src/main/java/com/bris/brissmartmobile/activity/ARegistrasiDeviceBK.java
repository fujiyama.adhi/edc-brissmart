package com.bris.brissmartmobile.activity;

import android.Manifest;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;

import com.bris.brissmartmobile.R;
import com.bris.brissmartmobile.model.UserDevice;
import com.bris.brissmartmobile.model.ValidateCardRequest;
import com.bris.brissmartmobile.model.ValidateCardResponse;
import com.bris.brissmartmobile.util.ApiInterface;
import com.bris.brissmartmobile.util.AppUtil;
import com.bris.brissmartmobile.util.Constants;
import com.bris.brissmartmobile.util.MasterCrypto;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import io.realm.Realm;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ryputra on 15/03/2018.
 */

public class ARegistrasiDeviceBK extends AppCompatActivity implements DialogInterface.OnKeyListener {
    private TextInputLayout tilCardNum, tilPinCard, tilPhoneNum, tilBirthDate;
    private EditText etCardNum, etPinCard, etPhoneNum, etBirthDate;
    EditText otp;
    private String user, cardNum, pinCard, phoneNum, birthDate;
    private Button btnLanjut, btnConfirm;
    private JSONObject json, resdata;
    private String responseJSON, grant_access_code, rcode = "", cmsg, msgErr;
    private String dsn, msisdn, device_id, imei;
    private Calendar myCalendar;
    private Realm realm;
    private ProgressDialog loading;
    private AlertDialog AlertDialog = null,
            confirmationDialog = null;
    private MasterCrypto masterCrypto;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registrasi_device);

        tilCardNum = (TextInputLayout) findViewById(R.id.til_param1Txt);
        etCardNum = (EditText) findViewById(R.id.param1Txt);
        tilPinCard = (TextInputLayout) findViewById(R.id.til_param2Txt);
        etPinCard = (EditText) findViewById(R.id.param2Txt);
        tilPhoneNum = (TextInputLayout) findViewById(R.id.til_param3Txt);
        etPhoneNum = (EditText) findViewById(R.id.param3Txt);
        tilBirthDate = (TextInputLayout) findViewById(R.id.til_param4Txt);
        etBirthDate = (EditText) findViewById(R.id.param4Txt);
        btnLanjut = (Button) findViewById(R.id.btn_submit);

        checkAndRequestPermissions();

        Bundle bundleDatas = getIntent().getExtras();
        user = bundleDatas.getString("user");

        if (user.equalsIgnoreCase("agen")) {
            tilPhoneNum.setVisibility(View.GONE);
            etPhoneNum.setVisibility(View.GONE);
            tilBirthDate.setVisibility(View.GONE);
            etBirthDate.setVisibility(View.GONE);
        } else {
            tilCardNum.setVisibility(View.GONE);
            etCardNum.setVisibility(View.GONE);
            tilPinCard.setVisibility(View.GONE);
            etPinCard.setVisibility(View.GONE);
        }

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        etCardNum.setFocusable(false);
        etCardNum.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                etCardNum.setFocusableInTouchMode(true);
                etCardNum.addTextChangedListener(new CardNumberFormattingTextWatcher());
                return false;
            }
        });

        etPinCard.setFocusable(false);
        etPinCard.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                etPinCard.setFocusableInTouchMode(true);
                return false;
            }
        });

        etPhoneNum.setFocusable(false);
        etPhoneNum.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                etPhoneNum.setFocusableInTouchMode(true);
                return false;
            }
        });

        etBirthDate.setFocusable(false);
        etBirthDate.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                etBirthDate.setFocusable(true);
                hideKeyboard(ARegistrasiDeviceBK.this);
                myCalendar = Calendar.getInstance();
                final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear,
                                          int dayOfMonth) {
                        myCalendar.set(Calendar.YEAR, year);
                        myCalendar.set(Calendar.MONTH, monthOfYear);
                        myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                        updateLabel();
                    }

                };

                etBirthDate.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // TODO Auto-generated method stub
                        new DatePickerDialog(ARegistrasiDeviceBK.this, date, myCalendar
                                .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                                myCalendar.get(Calendar.DAY_OF_MONTH)).show();
                    }
                });
                return false;
            }
        });

        realm = Realm.getDefaultInstance();

        HashMap<String, String> deviceInfo = AppUtil.getDeviceInfo(ARegistrasiDeviceBK.this);
        device_id = deviceInfo.get(Constants.DeviceInfo.DEVICE_ID);
        imei = deviceInfo.get(Constants.DeviceInfo.IMEI);

        main();
    }

    private boolean checkAndRequestPermissions() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && checkSelfPermission(Manifest.permission.READ_SMS) != PackageManager.PERMISSION_GRANTED) {
            int receiveSMS = ContextCompat.checkSelfPermission(this, Manifest.permission.RECEIVE_SMS);
            int readSMS = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_SMS);
            List<String> listPermissionsNeeded = new ArrayList<>();
            if (receiveSMS != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(Manifest.permission.RECEIVE_SMS);
            }
            if (readSMS != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(Manifest.permission.READ_SMS);
            }
            if (!listPermissionsNeeded.isEmpty()) {
                ActivityCompat.requestPermissions(this,
                        listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), 1);
                return false;
            }
            return true;
        }
        return true;

    }

    private void updateLabel() {
        String myFormat = "yyyy-MM-dd"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        etBirthDate.setText(sdf.format(myCalendar.getTime()));
    }

    public static class CardNumberFormattingTextWatcher implements TextWatcher {

        private static final char space = '-';

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void afterTextChanged(Editable s) {
            // Remove all spacing char
            int pos = 0;
            while (true) {
                if (pos >= s.length()) break;
                if (space == s.charAt(pos) && (((pos + 1) % 5) != 0 || pos + 1 == s.length())) {
                    s.delete(pos, pos + 1);
                } else {
                    pos++;
                }
            }

            // Insert char where needed.
            pos = 4;
            while (true) {
                if (pos >= s.length()) break;
                final char c = s.charAt(pos);
                // Only if its a digit where there should be a space we insert a space
                if ("0123456789".indexOf(c) >= 0) {
                    s.insert(pos, "" + space);
                }
                pos += 5;
            }
        }
    }

    private void main() {
        btnLanjut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (user.equalsIgnoreCase("agen")) {
                    if (etCardNum.getText().toString().length() == 0) {
                        tilCardNum.setError("Nomor Kartu ATM Tidak Boleh Kosong!");
                        return;
                    } else {
                        cardNum = etCardNum.getText().toString();
                        cardNum = cardNum.replace("-", "");
                    }

                    if (etPinCard.getText().toString().length() == 0) {
                        tilPinCard.setError("PIN Kartu ATM Tidak Boleh Kosong!");
                        return;
                    } else {
                        pinCard = etPinCard.getText().toString();
                    }
                } else {
                    if (etPhoneNum.getText().toString().length() == 0) {
                        tilPhoneNum.setError("Nomor Handphone Tidak Boleh Kosong!");
                        return;
                    } else {
                        phoneNum = etPhoneNum.getText().toString();
                    }

                    if (etBirthDate.getText().toString().length() == 0) {
                        tilBirthDate.setError("Tanggal Lahir Tidak Boleh Kosong!");
                        return;
                    } else {
                        birthDate = etBirthDate.getText().toString();
                    }
                }

                loading = ProgressDialog.show(ARegistrasiDeviceBK.this,
                        "",
                        "Memproses . . .", false, false);

                loading.show();

                if (user.equalsIgnoreCase("agen")) {
                    String cardNumEncrypt = null, cardPinEncrypt = null;

                    try {
                        masterCrypto = new MasterCrypto();
                        cardNumEncrypt = masterCrypto.encrypt(cardNum.trim());
                        cardPinEncrypt = masterCrypto.encrypt(pinCard.trim());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    ValidateCardRequest validateCardRequest;
                    validateCardRequest = new ValidateCardRequest(cardNumEncrypt, cardPinEncrypt);

                    try {
                        ApiInterface apiInterface = AppUtil.getClientBrissmart().create(ApiInterface.class);
                        Call<ValidateCardResponse> call = apiInterface.validateCard(validateCardRequest);
                        call.enqueue(new Callback<ValidateCardResponse>() {
                            @Override
                            public void onResponse(Call<ValidateCardResponse> call, Response<ValidateCardResponse> response) {
                                if (response.isSuccessful()) {
                                    if (response.body().getStatus().equalsIgnoreCase("00")) {
                                        msisdn = response.body().getMsisdn().toString();
                                        Log.d("Cek msisdn", msisdn);

                                        SoapActivity action = new SoapActivity();
                                        action.execute();

                                    } else {
                                        loading.dismiss();
                                        AppUtil.displayDialog(ARegistrasiDeviceBK.this, response.body().getMessage());
                                    }
                                } else {
                                    loading.dismiss();
                                    AppUtil.displayDialog(ARegistrasiDeviceBK.this, "Gagal terhubung ke server");
                                }
                            }

                            @Override
                            public void onFailure(Call<ValidateCardResponse> call, Throwable t) {
                                loading.dismiss();
                                AppUtil.displayDialog(ARegistrasiDeviceBK.this, "Gagal terhubung ke server");
                                Log.d("Failure", t.toString());
                            }
                        });
                    } catch (Exception e) {
                        loading.dismiss();
                        AppUtil.displayDialog(ARegistrasiDeviceBK.this, "Gagal terhubung ke server");
                        Log.e("TESST catch", e.toString());
                    }
                } else {
                    SoapActivity action = new SoapActivity();
                    action.execute();
                }


            }
        });
    }

    private class SoapActivity extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
        }

        @Override
        protected Void doInBackground(Void... params) {
            convertir();
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            if (msgErr.equalsIgnoreCase("true")) {
                AppUtil.displayDialog(ARegistrasiDeviceBK.this, responseJSON);
            }
        }
    }

    private void convertir() {
        try {
            String cmd;
            JSONObject cmdparam = new JSONObject();
            if (user.equalsIgnoreCase("agen")) {
                cmdparam.put("vdsn", msisdn);
                dsn = msisdn;
                cmd = "checkagendsn_activation";
            } else {
                cmdparam.put("vdsn", phoneNum);
                cmdparam.put("birthdate", birthDate);
                dsn = phoneNum;
                cmd = "checkbsadsn_activation";
            }
            cmdparam.put("device_id", device_id);
            cmdparam.put("imei", imei);

            String soapRes = AppUtil.soapReq(dsn, cmd, cmdparam);

            if (soapRes.equalsIgnoreCase("")) {
                loading.dismiss();
                msgErr = "true";
                responseJSON = "Gagal terhubung ke server";
            } else {
                loading.dismiss();
                json = new JSONObject(soapRes);
                resdata = json.getJSONObject("resdata");
                rcode = resdata.getString("rcode");
                cmsg = resdata.getString("cmsg");

                if (rcode.equalsIgnoreCase("00")) {
                    msgErr = "false";
                    new Thread() {
                        public void run() {
                            ARegistrasiDeviceBK.this.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    AppUtil.hideDialog();
                                    try {
                                        LayoutInflater li = LayoutInflater.from(ARegistrasiDeviceBK.this);
                                        final View confirmDialog = li.inflate(R.layout.dialog_confirm_registrasi_device, null);
                                        btnConfirm = (Button) confirmDialog.findViewById(R.id.btn_verifikasi);
                                        final TextView confirm = (TextView) confirmDialog.findViewById(R.id.txt_title_top);
                                        otp = (EditText) confirmDialog.findViewById(R.id.txt_pin);

                                        String str_star = "";
                                        for (int star = 0; star < dsn.length() - 4; star++) {
                                            if (star == 3 || star == 7)
                                                str_star = str_star + "*-";
                                            else
                                                str_star = str_star + "*";
                                        }
                                        String phone = str_star + dsn.substring(dsn.length() - 3, dsn.length());

                                        confirm.setText("Verifikasi Perangkat Dengan Kode OTP yang dikirim melalui SMS ke nomor " + phone);
                                        otp.setFocusable(false);
                                        otp.addTextChangedListener(new TextWatcher() {
                                            @Override
                                            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                                            }

                                            @Override
                                            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                                                if (charSequence.toString().trim().length() == 4) {
                                                    btnConfirm.setEnabled(true);
                                                    btnConfirm.setBackgroundResource(R.drawable.button_shape_blue);
                                                } else {
                                                    btnConfirm.setEnabled(false);
                                                    btnConfirm.setBackgroundResource(R.drawable.button_shape_accent);
                                                }
                                            }

                                            @Override
                                            public void afterTextChanged(Editable editable) {
                                            }
                                        });

                                        AlertDialog.Builder alert = new AlertDialog.Builder(ARegistrasiDeviceBK.this);
                                        alert.setView(confirmDialog);
                                        alert.setCancelable(false);
                                        confirmationDialog = alert.create();
                                        confirmationDialog.setOnKeyListener(ARegistrasiDeviceBK.this);
                                        confirmationDialog.show();

                                        btnConfirm.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View view) {

                                                AppUtil.initDialogProgress(ARegistrasiDeviceBK.this);
                                                AppUtil.showDialog();

                                                Thread thread = new Thread(new Runnable() {

                                                    @Override
                                                    public void run() {
                                                        try {
                                                            JSONObject cmdparam = new JSONObject();

                                                            cmdparam.put("vdsn", dsn);
                                                            cmdparam.put("otp", otp.getText().toString());
                                                            cmdparam.put("device_id", device_id);
                                                            cmdparam.put("imei", imei);
                                                            String soapRes = AppUtil.soapReq(dsn, "checkotp_activation", cmdparam);

                                                            if (soapRes.equalsIgnoreCase("")) {
                                                                AppUtil.hideDialog();
                                                                msgErr = "true";
                                                                responseJSON = "Gagal terhubung ke server";
                                                            } else {
                                                                json = new JSONObject(soapRes);
                                                                resdata = json.getJSONObject("resdata");
                                                                rcode = resdata.getString("rcode");
                                                                cmsg = resdata.getString("cmsg");

                                                                if (rcode.equalsIgnoreCase("00")) {
                                                                    grant_access_code = resdata.getString("grant_access_code");
                                                                    AppUtil.hideDialog();
                                                                    runOnUiThread(new Runnable() {
                                                                        public void run() {
                                                                            confirmationDialog.dismiss();
                                                                            insertUserDevice(dsn, user, device_id, imei, grant_access_code);
                                                                            Intent i = new Intent(ARegistrasiDeviceBK.this, ALoginScreen.class);
                                                                            Bundle datas = new Bundle();
                                                                            datas.putString("login", "defaultlogin");
                                                                            i.putExtras(datas);
                                                                            startActivity(i);
                                                                            finish();
                                                                        }
                                                                    });
                                                                } else {
                                                                    AppUtil.hideDialog();
                                                                    runOnUiThread(new Runnable() {
                                                                        public void run() {
                                                                            confirmationDialog.dismiss();
                                                                            AppUtil.displayDialog(ARegistrasiDeviceBK.this, cmsg);
                                                                        }
                                                                    });
                                                                }
                                                            }
                                                        } catch (Exception ex) {
                                                            msgErr = "true";
                                                            AppUtil.hideDialog();
                                                            Log.d("THREAD", ex.toString());
                                                            responseJSON = "Gagal terhubung ke server";
                                                        }
                                                    }
                                                });
                                                thread.start();
                                            }
                                        });
                                    } catch (Exception e) {
                                        Log.d("THREAD 1", e.toString());
                                        responseJSON = "Gagal terhubung ke server";
                                        msgErr = "true";
                                    }
                                }
                            });
                        }
                    }.start();
                } else {
                    msgErr = "true";
                    Log.d("else : ", "gagal");
                    responseJSON = cmsg;
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            Log.d("Gagal", ex.toString());
            responseJSON = "Gagal terhubung ke server";
            runOnUiThread(new Runnable() {
                public void run() {
                    AppUtil.displayDialog(ARegistrasiDeviceBK.this, responseJSON);
                }
            });
        }
    }

    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equalsIgnoreCase("otp")) {
                final String message = intent.getStringExtra("message");
                final String sender = intent.getStringExtra("Sender");
                otp.setText(message.replaceAll("\\D+", ""));
                Log.d("Message", sender + " : " + message);
                Log.e("OTP MESSSGE", message);
            }
        }
    };

    @Override
    protected void onResume() {
        LocalBroadcastManager.getInstance(this).registerReceiver(receiver, new IntentFilter("otp"));
        super.onResume();
    }


    @Override
    public void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(receiver);
    }

    public void insertUserDevice(String vdsn, String user, String device_id, String imei, String grant_access_code) {
        realm.beginTransaction();
        UserDevice userDevice = realm.createObject(UserDevice.class);
        userDevice.setVdsn(vdsn);
        userDevice.setUser(user);
        userDevice.setDevice_id(device_id);
        userDevice.setImei(imei);
        userDevice.setGrant_access_code(grant_access_code);
        realm.commitTransaction();
        Log.d("RESULT", "Cek User =" + userDevice.toString());
    }

    public static void hideKeyboard(Activity activity) {
        InputMethodManager inputManager = (InputMethodManager) activity
                .getSystemService(Context.INPUT_METHOD_SERVICE);

        // check if no view has focus:
        View currentFocusedView = activity.getCurrentFocus();
        if (currentFocusedView != null) {
            inputManager.hideSoftInputFromWindow(currentFocusedView.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

    @Override
    public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
            android.support.v7.app.AlertDialog.Builder builder = new AlertDialog.Builder(ARegistrasiDeviceBK.this);
            builder.setCancelable(false);
            builder.setMessage("Apakah anda yakin akan membatalkan proses aktivasi?");
            builder.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                    confirmationDialog.dismiss();
                }
            });
            builder.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                    confirmationDialog.show();
                }
            });
            if (AlertDialog == null || !AlertDialog.isShowing()) {
                AlertDialog = builder.create();
                AlertDialog.show();
            }
            return true;
        }
        return false;
    }
}
