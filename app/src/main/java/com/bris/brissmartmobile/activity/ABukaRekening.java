package com.bris.brissmartmobile.activity;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.bris.brissmartmobile.R;
import com.bris.brissmartmobile.fragment.FBukaRekeningBio;
import com.bris.brissmartmobile.util.AppUtil;
import com.bris.brissmartmobile.util.FormValidationUtil;
import com.bris.brissmartmobile.util.Menu;

import org.json.JSONObject;

public class ABukaRekening extends AppCompatActivity implements TextWatcher {

    private EditText et1, et2;
    private TextInputLayout til1, til2;
    private Spinner spinner_idtype;
    private Button btn1;
    String idtype, idtype_input;
    String param1, param2;
    String pageId;
    JSONObject json, resdata;
    String responseJSON, msgErr, rcode, ccode, cmsg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buka_rekening);

        String[] arraySpinner = new String[] {
                "KTP", "Kartu Pelajar"
        };

        et1 = (EditText) findViewById(R.id.param1Txt);
        til1 = (TextInputLayout) findViewById(R.id.til1);
        et2 = (EditText) findViewById(R.id.param2Txt);
        til2 = (TextInputLayout) findViewById(R.id.til2);

        spinner_idtype = (Spinner) findViewById(R.id.spinner_idtype);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, arraySpinner);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_idtype.setAdapter(adapter);

        et1.setFocusable(false);
        et1.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                et1.setFocusableInTouchMode(true);

                return false;
            }
        });

        et2.setFocusable(false);
        et2.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                et2.setFocusableInTouchMode(true);

                return false;
            }
        });

        btn1 = (Button) findViewById(R.id.btn_submit);

        Bundle bundleDatas = getIntent().getExtras();
        pageId = bundleDatas.getString(Menu.MENU_ID);
        AppUtil.toolbarRegular(ABukaRekening.this, pageId);

        try {
            onActionButton();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void onActionButton() {
        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (et1.getText().toString().length() == 0) {
                    til1.setError("Nama Tidak Boleh Kosong!");
                    return;
                } else {
                    param1 = et1.getText().toString();
                }

                if (et2.getText().toString().length() == 0) {
                    til2.setError("Nomor Identitas Tidak Boleh Kosong!");
                    return;
                } else {
                    param2 = et2.getText().toString();
                }

                if (spinner_idtype.getSelectedItem() == null) {
                    FormValidationUtil.displayCustomDialog(ABukaRekening.this,
                            getString(R.string.dialog_msg_jenis_id),
                            getString(R.string.dialog_header_txt_alert));
                    return;
                } else {
                    idtype_input = spinner_idtype.getSelectedItem().toString();
                }

                if (idtype_input.equalsIgnoreCase("KTP")) {
                    idtype = "01";
                } else {
                    idtype = "02";
                }

                SoapActivity action = new SoapActivity();
                action.execute();

            }
        });
    }

    private void convertir() {

        try {
            runOnUiThread(new Runnable() {
                public void run() {
                    AppUtil.initDialogProgress(ABukaRekening.this);
                    AppUtil.showDialog();
                }
            });

            JSONObject cmdparam = new JSONObject();
            cmdparam.put("idtype", idtype);
            cmdparam.put("idcard", param2);
            String soapRes = AppUtil.soapReq(AppUtil.getDsn(), "custid_check", cmdparam);

            if (soapRes.equalsIgnoreCase("")) {
                AppUtil.hideDialog();
                msgErr = "true";
                responseJSON = "Gagal terhubung ke server";
            } else {
                json = new JSONObject(soapRes);
                resdata = json.getJSONObject("resdata");
                rcode = resdata.getString("rcode");
                ccode = resdata.getString("ccode");
                cmsg = resdata.getString("cmsg");
                msgErr = "false";

                if (rcode.equalsIgnoreCase("00") && ccode.equalsIgnoreCase("00")) {
                    AppUtil.hideDialog();
                    Fragment fr = new FBukaRekeningBio();
                    Bundle datas = new Bundle();
                    datas.putString("idtype", idtype);
                    datas.putString("idcard", param2);
                    datas.putString("name", param1);
                    datas.putString(Menu.MENU_ID, pageId);
                    fr.setArguments(datas);
                    openFragmentSliding(fr);
                } else {
                    Log.d("else", "gagal");
                    AppUtil.hideDialog();
                    cmsg = resdata.getString("cmsg");
                    responseJSON = cmsg;
                    msgErr = "true";
                }
            }
        } catch (Exception ex) {
            AppUtil.hideDialog();
            msgErr = "true";
            Log.d("THREAD 2", ex.toString());
            responseJSON = "Transaksi Gagal";
        }
    }

    public void openFragmentSliding(Fragment fragment) {
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit)
                .replace(R.id.framelayout, fragment)
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {

    }

    private class SoapActivity extends AsyncTask<Void, Void, Void> {


        @Override
        protected void onPreExecute() {

        }

        @Override
        protected Void doInBackground(Void... params) {
            convertir();
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            if (msgErr.equalsIgnoreCase("true")) {
                AppUtil.displayDialog(ABukaRekening.this, responseJSON);
            }
        }
    }
}
