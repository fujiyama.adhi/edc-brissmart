package com.bris.brissmartmobile.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bris.brissmartmobile.R;
import com.bris.brissmartmobile.listener.MenuClickListener;
import com.bris.brissmartmobile.util.AppUtil;
import com.bris.brissmartmobile.util.ListMenu;

import java.util.List;

/**
 * Created by suminiwl on 6/22/17.
 */

public class MenuHomeBottomAdapterAgen extends BaseAdapter {
    private LayoutInflater layoutinflater;
    private List<ListMenu> listStorage;
    private Context context;
    private MenuClickListener mMenuClickListener;

    public MenuHomeBottomAdapterAgen(Context context, List<ListMenu> customizedListView, MenuClickListener menuClickListener) {
        this.context = context;
        this.layoutinflater =(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.listStorage = customizedListView;
        this.mMenuClickListener = menuClickListener;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder listViewHolder;
        if(convertView == null){
            listViewHolder = new ViewHolder();
            convertView = layoutinflater.inflate(R.layout.test_content_fragment_home_bottom_agen, parent, false);
            // auto height
           /* int height = parent.getMeasuredHeight() / 4;
            convertView.setMinimumHeight(height);*/
            AppUtil.customMainFonts(context.getAssets(), convertView);  // Custom Fonts
            listViewHolder.icon = (ImageView)convertView.findViewById(R.id.icon);
            listViewHolder.iconName = (TextView) convertView.findViewById(R.id.icon_name);
            convertView.setTag(listViewHolder);
            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    mMenuClickListener.onMenuClick(listViewHolder.iconName.getText().toString());

                }
            });
        } else {
            listViewHolder = (ViewHolder)convertView.getTag();
        }
        listViewHolder.icon.setImageResource(listStorage.get(position).getIcon());
        listViewHolder.iconName.setText(listStorage.get(position).getTitle());
        return convertView;
    }

    private static class ViewHolder{
        ImageView icon;
        TextView iconName;
    }

    @Override
    public int getCount() {
        return listStorage.size();
    }
    @Override
    public Object getItem(int position) {
        return position;
    }
    @Override
    public long getItemId(int position) {
        return position;
    }
}

