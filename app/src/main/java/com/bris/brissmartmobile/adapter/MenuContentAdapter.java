package com.bris.brissmartmobile.adapter;

import android.content.Context;
import android.support.constraint.ConstraintLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bris.brissmartmobile.R;
import com.bris.brissmartmobile.listener.MenuClickListener;
import com.bris.brissmartmobile.util.ListMenu;

import java.util.List;

/**
 * Created by valeputra on 7/4/17.
 */

public class MenuContentAdapter extends BaseAdapter {
    private LayoutInflater layoutinflater;
    private List<ListMenu> listStorage;
    private Context context;
    private MenuClickListener mMenuClickListener;

    public MenuContentAdapter(Context context, List<ListMenu> customizedListView, MenuClickListener menuClickListener) {
        this.context = context;
        this.layoutinflater =(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.listStorage = customizedListView;
        this.mMenuClickListener = menuClickListener;
    }

    @Override
    public int getCount() {
        return listStorage.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final MenuContentAdapter.ViewHolder listViewHolder;
        if (convertView == null) {
            listViewHolder = new ViewHolder();
            convertView = layoutinflater.inflate(R.layout.content_fragment_konten, parent, false);
            listViewHolder.mIcon = (ImageView) convertView.findViewById(R.id.iv_icon_submenu);
            listViewHolder.mTitle = (TextView) convertView.findViewById(R.id.tv_title_menu);
            listViewHolder.mIconNext = (ImageView) convertView.findViewById(R.id.iv_icon_next_submenu);
            listViewHolder.mLayout = (ConstraintLayout) convertView.findViewById(R.id.item_layout_konten);

            convertView.setTag(listViewHolder);
            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mMenuClickListener.onMenuClick(listViewHolder.mTitle.getText().toString());
                }
            });

        }else{
            listViewHolder = (MenuContentAdapter.ViewHolder)convertView.getTag();
        }

        listViewHolder.mIcon.setImageResource(listStorage.get(position).getIcon());
        listViewHolder.mTitle.setText(listStorage.get(position).getTitle());
        listViewHolder.mIconNext.setImageResource(listStorage.get(position).getIcon_next());

        return convertView;
    }

    static class ViewHolder{
        ImageView mIcon;
        TextView mTitle;
        ImageView mIconNext;
        ConstraintLayout mLayout;
    }
}
