package com.bris.brissmartmobile.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bris.brissmartmobile.R;
import com.bris.brissmartmobile.util.AppUtil;
import com.bris.brissmartmobile.util.ListViewMenu;

import java.util.List;

/**
 * Created by valeputra on 6/22/17.
 */

public class ListViewAdapter extends BaseAdapter {
    private LayoutInflater layoutinflaters;
    private List<ListViewMenu> listStorage;
    private Context context;

    @Override
    public int getCount() {
        return listStorage.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    public ListViewAdapter(Context context, List<ListViewMenu> customizedListView) {
        this.context = context;
        layoutinflaters =(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        listStorage = customizedListView;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ListViewAdapter.ViewHolder listViewHolder;
        if(convertView == null){
            listViewHolder = new ListViewAdapter.ViewHolder();
            convertView = layoutinflaters.inflate(R.layout.content_alistview_construct, parent, false);
            AppUtil.customMainFonts(context.getAssets(), convertView);  // custom fonts

            listViewHolder.mIcon = (ImageView) convertView.findViewById(R.id.iv_icon_submenu);
            listViewHolder.mTitle = (TextView)convertView.findViewById(R.id.tv_title_submenu);
            listViewHolder.mIconNext = (ImageView)convertView.findViewById(R.id.iv_icon_next_submenu);
            convertView.setTag(listViewHolder);
        } else {
            listViewHolder = (ListViewAdapter.ViewHolder)convertView.getTag();
        }

        listViewHolder.mIcon.setImageResource(listStorage.get(position).getIcon());
        listViewHolder.mTitle.setText(listStorage.get(position).getTitle());
        listViewHolder.mIconNext.setImageResource(listStorage.get(position).getIcon_next());
        return convertView;
    }

    static class ViewHolder{
        ImageView mIcon;
        TextView mTitle;
        ImageView mIconNext;
    }
}

