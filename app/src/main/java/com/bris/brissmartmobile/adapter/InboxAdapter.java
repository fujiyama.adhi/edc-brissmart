package com.bris.brissmartmobile.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.bris.brissmartmobile.R;
import com.bris.brissmartmobile.listener.InboxListener;
import com.bris.brissmartmobile.model.History;
import com.bris.brissmartmobile.model.UserBrissmart;

import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;

public class InboxAdapter extends RecyclerView.Adapter<InboxAdapter.MenuViewHolder> {

    private List<History> inboxList;
    private Context context;
    InboxListener menuClickListener;
    private SparseBooleanArray mSelectedItemsIds;
    private Realm realm;
    private String status;

    public InboxAdapter(List<History> inboxList, Context context, InboxListener menuClickListener)
    {
        this.inboxList = inboxList;
        this.context = context;
        this.menuClickListener = menuClickListener;
        mSelectedItemsIds = new SparseBooleanArray();
    }

    @Override
    public InboxAdapter.MenuViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_inbox, parent, false);
        return new InboxAdapter.MenuViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(InboxAdapter.MenuViewHolder holder, int position) {
        History inbox = inboxList.get(position);

        String message = inbox.getMessage().replace("\n"," ");
        if(message.length() > 35){
            message = message.substring(0,35);
            message = message + "...";
        }
        holder.tv_date.setText(inbox.getDatedisplay());
        holder.tv_text.setText(message);

        holder.itemView
                .setBackgroundColor(mSelectedItemsIds.get(position) ? 0x9934B5E4
                        : Color.TRANSPARENT);

        buildItemIcon(inbox.getMenuId(), holder);

        if(inbox.getStatusread().equals("0")){
            holder.tv_date.setTypeface(Typeface.DEFAULT_BOLD);
            holder.tv_text.setTypeface(Typeface.DEFAULT_BOLD);
        }else{
//            holder.ic_read.setBackgroundResource(R.drawable.custom_icon_bg_grey);
            holder.tv_date.setTypeface(Typeface.DEFAULT);
            holder.tv_text.setTypeface(Typeface.DEFAULT);
            holder.tv_date.setTextColor(ContextCompat.getColor(context, R.color.font_body));
            holder.tv_text.setTextColor(ContextCompat.getColor(context, R.color.font_body));
        }
    }

    @Override
    public int getItemCount() {
        return inboxList.size();
    }

    private void buildItemIcon(String menuId, MenuViewHolder holder) {
        if (menuId != null) {
            setIconImageAndBackgroundResource(menuId, holder);
        } else {
            String message = holder.tv_text.getText().toString();
            String pageTitle = message.substring(0, message.indexOf(":"));
            if (pageTitle.contains("Pembayaran")) {
                pageTitle = pageTitle.substring(pageTitle.indexOf("Pembayaran "), pageTitle.length()).trim();
            }
            setIconImageAndBackgroundResource(pageTitle, holder);
        }
    }

    private void setIconImageAndBackgroundResource(String menuId, MenuViewHolder holder) {
            if (getStatus().equalsIgnoreCase("Agen")) {
                if (menuId.equalsIgnoreCase(context.getString(R.string.submenu_title_cek_saldo)) ||
                        menuId.equalsIgnoreCase(context.getString(R.string.submenu_title_cek_mutasi))) {
                    holder.ic_read.setImageResource(R.drawable.ico_account_info);
                    holder.ic_read.setBackgroundResource(R.drawable.custom_icon_bg_blue);
                } else if (menuId.equalsIgnoreCase(context.getString(R.string.submenu_title_setor_tunai)
                        + " " + context.getString(R.string.submenu_title_rekening_cerdas)) ||
                        menuId.equalsIgnoreCase(context.getString(R.string.submenu_title_setor_tunai)
                                + " " + context.getString(R.string.submenu_title_rekening_bris)) ||
                        menuId.equalsIgnoreCase(context.getString(R.string.submenu_title_setor_tunai)
                                + " " + context.getString(R.string.submenu_title_rekening_bank_lain)) ||
                        menuId.equalsIgnoreCase(context.getString(R.string.submenu_title_tarik_tunai))) {
                    holder.ic_read.setImageResource(R.drawable.ico_saldo);
                    holder.ic_read.setBackgroundResource(R.drawable.custom_icon_bg_green);
                } else if (menuId.equalsIgnoreCase(context.getString(R.string.submenu_title_rekening_cerdas)) ||
                        menuId.equalsIgnoreCase(context.getString(R.string.submenu_title_rekening_bris)) ||
                        menuId.equalsIgnoreCase(context.getString(R.string.submenu_title_rekening_bank_lain))) {
                    holder.ic_read.setImageResource(R.drawable.ico_transfer);
                    holder.ic_read.setBackgroundResource(R.drawable.custom_icon_bg_green);
                } else if (menuId.equalsIgnoreCase(context.getString(R.string.submenu_title_tagihan_pln)) ||
                        menuId.equalsIgnoreCase(context.getString(R.string.submenu_title_telepon)) ||
                        menuId.equalsIgnoreCase(context.getString(R.string.submenu_title_internet)) ||
                        menuId.equalsIgnoreCase(context.getString(R.string.submenu_title_tv_berbayar)) ||
                        menuId.equalsIgnoreCase(context.getString(R.string.submenu_title_tiket_kai)) ||
                        menuId.equalsIgnoreCase(context.getString(R.string.submenu_title_tokopedia)) ||
                        menuId.equalsIgnoreCase(context.getString(R.string.submenu_title_institusi)) ||
                        menuId.equalsIgnoreCase(context.getString(R.string.submenu_title_pendidikan)) ||
                        menuId.equalsIgnoreCase(context.getString(R.string.submenu_title_asuransi))) {
                    holder.ic_read.setImageResource(R.drawable.ico_payment);
                    holder.ic_read.setBackgroundResource(R.drawable.custom_icon_bg_green);
                } else if (menuId.equalsIgnoreCase(context.getString(R.string.submenu_title_token_listrik)) ||
                        menuId.equalsIgnoreCase(context.getString(R.string.submenu_title_pulsa)) ||
                        menuId.equalsIgnoreCase(context.getString(R.string.submenu_title_paket_data)) ||
                        menuId.equalsIgnoreCase(context.getString(R.string.submenu_title_gojek))) {
                    holder.ic_read.setImageResource(R.drawable.ico_purchase);
                    holder.ic_read.setBackgroundResource(R.drawable.custom_icon_bg_red);
                } else {
                    holder.ic_read.setImageResource(R.drawable.ico_donasi);
                    holder.ic_read.setBackgroundResource(R.drawable.custom_icon_bg_yellow);
                }
            } else {
                if (menuId.equalsIgnoreCase(context.getString(R.string.submenu_title_cek_saldo))) {
                    holder.ic_read.setImageResource(R.drawable.ico_saldo);
                    holder.ic_read.setBackgroundResource(R.drawable.custom_icon_bg_blue);
                } else if (menuId.equalsIgnoreCase(context.getString(R.string.submenu_title_cek_mutasi))) {
                    holder.ic_read.setImageResource(R.drawable.ico_account_info);
                    holder.ic_read.setBackgroundResource(R.drawable.custom_icon_bg_green);
                } else {
                    holder.ic_read.setImageResource(R.drawable.ico_transfer);
                    holder.ic_read.setBackgroundResource(R.drawable.custom_icon_bg_orange);
                }
            }
    }

    public class MenuViewHolder extends RecyclerView.ViewHolder
            implements View.OnClickListener{

        public ImageView ic_read;
        public TextView tv_date;
        public TextView tv_text;
        public CheckBox checkBox;

        public MenuViewHolder(View itemView) {
            super(itemView);

            this.ic_read = (ImageView) itemView.findViewById(R.id.iv_icon_read_unread);
            this.tv_date = (TextView) itemView.findViewById(R.id.tv_text_date);
            this.tv_text = (TextView) itemView.findViewById(R.id.tv_text_inbox);
            this.checkBox = (CheckBox) itemView.findViewById(R.id.chkSelected);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            int position = getAdapterPosition();
            Log.d("adapter pos", inboxList.get(position).getTrxdate()+"");
            menuClickListener.onItemClick(inboxList.get(position).getTrxdate());
        }
    }

    public List<History> getInboxList(){
        return this.inboxList;
    }

    //Toggle selection methods
    public void toggleSelection(int position) {
        selectView(position, !mSelectedItemsIds.get(position));
    }

    //Put or delete selected position into SparseBooleanArray
    public void selectView(int position, boolean value) {
        if (value)
            mSelectedItemsIds.put(position, value);
        else
            mSelectedItemsIds.delete(position);

        notifyDataSetChanged();
    }

    //Get total selected count
    public int getSelectedCount() {
        return mSelectedItemsIds.size();
    }

    //Return all selected ids
    public SparseBooleanArray getSelectedIds() {
        return mSelectedItemsIds;
    }

    //Remove selected selections
    public void removeSelection() {
        mSelectedItemsIds = new SparseBooleanArray();
        notifyDataSetChanged();
    }

    public String getStatus() {
        realm = Realm.getDefaultInstance();
        RealmResults<UserBrissmart> user = realm.where(UserBrissmart.class).findAll();
        status = (user.get(0).getStatus().isEmpty()) ? status.toUpperCase() : user.get(0).getStatus();
        return status;
    }

}
