package com.bris.brissmartmobile.adapter;

import android.content.Context;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.bris.brissmartmobile.R;

import java.text.NumberFormat;
import java.util.Locale;

/**
 * Created by ryputra on 04/01/2018.
 */

public class ItemMutationAdapter extends RecyclerView.Adapter<ItemMutationAdapter.ViewHolder> {

    private String[] idate;
    private String[] isign;
    private String[] iamount;
    private Context mContext;

    public ItemMutationAdapter(String[] idate, String[] isign, String[] iamount, Context context) {
        this.idate = idate;
        this.isign = isign;
        this.iamount = iamount;
        this.mContext = context;
    }

    @Override
    public ItemMutationAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.rv_item_mutation, parent, false);
        return new ItemMutationAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ItemMutationAdapter.ViewHolder holder, final int position) {
//        final String[] val = datas;

//        Log.d("TEST BROO", String.valueOf(datas));

        if (position%2 == 0)
            holder.layoutBar.setBackgroundColor(ContextCompat.getColor(mContext, R.color.colorBackgroundSoftGrey));
        else
            holder.layoutBar.setBackgroundColor(ContextCompat.getColor(mContext, R.color.white));

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
//                Animation anim = AnimationUtils.loadAnimation(mContext, R.anim.fadeinup);
//                anim.setInterpolator((new AccelerateDecelerateInterpolator()));
//                anim.setFillAfter(true);
//                holder.layoutBar.startAnimation(anim);

//                for (int i = 0; i < datas.length; i++){
                    holder.tvDate.setText(idate[position]);
//                }
                int amount = Integer.parseInt(iamount[position]);
                String iamount_txt = NumberFormat.getNumberInstance(Locale.US).format(amount);
                String iamount = iamount_txt.replace(",",".");

                if (isign[position].equals("C")) {
                    holder.tvNominal.setText(iamount);
                    holder.tvTag.setText("KREDIT");
                    holder.tvTag.setBackgroundResource(R.drawable.tag_green);
                    holder.tvNominal.setTextColor(ContextCompat.getColor(mContext, R.color.greenBody));
                } else {
                    holder.tvNominal.setText("-"+iamount);
                    holder.tvNominal.setTextColor(ContextCompat.getColor(mContext, R.color.redBody));
                    holder.tvNominal.setPadding(0,0,80,0);
                    holder.tvTag.setText("DEBIT");
                    holder.tvTag.setTextColor(ContextCompat.getColor(mContext, R.color.redBody));
                    holder.tvTag.setBackgroundResource(R.drawable.tag_red);
                }
            }
        }, 600);
    }

    @Override
    public int getItemCount() {
        return idate.length;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tvDate;
        public TextView tvNominal;
        public TextView tvTag;
        public LinearLayout layoutBar;

        public ViewHolder(View itemView) {
            super(itemView);
            this.tvDate = (TextView) itemView.findViewById(R.id.tv_date);
            this.tvNominal = (TextView) itemView.findViewById(R.id.tv_nominal);
            this.tvTag = (TextView) itemView.findViewById(R.id.tv_tag);
            this.layoutBar = (LinearLayout) itemView.findViewById(R.id.item_mutation);
        }
    }
}
