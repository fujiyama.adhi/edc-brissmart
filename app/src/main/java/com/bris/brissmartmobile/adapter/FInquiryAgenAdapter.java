package com.bris.brissmartmobile.adapter;

import android.content.Context;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bris.brissmartmobile.R;

/**
 * Created by ryputra on 04/01/2018.
 */

public class FInquiryAgenAdapter extends RecyclerView.Adapter<FInquiryAgenAdapter.ViewHolder> {

    private String[] no;
    private String[] nama;
    private String[] alamat;
    private Context mContext;

    public FInquiryAgenAdapter(String[] no, String[] nama, String[] alamat, Context context) {
        this.no = no;
        this.nama = nama;
        this.alamat = alamat;
        this.mContext = context;
    }

    @Override
    public FInquiryAgenAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_inquiry_agen, parent, false);
        return new FInquiryAgenAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final FInquiryAgenAdapter.ViewHolder holder, final int position) {
        if (position%2 == 0)
            holder.layoutBar.setBackgroundColor(ContextCompat.getColor(mContext, R.color.white));
        else
            holder.layoutBar.setBackgroundColor(ContextCompat.getColor(mContext, R.color.colorBackgroundSoftGrey));

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                holder.tvNo.setText(no[position]);
                holder.tvNama.setText(nama[position]);
                holder.tvAlamat.setText(alamat[position]);
            }
        }, 600);
    }

    @Override
    public int getItemCount() {
        return nama.length;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tvNo;
        public TextView tvNama;
        public TextView tvAlamat;
        public LinearLayout layoutBar;

        public ViewHolder(View itemView) {
            super(itemView);
            this.tvNo = (TextView) itemView.findViewById(R.id.tv_no_agen);
            this.tvNama = (TextView) itemView.findViewById(R.id.tv_nama_agen);
            this.tvAlamat = (TextView) itemView.findViewById(R.id.tv_alamat_agen);
            this.layoutBar = (LinearLayout) itemView.findViewById(R.id.item_inquiry_agen);
        }
    }
}
