package com.bris.brissmartmobile.adapter;

/**
 * Created by valeputra on 7/4/17.
 */

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.bris.brissmartmobile.R;
import com.bris.brissmartmobile.activity.APromo;
import com.bris.brissmartmobile.activity.APromoDetail;
import com.bris.brissmartmobile.model.Promo;
import com.squareup.picasso.Picasso;

import java.util.List;

public class PromoAdapter extends RecyclerView.Adapter<PromoAdapter.ViewHolder> {

    private APromo activity;
    private List<Promo> mPromo;
    private View view;

    public PromoAdapter(APromo mainactivity, List<Promo> mPromo) {
        this.mPromo = mPromo;
        this.activity = mainactivity;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.content_apromo, parent, false);
        ViewHolder vh = new ViewHolder(view);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Promo item = mPromo.get(position);
        holder.bindPromo(item);
    }

    @Override
    public int getItemCount() {
        return mPromo.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView tvTitle;
        private ImageView ivPromo;
        private Promo mPromo;

        public ViewHolder(View itemView) {
            super(itemView);
            tvTitle = (TextView) itemView.findViewById(R.id.tv_promo_title);
            ivPromo = (ImageView) itemView.findViewById(R.id.iv_promo);
            itemView.setOnClickListener(this);
        }


        public void click(final Promo quesModel, final OnItemClickListener listener) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onClick(quesModel);
                }
            });
        }

        public void bindPromo(Promo item) {
            mPromo = item;
            Picasso.with(ivPromo.getContext())
                    .load(item.getPromo_img_url())
                    .placeholder(R.drawable.banner_placeholder)
                    .into(ivPromo);
            tvTitle.setText(item.getPromo_title());
        }

        @Override
        public void onClick(View v) {
            Context context = itemView.getContext();
            Intent intentPromoDetail = new Intent(context, APromoDetail.class);
            intentPromoDetail.putExtra("promo_title", mPromo.getPromo_title());
            intentPromoDetail.putExtra("promo_imgUrl", mPromo.getPromo_img_url());
            intentPromoDetail.putExtra("promo_detail", mPromo.getPromo_detail());
            intentPromoDetail.putExtra("promo_create_at", mPromo.getPromo_create_at());
            intentPromoDetail.putExtra("promo_due", mPromo.getPromo_valid_until());
            intentPromoDetail.putExtra("category", "");
            context.startActivity(intentPromoDetail);
        }
    }
    public interface OnItemClickListener {
        void onClick(Promo Promo);
    }
}