package com.bris.brissmartmobile.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bris.brissmartmobile.R;
import com.bris.brissmartmobile.listener.MenuClickListener;
import com.bris.brissmartmobile.util.AppUtil;
import com.bris.brissmartmobile.util.ListMenuBackground;

import java.util.List;

/**
 * Created by suminiwl on 30/09/2017.
 */

public class MenuHomeTopAdapterAgen extends BaseAdapter {
    private LayoutInflater layoutinflater;
    private List<ListMenuBackground> listStorage;
    private Context context;
    private MenuClickListener mMenuClickListener;

    public MenuHomeTopAdapterAgen(Context context, List<ListMenuBackground> customizedListView, MenuClickListener menuClickListener) {
        this.context = context;
        this.layoutinflater =(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.listStorage = customizedListView;
        this.mMenuClickListener = menuClickListener;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final MenuHomeTopAdapterAgen.ViewHolder listViewHolder;
        if(convertView == null){
            listViewHolder = new MenuHomeTopAdapterAgen.ViewHolder();
            convertView = layoutinflater.inflate(R.layout.test_content_fragment_home_agen, parent, false);
            AppUtil.customMainFonts(context.getAssets(), convertView);  // Custom Fonts
            listViewHolder.icon = (ImageView)convertView.findViewById(R.id.icon);
            listViewHolder.iconName = (TextView) convertView.findViewById(R.id.icon_name);
            listViewHolder.imgBackground = (ImageView) convertView.findViewById(R.id.img_background_menu);
            listViewHolder.imgColorOverlay = (RelativeLayout) convertView.findViewById(R.id.img_background_overlay);
            convertView.setTag(listViewHolder);
            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mMenuClickListener.onMenuClick(listViewHolder.iconName.getText().toString());
                }
            });
        } else {
            listViewHolder = (MenuHomeTopAdapterAgen.ViewHolder)convertView.getTag();
        }

        listViewHolder.icon.setImageResource(listStorage.get(position).getIcon());
        if (position == 0)
            listViewHolder.icon.setBackgroundResource(R.drawable.custom_icon_bg_blue);
        else if (position == 1)
            listViewHolder.icon.setBackgroundResource(R.drawable.custom_icon_bg_green);
        else if (position == 2)
            listViewHolder.icon.setBackgroundResource(R.drawable.custom_icon_bg_orange);
        else
            listViewHolder.icon.setBackgroundResource(R.drawable.custom_icon_bg_red);

        listViewHolder.iconName.setText(listStorage.get(position).getTitle());

        return convertView;
    }

    private static class ViewHolder {
        ImageView icon;
        TextView iconName;
        ImageView imgBackground;
        RelativeLayout imgColorOverlay;
    }

    @Override
    public int getCount() {
        return listStorage.size();
    }
    @Override
    public Object getItem(int position) {
        return position;
    }
    @Override
    public long getItemId(int position) {
        return position;
    }
}
