package com.bris.brissmartmobile.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bris.brissmartmobile.R;
import com.bris.brissmartmobile.model.BankingPromoResponse;
import com.bris.brissmartmobile.promo.PromoDetailActivity;
import com.squareup.picasso.Picasso;

import java.util.List;


/**
 * Created by valeputra on 7/27/17.
 */

public class HeadlineAdapter extends PagerAdapter {
    private Context context;
    private LayoutInflater layoutInflater;
    private List<BankingPromoResponse.DataPromo> listStorage;

    private int PAGES;
    private int LOOPS = 1000;
    public int FIRST_PAGE = PAGES * LOOPS / 2;

    public HeadlineAdapter(Context context, List<BankingPromoResponse.DataPromo> headlinePromo) {
        this.context = context;
        this.listStorage = headlinePromo;
        this.PAGES = headlinePromo.size();
    }

    @Override
    public Object instantiateItem(final ViewGroup container, final int position) {
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.item_headline, container, false);

        ImageView ivHeadline = (ImageView) view.findViewById(R.id.iv_swipe_headline);

        Picasso.with(context)
                .load(listStorage.get(position).getImgUrl())
                .placeholder(R.drawable.banner_placeholder)
                .into(ivHeadline);
        ((ViewPager) container).addView(view);

        ivHeadline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), PromoDetailActivity.class);

                Bundle bundle = new Bundle();
                bundle.putString("category", "produk");
                bundle.putString("promo_title", listStorage.get(position).getTitle());
                bundle.putString("promo_imgUrl", listStorage.get(position).getImgUrl());
                bundle.putString("promo_detail", listStorage.get(position).getDetail());
                intent.putExtras(bundle);
                view.getContext().startActivity(intent);
            }
        });

        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        ((ViewPager) container).removeView((ConstraintLayout) object);
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return (view == object);
    }

    @Override
    public int getCount() {
        return listStorage.size();
//        return PAGES * LOOPS;
    }

    @Override
    public float getPageWidth(int position) {
//        return super.getPageWidth(position);
        return 1f;
    }
}
