package com.bris.brissmartmobile.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bris.brissmartmobile.R;
import com.bris.brissmartmobile.listener.FavClickListener;
import com.bris.brissmartmobile.model.Favorite;

import java.util.List;

/**
 * Created by akbaranjas on 10/04/17.
 */

public class FFavoriteAdapter extends RecyclerView.Adapter<FFavoriteAdapter.MenuViewHolder> {

    private List<Favorite> favdata;
    private Context context;
    private String menu;
    FavClickListener favClickListener;

    public FFavoriteAdapter(List<Favorite> favdata, Context context, FavClickListener favClickListener, String menu) {
        this.favdata = favdata;
        this.context = context;
        this.favClickListener = favClickListener;
        this.menu = menu;
    }

    @Override
    public MenuViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_rv_fav_trans, parent, false);
        return new FFavoriteAdapter.MenuViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MenuViewHolder holder, int position) {
        Favorite fav = favdata.get(position);

        if (fav.getJenispembayaran().equalsIgnoreCase("DEBIT")) {
            if (fav.getSubmenutrx().equalsIgnoreCase("8001") || fav.getSubmenutrx().equalsIgnoreCase("8002")) {
                holder.tv_menu_title.setText(fav.getNamafav());
            } else {
                holder.tv_menu_title.setText(fav.getNamafav() + " | " + fav.getBsaacc());
            }
        } else {
            holder.tv_menu_title.setText(fav.getNamafav());
        }

        if (fav.getMenutrx().equalsIgnoreCase("8000")){
            if (fav.getSubmenutrx().equalsIgnoreCase("8009")) {
                String data1 = fav.getData1();
                String kodebank = data1.substring(0, data1.indexOf("|"));
                String namaprioritybank = data1.substring(data1.indexOf("|")+1, data1.length());
                String namabank = namaprioritybank.substring(0, namaprioritybank.indexOf("|"));
                holder.tv_menu_detail.setText(kodebank + " - " + namabank + " | " + fav.getData3());
            } else {
                holder.tv_menu_detail.setText(fav.getData3());
            }
        } else if (fav.getMenutrx().equalsIgnoreCase("7000")) {
            if (fav.getSubmenutrx().equalsIgnoreCase("7006") || fav.getSubmenutrx().equalsIgnoreCase("7007") || fav.getSubmenutrx().equalsIgnoreCase("7008")) {
                String data1 = fav.getData1();
                String kodeinstitusi = data1.substring(0, data1.indexOf("|"));
                String namainstitusi = data1.substring(data1.indexOf("|") + 1, data1.length());
                holder.tv_menu_detail.setText(kodeinstitusi + " - " + namainstitusi + " | " + fav.getData3());
            } else if (fav.getSubmenutrx().equalsIgnoreCase("7009")) {
                holder.tv_menu_detail.setText(fav.getData3());
            } else {
                holder.tv_menu_detail.setText(fav.getData4() + " | " + fav.getData3());
            }
        } else if (fav.getMenutrx().equalsIgnoreCase("6000")) {
            if (fav.getSubmenutrx().equalsIgnoreCase("6001")) {
                holder.tv_menu_detail.setText(fav.getData3());
            } else {
                holder.tv_menu_detail.setText(fav.getData4() + " | " + fav.getData3());
            }
        } else if (fav.getMenutrx().equalsIgnoreCase("9000")) {
            holder.tv_menu_detail.setText(fav.getData3());
        } else if (fav.getMenutrx().equalsIgnoreCase("1000")) {
            holder.tv_menu_detail.setText(fav.getData3());
        } else {
            holder.tv_menu_detail.setText(fav.getData1() + " | " + fav.getData3());
        }
//        if (fav.getMenutrx().equalsIgnoreCase("8001")) {
//            holder.tv_menu_detail.setText(fav.getData3());
//        } else if (fav.getMenutrx().equalsIgnoreCase("8002")) {
//            holder.tv_menu_detail.setText(fav.getData3());
//        } else {
//            holder.tv_menu_detail.setText(fav.getData1() + " | " + fav.getData3());
//        }
    }

    @Override
    public int getItemCount() {
        return favdata.size();
    }

    public class MenuViewHolder extends RecyclerView.ViewHolder
            implements View.OnClickListener{

        public TextView tv_menu_title;
        public TextView tv_menu_detail;
        public ImageView btn_hapus_fav;

        public MenuViewHolder(View itemView) {
            super(itemView);

            this.tv_menu_title = (TextView) itemView.findViewById(R.id.title_item_fav);
            this.tv_menu_detail = (TextView) itemView.findViewById(R.id.tv_detail_fav);
            this.btn_hapus_fav = (ImageView) itemView.findViewById(R.id.btn_hapus_fav_item);
            if (!menu.equalsIgnoreCase("")){
                btn_hapus_fav.setVisibility(View.GONE);
            }
            btn_hapus_fav.setOnClickListener(this);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            int position = getAdapterPosition();
            if (view == this.btn_hapus_fav) {
                favClickListener.deleteFav(favdata.get(position).getJenispembayaran(), favdata.get(position).getMenutrx(),
                        favdata.get(position).getSubmenutrx(), favdata.get(position).getNamafav()
                        );
            }
            else {
                if (menu.equalsIgnoreCase("FAVTRANS")) {
                    favClickListener.onclikFav(favdata.get(position).getNamafav(), favdata.get(position).getData3());
                }
//                else if(menu.equalsIgnoreCase("FAVTRANSA")) {
//                    favClickListener.onclikFav(favdata.get(position).getData1(), favdata.get(position).getData3());
//                    favClickListener.onclikFav(favdata.get(position).getNamafav(), favdata.get(position).getSubmenutrx());
//                    favClickListener.getFav(favdata.get(position).getData3(), favdata.get(position).getData1());
//                }
                else {
//                    favClickListener.onclikFav(favdata.get(position).getLabel(), favdata.get(position).getCategory());
                    favClickListener.onclikFav(favdata.get(position).getNamafav(), favdata.get(position).getData1());
                    favClickListener.getFav(favdata.get(position).getData3(), favdata.get(position).getData1());
//                    favClickListener.onClickFav(favdata.get(position).getLabel(), favdata.get(position).getCategory(),
//                            favdata.get(position).getKode_produk(), favdata.get(position).getDetail());
//                    favClickListener.onclikFav(favdata.get(position).getDetail(), favdata.get(position).getKode_produk());
                }
            }
        }
    }
}