package com.bris.brissmartmobile.app;

import android.app.Application;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;

import com.vfi.smartpos.deviceservice.aidl.IBeeper;
import com.vfi.smartpos.deviceservice.aidl.IDeviceService;
import com.vfi.smartpos.deviceservice.aidl.IEMV;
import com.vfi.smartpos.deviceservice.aidl.IMagCardReader;
import com.vfi.smartpos.deviceservice.aidl.IPinpad;
import com.vfi.smartpos.deviceservice.aidl.IPrinter;
import com.vfi.smartpos.deviceservice.aidl.IRFCardReader;
import com.vfi.smartpos.deviceservice.aidl.IScanner;
import com.vfi.smartpos.deviceservice.aidl.ISmartCardReader;

import io.realm.Realm;
import io.realm.RealmConfiguration;

/**
 * Created by akbaranjas on 23/03/17.
 * Updated by valeputra
 */

public class BRISSMARTMobileRealmApp extends Application {

    public static BRISSMARTMobileRealmApp instance;
    public static Context appContext;

    IDeviceService idevice;
    IPrinter printer;
    IMagCardReader msr;
    IScanner scanner;
    IRFCardReader irfCardReader;
    IBeeper mIBeeper;
    ISmartCardReader iSmartCardReader;
    ISmartCardReader iSamCardReader;
    IEMV iEMV;
    IPinpad ipinpad;
    IBeeper iBeeper;
    SharedPreferences preferences;

    public static BRISSMARTMobileRealmApp getInstance() {
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        appContext = this;


        //Baru
        instance = this;
        String prefname = "demo_app";
        preferences = getSharedPreferences(prefname, MODE_PRIVATE);
        Intent intent = new Intent();
        intent.setAction("com.vfi.smartpos.device_service");
        intent.setPackage("com.vfi.smartpos.deviceservice");
        boolean isSucc = bindService(intent, conn, Context.BIND_AUTO_CREATE);
        if (!isSucc) {
            Log.i("TAG", "deviceService connect fail!");
        } else {
            Log.i("TAG", "deviceService connect success");
        }





        //Lama
//        RealmConfiguration config = new RealmConfiguration.Builder(this).build();
//        Realm.setDefaultConfiguration(config);

        Realm.init(this);
        RealmConfiguration config = new RealmConfiguration.Builder()
                .name("brissmartmobile.realm")
                .schemaVersion(1)
                .build();
        Realm.setDefaultConfiguration(config);
    }

    public static Context getContext()
    {
        return appContext;
    }



    //Baru
    private ServiceConnection conn = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            idevice = IDeviceService.Stub.asInterface(service);
            try {
                iEMV = idevice.getEMV();
                ipinpad = idevice.getPinpad(1);
                iBeeper = idevice.getBeeper();
                mIBeeper = idevice.getBeeper();
                printer = idevice.getPrinter();
                msr = idevice.getMagCardReader();
                scanner = idevice.getScanner(0);    // 1 for the font, 0 for the rear
                irfCardReader = idevice.getRFCardReader();
                iSmartCardReader = idevice.getSmartCardReader(0);
                Intent statusIntent = new Intent("com.verifone.callback.STATUS");
                sendBroadcast(statusIntent);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {

        }
    };

    public IPrinter getPrinter() {
        return printer;
    }

    public IDeviceService getIdevice() {
        return idevice;
    }

    public IEMV getiEMV() {
        return iEMV;
    }

    public IPinpad getIpinpad() {
        return ipinpad;
    }

    public IRFCardReader getIrfCardReader() {
        return irfCardReader;
    }

    public IBeeper getmIBeeper() {
        return mIBeeper;
    }

    public IBeeper getiBeeper() {
        return iBeeper;
    }

    public ISmartCardReader getiSamCardReader() {
        return iSamCardReader;
    }

    public SharedPreferences getPreferences() {
        return preferences;
    } //Baru
}
