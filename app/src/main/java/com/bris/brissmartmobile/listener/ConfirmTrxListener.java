package com.bris.brissmartmobile.listener;

/**
 * Created by akbaranjas on 06/04/17.
 */

public interface ConfirmTrxListener {

    public void onConfirmClick(String pin);
    public void onCancelClick();
}
