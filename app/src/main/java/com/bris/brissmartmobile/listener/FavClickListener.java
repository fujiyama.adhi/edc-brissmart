package com.bris.brissmartmobile.listener;

/**
 * Created by akbaranjas on 11/04/17.
 */

public interface FavClickListener {
     void getFav(String label, String category);
//     void onclikFav(String label, String category);
     void onclikFav(String label, String kode_produk);
     void deleteFav(String jenispembayaran, String menutrx, String submenutrx, String label);
//     void onClickFav(String label, String category, String productCode, String detail);
}
