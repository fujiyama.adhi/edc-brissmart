package com.bris.brissmartmobile.listener;

/**
 * Created by akbaranjas on 13/01/17.
 */

public interface MenuClickListener {
    void onMenuClick(String menu);
}
