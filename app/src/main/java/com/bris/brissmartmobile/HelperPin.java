package com.bris.brissmartmobile;


import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.widget.EditText;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.Format;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class HelperPin {

    public static boolean isDateMoreOneYearAgo(String yyMMdd) {
        if (yyMMdd == null) {
            return true;
        }
        if (yyMMdd.equalsIgnoreCase("000000")) {
            return false;
        }
        Date dateCheck = parseStringToDate("yyMMdd", yyMMdd);
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.YEAR, -1);
        if (dateCheck.after(cal.getTime())) {
            return false;
        }
        return true;
    }

    public static Date parseStringToDate(String formatDate, String inputDate) {
        Date date = null;
        if (inputDate == null) {
            return date;
        }
        try {
            return new SimpleDateFormat(formatDate).parse(inputDate);
        } catch (ParseException e) {
            e.printStackTrace();
            return date;
        }
    }

    public static String formatViewCardNo(String cardNo) {
        StringBuilder sb = new StringBuilder();
        for (int i = 1; i <= cardNo.length(); i++) {
            sb.append(cardNo.charAt(i - 1));
            if (i % 4 == 0 && i != cardNo.length()) {
                sb.append("-");
            }
        }
        return sb.toString();
    }

    private static String padleft(String str, int len, char c) {
        str = str.trim();
        if (str.length() > len) {
            throw new RuntimeException("invalid len " + str.length() + "/"
                    + len);
        }
        String ret = "";
        int fill = len - str.length();
        while (fill-- > 0) {
            ret += c;
        }
        ret += str;
        return ret;
    }

    public static boolean isExpired(int totalDays) {
        System.out.println("expiry days:" + totalDays);
        Date date = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.YEAR, 1995);// for 6 hour
        calendar.set(Calendar.MONTH, 0);// for 0 min
        calendar.set(Calendar.DAY_OF_MONTH, 1);// for 0 sec
        calendar.set(Calendar.HOUR_OF_DAY, 0);// for 6 hour
        calendar.set(Calendar.MINUTE, 0);// for 0 min
        calendar.set(Calendar.SECOND, 0);// for 0 sec
        long gap = 86400000 * (long) totalDays;
        long after = (calendar.getTimeInMillis() + gap);
        System.out.println("next days:" + convertTime(after));
        System.out.println("current days:" + convertTime(System.currentTimeMillis()));
        System.out.println("long now:" + System.currentTimeMillis());
        System.out.println("long after:" + after);
        if (after < System.currentTimeMillis()){
            return true;
        } else {
            return false;
        }
    }

    public static String convertTime(long time){
        Date date = new Date(time);
        Format format = new SimpleDateFormat("yyyy MMM dd hh:mm:ss");
        return format.format(date);
    }

    public static String cleanString(String str){
        String[] clean = str.split(" ");
        StringBuffer result = new StringBuffer(str.length());
        for (int x = 0; x < clean.length;x++){
            String xx = clean[x];
            if (xx.length() > 0){
                result.append(xx);
                if (clean.length > 1)
                    result.append(" ");
            }
        }
        if (result.toString().trim().length() == 0){
            return " ";
        } else
            return result.toString();
    }


    public static String removeSpaces(String s) {
        return s.replaceAll(" ", "");
    }

    public static String hexToASCII(String hexValue) throws Exception {
        StringBuilder output = new StringBuilder("");
        for (int i = 0; i < hexValue.length(); i += 2) {
            output.append((char) Integer.parseInt(hexValue.substring(i, i + 2), 16));
        }
        return output.toString();
    }

    public static void setCurrency(final EditText edt) {
        edt.addTextChangedListener(new TextWatcher() {
            private String current = "";

            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!s.toString().equals(current)) {
                    edt.removeTextChangedListener(this);

                    Locale local = new Locale("id", "id");
                    String replaceable = String.format("[Rp,.\\s]",
                            NumberFormat.getCurrencyInstance().getCurrency()
                                    .getSymbol(local));
                    String cleanString = s.toString().replaceAll(replaceable,
                            "");

                    double parsed;
                    try {
                        parsed = Double.parseDouble(cleanString);
                    } catch (NumberFormatException e) {
                        parsed = 0.00;
                    }

                    NumberFormat formatter = NumberFormat
                            .getCurrencyInstance(local);
                    formatter.setMaximumFractionDigits(0);
                    formatter.setParseIntegerOnly(true);
                    String formatted = formatter.format((parsed));

                    String replace = String.format("[Rp\\s]",
                            NumberFormat.getCurrencyInstance().getCurrency()
                                    .getSymbol(local));
                    String clean = formatted.replaceAll(replace, "");

                    current = formatted;
                    edt.setText(clean);
                    edt.setSelection(clean.length());
                    edt.addTextChangedListener(this);
                }
            }
        });
    }

    public static void setCardNumber(final EditText edt) {
        edt.addTextChangedListener(new TextWatcher() {
            private String current = "";

            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!s.toString().equals(current)) {
                    String after = s.toString().replaceAll("-", "");
                    current = formatCardNumber(after);
                    edt.setText(current);
                    edt.setSelection(current.length());
                }
            }
        });
    }


    public static String formatCardNumber(String number){
        StringBuffer buffer = new StringBuffer();
        int length = 0;
        int total = 0;
        for (int x = 0; x < number.length(); x++){
            Character c = number.charAt(x);
            buffer.append(c);
            length++;
            total++;
            if (length == 4){
                length = 0;
                if (total < number.length()){
                    buffer.append("-");
                }
            }
        }

        return buffer.toString();
    }

    public static String safeDoubleToCurrency(Double val) {
        BigDecimal value = BigDecimal.valueOf(val.doubleValue());
        DecimalFormat kursIndonesia = (DecimalFormat) DecimalFormat.getCurrencyInstance();
        DecimalFormatSymbols formatRp = new DecimalFormatSymbols();
        formatRp.setCurrencySymbol("");
        formatRp.setMonetaryDecimalSeparator('.');
        formatRp.setGroupingSeparator(',');
        kursIndonesia.setDecimalFormatSymbols(formatRp);
        kursIndonesia.setParseBigDecimal(true);
        return kursIndonesia.format(value);
    }

    public static byte[] hexStr2Byte(String hexString) {
        if (hexString == null || hexString.length() == 0 ) {
            return new byte[] {0};
        }
        String hexStrTrimed = hexString.replace(" ", "");
        {
            String hexStr = hexStrTrimed;
            int len = hexStrTrimed.length();
            if( (len % 2 ) == 1 ){
                hexStr = hexStrTrimed + "0";
                ++len;
            }
            byte result [] = new byte[len/2];
            String s;
            for( int i=0; i< hexStr.length(); i++ ) {
                s = hexStr.substring(i,i+2);
                int v = Integer.parseInt(s, 16);

                result[i/2] = (byte) v;
                i++;
            }
            return  result;

        }
    }

    public static String convertHexToString(String hex){

        StringBuilder sb = new StringBuilder();
        StringBuilder temp = new StringBuilder();

        for( int i=0; i<hex.length()-1; i+=2 ){

            //grab the hex in pairs
            String output = hex.substring(i, (i + 2));
            //convert hex to decimal
            int decimal = Integer.parseInt(output, 16);
            //convert the decimal to character
            sb.append((char)decimal);

            temp.append(decimal);
        }

        return sb.toString();
    }

    public static String padRight(String str, int length, String pad){
        StringBuffer buffer = new StringBuffer(length);
        buffer.append(str);
        for (int x = str.length(); x < length; x++ ){
            buffer.append(pad);
        }
        return buffer.toString();
    }

    public static String leftPad(String value, Integer maxLength, String charPad) {
        if (value == null) {
            return "X";
        }
        if (value.length() >= maxLength.intValue()) {
            return value;
        }
        int diff = maxLength.intValue() - value.length();
        for (int i = 0; i < diff; i++) {
            value = charPad + value;
        }
        return value;
    }

    public static String hexString(byte[] b) {
        StringBuffer d = new StringBuffer(b.length * 2);

        for(int i = 0; i < b.length; ++i) {
            char hi = Character.forDigit(b[i] >> 4 & 15, 16);
            char lo = Character.forDigit(b[i] & 15, 16);
            d.append(Character.toUpperCase(hi));
            d.append(Character.toUpperCase(lo));
        }

        return d.toString();
    }

    public static String byte2HexStr(byte[] var0) {
        if (var0 == null) {
            return "";
        } else {
            String var1 ;
            StringBuilder var2 = new StringBuilder("");

            for (byte b : var0) {
                var1 = Integer.toHexString(b & 255);
                var2.append(var1.length() == 1 ? "0" + var1 : var1);
            }

            return var2.toString().toUpperCase().trim();
        }
    }


    public static int CHAR2INT(char c) {
        if(
                (c >= '0' && c <= '9' )
                        || (c == '=' )  // for track2
        ) {
            return c - '0';
        } else if(c >= 'a' && c <= 'f'){
            return c - 'a' +10;
        } else if(c >= 'A' && c <= 'F'){
            return c - 'A' +10;
        } else {
            return 0;
        }
    }

    public static boolean validApduResponse(byte[] data) {
        boolean response_sw1 = false;
        boolean response_sw2 = false;
        byte sw1 = data[data.length - 2];
        byte sw2 = data[data.length - 1];
        if (String.format("%02X ", sw1).contains("90") || String.format("%02X ", sw1).contains("91")){
            response_sw1 = true;
        }
        if (String.format("%02X ", sw2).contains("00")){
            response_sw2 = true;
        }
        return (response_sw1 & response_sw2);
    }

    public static Bitmap encodeAsBitmap(String encodedImage) {
        byte[] decodedString = Base64.decode(encodedImage, Base64.DEFAULT);
        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
        return decodedByte;
    }

    public static Bitmap resize(Bitmap image, int maxWidth) {
        int top = 150;
        int bottom = image.getHeight() - (top + 100);
        Bitmap header = crop(image, 0, top);
        image = crop(image, top, bottom);
        if (maxWidth > 0) {
            int width = image.getWidth();
            int height = image.getHeight();
            float ratioBitmap = (float) width / (float) height;
            float mHeight = (maxWidth/ratioBitmap);
            int maxHeight = (int) mHeight;



            float headerRatio = (float) header.getWidth()/(float) header.getHeight();
            int headerHeight = (int) (maxWidth/headerRatio);
            int finalWidth = maxWidth;
//            int finalHeight = maxHeight;
            int finalHeight = height - ((height*15)/100);
//            image = Bitmap.createScaledBitmap(image, finalWidth, finalHeight, true);
            header = getResizedBitmap(header, finalWidth, headerHeight, false);
            image = getResizedBitmap(image, finalWidth, finalHeight, false);
            Bitmap combine = merge(header, image);
            return combine;
//            image = changeBitmapContrastBrightness(image, 10, 0);
//            return image;
        } else {
            return image;
        }
    }


    public static Bitmap resizeReceipt(Bitmap image, int maxWidth) {
        if (maxWidth > 0) {
            int width = image.getWidth();
            int height = image.getHeight();
            float ratioBitmap = (float) width / (float) height;
            float mHeight = (maxWidth/ratioBitmap);
            int maxHeight = (int) mHeight;
            int finalWidth = maxWidth;
            int finalHeight = height;
            image = getResizedBitmap(image, finalWidth, finalHeight, false);
            return image;
        } else {
            return image;
        }
    }

    public static Bitmap resizeSignature(Bitmap image, int maxWidth) {
        if (maxWidth > 0) {
            int width = image.getWidth();
            int height = image.getHeight();
            float ratioBitmap = (float) width / (float) height;
            float mHeight = (maxWidth/ratioBitmap);
            int maxHeight = (int) mHeight;
            int finalWidth = maxWidth;
            int finalHeight = maxHeight;
            image = Bitmap.createScaledBitmap(image, finalWidth, finalHeight, true);
            return image;
        } else {
            return image;
        }
    }

    public static Bitmap merge(Bitmap header, Bitmap content) {
        int bitmap1Width = header.getWidth();
        int bitmap1Height = header.getHeight();
        int bitmap2Width = content.getWidth();
        int bitmap2Height = content.getHeight();

//        float marginLeft = (float) (bitmap1Width * 0.5 - bitmap2Width * 0.5);
//        float marginTop = (float) (bitmap1Height * 0.5 - bitmap2Height * 0.5);

        Bitmap finalBitmap = Bitmap.createBitmap(bitmap1Width, bitmap1Height + bitmap2Height + 50, header.getConfig());
        Canvas canvas = new Canvas(finalBitmap);
        canvas.drawBitmap(header, new Matrix(), null);
        canvas.drawBitmap(content, 0, bitmap1Height, null);
        return finalBitmap;
    }

    /**
     *
     * @param bmp input bitmap
     * @param contrast 0..10 1 is default
     * @param brightness -255..255 0 is default
     * @return new bitmap
     */
    public static Bitmap changeBitmapContrastBrightness(Bitmap bmp, float contrast, float brightness)
    {
        ColorMatrix cm = new ColorMatrix(new float[]
                {
                        contrast, 0, 0, 0, brightness,
                        0, contrast, 0, 0, brightness,
                        0, 0, contrast, 0, brightness,
                        0, 0, 0, 1, 0
                });

        Bitmap ret = Bitmap.createBitmap(bmp.getWidth(), bmp.getHeight(), bmp.getConfig());

        Canvas canvas = new Canvas(ret);

        Paint paint = new Paint();
        paint.setColorFilter(new ColorMatrixColorFilter(cm));
        canvas.drawBitmap(bmp, 0, 0, paint);

        return ret;
    }

    public static Bitmap crop(Bitmap image) {
        int top = (int) (0.04 * image.getHeight());
        int bottom = (int) (0.92 * image.getHeight());
        Bitmap croppedBitmap = Bitmap.createBitmap(
                image,
                0,
                top,
                image.getWidth(),
                bottom
        );
        return croppedBitmap;
    }

    public static Bitmap crop(Bitmap image, int start, int end) {
        Bitmap croppedBitmap = Bitmap.createBitmap(
                image,
                0,
                start,
                image.getWidth(),
                end
        );
        return croppedBitmap;
    }

    public static String parseDate(String data){
        StringBuffer buffer = new StringBuffer(data.length() + 6);
        buffer.append(data.substring(2, 4));
        buffer.append("/");
        buffer.append(data.substring(0, 2));
        buffer.append("/");
        Calendar c = Calendar.getInstance();
        c.setTimeInMillis(System.currentTimeMillis());
        int mYear = c.get(Calendar.YEAR);
        buffer.append(String.valueOf(mYear));
        return buffer.toString();
    }

    public static String parseTime(String data){
        StringBuffer buffer = new StringBuffer(data.length() + 2);
        buffer.append(data.substring(0, 2));
        buffer.append(":");
        buffer.append(data.substring(2, 4));
        buffer.append(":");
        buffer.append(data.substring(4, 6));
        return buffer.toString();
    }


    public static DecimalFormat getMONEYFORMAT() {
        return new DecimalFormat("###,###.##");
    }

    /**
     * @param bitmap the Bitmap to be scaled
     * @param threshold the maxium dimension (either width or height) of the scaled bitmap
     * @param isNecessaryToKeepOrig is it necessary to keep the original bitmap? If not recycle the original bitmap to prevent memory leak.
     * */

    public static Bitmap getScaledDownBitmap(Bitmap bitmap, int threshold, boolean isNecessaryToKeepOrig){
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        int newWidth = width;
        int newHeight = height;

        if(width > height && width > threshold){
            newWidth = threshold;
            newHeight = (int)(height * (float)newWidth/width);
        }

        if(width > height && width <= threshold){
            //the bitmap is already smaller than our required dimension, no need to resize it
            return bitmap;
        }

        if(width < height && height > threshold){
            newHeight = threshold;
            newWidth = (int)(width * (float)newHeight/height);
        }

        if(width < height && height <= threshold){
            //the bitmap is already smaller than our required dimension, no need to resize it
            return bitmap;
        }

        if(width == height && width > threshold){
            newWidth = threshold;
            newHeight = newWidth;
        }

        if(width == height && width <= threshold){
            //the bitmap is already smaller than our required dimension, no need to resize it
            return bitmap;
        }

        return getResizedBitmap(bitmap, newWidth, newHeight, isNecessaryToKeepOrig);
    }

    private static Bitmap getResizedBitmap(Bitmap bm, int newWidth, int newHeight, boolean isNecessaryToKeepOrig) {
        int width = bm.getWidth();
        int height = bm.getHeight();
        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;
        // CREATE A MATRIX FOR THE MANIPULATION
        Matrix matrix = new Matrix();
        // RESIZE THE BIT MAP
        matrix.postScale(scaleWidth, scaleHeight);

        // "RECREATE" THE NEW BITMAP
        Bitmap resizedBitmap = Bitmap.createBitmap(bm, 0, 0, width, height, matrix, false);
        if(!isNecessaryToKeepOrig){
            bm.recycle();
        }
        return resizedBitmap;
    }
}