package com.bris.brissmartmobile;

import android.content.res.AssetManager;
import android.os.Environment;

import java.io.File;

/**
 * Created by Simon on 2018/6/1.
 */

public class PrinterFonts {
    public static final String FONT_MONO = "MonoMono.ttf";

    public static String path = "";

    public static void initialize(AssetManager assets ) {
        String fileName = PrinterFonts.FONT_MONO;
        path = Environment.getExternalStorageDirectory().getPath().concat("/fonts/");
        File folder = new File(path);
        if (!folder.exists()){
            folder.mkdir();
        }
        ExtraFiles.copy(fileName, path , fileName, assets, false );
    }


}
